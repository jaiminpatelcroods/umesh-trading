package com.croods.umeshtrading.constant;

public class Constant {

	public static final long URID_CORPORATE=1;
	public static final long URID_COMPANY=2;
	public static final long URID_BRANCH=3;
	public static final long URID_USER=4;
	public static final String DEFAULT_LANGUAGE_CODE="en";
	
	public static final String CATEGORY="category";
	public static final String BRAND="brand";
	public static final String UOM="uom";
	public static final String CONTACT_COMPANY="company";
	public static final String CONTACT_BRANCH="branch";
	public static final String DASHBOARD="dashboard";
	public static final String PRODUCT="product";
	public static final String CATEGORY_BRANDS="categorybrands";
	public static final String UNIT_OF_MEASUREMENT="unitofmeasurement";
	public static final String STOCK="stock";
	public static final String STOCKTRASFER="stocktransfer";
	public static final String EXPENSE="expense";
	public static final String ACCOUNTS="accounts";
	public static final String REPORTS="reports";
	public static final String EMPLOYEE="employee";
	public static final String USERROLE="userrole";
	
	public static final String CREATE="create";
	public static final String VIEW="view";
	public static final String ISDELETE="delete";
	public static final String EDIT="edit";
	public static final String EXCEL="excel";
	public static final String PDF="pdf";
	public static final String MAIL="mail";
	
	public static final String ADDITIONAL_CHARGE="additional_charge";
	
	public static final String ACCOUNT_CONTACT="contact";
	public static final String ACCOUNT_CUSTOM="account_custom";
	
	public static final String ACCOUNTING_JOURNAL="journal_voucher";
	public static final String ACCOUNTING_DEBIT_NOTE="debit_note_accounting";
	public static final String ACCOUNTING_CREDIT_NOTE="credit_note_accounting";
	
	public static final String BANK="bank";
	public static final String TAX="tax";
	public static final String GENERAL_SETTINGS="generalsetting";
	
	
	//-----------------SALES---------------------
	public static final String SALES_ORDER="sales_order";
	public static final String SALES_INVOICE="	";
	public static final String SALES_BILL_OF_SUPPLY="billofsupply";
	public static final String SALES_ESTIMATE="estimate";
	public static final String SALES_DELIVERY_CHALLAN="delivery_challan";
	public static final String SALES_CREDIT_NOTE="creditnote";
	public static final String SALES_POS="pos";
	
	//-----------------PURCHASE---------------------
	public static final String PURCHASE_ORDER="order";
	public static final String PURCHASE_BILL="bill";
	public static final String PURCHASE_DEBIT_NOTE="debitnote";
	public static final String PURCHASE_REQUEST = "purchase_request";
	public static final String PURCHASE_DELIVERY_CHALLAN="deliverychallan";
	//-----------------CONTACT---------------------
	public static final String CONTACT_CUSTOMER="customers";
	public static final String CONTACT_SUPPLIER="suppliers";
	
	public static final String CONTACT_CATEGORY_RETAILER="retailer";
	public static final String CONTACT_CATEGORY_WHOLESALER="wholesaler"; 
	public static final String CONTACT_CATEGORY_OTHER="other";
	
	public static final String CONTACT_AGENT="agent";         
	public static final String CONTACT_TRANSPORT="transport";
	public static final String CONTACT_SALESMAN="salesman";
	public static final String CONTACT_COMPANY_MASTER="companymaster";
	public static final String CONTACT_LABOUR="labour";
	
	//-----------------JOB WORK---------------------
	public static final String JOBWORK="jobwork";
	public static final String JOBWORK_RECEIVE="jobwork_receive";
	public static final String JOBWORK_PAYMENT="jobwork_payment";
	
	public static final String BANK_TRANSACTION="bank_transaction";
	public static final String BANK_WITHDRAW="withdraw";
	public static final String BANK_DEPOSIT="deposit";
	public static final String BANK_TRANSFER="transfer";
	
	//-----------------ACCOUNT GROUP---------------------
	public static final long ACCOUNT_GROUP_CURRENT_ASSETS=1;
	public static final long ACCOUNT_GROUP_CURRENT_LIABILITIES=2;
	public static final long ACCOUNT_GROUP_FIXED_ASSETS=3;
	public static final long ACCOUNT_GROUP_DIRECT_INCOMES=4;
	public static final long ACCOUNT_GROUP_INDIRECT_INCOMES=5;
	public static final long ACCOUNT_GROUP_INDIRECT_EXPENSES=6;
	public static final long ACCOUNT_GROUP_DIRECT_EXPENSES=7;
	public static final long ACCOUNT_GROUP_BANK_ACCOUNT=8;
	public static final long ACCOUNT_GROUP_LOANS_AND_LIABILITIES=9;
	public static final long ACCOUNT_GROUP_BRANCH_AND_DIVISION=10;
	public static final long ACCOUNT_GROUP_CAPITAL_ACCOUNT=11;
	public static final long ACCOUNT_GROUP_CASH_IN_HAND=12;
	public static final long ACCOUNT_GROUP_STOCK_IN_HAND=13;
	public static final long ACCOUNT_GROUP_SUNDRY_CREDITORS=14;
	public static final long ACCOUNT_GROUP_SUNDRY_DEBTORS=15;
	public static final long ACCOUNT_GROUP_SALES_ACCOUNT=16;
	public static final long ACCOUNT_GROUP_DUTIES_AND_TAXES=17;
	public static final long ACCOUNT_GROUP_INVESTMENT=18;
	public static final long ACCOUNT_GROUP_PURCHASE_ACCOUNT=19;
	
	//-----------------ACCOUNT---------------------
	
	public static final String ACCOUNT_ADVERTISEMENT_EXPENSES="Advertisement Expenses";
	public static final String ACCOUNT_BANK_CHARGES="Bank Charges";
	public static final String ACCOUNT_BANK_COMMISSION="Bank Commission";
	public static final String ACCOUNT_BANK_LOAN="Bank Loan";
	public static final String ACCOUNT_COFFEE_AND_TEA_EXPENSES="Coffee/Tea Expenses";
	public static final String ACCOUNT_ELECTRICITY_EXPENSES="Electricity Expenses";
	public static final String ACCOUNT_INSURANCE="Insurance";
	public static final String ACCOUNT_LABOUR_CHARGES="Labour Charges";
	public static final String ACCOUNT_LOSS_BY_DAMAGE="Loss By Damage";
	public static final String ACCOUNT_LOSS_BY_FIRE="Loss by Fire";
	public static final String ACCOUNT_OFFICE_EXPENSES="Office Expenses";
	public static final String ACCOUNT_PETROL_EXPENSES="Petrol Expenses";
	public static final String ACCOUNT_ADVERTISEMENT="Advertisement";
	public static final String ACCOUNT_STATIONERY="Stationery";
	public static final String ACCOUNT_SALARY="Salary";
	public static final String ACCOUNT_SHOP_EXPENSES="Shop Expenses";
	public static final String ACCOUNT_SHOP_RENT="Shop Rent";
	public static final String ACCOUNT_TELEPHONE_EXPENSES="Telephone Expenses";
	public static final String ACCOUNT_TELEPHONE_SECURITIES="Telephone Securities";
	public static final String ACCOUNT_TRADE_EXPENSES="Trade Expenses";
	public static final String ACCOUNT_TRAIN_FREIGHT_AND_RENT="Train Freight & Rent";
	public static final String ACCOUNT_TRAVELLING_EXPENSES="Traveling Expenses";
	public static final String ACCOUNT_DISCOUNT_RECEIVED="Discount Received";
	public static final String ACCOUNT_CASH="Cash";
	public static final String ACCOUNT_SALES="Sales";
	public static final String ACCOUNT_SALES_RETURN="Sales Return";
	public static final String ACCOUNT_PURCHASE="Purchase";
	public static final String ACCOUNT_PURCHASE_RETURN="Purchase Return";
	public static final String ACCOUNT_ROUNDOFF_INCOME="Roundoff Income";
	public static final String ACCOUNT_ROUNDOFF_EXPENSE="Roundoff Expense";
	public static final String ACCOUNT_DISCOUNT_GIVEN="Discount Given";
	public static final String ACCOUNT_KASAR="Kasar";
	
	public static final String PAYMENT="payment";
	public static final String RECEIPT="receipt";
	public static final String SETTING="SETTINGS";
	
	public static final String TERMSCONDITION="termscondition";
	public static final String PREFIX="prefix";
	public static final String ADDITIONALCHARGE="additionalcharge";
	public static final String REPORTSETTING="reportsetting";
	public static final String SENDEMAIL="smsemail";
	public static final String TERM="term";
	
	public static final String REALPATH="https://s3-us-west-2.amazonaws.com/";
			
	//public static final String REALPATH="https://s3-us-west-2.amazonaws.com/elasticbeanstalk-us-west-2-854568369247/logo/";
	
	//--------------------admin-------------------
	
	public static final String CORPORATES="corporates";
	public static final String COMPLAIN="complain";
	public static final String BUSINESS="business";
	public static final String BPACKAGE="bpackage";
	public static final String ADDONS="addons";
	public static final String APPS="apps";
	public static final String MODULE="module";
	public static final String SECTOR="sector";
	public static final String DEALER="dealer";
	public static final String DEPARTMENT = "department";
	public static final String GLOBAL_ROLE = "global_role";
	public static final String LOCATION_COUNTRIES = "location_countries";
	public static final String LOCATION_STATE = "location_state";
	public static final String LOCATION_ZONE = "location_zone";
	public static final String LOCATION_CITY = "location_city";
	public static final String CURRENCY = "currency";
	public static final String GLOBAL_USER = "global_user";
	public static final String ENQUIRY = "enquiry";
	public static final String SUPPORT = "support";
	
	public static final long GLOBAL_INSERT=1;
	public static final long GLOBAL_EDIT=2;
	public static final long GLOBAL_VIEW=3;
	public static final long GLOBAL_DELETE=4;
	
	//-------STOCK TrANSFER STATUS ------------=-
	public static final String STOCK_TRANSFER_APPROVE="approve";
	public static final String STOCK_TRANSFER_REJECTED="rejected";
	public static final String STOCK_TRANSFER_OPEN="open";
	
	//-------SHOPIFY ------------=-
	public static final String GRETER_THEN_OR_EQUAL_TO="GRETER_THEN_OR_EQUAL_TO";
	
	public static final String CRM_COMPLAIN = "complain";
	public static final String CRM_LEAD = "lead";
	
	//-------Umesh Trading Material In Out-------
	/*public static final String JOBWORK_MATERIAL_OUT = "materialout";
	public static final String JOBWORK_ORDER = "jobworkorder";
	public static final String JOBWORK_MATERIAL_IN = "materialin";*/
	public static final String MATERIAL_IN = "materialin";
	public static final String MATERIAL_OUT = "sales";
}