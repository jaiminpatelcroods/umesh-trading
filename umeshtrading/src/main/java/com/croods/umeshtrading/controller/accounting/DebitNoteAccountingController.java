package com.croods.umeshtrading.controller.accounting;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.account.AccountService;
import com.croods.umeshtrading.service.account.DebitNoteAccountingService;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.account.DebitNoteAccountingVo;

@Controller
@RequestMapping("account/debitnote")
public class DebitNoteAccountingController {

	@Autowired
	DebitNoteAccountingService debitNoteAccountingService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	PrefixService prefixService;
	
	@GetMapping("")
	public ModelAndView debitNoteAccounting(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/debit-note/debit-note");
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		view.addObject("debitNoteAccountingVos",debitNoteAccountingService.findByBranchIdAndTransactionDateBetween(
				Long.parseLong(session.getAttribute("branchId").toString()), 
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())
			));
				
		return view;
	}
	
	@GetMapping("new")
	public ModelAndView debitNoteAccountingNew(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/debit-note/debit-note-new");
		String fromTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER + "," + Constant.ACCOUNT_CUSTOM;
		String toTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER;
		
		view.addObject("fromAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(fromTypes.split(","))));
		view.addObject("toAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(toTypes.split(","))));
		return view;
	}
	
	@PostMapping("save")
	public String debitNoteAccountingSave(@RequestParam Map<String,String> allRequestParams, HttpSession session, @ModelAttribute("debitNoteAccountingVo") DebitNoteAccountingVo debitNoteAccountingVo) throws NumberFormatException, ParseException {
		
		debitNoteAccountingVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		debitNoteAccountingVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		debitNoteAccountingVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		debitNoteAccountingVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		
		if(debitNoteAccountingVo.getDebitNoteAccountId() == 0) {
			
			long newVoucherNo = debitNoteAccountingService.findMaxVoucherNo(Constant.ACCOUNTING_CREDIT_NOTE, 
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), "DBT");
			
			debitNoteAccountingVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.ACCOUNTING_CREDIT_NOTE,
					Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
			debitNoteAccountingVo.setVoucherNo(newVoucherNo);
			
			debitNoteAccountingVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			debitNoteAccountingVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			debitNoteAccountingVo.setTransactionDate(dateFormat.parse(allRequestParams.get("transactionDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		debitNoteAccountingVo = debitNoteAccountingService.save(debitNoteAccountingVo);
		debitNoteAccountingService.saveTransation(debitNoteAccountingVo);
		
		return "redirect:/account/debitnote/"+debitNoteAccountingVo.getDebitNoteAccountId();
	}
	
	@GetMapping("{id}")
	public ModelAndView debitNoteAccountingView(@PathVariable("id") long debitNoteAccountId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/debit-note/debit-note-view");
		
		view.addObject("debitNoteAccountingVo", debitNoteAccountingService.findByDebitNoteAccountIdAndBranchId(debitNoteAccountId, Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView debitNoteAccountingEdit(@PathVariable("id") long debitNoteAccountId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/debit-note/debit-note-edit");
		
		view.addObject("debitNoteAccountingVo", debitNoteAccountingService.findByDebitNoteAccountIdAndBranchId(debitNoteAccountId, Long.parseLong(session.getAttribute("branchId").toString())));
		
		String fromTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER + "," + Constant.ACCOUNT_CUSTOM;
		String toTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER;
		
		view.addObject("fromAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(fromTypes.split(","))));
		view.addObject("toAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(toTypes.split(","))));
		
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String debitNoteAccountingDelete(@PathVariable("id") long debitNoteAccountId, HttpSession session) throws NumberFormatException, ParseException {
		
		debitNoteAccountingService.delete(debitNoteAccountId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/account/debitnote";
	}
}