package com.croods.umeshtrading.controller.additionalcharge;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.account.AccountService;
import com.croods.umeshtrading.service.additionalcharge.AdditionalChargeService;
import com.croods.umeshtrading.service.tax.TaxService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.additionalcharge.AdditionalChargeVo;
import com.croods.umeshtrading.vo.tax.TaxVo;

@Controller
@RequestMapping("additionalcharge")
public class AdditionalChargeController {

	@Autowired
	AdditionalChargeService additionalChargeService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	AccountService accountService;
	
	@GetMapping("")
	public ModelAndView additionalCharge(HttpSession session)
	{	
		ModelAndView view=new ModelAndView("additionalcharge/additionalcharge");
		view.addObject("additionalChargeVos",additionalChargeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("tax",taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;		
	}
	
	@PostMapping("/save")
	@ResponseBody
	public AdditionalChargeVo saveAdditionalCharge(@RequestParam(value = "additionalCharge") String additionalCharge,
			@RequestParam(value = "tax") String tax, @RequestParam(value = "defaultAmount") String defaultAmount,
			@RequestParam(value = "hsnCode") String hsnCode, HttpSession session) {
		AdditionalChargeVo additionalChargeVo = new AdditionalChargeVo();
		additionalChargeVo.setAdditionalCharge(additionalCharge);
		additionalChargeVo.setDefaultAmount(Double.parseDouble(defaultAmount));
		additionalChargeVo.setHsnCode(hsnCode);
		additionalChargeVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		additionalChargeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		additionalChargeVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		additionalChargeVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		additionalChargeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		additionalChargeVo.setCreatedOn(CurrentDateTime.getCurrentDate());

		TaxVo taxVo = new TaxVo();
		taxVo.setTaxId(Long.parseLong(tax));
		taxService.findByTaxId(Long.parseLong(tax));
		additionalChargeVo.setTaxVo(taxService.findByTaxId(Long.parseLong(tax)));

		AccountCustomVo accountCustomVo = new AccountCustomVo();
		accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setAccountName(additionalCharge);
		accountCustomVo.setAccounType(Constant.ADDITIONAL_CHARGE);

		AccountGroupVo accountGroupVo = new AccountGroupVo();
		accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_INDIRECT_EXPENSES);
		accountCustomVo.setGroup(accountGroupVo);

		accountService.insertAccount(accountCustomVo);

		additionalChargeVo.setAccountCustomVo(accountCustomVo);

		additionalChargeService.save(additionalChargeVo);

		return additionalChargeVo;
	}
	
	@PostMapping("/update")
	@ResponseBody
	public AdditionalChargeVo updateAdditionalCharge(@RequestParam(value = "additionalCharge") String additionalCharge,
			@RequestParam(value = "defaultAmount") String defaultAmount,@RequestParam(value = "additionalChargeId") long additionalChargeId,
			@RequestParam(value = "hsnCode") String hsnCode, @RequestParam(value = "tax") String tax,HttpSession session) {
		
		AdditionalChargeVo additionalChargeVo;
		additionalChargeVo = additionalChargeService.findByAdditionalChargeId(additionalChargeId);

		additionalChargeVo.setAdditionalCharge(additionalCharge);
		additionalChargeVo.setDefaultAmount(Double.parseDouble(defaultAmount));
		additionalChargeVo.setHsnCode(hsnCode);
		additionalChargeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		additionalChargeVo.setModifiedOn(CurrentDateTime.getCurrentDate());

		TaxVo taxVo = new TaxVo();
		taxVo.setTaxId(Long.parseLong(tax));
		additionalChargeVo.setTaxVo(taxVo);

		AccountCustomVo accountCustomVo = additionalChargeVo.getAccountCustomVo();
		accountCustomVo.setAccountName(additionalCharge);
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountService.insertAccount(accountCustomVo);

		return additionalChargeVo;
	}
	
	@PostMapping("/delete")
	@ResponseBody
	public String deleteAdditionalCharge(@RequestParam(value="id") long id, HttpSession session)
	{		
		additionalChargeService.delete(id,Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());		
		
		AdditionalChargeVo additionalChargeVo=new AdditionalChargeVo();
		additionalChargeVo=additionalChargeService.findByAdditionalChargeId(id);
		
		AccountCustomVo accountCustomVo=additionalChargeVo.getAccountCustomVo();
		accountCustomVo.setIsDeleted(1);
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountService.insertAccount(accountCustomVo);
		
		return "Sucess";
	}
	
	@RequestMapping(value= {"json","json/"})
	@ResponseBody
	public List<AdditionalChargeVo> additionalChargeListJSON(HttpSession session)
	{	
		List<AdditionalChargeVo> additionalChargeVos = additionalChargeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		additionalChargeVos.forEach( a -> {
			a.setAccountCustomVo(null);
			a.getTaxVo().setAccountCustomVo(null);
		});
		
		return additionalChargeVos;
	}
	
	@RequestMapping(value= {"{id}/json","{id}/json/"})
	@ResponseBody
	public AdditionalChargeVo additionalChargeDetailJSON(@PathVariable long id,HttpSession session)
	{	
		
		AdditionalChargeVo additionalChargeVo = additionalChargeService.findByAdditionalChargeId(id);
		
		additionalChargeVo.setAccountCustomVo(null);
		additionalChargeVo.getTaxVo().setAccountCustomVo(null);
		
		return additionalChargeVo;
	}
}
