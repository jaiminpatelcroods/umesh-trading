package com.croods.umeshtrading.controller.bank;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.account.AccountService;
import com.croods.umeshtrading.service.bank.BankService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.bank.BankTransactionVo;
import com.croods.umeshtrading.vo.bank.BankVo;

@Controller
@RequestMapping("bank")
public class BankController {
	
	@Autowired
	BankService bankService;
	
	@Autowired
	AccountService accountService;

	@Autowired
	LocationService locationService;
	
	@Autowired
	PrefixService prefixService;
	
	@GetMapping("")
	public ModelAndView bank(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/bank");
		view.addObject("bankVos",bankService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("new")
	public ModelAndView bankNew(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/bank-new");
		
		return view;
	}
	
	@PostMapping("save")
	public String bankSave(HttpSession session, @ModelAttribute("bank") BankVo bankVo) throws NumberFormatException, ParseException {
		
		
		bankVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		bankVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		bankVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		bankVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		AccountCustomVo accountCustomVo;
		
		if(bankVo.getBankId() == 0) {
			
			bankVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			bankVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			
			accountCustomVo = new AccountCustomVo();
			accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			accountCustomVo.setAccounType(Constant.BANK);

			AccountGroupVo accountGroupVo = new AccountGroupVo();
			accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_BANK_ACCOUNT);
			accountCustomVo.setGroup(accountGroupVo);

		} else {
			accountCustomVo = bankService.findAccountCustomVoByBankId(bankVo.getBankId());
		}
		
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setAccountName(bankVo.getBankName() + "-" + bankVo.getBankAcNo());
		
		accountService.insertAccount(accountCustomVo);
		
		bankVo.setAccountCustomVo(accountCustomVo);
		
		bankService.saveBank(bankVo);
		
		return "redirect:/bank/"+bankVo.getBankId();
	}
	
	@GetMapping("{id}")
	public ModelAndView bankView(@PathVariable("id") long bankId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/bank-view");
		
		BankVo bankVo = bankService.findByBankId(bankId);
		
		bankVo.setCountriesName(locationService.findCountriesNameByCountriesCode(bankVo.getCountriesCode()));
		bankVo.setStateName(locationService.findStateNameByStateCode(bankVo.getStateCode()));
		bankVo.setCityName(locationService.findCityNameByCityCode(bankVo.getCityCode()));
		
		view.addObject("bankVo",bankVo);
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView bankEdit(@PathVariable("id") long bankId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/bank-edit");
		
		BankVo bankVo = bankService.findByBankId(bankId);
		
		view.addObject("bankVo",bankVo);
		
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String bankDelete(@PathVariable("id") long bankId, HttpSession session) throws NumberFormatException, ParseException {
		
		AccountCustomVo accountCustomVo = bankService.findAccountCustomVoByBankId(bankId);
		
		accountService.deleteAccountCustom(accountCustomVo.getAccountCustomId(), Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		bankService.deleteBank(bankId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "redirect:/bank";
	}
	
	@PostMapping("/json" )
	@ResponseBody
	public List<BankVo> BankListAjex(HttpSession session) {
		return bankService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
	}
	
	@GetMapping("transaction")
	public ModelAndView bankTransaction(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/transaction/bank-transaction");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		view.addObject("bankTransactionVos",bankService.findByBranchIdAndTransactionDateBetween(Long.parseLong(session.getAttribute("branchId").toString()), 
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
		
		return view;
	}
	
	@GetMapping("transaction/new")
	public ModelAndView bankTransactionNew(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/transaction/bank-transaction-new");
		view.addObject("bankVos",bankService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@PostMapping("transaction/save")
	public String bankTransactionSave(@RequestParam Map<String,String> allRequestParams, HttpSession session, @ModelAttribute("bankTransactionVo") BankTransactionVo bankTransactionVo) throws NumberFormatException, ParseException {
		
		
		bankTransactionVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		bankTransactionVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		bankTransactionVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		bankTransactionVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		if(bankTransactionVo.getBankTransactionId()== 0) {
			
			bankTransactionVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			bankTransactionVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			
			long newVoucherNo = bankService.findMaxVoucherNo(Constant.BANK_TRANSACTION,
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), "TRF");
			bankTransactionVo.setVoucherNo(newVoucherNo);
			bankTransactionVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.BANK_TRANSACTION,
					Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());

		}
		
		if(bankTransactionVo.getTransactionType().equals(Constant.BANK_TRANSFER)) {
			bankTransactionVo.setDebitAmount(bankTransactionVo.getCreditAmount());
		}
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			bankTransactionVo.setTransactionDate(dateFormat.parse(allRequestParams.get("transactionDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		bankService.saveBankTransaction(bankTransactionVo);
		
		return "redirect:/bank/transaction/"+bankTransactionVo.getBankTransactionId();
	}
	
	@GetMapping("transaction/{id}")
	public ModelAndView bankTransactionNew(@PathVariable("id") long bankTransactionId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/transaction/bank-transaction-view");
		view.addObject("bankTransactionVo",bankService.findByBankTransactionIdAndBranchId(bankTransactionId, Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@GetMapping("transaction/{id}/edit")
	public ModelAndView bankTransactionEdit(@PathVariable("id") long bankTransactionId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("bank/transaction/bank-transaction-edit");
		view.addObject("bankVos",bankService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("bankTransactionVo",bankService.findByBankTransactionIdAndBranchId(bankTransactionId, Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@GetMapping("transaction/{id}/delete")
	public String bankTransactionDelete(@PathVariable("id") long bankTransactionId, HttpSession session) throws NumberFormatException, ParseException {
		bankService.deleteBankTransaction(bankTransactionId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "redirect:/bank/transaction";
	}
}
