package com.croods.umeshtrading.controller.casetype;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.brand.BrandService;
import com.croods.umeshtrading.service.casetype.CaseTypeService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.casetype.CaseTypeVo;

@Controller
@RequestMapping("/casetype")
public class CaseTypeController {
	
	@Autowired
	CaseTypeService caseTypeService;
	
	@Autowired
	BrandService brandService;
	
	@GetMapping("")
	public ModelAndView casetype(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("casetype/casetype");
 	
 		view.addObject("caseTypeVos",caseTypeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public CaseTypeVo saveCaseType(@RequestParam(value="caseName") String caseName,@RequestParam(value="caseDescription") String description,HttpSession session)
 	{
		CaseTypeVo caseTypeVo=new CaseTypeVo();
		caseTypeVo.setCaseName(caseName);
		caseTypeVo.setCaseDescription(description);
		caseTypeVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		caseTypeVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		caseTypeVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		caseTypeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		caseTypeVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		caseTypeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		caseTypeService.save(caseTypeVo);
		
		return caseTypeVo;
	}
	
	@PostMapping("update")
	@ResponseBody
	public CaseTypeVo updateCaseType(@RequestParam(value="caseName") String caseName,@RequestParam(value="caseDescription") String description,@RequestParam(value="caseTypeId") long caseTypeId, HttpSession session)
	{
		CaseTypeVo caseTypeVo = caseTypeService.findByCaseTypeId(caseTypeId);
		
		caseTypeVo.setCaseName(caseName);
		caseTypeVo.setCaseDescription(description);
		caseTypeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		caseTypeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		caseTypeService.save(caseTypeVo);
		
		return caseTypeVo;
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteCaseType(@RequestParam(value="id") long id, HttpSession session)
	{
	
		caseTypeService.deleteCaseType(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("{id}/json")
	@ResponseBody
	public CaseTypeVo caseTypeJson(@PathVariable(value="id") long caseTypeId, HttpSession session)
	{	
		return caseTypeService.findByCaseTypeId(caseTypeId);
	}
}
