package com.croods.umeshtrading.controller.categoryandbrand;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.brand.BrandService;
import com.croods.umeshtrading.service.category.CategoryService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.brand.BrandVo;
import com.croods.umeshtrading.vo.category.CategoryVo;

@Controller
@RequestMapping("/categorybrand")
public class CategoryAndBrandController {
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	BrandService brandService;
	
	@GetMapping("")
	public ModelAndView newContact(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("category-brand/category-brand");
 	
 		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		return view;			
	}
	
	@PostMapping("category/save")
	@ResponseBody
	public CategoryVo saveCategory(@RequestParam(value="name") String name,@RequestParam(value="description") String description,HttpSession session)
 	{
		
		System.err.println("At category brand controller...");
		
		CategoryVo cat=new CategoryVo();
		cat.setCategoryName(name);
		cat.setCategoryDescription(description);
		cat.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		cat.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		cat.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		cat.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		cat.setCreatedOn(CurrentDateTime.getCurrentDate());
		cat.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		categoryService.save(cat);
		
		return cat;
	}
	
	@PostMapping("brand/save")
	@ResponseBody
	public BrandVo saveBrand(@RequestParam(value="name") String name,@RequestParam(value="description") String description,HttpSession session)
	{
		BrandVo brand=new BrandVo();
		brand.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		brand.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		brand.setBrandName(name);
		brand.setBrandDescription(description);
		
		brand.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		brand.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		brand.setCreatedOn(CurrentDateTime.getCurrentDate());
		brand.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		brandService.save(brand);
		
		return brand;
	}
	
	@PostMapping("category/update")
	@ResponseBody
	public CategoryVo updateUategory(@RequestParam(value="name") String name,@RequestParam(value="description") String description,@RequestParam(value="id") long id, HttpSession session)
	{
		CategoryVo cat=new CategoryVo();
		cat=categoryService.findByCategoryId(id);
		
		cat.setCategoryName(name);
		cat.setCategoryDescription(description);
		
		cat.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		cat.setModifiedOn(CurrentDateTime.getCurrentDate());
		categoryService.save(cat);
		
		return cat;
	}
	
	@PostMapping("brand/update")
	@ResponseBody
	public BrandVo updateBrand(@RequestParam(value="name") String name,@RequestParam(value="description") String description,@RequestParam(value="id") long id, HttpSession session)
	{
		BrandVo brand=new BrandVo();
		brand=brandService.findByBrandId(id);
		
		brand.setBrandName(name);
		brand.setBrandDescription(description);
		
		brand.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		brand.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		brandService.save(brand);
		
		return brand;
	}
	
	@PostMapping("category/delete")
	@ResponseBody
	public String deleteCategory(@RequestParam(value="id") long id, HttpSession session)
	{
	
		categoryService.deleteCategory(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("brand/delete")
	@ResponseBody
	public String deleteBrand(@RequestParam(value="id") long id, HttpSession session)
	{
	
		brandService.deleteBrand(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	
	@GetMapping("/category")
	@ResponseBody
	public List<CategoryVo> categoryList(HttpSession session) {
		System.out.println("at category---list");
		return categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
	}
	
	@GetMapping("/brand")
	@ResponseBody
	public List<BrandVo> brandList(HttpSession session) {
		System.out.println("brrrrrrrand---list");
		return brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
	}
	
}
