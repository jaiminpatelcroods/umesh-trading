package com.croods.umeshtrading.controller.color;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.color.ColorService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.color.ColorVo;

@Controller
@RequestMapping("/color")
public class ColorController {

	@Autowired
	ColorService colorService;
	
	@GetMapping("")
	public ModelAndView color(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("color/color");
 	
 		view.addObject("colorVos",colorService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public ColorVo saveColor(@RequestParam(value="colorName") String colorName,@RequestParam(value="colorCode") String colorCode, HttpSession session)
 	{
		ColorVo colorVo = new ColorVo();
		
		colorVo.setColorCode(colorCode);
		colorVo.setColorName(colorName);
		
		colorVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		colorVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		colorVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		colorVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		colorVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		colorVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		colorService.save(colorVo);
		
		return colorVo;
	}
	
	@PostMapping("update")
	@ResponseBody
	public ColorVo updateColor(@RequestParam(value="colorId") long colorId, @RequestParam(value="colorName") String colorName,@RequestParam(value="colorCode") String colorCode, HttpSession session)
	{
		ColorVo colorVo = colorService.findByColorId(colorId);
		
		colorVo.setColorCode(colorCode);
		colorVo.setColorName(colorName);
		
		colorVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		colorVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		colorService.save(colorVo);
		
		return colorVo;
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteColor(@RequestParam(value="id") long id, HttpSession session)
	{
		colorService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
}
