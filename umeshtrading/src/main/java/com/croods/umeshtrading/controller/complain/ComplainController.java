package com.croods.umeshtrading.controller.complain;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.casetype.CaseTypeService;
import com.croods.umeshtrading.service.complain.ComplainService;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.complain.ComplainVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.purchase.PurchaseVo;

@Controller
@RequestMapping("complain")
public class ComplainController {

	@Autowired
	ComplainService complainService;
	
	@Autowired
	CaseTypeService caseTypeService;
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	LocationService locationService;
	
	@GetMapping("")
	public ModelAndView complain(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("complain/complain");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<ComplainVo> complainDatatable(@Valid DataTablesInput input, HttpSession session) throws NumberFormatException, ParseException {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());

		Specification<ComplainVo>  specification =new Specification<ComplainVo>() {
			
			@Override
			public Predicate toPredicate(Root<ComplainVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<ComplainVo> dataTablesOutput = complainService.findAll(input, null,specification);
		dataTablesOutput.getData().forEach( x -> {
			x.getContactVo().setContactAddressVos(null);
			x.getContactVo().setContactOtherVos(null);
			x.getContactVo().setAccountCustomVo(null);
		});
		
		return dataTablesOutput;
	}
	
	@GetMapping("new")
	public ModelAndView complainNew(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("complain/complain-new");
		
		view.addObject("caseTypeVos", caseTypeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		return view;
	}
	
	@PostMapping("/save")
	public String saveComplain(@RequestParam Map<String,String> allRequestParams, @ModelAttribute("complainVo") ComplainVo complainVo,HttpSession session) {
		
		complainVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		complainVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		complainVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		complainVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		if(complainVo.getComplainId() == 0) {
			complainVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			complainVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			
			complainVo.setComplainNo(complainService.findMaxComplainNo(Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), Constant.CRM_COMPLAIN, "CMP"));
			
			complainVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.CRM_COMPLAIN, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		}
		
		complainService.save(complainVo);
		
		return "redirect:/complain/"+complainVo.getComplainId();
	}
	
	@GetMapping("{id}")
	public ModelAndView complainView(@PathVariable("id") long complainId,HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("complain/complain-view");
		ComplainVo complainVo = complainService.findByComplainIdAndBranchId(complainId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		complainVo.getContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
		
		complainVo.getContactVo().getContactAddressVos().forEach(p -> {
			p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
			p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
			p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
		});
		
		view.addObject("complainVo", complainVo);
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView complainEdit(@PathVariable("id") long complainId, HttpSession session) {
		
		ModelAndView view = new ModelAndView("complain/complain-edit");
		
		view.addObject("caseTypeVos", caseTypeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		
		ComplainVo complainVo = complainService.findByComplainIdAndBranchId(complainId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		view.addObject("complainVo", complainVo);
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String complainDelete(@PathVariable("id") long complainId, HttpSession session) {
		
		complainService.delete(complainId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "redirect:/complain";
	}
}
