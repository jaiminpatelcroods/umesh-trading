package com.croods.umeshtrading.controller.contact;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.repository.contact.ContactRepository;
import com.croods.umeshtrading.service.account.AccountService;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.contact.ContactAddressVo;
import com.croods.umeshtrading.vo.contact.ContactVo;

@Controller
@RequestMapping("/contact/{type}")
public class ContactController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	ProductService productService;
	
	ContactVo contactVo;
	
	@GetMapping("")
	public ModelAndView contact(@PathVariable(value="type") String type, HttpSession session)
 	{	
		ModelAndView view=new ModelAndView();
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addObject("displayContactType","Customer");
			view.addObject("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addObject("displayContactType","Supplier");
			view.addObject("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addObject("displayContactType","Agent");
			view.addObject("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addObject("displayContactType","Transport");
			view.addObject("type","transport");
		}
		
		List<ContactVo> contactVos = contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), type);
		
		contactVos.forEach(c -> {
			c.getContactAddressVos().removeIf( a ->
				a.getIsDefault() ==0 || a.getIsDeleted() == 1);
			
			c.getContactAddressVos().forEach( a -> {
				a.setCityName(locationService.findCityNameByCityCode(a.getCityCode()));
			});
		});
		
		view.addObject("contactVos", contactVos);
		
		view.setViewName("contact/contact");
		return view;
	}
	
	@RequestMapping("new")
	public ModelAndView newContact(HttpSession session,@PathVariable(value="type") String type)
 	{	
		ModelAndView view=new ModelAndView();
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addObject("displayContactType","Customer");
			view.addObject("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addObject("displayContactType","Supplier");
			view.addObject("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addObject("displayContactType","Agent");
			view.addObject("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addObject("displayContactType","Transport");
			view.addObject("type","transport");
		}
		view.setViewName("contact/contact-new");
		
		//For Agent dropdown in customers Start
		view.addObject("contactAgentList", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		
		return view;
	}
	
	@PostMapping("/save")
	public String save(HttpSession session,@PathVariable(value="type")String type,@RequestParam Map<String,String> allRequestParams, @ModelAttribute("contactVo") ContactVo contactVo, BindingResult bindingResult) {
		
    	if(contactVo.getContactAddressVos() != null && contactVo.getContactAddressVos().size() != 0) {
    		contactVo.getContactAddressVos().forEach(c -> c.setContactVo(contactVo));
    	}
    	if(contactVo.getContactOtherVos() != null && contactVo.getContactOtherVos().size() != 0) {
    		contactVo.getContactOtherVos().forEach(c -> c.setContactVo(contactVo));
    	}
    	
    	AccountCustomVo accountCustomVo = new AccountCustomVo();
    	
    	if(contactVo.getContactId() == 0) {
	    	
    		contactVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
        	contactVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	}
    	
    	if(type.equals(Constant.CONTACT_CUSTOMER) ||
    			type.equals(Constant.CONTACT_SUPPLIER)) {
    		
    		if(contactVo.getContactId() == 0) {
    			accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	    	accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	    	accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
    	    	accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	    	
    	    	AccountGroupVo accountGroupVo = new AccountGroupVo();
    	    	if (type.equals(Constant.CONTACT_CUSTOMER)) {			
    				accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_SUNDRY_DEBTORS);			
    				accountCustomVo.setAccounType(Constant.CONTACT_CUSTOMER);
    			} else if (type.equals(Constant.CONTACT_SUPPLIER)) {
    				accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_SUNDRY_CREDITORS);
    				accountCustomVo.setAccounType(Constant.CONTACT_SUPPLIER);
    			}
    	    	
    	    	accountCustomVo.setGroup(accountGroupVo);
    		} else {
    			accountCustomVo = contactService.findAccountCustomVoByContactId(contactVo.getContactId());
    		}
    		
    		accountCustomVo.setAccountName(contactVo.getCompanyName());
        	accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
        	accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
        	
        	accountService.insertAccount(accountCustomVo);
        	
        	contactVo.setAccountCustomVo(accountCustomVo);
    	}
    	
    	contactVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	contactVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	contactVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	contactVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	contactVo.setType(type);
    	
    	contactService.saveContact(contactVo);
    	
    	if(allRequestParams.containsKey("deleteAddress") == true) {		
			if(!allRequestParams.get("deleteAddress").equals("")) {
				String address=allRequestParams.get("deleteAddress").substring(0, allRequestParams.get("deleteAddress").length()-1);
				List<Long> contactAddressIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				contactService.deleteContactAddressByIdIn(contactAddressIds);
			}
			
		}
    	
    	if(allRequestParams.containsKey("deletedContactOther") == true) {		
			if(!allRequestParams.get("deletedContactOther").equals("")) {
				String address=allRequestParams.get("deletedContactOther").substring(0, allRequestParams.get("deletedContactOther").length()-1);
				List<Long> contactOtherIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				contactService.deleteContactOtherByIdIn(contactOtherIds);
			}
			
		}
		return "redirect:/contact/"+type+"/"+contactVo.getContactId();
	}
	
	@GetMapping("{id}")
	public ModelAndView contactView(@PathVariable("id") long contactId, @PathVariable("type") String type, HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view = new ModelAndView("contact/contact-view");
		/*ContactVo contactVo1 = contactRepository.findCompanyNameByContactId(contactId);
		System.out.println(contactVo1.getCompanyName());
		
		System.out.println(contactVo1.getGstin());*/
		//System.out.println(contactVo1.getContactAddressVos().size());
		/*System.out.println(contactVo1.getGstin());
		System.out.println(contactVo1.getContactAddressVos().size());*/
		
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addObject("displayContactType","Customer");
			view.addObject("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addObject("displayContactType","Supplier");
			view.addObject("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addObject("displayContactType","Agent");
			view.addObject("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addObject("displayContactType","Transport");
			view.addObject("type","transport");
		}
		
		ContactVo contactVo = contactService.findByContactIdAndBranchIdAndType(contactId, Long.parseLong(session.getAttribute("branchId").toString()), type);
		
		contactVo.getContactAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		try {
			contactVo.getContactAddressVos().forEach( c -> {
				c.setCountriesName(locationService.findCountriesNameByCountriesCode(c.getCountriesCode()));
				c.setStateName(locationService.findStateNameByStateCode(c.getStateCode()));
				c.setCityName(locationService.findCityNameByCityCode(c.getCityCode()));
			});
		} catch(Exception e) {
			
		}
		
		view.addObject("contactVo",contactVo);
		
		/*Customer Intrested Product */
		view.addObject("productVariantList", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		/*Agent dropdown in customers*/
		view.addObject("contactAgentList", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
				
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView contactEdit(@PathVariable("id") long contactId, @PathVariable("type") String type, HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view = new ModelAndView("contact/contact-edit");
		
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addObject("displayContactType","Customer");
			view.addObject("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addObject("displayContactType","Supplier");
			view.addObject("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addObject("displayContactType","Agent");
			view.addObject("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addObject("displayContactType","Transport");
			view.addObject("type","transport");
		}
		
		ContactVo contactVo = contactService.findByContactIdAndBranchIdAndType(contactId, Long.parseLong(session.getAttribute("branchId").toString()), type);
		
		contactVo.getContactAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		view.addObject("contactVo",contactVo);
		/*Customer Intrested Product */
		view.addObject("productVariantList", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		/*Agent dropdown in customers*/
		view.addObject("contactAgentList", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		
		return view;
	}
	
	@PostMapping("{id}/address/changedefault/{contactAddressId}")
	@ResponseBody
	public String changeDefaultAddress(@PathVariable("id") long contactId,@PathVariable("contactAddressId") long contactAddressId, @PathVariable("type") String type, HttpSession session) throws NumberFormatException, ParseException {
		
		contactService.changeDefaultAddress(contactId, contactAddressId);
		
		return "success";
	}
	
	@RequestMapping("/{id}/delete")
    public String deleteContact(@PathVariable long id,@PathVariable(value="type")String type, HttpSession session){
		
		contactService.deleteContact(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		AccountCustomVo accountCustomVo = contactService.findAccountCustomVoByContactId(id);
		
		accountService.deleteAccountCustom(accountCustomVo.getAccountCustomId(), Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
        return  "redirect:/contact/"+type;
    }
	
	@RequestMapping("/{id}/json")
	@ResponseBody
    public ContactVo contactViewJSON(@PathVariable(value="id") long contactId, @PathVariable(value="type") String type, HttpSession session){
		
		ContactVo contactVo = contactService.findByContactIdAndBranchIdAndType(contactId, Long.parseLong(session.getAttribute("branchId").toString()), type);
		
		System.err.println("contactVo ::: "+contactVo);
		System.err.println("contactVo ::: address vo ::::: "+contactVo.getContactAddressVos().size());
		
    	contactVo.getContactAddressVos().removeIf( c -> c.getIsDeleted()==1);
    	Collections.sort(contactVo.getContactAddressVos(), Comparator.comparingInt(ContactAddressVo::getIsDefault).reversed());
    	
		contactVo.getContactAddressVos().forEach(p -> {
			try {
				p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
			} catch (Exception e) {}
			
			try {
				p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
			} catch (Exception e) {}
			
			try {
				p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
			} catch (Exception e) {}
			
			p.setContactVo(null);
		});
    	contactVo.setContactOtherVos(null);
    	/*return json.use(JsonView.with(contactVos)
    			.onClass(ContactVo.class, Match.match().exclude("contactAddressVos.contact")))
    			.returnValue().get(0);*/
		contactVo.setAccountCustomVo(null);
		
		return contactVo;
	}
}
