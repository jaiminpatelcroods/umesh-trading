package com.croods.umeshtrading.controller.dashboard;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.repository.material.MaterialRepository;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.service.stock.StockTransactionService;
import com.croods.umeshtrading.util.NumberToWord;


@Controller
@RequestMapping("dashboard")
public class DashboardController {

	
	@Autowired
	ContactService contactService;
	@Autowired
	ProductService productService; 
	@Autowired
	StockTransactionService stockTransactionService;
	
	@Autowired
	MaterialRepository materialRepository;
	
	@RequestMapping(value={"/dashboard","","/"})
	public ModelAndView dashboard(HttpSession session) throws ParseException, NumberFormatException, ServletException, IOException {
		ModelAndView view=new ModelAndView("dashboard/dashboard");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate, endDate;
		
    	startDate = dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString());
    	endDate =dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString());
		
		view.addObject("Total_Customer",contactService.countByCompanyIdAndType(Long.parseLong(session.getAttribute("companyId").toString()),Constant.CONTACT_CUSTOMER));
		view.addObject("Total_Suppliers",contactService.countByCompanyIdAndType(Long.parseLong(session.getAttribute("companyId").toString()),Constant.CONTACT_SUPPLIER));
		view.addObject("Total_Product",productService.countByCompanyIdAndIsDeleted(Long.parseLong(session.getAttribute("companyId").toString()), 0));
		
		double ts=stockTransactionService.getTotalAllInPriceQty(Long.parseLong(session.getAttribute("branchId").toString()), startDate, endDate, session.getAttribute("financialYear").toString());
		view.addObject("Total_Stock",NumberToWord.getCheckData(""+ts));
		
		List<Map<Double, String>> list = materialRepository.get15daySalesAll(Long.parseLong(session.getAttribute("branchId").toString()));
		String s=list.stream().map(a -> String.valueOf(a.get("materialdate"))).collect(Collectors.joining("','"));
		String s1=list.stream().map(a -> String.valueOf(a.get("total"))).collect(Collectors.joining(","));
		System.err.println(s1);
		
		//System.err.println(list.get(0).get("total"));
	
		view.addObject("sales_date",s);
		view.addObject("sales_data",s1);
		
		view.addObject("Total_Wholesaler",contactService.countByCompanyIdAndContactTypeAndIsDeleted(Long.parseLong(session.getAttribute("companyId").toString()),Constant.CONTACT_CATEGORY_WHOLESALER,0));
		view.addObject("Total_Retailer",contactService.countByCompanyIdAndContactTypeAndIsDeleted(Long.parseLong(session.getAttribute("companyId").toString()),Constant.CONTACT_CATEGORY_RETAILER,0));
		view.addObject("Total_Other",contactService.countByCompanyIdAndContactTypeAndIsDeleted(Long.parseLong(session.getAttribute("companyId").toString()),Constant.CONTACT_CATEGORY_OTHER,0));
		
		return view;
	}
	
	/*@PostMapping("/bestsellingproduct")
	@ResponseBody
	public List<Map<String, String>>bestselProduct(HttpSession session,@RequestParam("type")String type) throws ParseException {
		Date startDate = null, endDate=null;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	System.err.println("{}(@){}");
    	Calendar calendar = Calendar.getInstance();
		if(type.equals("lastMonth")) {
			
	        calendar.add(Calendar.MONTH, -1);

	        int max = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	      //  System.err.println("max::"+max);
	        calendar.set(Calendar.DAY_OF_MONTH, 1);
	        
	        startDate = dateFormat.parse(dateFormat.format(calendar.getTime()).toString());
	        calendar.set(Calendar.DAY_OF_MONTH, max);
			endDate =dateFormat.parse(dateFormat.format(calendar.getTime()).toString());
	        //System.err.println("date::"+ calendar.getTime());
			System.err.println("startdate:"+startDate+"enddate:"+endDate);
		}else if(type.equals("allTime")) {
			
			SalesVo salesVo=salesService.findTop1BySalesItemIdAndBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			if(salesVo==null) {
				startDate = dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString());
			}else {
			startDate=salesVo.getSalesDate();
			}
			endDate =dateFormat.parse(dateFormat.format(calendar.getTime()).toString());
			System.err.println("startdate:"+startDate+"enddate:"+endDate);
			
		}else if(type.equals("lastYear")) {
			// calendar.add(Calendar.YEAR, -1);

		       // int max = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		      //  System.err.println("max::"+max);
		        calendar.set(Calendar.DAY_OF_MONTH, 1);
		        
		        startDate = dateFormat.parse(dateFormat.format(calendar.getTime()).toString());
		        calendar.set(Calendar.DAY_OF_MONTH, 31);
				endDate =dateFormat.parse(dateFormat.format(calendar.getTime()).toString());
				System.err.println("startdate:"+startDate+"enddate:"+endDate);
				
			
			
	    	calendar.setTime(dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()));
	    	 calendar.add(Calendar.YEAR, -1);
	    	 startDate = dateFormat.parse(dateFormat.format(calendar.getTime()).toString());
	    	 
	    	 calendar.setTime(dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
	    	 calendar.add(Calendar.YEAR, -1);
	    	 endDate = dateFormat.parse(dateFormat.format(calendar.getTime()).toString());
	 		System.err.println("startdate:"+startDate+"enddate:"+endDate);
			
			
		}
		//startDate = dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString());
    	
		//endDate =dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString());
		
		return salesService.getBestSellingProduct(Long.parseLong(session.getAttribute("branchId").toString()),startDate,endDate);	
	}*/
	
	/*@PostMapping("/salesVsPurchase")
	@ResponseBody
	public List<Map<String, String>> salesvsPurchase(HttpSession session) throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate, endDate;
		
    	startDate = dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString());
    	endDate =dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString());
		
		return salesService.getsalesVsPurchase(Long.parseLong(session.getAttribute("branchId").toString()),startDate,endDate);
	}*/
	
}
