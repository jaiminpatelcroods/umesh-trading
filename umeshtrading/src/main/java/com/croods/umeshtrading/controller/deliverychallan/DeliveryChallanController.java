package com.croods.umeshtrading.controller.deliverychallan;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.deliverychallan.DeliveryChallanService;
import com.croods.umeshtrading.service.employee.EmployeeService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.deliverychallan.DeliveryChallanVo;
import com.croods.umeshtrading.vo.purchase.PurchaseVo;

@Controller
@RequestMapping("deliverychallan")
public class DeliveryChallanController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	LocationService locationService;
	
	@GetMapping("/new")
	public ModelAndView newDeliveryChallan(HttpSession session, @Param(value = "contactId") String contactId) {
		
		ModelAndView view = new ModelAndView("deliverychallan/deliverychallan-new");
		
		long newChallanNo = deliveryChallanService.findMaxDeliveryChallanNo(
				Long.parseLong(session.getAttribute("companyId").toString()),
				Long.parseLong(session.getAttribute("branchId").toString()),
				Long.parseLong(session.getAttribute("userId").toString()), Constant.PURCHASE_DELIVERY_CHALLAN, "DC");
		
		view.addObject("deliveryChallanNo", newChallanNo);
		
		view.addObject("prefix",prefixService.findByPrefixTypeAndBranchId(Constant.PURCHASE_DELIVERY_CHALLAN, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		view.addObject("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		return view;
	}
	
	@PostMapping("/save")
	public String saveDeliveryChallan(@RequestParam Map<String,String> allRequestParams, @ModelAttribute("deliveryChallanVo") DeliveryChallanVo deliveryChallanVo,HttpSession session) {
		
		deliveryChallanVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		deliveryChallanVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		deliveryChallanVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		deliveryChallanVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		if(deliveryChallanVo.getDeliveryChallanId() == 0) {
			deliveryChallanVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			deliveryChallanVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		}
		
		if(deliveryChallanVo.getDeliveryChallanItemVos() != null) {
			deliveryChallanVo.getDeliveryChallanItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			deliveryChallanVo.getDeliveryChallanItemVos().forEach( item -> item.setDeliveryChallanVo(deliveryChallanVo));
		}
		
		if(allRequestParams.get("deleteDeliveryChallanItemIds") != null &&
				!allRequestParams.get("deleteDeliveryChallanItemIds").equals("")) {
			String address=allRequestParams.get("deleteDeliveryChallanItemIds").substring(0, allRequestParams.get("deleteDeliveryChallanItemIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			deliveryChallanService.deleteDeliveryChallanItemIds(l);
		}
		
		deliveryChallanService.save(deliveryChallanVo);
		
		return "redirect:/deliverychallan/"+deliveryChallanVo.getDeliveryChallanId();
	}
	
	@GetMapping("/{id}")
	public ModelAndView viewDeliveryChallan(@PathVariable("id") long deliveryChallanId, HttpSession session) {
		
		DeliveryChallanVo deliveryChallanVo = deliveryChallanService.findByDeliveryChallanIdAndBranchId(deliveryChallanId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		ModelAndView view = new ModelAndView("deliverychallan/deliverychallan-view");
		
		deliveryChallanVo.getContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
		
		deliveryChallanVo.getContactVo().getContactAddressVos().forEach(p -> {
			p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
			p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
			p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
		});
		
		view.addObject("deliveryChallanVo", deliveryChallanVo);
		return view;
	}
	
	@GetMapping("/{id}/edit")
	public ModelAndView editDeliveryChallan(@PathVariable("id") long deliveryChallanId, HttpSession session) {
		
		DeliveryChallanVo deliveryChallanVo = deliveryChallanService.findByDeliveryChallanIdAndBranchId(deliveryChallanId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		ModelAndView view = new ModelAndView("deliverychallan/deliverychallan-edit");
		
		deliveryChallanVo.getContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
		
		deliveryChallanVo.getContactVo().getContactAddressVos().forEach(p -> {
			p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
			p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
			p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
		});
		
		view.addObject("deliveryChallanVo", deliveryChallanVo);
		
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		view.addObject("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
}
