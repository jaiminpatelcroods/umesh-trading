package com.croods.umeshtrading.controller.department;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.department.DepartmentService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.department.DepartmentVo;

@Controller
@RequestMapping("/department")
public class DepartmentController {
	
	@Autowired
	DepartmentService departmentService;
	
	@GetMapping("")
	public ModelAndView newDepartment(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("department/department");
 	
 		view.addObject("departmentVos",departmentService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public DepartmentVo saveDepartment(@RequestParam(value="name") String name,HttpSession session)
 	{
		DepartmentVo departmentVo=new DepartmentVo();
		departmentVo.setDepartmentName(name);
		
		departmentVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		departmentVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		departmentVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		departmentVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		departmentVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		departmentVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		departmentService.save(departmentVo);
		
		return departmentVo;
	}
	
	
	@PostMapping("update")
	@ResponseBody
	public DepartmentVo updateUategory(@RequestParam(value="name") String name,@RequestParam(value="id") long id, HttpSession session)
	{
		DepartmentVo departmentVo=new DepartmentVo();
		departmentVo=departmentService.findByDepartmentId(id);
		
		departmentVo.setDepartmentName(name);
				
		departmentVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		departmentVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		departmentService.save(departmentVo);
		
		return departmentVo;
	}
	
	
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteDepartment(@RequestParam(value="id") long id, HttpSession session)
	{
	
		departmentService.deleteDepartment(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
}
