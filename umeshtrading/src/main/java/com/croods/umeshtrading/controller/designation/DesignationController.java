package com.croods.umeshtrading.controller.designation;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.designation.DesignationService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.designation.DesignationVo;

@Controller
@RequestMapping("/designation")
public class DesignationController {
	
	@Autowired
	DesignationService designationService;
	
	@GetMapping("")
	public ModelAndView newDesignation(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("designation/designation");
 	
 		view.addObject("designationVos",designationService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public DesignationVo saveDesignation(@RequestParam(value="name") String name,HttpSession session)
 	{
		DesignationVo designationVo=new DesignationVo();
		designationVo.setDesignationName(name);
		
		designationVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		designationVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		designationVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		designationVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		designationVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		designationVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		designationService.save(designationVo);
		
		return designationVo;
	}
	
	
	@PostMapping("update")
	@ResponseBody
	public DesignationVo updateUategory(@RequestParam(value="name") String name, @RequestParam(value="id") long id, HttpSession session)
	{
		DesignationVo designationVo=new DesignationVo();
		designationVo=designationService.findByDesignationId(id);
		
		designationVo.setDesignationName(name);
				
		designationVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		designationVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		designationService.save(designationVo);
		
		return designationVo;
	}
	
	
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteDesignation(@RequestParam(value="id") long id, HttpSession session)
	{
		designationService.deleteDesignation(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
}
