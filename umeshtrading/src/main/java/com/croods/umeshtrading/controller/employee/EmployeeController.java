package com.croods.umeshtrading.controller.employee;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.department.DepartmentService;
import com.croods.umeshtrading.service.designation.DesignationService;
import com.croods.umeshtrading.service.employee.EmployeeService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.userfront.UserFrontService;
import com.croods.umeshtrading.service.userrole.UserRoleService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.employee.EmployeeVo;
import com.croods.umeshtrading.vo.userfront.UserFrontVo;
import com.croods.umeshtrading.vo.userrole.UserRoleVo;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	DesignationService designationService;
	
	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	UserFrontService userFrontService;
	
	@GetMapping("")
	public ModelAndView employee(HttpSession session) {
		ModelAndView view=new ModelAndView("employee/employee");
		
		List<EmployeeVo> employeeVos = employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		
		/*employeeVos.forEach(e -> {
			e.setCountriesName(locationService.findCountriesNameByCountriesCode(e.getCountriesCode()));
	    	e.setStateName(locationService.findStateNameByStateCode(e.getStateCode()));
	    	e.setCityName(locationService.findCityNameByCityCode(e.getCityCode()));
		});*/
		
		view.addObject("employeeVos", employeeVos);
		return view;
	}
	
	@GetMapping("new")
	public ModelAndView newEmployee(HttpSession session) {
		ModelAndView view=new ModelAndView("employee/employee-new");
		
		view.addObject("departmentVos",departmentService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("designationVos",designationService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("userRoleVos", userRoleService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@PostMapping("/save")
	public String saveEmployee(@RequestParam Map<String,String> allRequestParams, HttpSession session, @ModelAttribute("employeeVo") EmployeeVo employeeVo, BindingResult bindingResult) {
		
		UserFrontVo userFrontVo = new UserFrontVo();
		
		if(employeeVo.getEmployeeId() == 0) {
			employeeVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			employeeVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		} else {
			if(employeeVo.getUserFrontVo() != null) {
				userFrontVo = userFrontService.findByUserFrontId(employeeVo.getUserFrontVo().getUserFrontId());
				userFrontVo = employeeVo.getUserFrontVo();
			}
		}
		
		if(employeeVo.getEmployeeContactVos() != null && employeeVo.getEmployeeContactVos().size() != 0) {
			employeeVo.getEmployeeContactVos().forEach(c -> c.setEmployeeVo(employeeVo));
    	}
		
		employeeVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	employeeVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	employeeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	employeeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	
    	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	
    	try {
			if(allRequestParams.get("anniversaryDate") != null)
				employeeVo.setAnniversaryDate(dateFormat.parse(allRequestParams.get("anniversaryDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	try {
			if(allRequestParams.get("dateOfBirth") != null)
				employeeVo.setDateOfBirth(dateFormat.parse(allRequestParams.get("dateOfBirth")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	if(allRequestParams.get("userRoleId") != "") {
    		
    		userFrontVo.setName(employeeVo.getEmployeeName());
    		userFrontVo.setCityCode(employeeVo.getCityCode());
    		userFrontVo.setStateCode(employeeVo.getStateCode());
    		userFrontVo.setCountriesCode(employeeVo.getCountriesCode());
    		userFrontVo.setContactNo(employeeVo.getEmployeeMobileno());
    		
    		userFrontVo.setUserName(allRequestParams.get("userName"));
    		
    		if(employeeVo.getUserFrontVo() == null) {
    			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        		userFrontVo.setPassword(passwordEncoder.encode(allRequestParams.get("password")));
        		userFrontVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    		}
    		
    		userFrontVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    		UserRoleVo userRoleVo = new UserRoleVo();
    		userRoleVo.setUserRoleId(Long.parseLong(allRequestParams.get("userRoleId")));
    		List<UserRoleVo> userRoleVos = new ArrayList<UserRoleVo>();
    		userRoleVos.add(userRoleVo);
    		
    		userFrontVo.setRoles(userRoleVos);
    		
    		UserFrontVo parentUserFront =new UserFrontVo();
    		parentUserFront.setUserFrontId(Long.parseLong(session.getAttribute("branchId").toString()));
    		
    		userFrontVo.setUserFrontVo(parentUserFront);
    		
    		userFrontVo.setStatus("active");
    		
    		userFrontService.save(userFrontVo);
    		
    		employeeVo.setUserFrontVo(userFrontVo);
    	}
    	employeeService.save(employeeVo);
    	
    	if(allRequestParams.containsKey("deletedContactOther") == true) {		
			if(!allRequestParams.get("deletedContactOther").equals("")) {
				String address=allRequestParams.get("deletedContactOther").substring(0, allRequestParams.get("deletedContactOther").length()-1);
				List<Long> employeeContactIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				employeeService.deleteEmployeeContactByIdIn(employeeContactIds);
			}
			
		}
		return "redirect:/employee/"+employeeVo.getEmployeeId();
	}
	@GetMapping("{id}")
	public ModelAndView viewEmployee(@PathVariable("id") long employeeId, HttpSession session) {
		
		ModelAndView view=new ModelAndView("employee/employee-view");
		EmployeeVo employeeVo = employeeService.findByEmployeeIdAndBranchId(employeeId, Long.parseLong(session.getAttribute("branchId").toString()));
		employeeVo.setCountriesName(locationService.findCountriesNameByCountriesCode(employeeVo.getCountriesCode()));
    	employeeVo.setStateName(locationService.findStateNameByStateCode(employeeVo.getStateCode()));
    	employeeVo.setCityName(locationService.findCityNameByCityCode(employeeVo.getCityCode()));
		view.addObject("employeeVo", employeeVo);
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView editEmployee(@PathVariable("id") long employeeId, HttpSession session) {
		
		ModelAndView view=new ModelAndView("employee/employee-edit");
		view.addObject("employeeVo", employeeService.findByEmployeeIdAndBranchId(employeeId, Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("departmentVos",departmentService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("designationVos",designationService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("userRoleVos", userRoleService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String deleteEmployee(@PathVariable("id") long employeeId, HttpSession session) {
		employeeService.deleteEmployee(employeeId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/employee";
	}
}
