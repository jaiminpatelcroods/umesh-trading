package com.croods.umeshtrading.controller.financial;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.croods.umeshtrading.service.financial.FinancialService;
import com.croods.umeshtrading.service.userfront.UserFrontService;
import com.croods.umeshtrading.util.CurrentDateTime;

@Controller
@RequestMapping("/financial")
public class FinancialController {

	@Autowired
	FinancialService financialService;
	
	@Autowired
	UserFrontService userFrontService;
	
	@RequestMapping("year/set/{year}")
	public String setFinancialYear(@PathVariable("year")String yearInterval, HttpSession session) {
		
		userFrontService.setDefaultYearInterval(Long.parseLong(session.getAttribute("branchId").toString()), yearInterval, CurrentDateTime.getCurrentDate());
		session.setAttribute("financialYear", yearInterval);
		
		session.setAttribute("firstDateFinancialYear",CurrentDateTime.getFirstDate(financialService.findByMonthInterval(session.getAttribute("monthInterval").toString()),yearInterval));
		session.setAttribute("lastDateFinancialYear",CurrentDateTime.getLastDate(financialService.findByMonthInterval(session.getAttribute("monthInterval").toString()),yearInterval));
		return "redirect:/setting";
	}
}
