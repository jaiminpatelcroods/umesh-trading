package com.croods.umeshtrading.controller.floor;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.floor.FloorService;
import com.croods.umeshtrading.service.place.PlaceService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.floor.FloorVo;

@Controller
@RequestMapping("floor")
public class FloorController {

	@Autowired
	FloorService floorService;
	
	@Autowired
	PlaceService placeService;
	
	@GetMapping("")
	public ModelAndView floor(HttpSession session) {
		ModelAndView view = new ModelAndView("floor/floor");
		view.addObject("placeList",placeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("floorVos", floorService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	/*@PostMapping("save")
	@ResponseBody
	public FloorVo floorSave(@RequestParam(value="placeId") long placeId,@RequestParam(value="floorName") String floorName,@RequestParam(value="description") String description,@RequestParam(value="floorCode") String floorCode,HttpSession session) 
	{
		FloorVo floorVo=new FloorVo();
		if(placeId!=0) 
			floorVo.setPlaceVo(placeVo);
		floorVo.setFloorName(floorName);
		floorVo.setFloorCode(floorCode);
		floorVo.setDescription(description);
		floorVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		floorVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		floorVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		floorVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		floorVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		floorVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		floorService.save(floorVo);
		return floorVo;
	}*/
	
	@PostMapping("save")
	@ResponseBody
	public FloorVo floorSave(HttpSession session, @ModelAttribute("floor")FloorVo floor, BindingResult bindingResult, Model model) 
	{
		floor.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		floor.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		floor.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		floor.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		floor.setCreatedOn(CurrentDateTime.getCurrentDate());
		floor.setModifiedOn(CurrentDateTime.getCurrentDate());
		floorService.save(floor);
		return floor;
	}
	
	/*
	 * @PostMapping("/update")
	 * 
	 * @ResponseBody public FloorVo floorUpdate(@RequestParam(value="floorName")
	 * String floorName,@RequestParam(value="description") String
	 * description,@RequestParam(value="floorCode") String
	 * floorCode,@RequestParam(value="id") long id, HttpSession session) { FloorVo
	 * floorVo = new FloorVo(); floorVo = floorService.findByFloorId(id);
	 * 
	 * floorVo.setFloorName(floorName); floorVo.setFloorCode(floorCode);
	 * floorVo.setDescription(description);
	 * floorVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()))
	 * ; floorVo.setModifiedOn(CurrentDateTime.getCurrentDate());
	 * floorService.save(floorVo); return floorVo; }
	 */
	
	@PostMapping("/update")
	@ResponseBody
	public FloorVo floorUpdate(HttpSession session,@ModelAttribute("floor")FloorVo floor, BindingResult bindingResult, Model model)
	{	
		floor.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		floor.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		floor.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		floor.setModifiedOn(CurrentDateTime.getCurrentDate());
		floorService.save(floor);
		return floor;
	}
	
	@PostMapping("/delete")
	@ResponseBody
	public String floorDelete(@RequestParam(value="id") long id, HttpSession session)
	{
		floorService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "Sucess";
	}
	
	@GetMapping("/place/{id}")
	@ResponseBody
	public List<FloorVo> floorList(@PathVariable long id) {
		System.out.println("at floor--- place---list");
		return floorService.findByPlaceVoPlaceId(id);
	}
}
