package com.croods.umeshtrading.controller.lead;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.lead.LeadService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.contact.ContactVo;
import com.croods.umeshtrading.vo.lead.LeadVo;

@Controller
@RequestMapping("/lead")
public class LeadController {

	@Autowired
	LocationService locationService;
	
	@Autowired
	LeadService leadService;
	
	@GetMapping("")
	public ModelAndView lead(HttpSession session)
 	{	
		ModelAndView view=new ModelAndView();
		
		view.setViewName("lead/lead");
		return view;
	}
	
	@RequestMapping("new")
	public ModelAndView newContact()
 	{	
		ModelAndView view=new ModelAndView();
		
		view.setViewName("lead/lead-new");
		return view;
	}
	
	@PostMapping("/save")
	public String save(HttpSession session, @RequestParam Map<String,String> allRequestParams, @ModelAttribute("leadVo") LeadVo leadVo, BindingResult bindingResult) {
		
    	if(leadVo.getLeadAddressVos() != null && leadVo.getLeadAddressVos().size() != 0) {
    		leadVo.getLeadAddressVos().forEach(c -> c.setLeadVo(leadVo));
    	}
    	if(leadVo.getLeadContactVos() != null && leadVo.getLeadContactVos().size() != 0) {
    		leadVo.getLeadContactVos().forEach(c -> c.setLeadVo(leadVo));
    	}
    	
    	if(leadVo.getLeadId() == 0) {
	    	
    		leadVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
        	leadVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	}
    	
    	leadVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	leadVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	leadVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	leadVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	
    	leadService.saveLead(leadVo);
    	
    	if(allRequestParams.containsKey("deleteAddress") == true) {		
			if(!allRequestParams.get("deleteAddress").equals("")) {
				String address=allRequestParams.get("deleteAddress").substring(0, allRequestParams.get("deleteAddress").length()-1);
				List<Long> leadAddressIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				leadService.deleteLeadAddressByIdIn(leadAddressIds);
			}
			
		}
    	
    	if(allRequestParams.containsKey("deletedLeadContact") == true) {		
			if(!allRequestParams.get("deletedLeadContact").equals("")) {
				String address=allRequestParams.get("deletedLeadContact").substring(0, allRequestParams.get("deletedLeadContact").length()-1);
				List<Long> leadContactIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				leadService.deleteLeadContactByIdIn(leadContactIds);
			}
			
		}
		return "redirect:/lead/"+leadVo.getLeadId();
	}
	
	@GetMapping("{id}")
	public ModelAndView leadView(@PathVariable("id") long leadId, HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view = new ModelAndView("lead/lead-view");
		
		LeadVo leadVo = leadService.findByLeadIdAndBranchId(leadId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		leadVo.getLeadAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		try {
			leadVo.getLeadAddressVos().forEach( c -> {
				c.setCountriesName(locationService.findCountriesNameByCountriesCode(c.getCountriesCode()));
				c.setStateName(locationService.findStateNameByStateCode(c.getStateCode()));
				c.setCityName(locationService.findCityNameByCityCode(c.getCityCode()));
			});
		} catch(Exception e) {
			
		}
		
		view.addObject("leadVo",leadVo);
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView leadEdit(@PathVariable("id") long leadId, HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view = new ModelAndView("lead/lead-edit");
		
		LeadVo leadVo = leadService.findByLeadIdAndBranchId(leadId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		leadVo.getLeadAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		view.addObject("leadVo",leadVo);
		return view;
	}
	
	@RequestMapping("/{id}/delete")
    public String deleteLead(@PathVariable long leadId, HttpSession session){
		
		leadService.deleteLead(leadId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
        return  "redirect:/lead";
    }
}
