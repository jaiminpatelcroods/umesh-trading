package com.croods.umeshtrading.controller.location;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.vo.location.CityVo;
import com.croods.umeshtrading.vo.location.CountriesVo;
import com.croods.umeshtrading.vo.location.StateVo;

@Controller
@RequestMapping("/location")
public class LocationController {

	@Autowired
	LocationService locationService;
	
	@RequestMapping("/country")
	@ResponseBody
	public List<CountriesVo> findAllCountry()
	{
		return locationService.findAllCountries();
	}
	
	@RequestMapping("/state/{id}")
	@ResponseBody
	public List<StateVo> findState(@PathVariable(value = "id") String id) {
		return locationService.findByCountriesCode(id);
	}
	
	@RequestMapping("/city/{id}")
	@ResponseBody
	public List<CityVo> findCity(@PathVariable(value = "id") String id) {
		
		return locationService.findByStateCode(id);
	}
}
