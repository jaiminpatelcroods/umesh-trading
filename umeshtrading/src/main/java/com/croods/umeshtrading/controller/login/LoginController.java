package com.croods.umeshtrading.controller.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	@RequestMapping(value= {"login",""})
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("signup-login/login");
		
		return modelAndView;
	}
	
	@RequestMapping("success")
	@ResponseBody
	public String suc() {
		return "SUCCESS";
	}
}
