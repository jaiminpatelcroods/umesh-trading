package com.croods.umeshtrading.controller.navmenu;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.navmenu.NavMenuService;
import com.croods.umeshtrading.vo.navmenu.NavMenuActionVo;
import com.croods.umeshtrading.vo.navmenu.NavMenuPermissionVo;
import com.croods.umeshtrading.vo.navmenu.NavMenuVo;
import com.croods.umeshtrading.vo.navmenu.NavSubMenuVo;
import com.croods.umeshtrading.vo.userfront.UserFrontVo;
import com.croods.umeshtrading.vo.userrole.UserRoleVo;

@Controller
public class NavMenuController {
	
	@Autowired
	NavMenuService navMenuService;
	
	@GetMapping("nav")
	public ModelAndView nav() { 
		ModelAndView view = new ModelAndView("navmenu/navmenu");
		
		view.addObject("navMenuVos", navMenuService.findAllNavMenu());
		
		return view;
	}
	
	@GetMapping("navsub")
	public ModelAndView navsub() { 
		ModelAndView view = new ModelAndView("navmenu/navsubmenu");
		
		view.addObject("navMenuVos", navMenuService.findAllNavMenu());
		
		return view;
	}
	@GetMapping("construction")
	public ModelAndView construction() { 
		ModelAndView view = new ModelAndView("construction/construction");
		
		return view;
	}
	@PostMapping("navmenu/save")
	public String saveNav(@RequestParam Map<String,String> allRequestParams) {
		NavMenuVo navMenuVo = new  NavMenuVo();
		
		navMenuVo.setIconClass("m-menu__link-icon flaticon-line-graph");
		navMenuVo.setMenuURL(allRequestParams.get("menuURL"));
		navMenuVo.setOrdering(Integer.parseInt(allRequestParams.get("ordering")));
		navMenuVo.setStatus("active");
		navMenuVo.setTitle(allRequestParams.get("title"));
		
		navMenuService.saveNavMenu(navMenuVo);
		
		return "redirect:/nav";
	}
	
	@PostMapping("navsubmenu/save")
	public String saveNavSub(@RequestParam Map<String,String> allRequestParams) {
		
		for(int i=0; i<10; i++) {
			System.out.println(allRequestParams.get("menuId"+i) != "");
			if(!allRequestParams.get("menuId"+i).equals("khali")) {
				NavSubMenuVo navSubMenuVo = new NavSubMenuVo();
				
				navSubMenuVo.setIconClass("m-menu__link-icon flaticon-line-graph");
				navSubMenuVo.setMenuURL(allRequestParams.get("menuURL"+i));
				navSubMenuVo.setOrdering(Integer.parseInt(allRequestParams.get("ordering"+i)));
				navSubMenuVo.setStatus("active");
				navSubMenuVo.setSubMenuAction(allRequestParams.get("action"+i));
				navSubMenuVo.setTitle(allRequestParams.get("title"+i));
				navSubMenuVo.setType(allRequestParams.get("type"+i));
				
				NavMenuVo navMenuVo = new  NavMenuVo();
				navMenuVo.setNavMenuId(Long.parseLong(allRequestParams.get("menuId"+i)));
				navSubMenuVo.setNavMenuVo(navMenuVo);
				
				navMenuService.saveNavSubMenu(navSubMenuVo);
			}
		}
		
		return "redirect:/navsub";
	}
	
	@GetMapping("/insertpermision")
	@ResponseBody
	public String login(HttpServletRequest req)
 	{
		UserFrontVo userFrontVo =new UserFrontVo();
		userFrontVo.setUserFrontId(2);
		
		UserRoleVo roleVo =new UserRoleVo();
		roleVo.setUserRoleId(Constant.URID_COMPANY);
		
		List<NavSubMenuVo> navSubMenuVos = navMenuService.findAllNavSubMenu();
		for (NavSubMenuVo navSubMenuVo : navSubMenuVos) {
			String actions[] = navSubMenuVo.getSubMenuAction().split(",");
			
			for (int j=0;j<actions.length;j++) {
				if(!actions[j].equals("")) {
					NavMenuActionVo actionVo =new NavMenuActionVo();
					actionVo.setNavMenuActionId(Long.parseLong(actions[j]));
					
					NavMenuPermissionVo navMenuPermissionVo =new NavMenuPermissionVo();
					
					navMenuPermissionVo.setNavMenuVo(navSubMenuVo.getNavMenuVo());
					navMenuPermissionVo.setNavSubMenuVo(navSubMenuVo);
					navMenuPermissionVo.setStatus("active");
					navMenuPermissionVo.setUserFrontVo(userFrontVo);
					navMenuPermissionVo.setUserRoleVo(roleVo);
					navMenuPermissionVo.setNavMenuActionVo(actionVo);
					
					navMenuService.saveNavMenuPermission(navMenuPermissionVo);
				}
			}
			
			
		}
		
		return "SuCCESS ";
	}
}
