package com.croods.umeshtrading.controller.payment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.Payment.PaymentService;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.purchase.PurchaseService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.payment.PaymentBillVo;
import com.croods.umeshtrading.vo.payment.PaymentVo;
import com.croods.umeshtrading.vo.purchase.PurchaseVo;

@Controller
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	PaymentService paymentService;

	@Autowired
	ContactService contactService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	PurchaseService purchaseService;
	
	@GetMapping("")
	public ModelAndView payment(HttpSession session) {
		
		ModelAndView view = new ModelAndView("payment/payment");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<PaymentVo> purchaseRequestDatatable(@Valid DataTablesInput input, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<PaymentVo>  specification =new Specification<PaymentVo>() {
			
			@Override
			public Predicate toPredicate(Root<PaymentVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				try {
					predicates.add(criteriaBuilder.between(root.get("paymentDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<PaymentVo> dataTablesOutput=paymentService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setPaymentBillVos(null);
			p.setBankVo(null);
			p.getContactVo().setContactAddressVos(null);
			p.getContactVo().setContactOtherVos(null);
			p.getContactVo().setAccountCustomVo(null);
		});
		return dataTablesOutput;
	}
	
	@GetMapping("/new")
	public ModelAndView newPayment(HttpSession session, @Param(value = "contactId") String contactId) {
		
		ModelAndView view = new ModelAndView("payment/payment-new");
		
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		return view;
	}
	
	@PostMapping("/save")
	public String savePayment(@RequestParam Map<String,String> allRequestParams,@ModelAttribute("PaymentVo") PaymentVo paymentVo, HttpSession session ) {
		
		double totalKasar=0;
	
		if(paymentVo.getPaymentBillVos()!=null) {
			paymentVo.getPaymentBillVos().removeIf(pay -> pay.getPurchaseVo() == null);
			paymentVo.getPaymentBillVos().forEach(pay->pay.setPaymentVo(paymentVo));
			totalKasar=paymentVo.getPaymentBillVos().stream().mapToDouble(pay->pay.getKasar()).sum();
		}
		
		if(paymentVo.getPaymentId()==0l) {
			paymentVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			paymentVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		}
		
		paymentVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		paymentVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		paymentVo.setPaymentNo(paymentService.findMaxPaymentNo(
				Long.parseLong(session.getAttribute("companyId").toString()),
				Long.parseLong(session.getAttribute("branchId").toString()),
				Long.parseLong(session.getAttribute("userId").toString()),Constant.PAYMENT, "PAY"));
		paymentVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.PAYMENT, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		paymentVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		paymentVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		if(allRequestParams.get("deletedBillIds") != null && !allRequestParams.get("deletedBillIds").equals("")) {
			
			String deletedBillIds = allRequestParams.get("deletedBillIds").substring(0, allRequestParams.get("deletedBillIds").length()-1);
			List<Long> billIds= Arrays.asList(deletedBillIds.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());	
			paymentService.deletePaymentBillVoByIn(billIds);
			
		}

		PaymentVo paymentVo2=paymentService.save(paymentVo);
		
		
		
		if(paymentVo2.getPaymentBillVos() != null)
			for(PaymentBillVo paymentBillVo:paymentVo2.getPaymentBillVos()){	
				purchaseService.addPaidAmountByPurchaseId(paymentBillVo.getPurchaseVo().getPurchaseId(),paymentBillVo.getPayment()+paymentBillVo.getKasar(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
			}	
		return "redirect:/payment/"+paymentVo2.getPaymentId();
	}
	
	@GetMapping("/{id}")
	public ModelAndView paymentView(@PathVariable(value="id") long paymentId, HttpSession session) {
		ModelAndView view=new ModelAndView("payment/payment-view");
		
		view.addObject("paymentVo", paymentService.findByPaymentIdAndBranchId(paymentId,Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("/{id}/edit")
	public ModelAndView paymentEdit(@PathVariable(value="id") long paymentId, HttpSession session) {
		ModelAndView view=new ModelAndView("payment/payment-edit");
		
		view.addObject("paymentVo", paymentService.findByPaymentIdAndBranchId(paymentId,Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		return view;
	}
}
