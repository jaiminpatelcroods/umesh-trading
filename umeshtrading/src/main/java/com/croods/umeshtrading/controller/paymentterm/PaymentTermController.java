package com.croods.umeshtrading.controller.paymentterm;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.paymentterm.PaymentTermService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.paymentterm.PaymentTermVo;

@Controller
@RequestMapping("paymentterm")
public class PaymentTermController {

	@Autowired
	PaymentTermService paymentTermService;
	
	@GetMapping("")
	public ModelAndView paymentTerm(HttpSession session)
	{	
		ModelAndView view=new ModelAndView("paymentterm/paymentterm");
		view.addObject("paymentTermVos",paymentTermService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;		
	}
	
	@PostMapping("/save")
	@ResponseBody
	public PaymentTermVo insertPaymentTerm(@RequestParam(value="name") String name,@RequestParam(value="day") String day,HttpSession session)
	{
		PaymentTermVo paymentTermVo=new PaymentTermVo();
		paymentTermVo.setPaymentTermDay(Integer.parseInt(day));
		paymentTermVo.setPaymentTermName(name);
		paymentTermVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		paymentTermVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		paymentTermVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		paymentTermVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));				
		paymentTermVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		paymentTermVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		paymentTermService.save(paymentTermVo);
		
		return paymentTermVo;
	}
	
	@PostMapping("/update")
	@ResponseBody
	public PaymentTermVo update(@RequestParam(value="name") String name,@RequestParam(value="day") String day,@RequestParam(value="id") long id,HttpSession session)
	{		
		
		PaymentTermVo paymentTermVo=new PaymentTermVo();		
		paymentTermVo=paymentTermService.findByPaymentTermId(id);			
		paymentTermVo.setPaymentTermName(name);
		paymentTermVo.setPaymentTermDay(Integer.parseInt(day));	
		paymentTermVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		paymentTermVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		paymentTermService.save(paymentTermVo);
		return paymentTermVo;
	}
	
	@PostMapping("/delete")
	@ResponseBody
	public String deletePaymentTerm(@RequestParam(value="id") long id, HttpSession session)
	{		
		paymentTermService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());		
		
		return "Sucess";
	}
}
