package com.croods.umeshtrading.controller.place;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.place.PlaceService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.place.PlaceVo;

@Controller
@RequestMapping("place")
public class PlaceController {

	@Autowired
	PlaceService placeService;
	
	@GetMapping("")
	public ModelAndView place(HttpSession session) {
		ModelAndView view = new ModelAndView("place/place");
		view.addObject("placeVos", placeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	@PostMapping("save")
	@ResponseBody
	public PlaceVo placeSave(@RequestParam(value="placeName") String placeName,@RequestParam(value="description") String description,@RequestParam(value="placeCode") String placeCode,HttpSession session) 
	{
		PlaceVo placeVo=new PlaceVo();
		placeVo.setPlaceName(placeName);
		placeVo.setPlaceCode(placeCode);
		placeVo.setDescription(description);
		placeVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		placeVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		placeVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		placeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		placeVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		placeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		placeService.save(placeVo);
		return placeVo;
	}
	
	@PostMapping("/update")
	@ResponseBody
	public PlaceVo placeUpdate(@RequestParam(value="placeName") String placeName,@RequestParam(value="description") String description,@RequestParam(value="placeCode") String placeCode,@RequestParam(value="id") long id, HttpSession session)
	{	
		PlaceVo placeVo = new PlaceVo();
		placeVo = placeService.findByPlaceId(id);
		
		placeVo.setPlaceName(placeName);
		placeVo.setPlaceCode(placeCode);
		placeVo.setDescription(description);
		placeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		placeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		placeService.save(placeVo);
		return placeVo;
	}
	
	@PostMapping("/delete")
	@ResponseBody
	public String placeDelete(@RequestParam(value="id") long id, HttpSession session)
	{
		placeService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "Sucess";
	}
	
	
	
}
