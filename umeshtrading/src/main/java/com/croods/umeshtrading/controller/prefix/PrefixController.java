package com.croods.umeshtrading.controller.prefix;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.prefix.PrefixVo;

@Controller
@RequestMapping("/prefix")
public class PrefixController {

	@Autowired
	PrefixService prefixService;
	
	@GetMapping("")
	public ModelAndView prefix(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("prefix/prefix");
 	
 		view.addObject("prefixVos",prefixService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
 		return view;			
	}
	
	@PostMapping("update")
	@ResponseBody
	public PrefixVo updatePrefix(@RequestParam(value="prefixId") long prefixId, @RequestParam(value="prefixType") String prefixType,@RequestParam(value="prefix") String prefix, @RequestParam(value="sequenceNo") long sequenceNo, HttpSession session)
	{
		PrefixVo prefixVo= prefixService.findByPrefixId(prefixId);
		
		prefixVo.setPrefixType(prefixType);
		prefixVo.setPrefix(prefix);
		
		if(prefixVo.getSequenceNo() != sequenceNo) {
			prefixVo.setSequenceNo(sequenceNo);
			prefixVo.setIsChangeSequnce(1);
		}
		prefixVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		prefixService.save(prefixVo);
		
		return prefixVo;
	}
}
