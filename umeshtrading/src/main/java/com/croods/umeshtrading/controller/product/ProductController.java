package com.croods.umeshtrading.controller.product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.repository.stock.StockTransactionRepository;
import com.croods.umeshtrading.service.brand.BrandService;
import com.croods.umeshtrading.service.category.CategoryService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.service.stock.StockService;
import com.croods.umeshtrading.service.tax.TaxService;
import com.croods.umeshtrading.service.unitofmeasurement.UnitOfMeasurementService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.product.ProductAttributeVo;
import com.croods.umeshtrading.vo.product.ProductVariantVo;
import com.croods.umeshtrading.vo.product.ProductVo;



@Controller
@RequestMapping("product")
public class ProductController {

	@Autowired
	CategoryService categoryService;
	
	@Autowired
	BrandService brandService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	StockTransactionRepository stockTransactionRepository;
	
	JSONArray jsonArray;
	JSONObject jsonObject;
	Double totalQty=0.0;
	Double lastPurchasePrice;
	
	@GetMapping("")
	public ModelAndView product(HttpSession session) {
		ModelAndView view =new ModelAndView("product/product");
		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<ProductVo> productDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long companyId = Long.parseLong(session.getAttribute("companyId").toString());

		Specification<ProductVo>  specification =new Specification<ProductVo>() {
			
			@Override
			public Predicate toPredicate(Root<ProductVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("companyId"), companyId));
				
				if(!allRequestParams.get("category").equals("")) {
					predicates.add(criteriaBuilder.equal(root.get("categoryVo").get("categoryId"), Long.parseLong(allRequestParams.get("category").toString())));
				}
				if(!allRequestParams.get("brand").equals("")) {
					predicates.add(criteriaBuilder.equal(root.get("brandVo").get("brandId"), Long.parseLong(allRequestParams.get("brand").toString())));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<ProductVo> dataTablesOutput=productService.findAll(input, null,specification);
		return dataTablesOutput;
	}
	
	@GetMapping("new")
	public ModelAndView productNew(HttpSession session) {
		ModelAndView view =new ModelAndView("product/product-new");
		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("unitOfMeasurementVos", unitOfMeasurementService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("taxVos", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	@PostMapping("/save")
	public String saveProduct(HttpSession session,@RequestParam Map<String,String> allRequestParams, @ModelAttribute("product") ProductVo product, BindingResult bindingResult, Model model)
 	{			
		if(product.getProductId() == 0) {
			product.setCreatedOn(CurrentDateTime.getCurrentDate());
	    	product.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		}
		product.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		product.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	product.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	product.setModifiedOn(CurrentDateTime.getCurrentDate());
    	/*
		 * System.err.println("product..sale.."+product.getSalePrice() +"purchase..."+
		 * product.getPurchasePrice()+"  discount"+product.getDiscount());
		 * product.setPurchasePrice(product.getPurchasePrice());
		 * product.setSalePrice(product.getSalePrice());
		 * 
		 * product.setDiscount(product.getDiscount());
		 */
    	
		/*for(int i=0;i<product.getProductVariantVos().size();i++)
		{
			ProductVariantVo productVariantVo=product.getProductVariantVos().get(i);
			productVariantVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			productVariantVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			productVariantVo.setProductVo(product);			
		}

		if(product.getHaveVariation() == 1) {
			for(int i=0;i<product.getProductAttributeVos().size();i++)		
			{
				ProductAttributeVo productAttributeVo=product.getProductAttributeVos().get(i);
				productAttributeVo.setProductVo(product);			
			}
		}*/
		productService.saveProduct(product);
		return  "redirect:/product/"+product.getProductId();
	}
	
	@GetMapping("{id}")
	public ModelAndView productView(@PathVariable("id") long productId, HttpSession session) {
		ModelAndView view =new ModelAndView("product/product-view");
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		totalQty =  stockService.availableProductQty(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		view.addObject("productVo", productVo);
		view.addObject("totalQty", totalQty);
		return view;
	}
	
	@PostMapping("{id}/json")
	@ResponseBody
	public ProductVo productViewJSON(@PathVariable("id") long productId, HttpSession session) {
		
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId, Long.parseLong(session.getAttribute("companyId").toString()));
		//productVo.getProductVariantVos().forEach(p -> p.setProductVo(null));
		//productVo.setProductAttributeVos(null);
		
		return productVo;
	}
	
	@PostMapping("variant/{id}/json")
	@ResponseBody
	public ProductVariantVo productVariantJSON(@PathVariable("id") long productVariantId, HttpSession session) {
		
		ProductVariantVo productVariantVo = productService.findProductVariantByIdAndCompanyId(productVariantId, Long.parseLong(session.getAttribute("companyId").toString()));
		//productVariantVo.getProductVo().setProductVariantVos(null);
		//productVariantVo.getProductVo().setProductAttributeVos(null);
		
		return productVariantVo;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView productEdit(@PathVariable("id") long productId, HttpSession session) {
		ModelAndView view =new ModelAndView("product/product-edit");
		
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("taxVos", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("unitOfMeasurementVos", unitOfMeasurementService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("productVo", productVo);
		return view;
	}
	
	@GetMapping("variant/json")
	@ResponseBody
	public String productVariantMultiSelectJSON(@RequestParam Map<String,String> allRequestParams, HttpSession session) {
		jsonArray = new JSONArray();
		jsonObject = new JSONObject();
		List<ProductVariantVo> productVariantVos = productService.findProductVariants(allRequestParams.get("q"), Long.parseLong(session.getAttribute("companyId").toString()));
		productVariantVos.forEach(p-> {
			JSONObject json1 = new JSONObject();
			try{
			json1.put("id", p.getProductVariantId());
			json1.put("text", p.getProductVo().getName()+" "+p.getVariantName());
			}catch(Exception e){}
			jsonArray.put(json1);
		});
		try {
			jsonObject.put("total_count", productVariantVos.size());
			jsonObject.put("incomplete_results",true);
			jsonObject.put("items",jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
	
	@RequestMapping("/{id}/delete")
    public String deleteProduct(@PathVariable Long id){
    	productService.deleteProduct(id);          
        return  "redirect:/product/";
    }
	
	@GetMapping("/place/{id}")
	@ResponseBody
	public List<Map<String, String>> placeListForAvailableProduct(@PathVariable("id") long productId, HttpSession session) {
		//return stockService.availableProductPlace(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		
		List<Map<String, String>> placeIds = stockService.availableProductPlace(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		
		System.err.println("placeIds sizeeee "+placeIds.size());
		
		
		return placeIds;
	}
	
	@GetMapping("{placeid}/floor/{id}")
	@ResponseBody
	public List<Map<String, String>> floorListForAvailableProduct(@PathVariable("placeid") long placeId,@PathVariable("id") long productId, HttpSession session) {
		return stockService.availableProductFloor(placeId,productId,Long.parseLong(session.getAttribute("companyId").toString()));
	}
	
	@GetMapping("{floorid}/rack/{id}")
	@ResponseBody
	public List<Map<String, String>> rackListForAvailableProduct(@PathVariable("floorid") long floorId,@PathVariable("id") long productId, HttpSession session) {
		System.err.println("at call for rack");
		return stockService.availableProductRack(floorId,productId,Long.parseLong(session.getAttribute("companyId").toString()));
	}
	
	
	@GetMapping("{id}/qtyandprice")
	@ResponseBody 
	public String productQtyPrice(@PathVariable("id") long productId,HttpSession session)
	{
		//jsonArray = new JSONArray();
		jsonObject = new JSONObject();
		totalQty =  stockService.availableProductQty(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		try {
			jsonObject.put("totalqty", totalQty);
			
			//jsonObject.put("items",jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
	
	@GetMapping("{id}/qtyandprice/{placeid}")
	@ResponseBody 
	public String productQtyPriceOnPlace(@PathVariable("id") long productId,@PathVariable("placeid") long placeId,HttpSession session)
	{
		jsonObject = new JSONObject();
		totalQty =  stockService.availableProductQtyOnPlace(productId,placeId,Long.parseLong(session.getAttribute("companyId").toString()));
		try {
			jsonObject.put("placeqty", totalQty);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
	
	
	@GetMapping("{id}/lastpurchaseprice")
	@ResponseBody 
	public String productLastPurchasePrice(@PathVariable("id") long productId,HttpSession session)
	{
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		jsonObject = new JSONObject();
		lastPurchasePrice =  stockTransactionRepository.getLastPucrchasePrice(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		try {
			jsonObject.put("marketPrice",productVo.getMarketPrice());
			jsonObject.put("purchasePrice",productVo.getPurchasePrice());
			if(null==lastPurchasePrice)
				jsonObject.put("lastPurchasePrice","N.A.");
			else
				jsonObject.put("lastPurchasePrice", lastPurchasePrice);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
}
