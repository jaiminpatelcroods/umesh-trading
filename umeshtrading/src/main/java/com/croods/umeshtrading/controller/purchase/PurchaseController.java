package com.croods.umeshtrading.controller.purchase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.additionalcharge.AdditionalChargeService;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.paymentterm.PaymentTermService;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.service.purchase.PurchaseService;
import com.croods.umeshtrading.service.termsandcondition.TermsAndConditionService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.contact.ContactAddressVo;
import com.croods.umeshtrading.vo.purchase.PurchaseVo;

@Controller
@RequestMapping("/purchase/{type}")
public class PurchaseController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	PaymentTermService paymentTermService;
	
	@Autowired
	LocationService locationService;

	@Autowired
	TermsAndConditionService termsAndConditionService;
	
	@Autowired
	AdditionalChargeService additionalChargeService;
	
	@GetMapping("")
	public ModelAndView purchaseList(HttpSession session, @PathVariable(value="type") String type) {
		ModelAndView view = new ModelAndView("purchase/purchase");
		
		view.addObject("type",type);
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addObject("displayType", "Supplier Bill");
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addObject("displayType", "Purchase Order");
		}else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addObject("displayType", "Debit Note");
		}
		
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<PurchaseVo> purchaseRequestDatatable(@Valid DataTablesInput input, @PathVariable(value="type") String type, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<PurchaseVo>  specification =new Specification<PurchaseVo>() {
			
			@Override
			public Predicate toPredicate(Root<PurchaseVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				predicates.add(criteriaBuilder.equal(root.get("type"),type));
				try {
					predicates.add(criteriaBuilder.between(root.get("purchaseDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<PurchaseVo> dataTablesOutput=purchaseService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setPurchaseItemVos(null);
			p.getContactVo().setContactOtherVos(null);
			p.getContactVo().setContactAddressVos(null);
			p.setContactAgentVo(null);
			p.setContactTransportVo(null);
		});
		return dataTablesOutput;
	}
	
	@GetMapping("/new")
	public ModelAndView newPurchase(HttpSession session, @PathVariable(value="type")String type, @Param(value = "contactId") String contactId) {
		
		ModelAndView view = new ModelAndView("purchase/purchase-new");
		
		view.addObject("type",type);
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addObject("displayType", "Supplier Bill");
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addObject("displayType", "Purchase Order");
		}else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addObject("displayType", "Debit Note");
		}
		
		long newPurchaseNo = purchaseService.findMaxPurchaseNo(
				Long.parseLong(session.getAttribute("companyId").toString()),
				Long.parseLong(session.getAttribute("branchId").toString()),
				Long.parseLong(session.getAttribute("userId").toString()), type, "BIL");
		
		view.addObject("purchaseNo", newPurchaseNo);
		
		view.addObject("prefix",prefixService.findByPrefixTypeAndBranchId(type, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		view.addObject("contactAgentVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		view.addObject("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		view.addObject("productVos", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("termsAndConditionVos", termsAndConditionService.findByCompanyIdAndIsDefaultAndModules(Long.parseLong(session.getAttribute("companyId").toString()), 1, type));
		view.addObject("paymentTermVos", paymentTermService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@PostMapping("purchaseno/verify")
	@ResponseBody
	public String verifyPurchaseNo(@PathVariable(value="type")String type, @RequestParam Map<String,String> allRequestParams, HttpSession session) {
		boolean isExist = false;
		if(allRequestParams.get("purchaseId") == null) {
			isExist = purchaseService.isExistPurchaesNo(Long.parseLong(session.getAttribute("branchId").toString()), type, allRequestParams.get("prefix"), Long.parseLong(allRequestParams.get("purchaseNo")));
		} else {
			isExist = purchaseService.isExistPurchaesNo(Long.parseLong(session.getAttribute("branchId").toString()), type, allRequestParams.get("prefix"), Long.parseLong(allRequestParams.get("purchaseNo")), Long.parseLong(allRequestParams.get("purchaseId")));
		}
		return "{ \"valid\": "+isExist+" }";
	}
	
	@PostMapping("/save")
	public String savePurchase(@RequestParam Map<String,String> allRequestParams, @PathVariable(value="type") String type, @ModelAttribute("purchaseVo") PurchaseVo purchaseVo,HttpSession session) {
		
		ContactAddressVo contactAddressVo;
		
		purchaseVo.setType(type);
		purchaseVo.setStatus("open");
		purchaseVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		purchaseVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		purchaseVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		purchaseVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		PurchaseVo purchaseVo2 = null;
		
		if(allRequestParams.get("billingAddressId").equals("0") || allRequestParams.get("shippingAddressId").equals("0")) {
			purchaseVo2 = purchaseService.findByPurchaseIdAndBranchId(purchaseVo.getPurchaseId(), purchaseVo.getBranchId());
		}
		
		//--------------Set Billing Address Details ------------------------
		if(!allRequestParams.get("billingAddressId").equals("0")) {
			contactAddressVo = contactService.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("billingAddressId")));
			
			purchaseVo.setBillingAddressLine1(contactAddressVo.getAddressLine1());
			purchaseVo.setBillingAddressLine2(contactAddressVo.getAddressLine2());
			purchaseVo.setBillingCityCode(contactAddressVo.getCityCode());
			purchaseVo.setBillingCompanyName(contactAddressVo.getCompanyName());
			purchaseVo.setBillingCountriesCode(contactAddressVo.getCountriesCode());
			purchaseVo.setBillingPinCode(contactAddressVo.getPinCode());
			purchaseVo.setBillingStateCode(contactAddressVo.getStateCode());
		} else if (purchaseVo2 != null){
			purchaseVo.setBillingAddressLine1(purchaseVo2.getBillingAddressLine1());
			purchaseVo.setBillingAddressLine2(purchaseVo2.getBillingAddressLine2());
			purchaseVo.setBillingCityCode(purchaseVo2.getBillingCityCode());
			purchaseVo.setBillingCompanyName(purchaseVo2.getBillingCompanyName());
			purchaseVo.setBillingCountriesCode(purchaseVo2.getBillingCountriesCode());
			purchaseVo.setBillingPinCode(purchaseVo2.getBillingPinCode());
			purchaseVo.setBillingStateCode(purchaseVo2.getBillingStateCode());
		}
		
		
		//--------------Set Shipping Address Details ------------------------
		if(!allRequestParams.get("shippingAddressId").equals("0")) {
			contactAddressVo = contactService.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("shippingAddressId")));
			
			purchaseVo.setShippingAddressLine1(contactAddressVo.getAddressLine1());
			purchaseVo.setShippingAddressLine2(contactAddressVo.getAddressLine2());
			purchaseVo.setShippingCityCode(contactAddressVo.getCityCode());
			purchaseVo.setShippingCompanyName(contactAddressVo.getCompanyName());
			purchaseVo.setShippingCountriesCode(contactAddressVo.getCountriesCode());
			purchaseVo.setShippingPinCode(contactAddressVo.getPinCode());
			purchaseVo.setShippingStateCode(contactAddressVo.getStateCode());
		} else if (purchaseVo2 != null){
			purchaseVo.setShippingAddressLine1(purchaseVo2.getShippingAddressLine1());
			purchaseVo.setShippingAddressLine2(purchaseVo2.getShippingAddressLine2());
			purchaseVo.setShippingCityCode(purchaseVo2.getShippingCityCode());
			purchaseVo.setShippingCompanyName(purchaseVo2.getShippingCompanyName());
			purchaseVo.setShippingCountriesCode(purchaseVo2.getShippingCountriesCode());
			purchaseVo.setShippingPinCode(purchaseVo2.getShippingPinCode());
			purchaseVo.setShippingStateCode(purchaseVo2.getShippingStateCode());
		}
		
		if(purchaseVo.getPurchaseId() == 0) {
			purchaseVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			purchaseVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			purchaseVo.setPaidAmount(0.0);
		}
		
		if(purchaseVo.getPurchaseAdditionalChargeVos() != null) { 
			purchaseVo.getPurchaseAdditionalChargeVos().removeIf( rm -> rm.getAdditionalChargeVo() == null);
			purchaseVo.getPurchaseAdditionalChargeVos().forEach(item1 -> item1.setPurchaseVo(purchaseVo));
		}
		
		if(purchaseVo.getPurchaseItemVos() != null) {
			purchaseVo.getPurchaseItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			purchaseVo.getPurchaseItemVos().forEach( item -> item.setPurchaseVo(purchaseVo));
		}
		
		if(allRequestParams.get("deletePurchaseItemIds") != null &&
				!allRequestParams.get("deletePurchaseItemIds").equals("")) {
			String address=allRequestParams.get("deletePurchaseItemIds").substring(0, allRequestParams.get("deletePurchaseItemIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			purchaseService.deletePurchaseItemByIdIn(l);
		}
		
		if(allRequestParams.get("deleteAdditionalChargeIds") != null &&
				!allRequestParams.get("deleteAdditionalChargeIds").equals("")) {
			
			String address=allRequestParams.get("deleteAdditionalChargeIds").substring(0, allRequestParams.get("deleteAdditionalChargeIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			purchaseService.deletePurchaseAdditionalChargeByIdIn(l);
		}
		
		PurchaseVo purchaseVo3 = purchaseService.save(purchaseVo);
		
		return "redirect:/purchase/"+type+"/"+purchaseVo.getPurchaseId();
	}
	
	@GetMapping("{id}")
	public ModelAndView purchaseDetails(@PathVariable String type, @PathVariable long id, HttpSession session) {
		
		ModelAndView view = new ModelAndView("purchase/purchase-view");
		
		view.addObject("type",type);
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addObject("displayType", "Supplier Bill");
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addObject("displayType", "Purchase Order");
		}else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addObject("displayType", "Debit Note");
		}
		
		
		PurchaseVo purchaseVo=purchaseService.findByPurchaseIdAndBranchId(id, Long.parseLong(session.getAttribute("branchId").toString()));
		
		purchaseVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getShippingCountriesCode()));
		purchaseVo.setShippingStateName(locationService.findStateNameByStateCode(purchaseVo.getShippingStateCode()));
		purchaseVo.setShippingCityName(locationService.findCityNameByCityCode(purchaseVo.getShippingCityCode()));
		
		purchaseVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getBillingCountriesCode()));
		purchaseVo.setBillingStateName(locationService.findStateNameByStateCode(purchaseVo.getBillingStateCode()));
		purchaseVo.setBillingCityName(locationService.findCityNameByCityCode(purchaseVo.getBillingCityCode()));
		
		view.addObject("productIds", purchaseVo.getPurchaseItemVos().stream().map(p -> p.getProductVariantVo().getProductVo().getProductId()).collect(Collectors.toSet()));
		
		view.addObject("purchaseVo", purchaseVo);
		
		if(!purchaseVo.getTermsAndConditionIds().equals("")) {
			List<Long> termandconditionIds = (List<Long>) Arrays.asList(purchaseVo.getTermsAndConditionIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());;
		
			view.addObject("termsAndConditionVos", termsAndConditionService.findByTermsandConditionIdIn(termandconditionIds));
		}
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView editPurchase(@PathVariable(value="id") long purchaseId, @PathVariable(value="type")String type, HttpSession session) {
		
		ModelAndView view = new ModelAndView("purchase/purchase-edit");
		
		view.addObject("type",type);
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addObject("displayType", "Supplier Bill");
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addObject("displayType", "Purchase Order");
		}else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addObject("displayType", "Debit Note");
		}
		
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		view.addObject("contactAgentVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		view.addObject("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		view.addObject("productVos", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("termsAndConditionVos", termsAndConditionService.findByCompanyIdAndIsDefaultAndModules(Long.parseLong(session.getAttribute("companyId").toString()), 1, "invoice"));
		view.addObject("paymentTermVos", paymentTermService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		PurchaseVo purchaseVo=purchaseService.findByPurchaseIdAndBranchId(purchaseId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		purchaseVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getShippingCountriesCode()));
		purchaseVo.setShippingStateName(locationService.findStateNameByStateCode(purchaseVo.getShippingStateCode()));
		purchaseVo.setShippingCityName(locationService.findCityNameByCityCode(purchaseVo.getShippingCityCode()));
		
		purchaseVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getBillingCountriesCode()));
		purchaseVo.setBillingStateName(locationService.findStateNameByStateCode(purchaseVo.getBillingStateCode()));
		purchaseVo.setBillingCityName(locationService.findCityNameByCityCode(purchaseVo.getBillingCityCode()));
		
		view.addObject("productIds", purchaseVo.getPurchaseItemVos().stream().map(p -> p.getProductVariantVo().getProductVo().getProductId()).collect(Collectors.toSet()));
		
		view.addObject("purchaseVo", purchaseVo);
		
		if(purchaseVo.getPurchaseAdditionalChargeVos().size() > 0) {
			view.addObject("additionalChargeVos", additionalChargeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		}
		
		if(!purchaseVo.getTermsAndConditionIds().equals("")) {
			List<Long> termandconditionIds = (List<Long>) Arrays.asList(purchaseVo.getTermsAndConditionIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());;
		
			view.addObject("termsAndConditionVos", termsAndConditionService.findByTermsandConditionIdIn(termandconditionIds));
		}
		
		return view;
	}
	
	@PostMapping("{id}/pending/json")
	@ResponseBody
	public List<PurchaseVo> purchasePendingListJson(@PathVariable("type") String type, @PathVariable("id") long contactId, HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List<PurchaseVo> purchasevos=purchaseService.findByTypeAndBranchIdAndContactIdAndPurchaseDateBetween(type, 
				Long.parseLong(session.getAttribute("branchId").toString()),
				contactId,
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
		
		purchasevos.forEach(p->p.setContactVo(null));
		purchasevos.forEach(pr->pr.setPurchaseItemVos(null));
		purchasevos.forEach(prc->prc.setPurchaseAdditionalChargeVos(null));
		
		return purchasevos;
	}
	
	@PostMapping("{id}/json")
	@ResponseBody
	public PurchaseVo purchaseDetailsJson(@PathVariable long id, HttpSession session) throws NumberFormatException, ParseException {
		PurchaseVo purchaseVo=purchaseService.findByPurchaseIdAndBranchId(id,Long.parseLong(session.getAttribute("branchId").toString()));
		purchaseVo.getContactVo().getContactAddressVos().forEach(ad->ad.setContactVo(null));
		purchaseVo.setPurchaseVo(null);
		purchaseVo.setPurchaseItemVos(null);
		purchaseVo.setPurchaseAdditionalChargeVos(null);
		return purchaseVo;
	}
}
