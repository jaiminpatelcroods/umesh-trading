package com.croods.umeshtrading.controller.purchaserequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.service.purchaserequest.PurchaseRequestService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.journalvoucher.JournalVoucherVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.purchaserequest.PurchaseRequestVo;

@Controller
@RequestMapping("purchaserequest")
public class PurchaseRequestController {

	@Autowired
	ProductService productService;
	
	@Autowired
	PurchaseRequestService purchaseRequestService;
	
	@Autowired
	PrefixService prefixService;
	
	
	@GetMapping("")
	public ModelAndView purchaseRequest(HttpSession session) {
		ModelAndView view = new ModelAndView("purchaserequest/purchaserequest");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<PurchaseRequestVo> purchaseRequestDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<PurchaseRequestVo>  specification =new Specification<PurchaseRequestVo>() {
			
			@Override
			public Predicate toPredicate(Root<PurchaseRequestVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				
				try {
					predicates.add(criteriaBuilder.between(root.get("requestDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<PurchaseRequestVo> dataTablesOutput=purchaseRequestService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> p.setPurchaseRequestItemVos(null));
		return dataTablesOutput;
	}
	
	@GetMapping("new")
	public ModelAndView purchaseRequestNew(HttpSession session) {
		ModelAndView view = new ModelAndView("purchaserequest/purchaserequest-new");
		
		view.addObject("productVos", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		view.addObject("requestNo",  purchaseRequestService.findMaxRequestNo(Constant.PURCHASE_REQUEST, 
				Long.parseLong(session.getAttribute("companyId").toString()), 
				Long.parseLong(session.getAttribute("branchId").toString()), 
				Long.parseLong(session.getAttribute("userId").toString()), "PRQ"));
		
		view.addObject("prefix",prefixService.findByPrefixTypeAndBranchId(Constant.PURCHASE_REQUEST,
				Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		return view;
	}
	
	@PostMapping("save")
	public String purchaseRequestSave(@RequestParam Map<String,String> allRequestParams, HttpSession session, @ModelAttribute("purchaseRequestVo") PurchaseRequestVo purchaseRequestVo) throws NumberFormatException, ParseException {
		
		purchaseRequestVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		purchaseRequestVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		purchaseRequestVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		purchaseRequestVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		if(purchaseRequestVo.getPurchaseRequestId() == 0) {
			purchaseRequestVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			purchaseRequestVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		}
		
		if(purchaseRequestVo.getPurchaseRequestItemVos().size() != 0) {
			purchaseRequestVo.getPurchaseRequestItemVos().removeIf(p -> p.getProductVariantVo() == null);
			purchaseRequestVo.getPurchaseRequestItemVos().forEach(p -> p.setPurchaseRequestVo(purchaseRequestVo));
		}
		
		purchaseRequestService.save(purchaseRequestVo);
		
		if(allRequestParams.get("deletePurchaseRequestItemIds") != null &&
				!allRequestParams.get("deletePurchaseRequestItemIds").equals("")) {
			String address=allRequestParams.get("deletePurchaseRequestItemIds").substring(0, allRequestParams.get("deletePurchaseRequestItemIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			purchaseRequestService.deletePurchaseRequestItemByIdIn(l);
		}
		
		return "redirect:/purchaserequest/"+purchaseRequestVo.getPurchaseRequestId();
	}
	
	@GetMapping("{id}")
	public ModelAndView purchaseRequestView(@PathVariable("id") long purchaseRequestId, HttpSession session) {
		ModelAndView view = new ModelAndView("purchaserequest/purchaserequest-view");
		
		PurchaseRequestVo purchaseRequestVo = purchaseRequestService.findByPurchaseRequestIdAndBranchId( purchaseRequestId,Long.parseLong(session.getAttribute("branchId").toString()));
		
		view.addObject("purchaseRequestVo", purchaseRequestVo);
		view.addObject("productIds", purchaseRequestVo.getPurchaseRequestItemVos().stream().map(p -> p.getProductVariantVo().getProductVo().getProductId()).collect(Collectors.toSet()));
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView purchaseRequestEdit(@PathVariable("id") long purchaseRequestId, HttpSession session) {
		ModelAndView view = new ModelAndView("purchaserequest/purchaserequest-edit");
		
		PurchaseRequestVo purchaseRequestVo = purchaseRequestService.findByPurchaseRequestIdAndBranchId( purchaseRequestId,Long.parseLong(session.getAttribute("branchId").toString()));
		view.addObject("productVos", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("purchaseRequestVo", purchaseRequestVo);
		view.addObject("productIds", purchaseRequestVo.getPurchaseRequestItemVos().stream().map(p -> p.getProductVariantVo().getProductVo().getProductId()).collect(Collectors.toSet()));
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String purchaseRequestDelete(@PathVariable("id") long purchaseRequestId, HttpSession session) {
		
		purchaseRequestService.delete(purchaseRequestId, Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
		
		return "redirect:/purchaserequest";
	}
}
