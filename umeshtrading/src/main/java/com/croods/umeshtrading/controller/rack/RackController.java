package com.croods.umeshtrading.controller.rack;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.floor.FloorService;
import com.croods.umeshtrading.service.place.PlaceService;
import com.croods.umeshtrading.service.rack.RackService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.floor.FloorVo;
import com.croods.umeshtrading.vo.rack.RackVo;

@Controller
@RequestMapping("/rack")
public class RackController {

	@Autowired
	PlaceService placeService;
	
	@Autowired
	FloorService floorService;
	
	@Autowired
	RackService rackService;
	
	@GetMapping("")
	public ModelAndView rack(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("rack/rack");
		view.addObject("placeList", placeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("floorList", floorService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("rackVos",rackService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public RackVo saveRack(HttpSession session,@ModelAttribute("rackVo") RackVo rackVo)
 	{
		rackVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		rackVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		rackVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		rackVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		rackVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		rackVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		rackService.save(rackVo);
		return rackVo;
	}
	
	@PostMapping("update")
	@ResponseBody
	public RackVo updateRack(HttpSession session,@ModelAttribute("rackVo") RackVo rackVo)
	{
		rackVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		rackVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		rackService.save(rackVo);
		return rackVo;
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteRack(@RequestParam(value="id") long id, HttpSession session)
	{
		rackService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "Sucess";
	}
	
	@GetMapping("/floor/{id}")
	@ResponseBody
	public List<RackVo> floorList(@PathVariable long id) {
		
		return rackService.findByFloorVoFloorId(id);
	}
}
