package com.croods.umeshtrading.controller.report;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.util.JasperExporter;

@Controller
@RequestMapping("/report")
public class ReportController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	ProductService productService;
	
	HashMap<String, Comparable> jasperParameter;
	
	@GetMapping("")
	public ModelAndView reports() {
		ModelAndView view=new ModelAndView("report/report");
		return view;
	}
	
	
	
	@RequestMapping("/productwiseledger")
	public ModelAndView companyProductLedger(HttpSession session) throws NumberFormatException, ParseException {

		ModelAndView view=new ModelAndView("/report/product/company-product-ledger");
		view.addObject("ProductList", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	@PostMapping("/productwiseledgerReport")// Product Wise PDF/view
	public void companyProductLedgerReport(HttpSession session, @RequestParam Map<String,String> allRequestParams, HttpServletRequest request, HttpServletResponse response) throws ParseException {
		
		//SalesVo salesVo=salesService.findBySalesIdAndBranchId(id, Long.parseLong(session.getAttribute("branchId").toString()));
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String types = "";
	
	    java.sql.Date fromDate = new java.sql.Date((dateFormat.parse(request.getParameter("fromDate"))).getTime());
		java.sql.Date toDate = new java.sql.Date((dateFormat.parse(request.getParameter("toDate"))).getTime());
		java.sql.Date start_Date = new java.sql.Date((dateFormat.parse(request.getSession().getAttribute("firstDateFinancialYear").toString())).getTime());
		
		jasperParameter = new HashMap<String, Comparable>();
		jasperParameter.put("from_date",fromDate);
		jasperParameter.put("to_date",toDate);
		jasperParameter.put("contact_id",0L);
		jasperParameter.put("company_id", Long.parseLong(session.getAttribute("companyId").toString()));
		jasperParameter.put("product_id",Long.parseLong(allRequestParams.get("productId")));
		jasperParameter.put("year_start_date", start_Date);

		JasperExporter jasperExporter = new JasperExporter(); 
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "umesh_report" + System.getProperty("file.separator"));
		try {			
					
				if(request.getParameter("formType").equals("view")){
					System.out.println("IN HTML METHOD");
					jasperExporter.jasperExporterHTML(jasperParameter,
							request.getServletContext().getRealPath("/") + "umesh_report"
									+ System.getProperty("file.separator") +"/product_ledger/company_product_ledger.jrxml","product_ledger", response);					
				}else{
					System.out.println("IN PDF METHOD");
					jasperExporter.jasperExporterPDF(jasperParameter,
							request.getServletContext().getRealPath("/") + "umesh_report"
									+ System.getProperty("file.separator") +"/product_ledger/company_product_ledger.jrxml","product_ledger", response);					
				}
		
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	
	@RequestMapping("/customerproductledger")
	public ModelAndView customerProductLedger(HttpSession session) throws NumberFormatException, ParseException {

		ModelAndView view=new ModelAndView("/report/product/customer-product-ledger");
		view.addObject("ContactList", contactService.contactList(Long.parseLong(session.getAttribute("companyId").toString()), Constant.CONTACT_CUSTOMER));
		view.addObject("ProductList", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	@PostMapping("customerproductledgerReport")//sales By Product PDF/view
	public void customerProductLedgerReport(HttpSession session, @RequestParam Map<String,String> allRequestParams, HttpServletRequest request, HttpServletResponse response) throws ParseException {
		
		//SalesVo salesVo=salesService.findBySalesIdAndBranchId(id, Long.parseLong(session.getAttribute("branchId").toString()));
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String types = "";
	
	    java.sql.Date fromDate = new java.sql.Date((dateFormat.parse(request.getParameter("fromDate"))).getTime());
		java.sql.Date toDate = new java.sql.Date((dateFormat.parse(request.getParameter("toDate"))).getTime());
		java.sql.Date start_Date = new java.sql.Date((dateFormat.parse(request.getSession().getAttribute("firstDateFinancialYear").toString())).getTime());
		
		jasperParameter = new HashMap<String, Comparable>();
		jasperParameter.put("from_date",fromDate);
		jasperParameter.put("to_date",toDate);
		jasperParameter.put("contact_id",Long.parseLong(allRequestParams.get("contactId")));
		jasperParameter.put("company_id", Long.parseLong(session.getAttribute("companyId").toString()));
		jasperParameter.put("product_id",Long.parseLong(allRequestParams.get("productId")));
		jasperParameter.put("year_start_date", start_Date);

		JasperExporter jasperExporter = new JasperExporter(); 
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "umesh_report" + System.getProperty("file.separator"));
		try {			
					
				if(request.getParameter("formType").equals("view")){
					System.out.println("IN HTML METHOD");
					jasperExporter.jasperExporterHTML(jasperParameter,
							request.getServletContext().getRealPath("/") + "umesh_report"
									+ System.getProperty("file.separator") +"/product_ledger/company_product_ledger.jrxml","productwise_product_ledger", response);					
				}else if(request.getParameter("formType").equals("pdf")){
					System.out.println("IN PDF METHOD");
					jasperExporter.jasperExporterPDF(jasperParameter,
							request.getServletContext().getRealPath("/") + "umesh_report"
									+ System.getProperty("file.separator") +"/product_ledger/company_product_ledger.jrxml","productwise_product_ledger", response);					
				}else if(request.getParameter("formType").equals("excel")){
					System.out.println("IN EXCEL METHOD");
					jasperExporter.jasperExporterEXCEL(jasperParameter,
							request.getServletContext().getRealPath("/") + "umesh_report"
									+ System.getProperty("file.separator") +"/product_ledger/company_product_ledger.jrxml", "productwise_product_ledger", response);
				}
		
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
