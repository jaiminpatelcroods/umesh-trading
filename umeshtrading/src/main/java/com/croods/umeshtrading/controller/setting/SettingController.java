package com.croods.umeshtrading.controller.setting;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.financial.FinancialService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.userfront.UserFrontService;
import com.croods.umeshtrading.vo.userfront.UserFrontVo;

@Controller
@RequestMapping("setting")
public class SettingController {

	@Autowired
	FinancialService financialService;
	
	@Autowired
	UserFrontService userFrontService;
	
	@Autowired
	LocationService locationService;
	
	@GetMapping("")
	public ModelAndView setting(HttpSession session) {
		ModelAndView view = new ModelAndView("setting/setting");
		view.addObject("financialYearVos", financialService.findAllFinancialYear());
		UserFrontVo userFrontVo = userFrontService.findByUserFrontId(Long.parseLong(session.getAttribute("userId").toString()));
		
		try {
			userFrontVo.setCountriesName(locationService.findCountriesNameByCountriesCode(userFrontVo.getCountriesCode()));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			userFrontVo.setStateName(locationService.findStateNameByStateCode(userFrontVo.getStateCode()));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println(userFrontVo.getCityCode());
			System.out.println(locationService.findCityNameByCityCode(locationService.findCityNameByCityCode(userFrontVo.getCityCode())));
			userFrontVo.setCityName(locationService.findCityNameByCityCode(locationService.findCityNameByCityCode(userFrontVo.getCityCode())));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		view.addObject("userFrontVo", userFrontVo);
		return view;
	}
}
