package com.croods.umeshtrading.controller.stock;

import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.repository.contact.ContactRepository;
import com.croods.umeshtrading.repository.material.MaterialRepository;
import com.croods.umeshtrading.repository.stock.StockRepository;
import com.croods.umeshtrading.service.contact.ContactService;
import com.croods.umeshtrading.service.floor.FloorService;
import com.croods.umeshtrading.service.location.LocationService;
import com.croods.umeshtrading.service.material.MaterialService;
import com.croods.umeshtrading.service.place.PlaceService;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.product.ProductService;
import com.croods.umeshtrading.service.rack.RackService;
import com.croods.umeshtrading.service.stock.StockService;
import com.croods.umeshtrading.service.tax.TaxService;
import com.croods.umeshtrading.vo.floor.FloorVo;
import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.stock.StockVo;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Controller
@RequestMapping("/stock")
public class StockController {

	@Autowired
	ContactService contactService;

	@Autowired
	ProductService productService;

	@Autowired
	MaterialService materialService;

	@Autowired
	PrefixService prefixService;

	/*@Autowired
	PaymentTermService paymentTermService;*/

	@Autowired
	LocationService locationService;

	/*@Autowired
	AdditionalChargeService additionalChargeService;*/

	/*@Autowired
	TermsAndConditionService termsAndConditionService;*/

	@Autowired
	MaterialRepository materialRepository;

	@Autowired
	ContactRepository contactRepository;

	@Autowired
	TaxService taxService;
	
	@Autowired
	StockService stockService;

	@Autowired
	PlaceService placeService;
	
	@Autowired
	FloorService floorService;
	
	@Autowired
	RackService rackService;
	
	@Autowired
	StockRepository stockRepository;
	
	StockVo stockVo;
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	HashMap jasperParameter;

	@GetMapping("")
	public ModelAndView stockList(HttpSession session) 
	{
		ModelAndView view = new ModelAndView("stock/stock");
		view.addObject("displayType","Stock");
		view.addObject("productList", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("placeList", placeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("floorList", floorService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		return view;
	}

	@PostMapping("/datatable")
	@ResponseBody
	public DataTablesOutput<StockVo> stockDatatable(@Valid DataTablesInput input, @RequestParam Map<String, String> allRequestParams, HttpSession session)throws NumberFormatException, ParseException 
	{
		long companyId = Long.parseLong(session.getAttribute("companyId").toString());
		System.out.println("At Datattttttttttttable");
		System.out.println(allRequestParams.get("productId"));
		System.out.println(allRequestParams.get("placeId"));
		System.out.println(allRequestParams.get("floorId"));
		input.getColumns().forEach(q -> System.out.println(q.getData()));
		
		Specification<StockVo> specification = new Specification<StockVo>() 
		{
			@Override
			public Predicate toPredicate(Root<StockVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) 
			{
				List<Predicate> predicates = new ArrayList<Predicate>();
				
				predicates.add(criteriaBuilder.equal(root.get("companyId"), companyId));
				predicates.add(criteriaBuilder.equal(root.get("productVo").get("isDeleted"), 0));
				
				if (!allRequestParams.get("productId").equals("0")) 
				{
					ProductVo productVo = new ProductVo();
					productVo.setProductId(Long.parseLong(allRequestParams.get("productId")));
					predicates.add(criteriaBuilder.equal(root.get("productVo"), productVo));
				}
				if (!allRequestParams.get("placeId").equals("0")) 
				{
					PlaceVo placeVo = new PlaceVo();
					placeVo.setPlaceId(Long.parseLong(allRequestParams.get("placeId")));
					predicates.add(criteriaBuilder.equal(root.get("placeVo"), placeVo));
				}
				if (!allRequestParams.get("floorId").equals("0")) 
				{
					FloorVo floorVo = new FloorVo();
					floorVo.setFloorId(Long.parseLong(allRequestParams.get("floorId")));
					predicates.add(criteriaBuilder.equal(root.get("floorVo"), floorVo));
				}
				
				query.orderBy(criteriaBuilder.desc(root.get("productVo")));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};

		DataTablesOutput<StockVo> a = stockRepository.findAll(input, null, specification);
		
		a.getData().forEach(x -> {
			if (x.getProductVo() != null) 
			{
				x.getProductVo().setBrandVo(null);
				x.getProductVo().setCategoryVo(null);
				x.getProductVo().setUnitOfMeasurementVo(null);
				x.getProductVo().setPurchaseTaxVo(null);
				x.getProductVo().setSalesTaxVo(null);
			}
			
		});
		return a;
	}

	/*@GetMapping("/new")
	public ModelAndView newMaterial(HttpSession session, @PathVariable(value = "type") String type,@Param(value = "contactId") String contactId) 
	{
		ModelAndView view = new ModelAndView("material/material-new");
		String defaultPrefix = "";
		if (type.equals(Constant.MATERIAL_OUT)) 
		{
			view.addObject("displayType", "Sales");
			view.addObject("ContactList", contactService
					.contactList(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
			defaultPrefix = "INV";
		}
		else if(type.equals(Constant.MATERIAL_IN)) 
		{
			view.addObject("displayType", "Material In");
			view.addObject("ContactList", contactService
					.contactList(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
			defaultPrefix = "MTIN";
		}
		view.addObject("type", type);
		long newMaterialNo = materialService.findMaxMaterialNo(
				Long.parseLong(session.getAttribute("branchId").toString()), type, defaultPrefix,
				Long.parseLong(session.getAttribute("userId").toString()));
		view.addObject("NewMaterialNo", newMaterialNo);
		view.addObject("materialPrefix", prefixService
				.findByPrefixTypeAndBranchId(type,Long.parseLong(session.getAttribute("branchId").toString())).get(0)
				.getPrefix());
		view.addObject("ProductList", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("PlaceList", palceService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("taxList", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		if (contactId != null) {
			view.addObject("contactId", contactId);
		}
		return view;
	}
	
	@PostMapping("/create")
	public String insertMaterial(@RequestParam Map<String, String> allRequestParams, @PathVariable(value = "type") String type, 
			@ModelAttribute("materialVo") MaterialVo materialVo, HttpSession session) {

		ContactAddressVo contactAddressVo;

		materialVo.setType(type);
		materialVo.setStatus("open");
		materialVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		materialVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		materialVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		try {
			materialVo.setMaterialDate(dateFormat.parse(allRequestParams.get("materialDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		MaterialVo materialVo2 = null;
		if (allRequestParams.get("billingAddressId").equals("0")
				|| allRequestParams.get("shippingAddressId").equals("0")) {
			materialVo2 = materialService.findByMaterialIdAndCompanyId(materialVo.getMaterialId(), materialVo.getCompanyId());
		}

		// --------------Set Billing Address Details ------------------------
		if (!allRequestParams.get("billingAddressId").equals("0")) {
			contactAddressVo = contactService.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("billingAddressId")));

			materialVo.setBillingAddressLine1(contactAddressVo.getAddressLine1());
			materialVo.setBillingAddressLine2(contactAddressVo.getAddressLine2());
			materialVo.setBillingCityCode(contactAddressVo.getCityCode());
			materialVo.setBillingCompanyName(contactAddressVo.getCompanyName());
			materialVo.setBillingCountriesCode(contactAddressVo.getCountriesCode());
			materialVo.setBillingName(contactAddressVo.getCompanyName());
			materialVo.setBillingPinCode(contactAddressVo.getPinCode());
			materialVo.setBillingStateCode(contactAddressVo.getStateCode());
		} else if (materialVo2 != null) {
			materialVo.setBillingAddressLine1(materialVo2.getBillingAddressLine1());
			materialVo.setBillingAddressLine2(materialVo2.getBillingAddressLine2());
			materialVo.setBillingCityCode(materialVo2.getBillingCityCode());
			materialVo.setBillingCompanyName(materialVo2.getBillingCompanyName());
			materialVo.setBillingCountriesCode(materialVo2.getBillingCountriesCode());
			materialVo.setBillingName(materialVo2.getBillingName());
			materialVo.setBillingPinCode(materialVo2.getBillingPinCode());
			materialVo.setBillingStateCode(materialVo2.getBillingStateCode());
		}

		// --------------Set Shipping Address Details ------------------------
		if (!allRequestParams.get("shippingAddressId").equals("0")) {
			contactAddressVo = contactService
					.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("shippingAddressId")));

			materialVo.setShippingAddressLine1(contactAddressVo.getAddressLine1());
			materialVo.setShippingAddressLine2(contactAddressVo.getAddressLine2());
			materialVo.setShippingCityCode(contactAddressVo.getCityCode());
			materialVo.setShippingCompanyName(contactAddressVo.getCompanyName());
			materialVo.setShippingCountriesCode(contactAddressVo.getCountriesCode());
			materialVo.setShippingName(contactAddressVo.getCompanyName());
			materialVo.setShippingPinCode(contactAddressVo.getPinCode());
			materialVo.setShippingStateCode(contactAddressVo.getStateCode());
		} else if (materialVo2 != null) {
			materialVo.setShippingAddressLine1(materialVo2.getShippingAddressLine1());
			materialVo.setShippingAddressLine2(materialVo2.getShippingAddressLine2());
			materialVo.setShippingCityCode(materialVo2.getShippingCityCode());
			materialVo.setShippingCompanyName(materialVo2.getShippingCompanyName());
			materialVo.setShippingCountriesCode(materialVo2.getShippingCountriesCode());
			materialVo.setShippingName(materialVo2.getShippingName());
			materialVo.setShippingPinCode(materialVo2.getShippingPinCode());
			materialVo.setShippingStateCode(materialVo2.getShippingStateCode());
		}

		if (materialVo.getMaterialId() == 0) {
			materialVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			materialVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			materialVo.setPaidAmount(0.0);
		}

		System.err.println("BEFORE MATERIAL ITEM LOOP");
		
		if (materialVo.getMaterialItemVos() != null) {
			materialVo.getMaterialItemVos().removeIf(a -> a.getProductVo() == null);
			materialVo.getMaterialItemVos().forEach(item -> item.setMaterialVo(materialVo));
			materialVo.getMaterialItemVos().forEach(item -> item.setType(type));
			
			//update Stock
			if(type.equals(Constant.MATERIAL_OUT))
			{
				materialVo.getMaterialItemVos().forEach(item -> {
					
					 if(!item.getOldProductRackQty().equals(""))
					 {
						 System.err.println("IN IF FOR getOLDPRODUCT QTY===============");
						 
						 String[] a = item.getOldProductRackQty().split(",");
						
						if(Long.parseLong(a[0]) != item.getProductVo().getProductId() || Long.parseLong(a[1]) != item.getRackVo().getRackId() || Double.parseDouble(a[2]) != item.getQty())
						{
							//check if stock exist for old product
							List<StockVo> oldStockList =  stockService.findByProductVoProductIdAndRackVoRackIdAndCompanyId(Long.parseLong(a[0]), Long.parseLong(a[1]), Long.parseLong(session.getAttribute("companyId").toString()));
							//update stock if exist for old product  +oldQty -0.0
							if(oldStockList.size()>0){
								ProductVo productVo = new ProductVo();
								productVo.setProductId(Long.parseLong(a[0]));
								RackVo rackVo = new RackVo();
								rackVo.setRackId(Long.parseLong(a[1]));
								
								stockService.updateStockWithoutContact(productVo, rackVo, 0.0, Double.parseDouble(a[2]), Long.parseLong(session.getAttribute("companyId").toString()));
							}
						}
					}
					//check if stock exist for new product
					List<StockVo> newStockList =  stockService.findByProductVoProductIdAndRackVoRackIdAndCompanyId(item.getProductVo().getProductId(), item.getRackVo().getRackId(), Long.parseLong(session.getAttribute("companyId").toString()));
					//update stock if exist for new product -newQty +0.0
					if(newStockList.size()>0){
						stockService.updateStockWithoutContact(item.getProductVo(), item.getRackVo(), item.getQty(), 0.0, Long.parseLong(session.getAttribute("companyId").toString()));
					}else{//insert if not
						stockVo = new StockVo();
						//stockVo.setBatchNo(item.getBatchNo() == null? "": item.getBatchNo());
						stockVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
						stockVo.setProductVo(item.getProductVo());
						stockVo.setPlaceVo(item.getPlaceVo());
						stockVo.setFloorVo(item.getFloorVo());
						stockVo.setRackVo(item.getRackVo());
						//stockVo.setContactVo(materialVo.getContactVo());
						stockVo.setQuantity(Double.parseDouble("-"+item.getQty()));
						stockService.save(stockVo);
					}
						
					
				});
			}
			else if(type.equals(Constant.MATERIAL_IN))
			{
				materialVo.getMaterialItemVos().forEach(item -> {
					
					if(!item.getOldProductRackQty().equals(""))
					{
						String[] a = item.getOldProductRackQty().split(",");
						if(Long.parseLong(a[0]) != item.getProductVo().getProductId() || Long.parseLong(a[1]) != item.getRackVo().getRackId()  || Double.parseDouble(a[2]) != item.getQty())
						{
							//check if stock exist for old product
							List<StockVo> oldStockList =  stockService.findByProductVoProductIdAndRackVoRackIdAndCompanyId(Long.parseLong(a[0]), Long.parseLong(a[1]), Long.parseLong(session.getAttribute("companyId").toString()));
							//update stock if exist for old product  -oldQty +0.0
							if(oldStockList.size()>0)
							{
								ProductVo productVo = new ProductVo();
								productVo.setProductId(Long.parseLong(a[0]));
								RackVo rackVo = new RackVo();
								rackVo.setRackId(Long.parseLong(a[1]));
								
								stockService.updateStockWithoutContact(productVo, rackVo,  Double.parseDouble(a[2]), 0.0, Long.parseLong(session.getAttribute("companyId").toString()));
							}
						}
					}
					//check if stock exist for new product
					List<StockVo> newStockList =  stockService.findByProductVoProductIdAndRackVoRackIdAndCompanyId(item.getProductVo().getProductId(), item.getRackVo().getRackId(), Long.parseLong(session.getAttribute("companyId").toString()));
					//update stock if exist for new product +newQty -0.0
					if(newStockList.size()>0){
						stockService.updateStockWithoutContact(item.getProductVo(), item.getRackVo(), 0.0, item.getQty(), Long.parseLong(session.getAttribute("companyId").toString()));
					}else{//insert if not
						stockVo = new StockVo();
						//stockVo.setBatchNo(item.getBatchNo() == null? "": item.getBatchNo());
						stockVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
						stockVo.setProductVo(item.getProductVo());
						stockVo.setPlaceVo(item.getPlaceVo());
						stockVo.setFloorVo(item.getFloorVo());
						stockVo.setRackVo(item.getRackVo());
						//stockVo.setContactVo(materialVo.getContactVo());
						stockVo.setQuantity(item.getQty());
						
						System.err.println("BEFORE STOCK MASTER VO SAVE.....FOR NEW RACK AND PRODUCT...");
						
						stockService.save(stockVo);
					}
				});
				
				
			} 
			//update Stock
		}

		if (allRequestParams.get("deleteMaterialItemIds") != null
				&& !allRequestParams.get("deleteMaterialItemIds").equals("")) {
			String address = allRequestParams.get("deleteMaterialItemIds").substring(0,
					allRequestParams.get("deleteMaterialItemIds").length() - 1);
			List<Long> l = Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());

			materialService.deleteMaterialItem(materialVo.getCompanyId(), l);
		}

		System.err.println("BEFORE MATERIAL VO SAVE.....");

		materialVo2 = materialService.save(materialVo);

		//Stock Transaction
		if (materialVo2.getType().equals(Constant.MATERIAL_OUT)) {
			materialService.insertMaterialOutTransaction(materialVo2, session.getAttribute("financialYear").toString());
		} else if (materialVo2.getType().equals(Constant.MATERIAL_IN)) {
			materialService.insertMaterialInTransaction(materialVo2, session.getAttribute("financialYear").toString());
		}

		return "redirect:/material/" + type + "/" + materialVo2.getMaterialId();
	}
	
	@GetMapping("/{id}")
	public ModelAndView materialDetails(HttpSession session, @PathVariable(value = "type") String type,
			@PathVariable(value = "id") long id) {
		ModelAndView view = new ModelAndView("material/material-view");
		view.addObject("type", type);

		if (type.equals(Constant.MATERIAL_OUT)) {
			view.addObject("displayType", "Sales");
		}else if (type.equals(Constant.MATERIAL_IN)) {
			view.addObject("displayType", "Material In");
		}
		MaterialVo materialVo = materialService.findByMaterialIdAndCompanyId(id,
				Long.parseLong(session.getAttribute("companyId").toString()));
		materialVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(materialVo.getShippingCountriesCode()));
		materialVo.setShippingStateName(locationService.findStateNameByStateCode(materialVo.getShippingStateCode()));
		materialVo.setShippingCityName(locationService.findCityNameByCityCode(materialVo.getShippingCityCode()));
		materialVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(materialVo.getBillingCountriesCode()));
		materialVo.setBillingStateName(locationService.findStateNameByStateCode(materialVo.getBillingStateCode()));
		materialVo.setBillingCityName(locationService.findCityNameByCityCode(materialVo.getBillingCityCode()));

//		if (!materialVo.getTermsAndConditionIds().equals("")) {
//			List<Long> termandconditionIds = (List<Long>) Arrays
//					.asList(materialVo.getTermsAndConditionIds().split("\\s*,\\s*")).stream().map(Long::parseLong)
//					.collect(Collectors.toList());
//			;
//			view.addObject("TermsAndCondition", termsAndConditionService.getTermAndConditionList(termandconditionIds));
//		}
		view.addObject("materialVo", materialVo);
		return view;
	}
	
	@GetMapping("/{id}/edit")
	public ModelAndView materialEdit(HttpSession session, @PathVariable(value = "type") String type,
			@PathVariable(value = "id") long id) {
		ModelAndView view = new ModelAndView("material/material-edit");
		
		view.addObject("type", type);

		if (type.equals(Constant.MATERIAL_OUT)) {
			view.addObject("displayType", "Sales");
			view.addObject("ContactList", contactService
					.contactList(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		} else if (type.equals(Constant.MATERIAL_IN)) {
			view.addObject("displayType", "Material In");
			view.addObject("ContactList", contactService
					.contactList(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		}

		MaterialVo materialVo = materialService.findByMaterialIdAndCompanyId(id,
				Long.parseLong(session.getAttribute("companyId").toString()));
		materialVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(materialVo.getShippingCountriesCode()));
		materialVo.setShippingStateName(locationService.findStateNameByStateCode(materialVo.getShippingStateCode()));
		materialVo.setShippingCityName(locationService.findCityNameByCityCode(materialVo.getShippingCityCode()));
		materialVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(materialVo.getBillingCountriesCode()));
		materialVo.setBillingStateName(locationService.findStateNameByStateCode(materialVo.getBillingStateCode()));
		materialVo.setBillingCityName(locationService.findCityNameByCityCode(materialVo.getBillingCityCode()));
		
		view.addObject("materialVo", materialVo);
		view.addObject("ProductList", productService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("PlaceList", palceService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("FloorList", floorService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("RackList", rackService.findByBranchId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("taxList", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));

		return view;
	}
	
	
	@GetMapping("{id}/delete")
	public String materialDelete(@PathVariable String type, @PathVariable long id, HttpSession session) 
	{
		
		materialService.deleteMaterial(Long.parseLong(session.getAttribute("companyId").toString()), id, type);
		MaterialVo materialVo = materialService.findByMaterialIdAndCompanyId(id, Long.parseLong(session.getAttribute("companyId").toString()));
		
		//update Stock
		if(type.equals(Constant.MATERIAL_OUT)){
			materialVo.getMaterialItemVos().forEach(item -> {
				//check if stock exist for new product
				List<StockVo> newStockList =  stockService.findByProductVoProductIdAndRackVoRackIdAndCompanyId(item.getProductVo().getProductId(), item.getRackVo().getRackId(), Long.parseLong(session.getAttribute("companyId").toString()));
				//update stock if exist for new product -newQty +0.0
				if(newStockList.size()>0){
					stockService.updateStockWithoutContact(item.getProductVo(),item.getRackVo(), 0.0,item.getQty(), Long.parseLong(session.getAttribute("companyId").toString()));
				}
			});
		}else if(type.equals(Constant.MATERIAL_IN)){
			materialVo.getMaterialItemVos().forEach(item -> {
				//check if stock exist for new product
				List<StockVo> newStockList =  stockService.findByProductVoProductIdAndRackVoRackIdAndCompanyId(item.getProductVo().getProductId(), item.getRackVo().getRackId(), Long.parseLong(session.getAttribute("companyId").toString()));
				//update stock if exist for new product +newQty -0.0
				if(newStockList.size()>0){
					stockService.updateStockWithoutContact(item.getProductVo(), item.getRackVo(), item.getQty(), 0.0, Long.parseLong(session.getAttribute("companyId").toString()));
				}
			});
		} 
		//update Stock
		
		
		return "redirect:/material/" + type;
	}*/
	
	
	/*@GetMapping("/contact/{id}/json")
	@ResponseBody
	public List<MaterialVo> materialListJSON(HttpSession session, @PathVariable(value = "type") String type,
			@PathVariable(value = "id") long contactId) {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		List<MaterialVo> materialVos = new ArrayList<MaterialVo>();

		try {

			List<String> materialTypes = new ArrayList<String>();
			materialTypes.add(Constant.JOBWORK_ORDER);

			materialVos = materialService.findByTypesAndContactAndCompanyIdAndMaterialDateBetween(materialTypes,
					Long.parseLong(session.getAttribute("companyId").toString()),
					dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
					dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), contactId);

			Collections.reverse(materialVos);

			materialVos.forEach(materialItem -> {
				materialItem.setMaterialItemVos(null);
				materialItem.setMaterialAdditionalChargeVos(null);
				materialItem.setContactVo(null);
			});
		} catch (NumberFormatException | ParseException e) {
			e.printStackTrace();
		}

		return materialVos;
	}

	

	

	

	

	@PostMapping("{id}/pending/json")
	@ResponseBody
	public List<MaterialVo> materialPendingListJson(@PathVariable String type, @PathVariable long id, HttpSession session)
			throws NumberFormatException, ParseException {
		System.out.println("->->-_>->_->_/.->>>>>>>>>>>>>>>>>>>-----materialPendingListJson");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List<MaterialVo> materialvos = materialService.getListOfAllUnpaidBill(type,
				Long.parseLong(session.getAttribute("companyId").toString()), 0,
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), id);

		materialvos.forEach(p -> p.setContactVo(null));
		materialvos.forEach(pr -> pr.setMaterialItemVos(null));

		materialvos.forEach(prc -> prc.setMaterialAdditionalChargeVos(null));

		return materialvos;
	}

	@PostMapping("{id}/json")
	@ResponseBody
	public MaterialVo materialDetailsJson(@PathVariable long id, HttpSession session)
			throws NumberFormatException, ParseException {
		MaterialVo materialVo = materialService.findByMaterialIdAndCompanyId(id,
				Long.parseLong(session.getAttribute("companyId").toString()));
		materialVo.setContactVo(null);
		materialVo.setMaterialVo(null);
		materialVo.setMaterialAdditionalChargeVos(null);

		materialVo.getMaterialItemVos().forEach(abc -> {
			abc.setMaterialVo(null);
			abc.setTaxVo(null);
			abc.getProductVo().setTaxVo(null);
			abc.getProductVo().setUnitOfMeasurementVo(null);

		});

		return materialVo;
	}

	@RequestMapping("/{contactId}/datatable")
	@ResponseBody
	public DataTablesOutput<MaterialVo> materialByContactDatatable(@PathVariable String type,
			@PathVariable long contactId, @Valid DataTablesInput input, HttpSession session)
			throws NumberFormatException, ParseException {

		ContactVo contactVo = contactRepository.findByContactId(contactId);
		long companyId = Long.parseLong(session.getAttribute("companyId").toString());

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<MaterialVo> specification = new Specification<MaterialVo>() {

			@Override
			public Predicate toPredicate(Root<MaterialVo> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("companyId"), companyId));
				predicates.add(criteriaBuilder.equal(root.get("contactVo"), contactVo));
				try {
					predicates.add(criteriaBuilder.between(root.get("materialDate"),
							dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};

		DataTablesOutput<MaterialVo> a = materialRepository.findAll(input, null, specification);
		a.getData().forEach(x -> {
			if (x.getContactVo() != null) {
				x.getContactVo().setContactAddressVos(null);

			}
			x.setMaterialItemVos(null);
		});

		return a;
	}

	@RequestMapping("/materialOrder/{materialId}/datatable")
	@ResponseBody
	public DataTablesOutput<MaterialVo> creditNoteMaterialdatatable(@PathVariable String type,
			@PathVariable long materialId, @Valid DataTablesInput input, HttpSession session)
			throws NumberFormatException, ParseException {
		System.out.println("&*&*&*&*&**&*&*&*&*&*&*&*&*&*&*&**&*&*&*&**&*&*&*&*");
		long companyId = Long.parseLong(session.getAttribute("companyId").toString());
		MaterialVo materialVo = new MaterialVo();
		materialVo.setMaterialId(materialId);

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<MaterialVo> specification = new Specification<MaterialVo>() {

			@Override
			public Predicate toPredicate(Root<MaterialVo> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("materialVo"), materialVo));
				predicates.add(criteriaBuilder.equal(root.get("companyId"), companyId));
				
				 * try { predicates.add(criteriaBuilder.between(root.get("purchaseDate"),
				 * dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				 * dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				 * } catch (ParseException e) {
				 * e.printStackTrace(); }
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};

		DataTablesOutput<MaterialVo> a = materialRepository.findAll(input, specification);

		// a.getData().forEach(v->{v.getContactVo().setContactAddressVos(null);v.setMaterialItemVos(null);v.get});
		a.getData().forEach(x -> {
			x.setMaterialVo(null);
			x.setMaterialItemVos(null);
			x.setMaterialAdditionalChargeVos(null);
			x.setContactVo(null);
		});

		return a;
	}
	
	@PostMapping("/totalMaterialOrder/json")
	@ResponseBody
	public long totalEmployee(HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		long totalOrderQty=materialService.getTotalMaterialOrderQty(Long.parseLong(session.getAttribute("companyId").toString()),
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
		
		return totalOrderQty;
	}*/

}
