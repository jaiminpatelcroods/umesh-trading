package com.croods.umeshtrading.controller.tax;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.service.account.AccountService;
import com.croods.umeshtrading.service.tax.TaxService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.tax.TaxVo;

@Controller
@RequestMapping("/tax")
public class TaxController {

	@Autowired
	TaxService taxService;
	
	@Autowired
	AccountService accountService;
	
	@GetMapping("")
	public ModelAndView tax(HttpSession session) {
		ModelAndView view = new ModelAndView("tax/tax");
		
		view.addObject("taxVos", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		return view;
	}
	
	@GetMapping("2")
	public ModelAndView tax2(HttpSession session) {
		ModelAndView view = new ModelAndView("tax/tax-frame");
		
		view.addObject("taxVos", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		return view;
	}
	@PostMapping("create")
	@ResponseBody
	public TaxVo taxCreate(@RequestParam(value="name") String name,@RequestParam(value="rate") String rate, HttpSession session) {
		
		TaxVo tax=new TaxVo();
		tax.setTaxName(name);
		tax.setTaxRate(Double.parseDouble( rate));
		tax.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		tax.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		tax.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		tax.setCreatedOn(CurrentDateTime.getCurrentDate());
		tax.setModifiedOn(CurrentDateTime.getCurrentDate());
		tax.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		AccountCustomVo accountCustomVo=new AccountCustomVo();
		accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());		
		accountCustomVo.setAccountName(name+"("+rate+"%)");
		
		AccountGroupVo accountGroupVo=new AccountGroupVo();
			
		accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_DUTIES_AND_TAXES);			
		accountCustomVo.setAccounType(Constant.TAX);
		accountCustomVo.setGroup(accountGroupVo);
		accountService.insertAccount(accountCustomVo);
		
		tax.setAccountCustomVo(accountCustomVo);
		taxService.taxSave(tax);
		
		return tax;
	}
	
	@PostMapping("/update")
	@ResponseBody
	public TaxVo updateTax(@RequestParam(value="name") String name,@RequestParam(value="rate") String rate,@RequestParam(value="id") long id, HttpSession session)
	{		
		
		TaxVo tax=new TaxVo();
		tax=taxService.findByTaxId(id);
		tax.setTaxName(name);
		tax.setTaxRate(Double.parseDouble(rate));
		tax.setModifiedOn(CurrentDateTime.getCurrentDate());
		tax.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		AccountCustomVo accountCustomVo=tax.getAccountCustomVo();
		accountCustomVo.setAccountName(name+"("+rate+"%)");
		accountCustomVo.setAccounType(Constant.TAX);
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountService.insertAccount(accountCustomVo);
		taxService.taxSave(tax);
		return tax;
	}
	
	@PostMapping("/delete")
	@ResponseBody
	public String deleteTax(@RequestParam(value="id") long id, HttpSession session)
	{		
		taxService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());		
		TaxVo tax=new TaxVo();
		tax=taxService.findByTaxId(id);
		
		AccountCustomVo accountCustomVo=tax.getAccountCustomVo();
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setIsDeleted(1);
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountService.insertAccount(accountCustomVo);
		return "Sucess";
	}
	
	@GetMapping("/taxlist")
	@ResponseBody
	public List<TaxVo> taxList(HttpSession session) {
		return taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
	}
}
