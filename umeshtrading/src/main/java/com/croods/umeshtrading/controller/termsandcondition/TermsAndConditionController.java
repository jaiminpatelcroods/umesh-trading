package com.croods.umeshtrading.controller.termsandcondition;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.umeshtrading.service.termsandcondition.TermsAndConditionService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.termsandcondition.TermsAndConditionVo;

@Controller
@RequestMapping("termsandcondition")
public class TermsAndConditionController {

	@Autowired
	TermsAndConditionService termsAndConditionService;
	
	@GetMapping("")
	public ModelAndView termsAndCondition(HttpSession session)
	{
		ModelAndView view =new ModelAndView("termsandcondition/termsandcondition");
		view.addObject("termsandConditionVos",termsAndConditionService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		return view;	
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public TermsAndConditionVo save(@RequestParam(value = "termsCondition") String termsCondition,
			@RequestParam(value = "isDefault") String isDefault,@RequestParam(value = "modules") String modules, HttpSession session) {
		
		TermsAndConditionVo termsandConditionVo =  new TermsAndConditionVo();
		termsandConditionVo.setTermsCondition(termsCondition);
		termsandConditionVo.setModules(modules);
		termsandConditionVo.setIsDefault(Integer.parseInt(isDefault));
		termsandConditionVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		termsandConditionVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		termsandConditionVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		termsandConditionVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		termsandConditionVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		termsandConditionVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		termsAndConditionService.save(termsandConditionVo);

		return termsandConditionVo;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public TermsAndConditionVo update(@RequestParam(value="termsCondition") String termsCondition,@RequestParam(value="isDefault") String isDefault,@RequestParam(value="id") long id,@RequestParam(value = "modules") String modules, HttpSession session)
	{	
		TermsAndConditionVo termsandConditionVo=termsAndConditionService.findByTermsandConditionId(id);
		termsandConditionVo.setTermsCondition(termsCondition);
		termsandConditionVo.setIsDefault(Integer.parseInt(isDefault));
		termsandConditionVo.setModules(modules);
		termsandConditionVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		termsandConditionVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		termsAndConditionService.save(termsandConditionVo);

		return termsandConditionVo;	
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam(value="id") long id,HttpSession session)
	{	
		termsAndConditionService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "Success";	
	}
	
	@GetMapping("{type}/json")
	@ResponseBody
	public List<TermsAndConditionVo> termsAndConditionJSON(@PathVariable("type") String type, HttpSession session)
	{
		return termsAndConditionService.findByCompanyIdAndModules(Long.parseLong(session.getAttribute("companyId").toString()), type);	
	}
}
