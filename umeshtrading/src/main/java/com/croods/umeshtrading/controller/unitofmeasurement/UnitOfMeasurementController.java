package com.croods.umeshtrading.controller.unitofmeasurement;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.services.appstream.model.Session;
import com.croods.umeshtrading.service.unitofmeasurement.UnitOfMeasurementService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.unitofmeasurement.UnitOfMeasurementVo;

@Controller
@RequestMapping("unitofmeasurement")
public class UnitOfMeasurementController {

	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@GetMapping("")
	public ModelAndView unitofmeasurement(HttpSession session) {
		ModelAndView view = new ModelAndView("unitofmeasurement/unitofmeasurement");
		
		view.addObject("unitOfMeasurementVos", unitOfMeasurementService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		return view;
	}
	
	@PostMapping("save")
	@ResponseBody
	public UnitOfMeasurementVo unitofmeasurementSave(@RequestParam(value="name") String name,@RequestParam(value="decimal") String decimal,@RequestParam(value="code") String code,HttpSession session) {
		UnitOfMeasurementVo unitOfMeasurementVo=new UnitOfMeasurementVo();
		
		unitOfMeasurementVo.setMeasurementName(name);
		unitOfMeasurementVo.setMeasurementCode(code);
		unitOfMeasurementVo.setNoOfDecimalPlaces(Integer.parseInt(decimal));
		unitOfMeasurementVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		unitOfMeasurementVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		unitOfMeasurementVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		unitOfMeasurementVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		unitOfMeasurementVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		unitOfMeasurementVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		unitOfMeasurementService.save(unitOfMeasurementVo);
		return unitOfMeasurementVo;
	}
	
	@PostMapping("/update")
	@ResponseBody
	public UnitOfMeasurementVo unitofmeasurementUpdate(@RequestParam(value="name") String name,@RequestParam(value="noOfDecimalPlaces") String noOfDecimalPlaces,@RequestParam(value="code") String code,@RequestParam(value="id") long id, HttpSession session)
	{	
		UnitOfMeasurementVo unitOfMeasurementVo=new UnitOfMeasurementVo();
		unitOfMeasurementVo=unitOfMeasurementService.findByMeasurementId(id);
		
		unitOfMeasurementVo.setMeasurementName(name);
		unitOfMeasurementVo.setNoOfDecimalPlaces(Integer.parseInt(noOfDecimalPlaces));
		unitOfMeasurementVo.setMeasurementCode(code);
		
		unitOfMeasurementVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		unitOfMeasurementVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		unitOfMeasurementService.save(unitOfMeasurementVo);
		return unitOfMeasurementVo;
	}
	
	@PostMapping("/delete")
	@ResponseBody
	public String unitofmeasurementDelete(@RequestParam(value="id") long id, HttpSession session)
	{
		unitOfMeasurementService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "Sucess";
	}
	
	@GetMapping("/uom")
	@ResponseBody
	public List<UnitOfMeasurementVo> uomList(HttpSession session) {
		return unitOfMeasurementService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
	}
}
