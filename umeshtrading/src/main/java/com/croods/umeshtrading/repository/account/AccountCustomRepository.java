package com.croods.umeshtrading.repository.account;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.account.AccountCustomVo;

@Repository
public interface AccountCustomRepository extends JpaRepository<AccountCustomVo, Long>, DataTablesRepository<AccountCustomVo, Long> {
	
	List<AccountCustomVo> findByBranchIdAndAccounTypeAndIsDeletedOrderByAccountCustomIdDesc(long branchId, String type,int isDeleted);
	
	List<AccountCustomVo> findByBranchIdAndGroupAccountGroupIdAndIsDeletedOrderByAccountCustomIdDesc(long branchId, long groupId, int isDeleted);
	
	List<AccountCustomVo> findByBranchIdAndIsDeletedOrderByGroupAccountGroupIdDescAccountCustomIdDesc(long branchId, int isDeleted);
	
	@Query("from AccountCustomVo where isDeleted=0 and branchId=?1  and accounType in (?2)  ORDER BY accountCustomId DESC")
	List<AccountCustomVo> findByBranchIdAndAccounTypes(long branchId,List<String> list);
	
	@Query("from AccountCustomVo where isDeleted=0 and branchId=?1  and accounType not in (?2)  ORDER BY accountCustomId DESC")
	List<AccountCustomVo> findByBranchIdAndAccounTypesNotIn(long branchId,List<String> list);

	AccountCustomVo findByAccountNameAndBranchId(String accountName, long branchId);

	List<AccountCustomVo> findByBranchIdAndIsDeletedAndAccountNameContainingIgnoreCaseOrGroupAccountGroupNameContainingIgnoreCase(
			long branchId, int i, String search, String search2, Pageable pageable);

	List<AccountCustomVo> findByBranchIdAndIsDeleted(long branchId, int i, Pageable pageable);
	
	int countByBranchIdAndIsDeletedAndAccountNameContainingIgnoreCaseOrGroupAccountGroupNameContainingIgnoreCase(
			long branchId, int i, String search, String search2);

	int countByBranchIdAndIsDeleted(long branchId, int i);

	@Modifying
	@Query("UPDATE AccountCustomVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE accountCustomId = ?1")
	void delete(long accountCustomId, long userId, String modifiedOn);
	
}
