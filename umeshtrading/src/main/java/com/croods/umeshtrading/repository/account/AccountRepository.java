package com.croods.umeshtrading.repository.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.account.AccountVo;

@Repository
public interface AccountRepository extends JpaRepository<AccountVo, Long>{

}
