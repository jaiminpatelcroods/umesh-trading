package com.croods.umeshtrading.repository.account;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.account.DebitNoteAccountingVo;

@Repository
public interface DebitNoteAccountingRepository extends JpaRepository<DebitNoteAccountingVo, Long>{
	
	List<DebitNoteAccountingVo> findByBranchIdAndIsDeletedAndTransactionDateBetween(long branchId, int isDeleted, Date transctionDateStart, Date transctionDateEnd);
	
	DebitNoteAccountingVo findByDebitNoteAccountIdAndBranchIdAndIsDeleted(long debitNoteAccountId, long branchId,int isDeleted);
	
	@Modifying
	@Query("update DebitNoteAccountingVo set isDeleted = 1, alterBy = ?2, modifiedOn = ?3 where debitNoteAccountId=?1")
	void delete(long id, long alterBy, String modifiedOn);

	@Query(value="select max(voucherNo) from DebitNoteAccountingVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxVoucherNo(long branchId, String prefix);
}
