package com.croods.umeshtrading.repository.bank;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.bank.BankVo;

@Repository
public interface BankRepository extends JpaRepository<BankVo, Long> {
	
	List<BankVo> findByBranchIdAndIsDeletedOrderByBankIdDesc(long branchId, int isDeleted);

	BankVo findByBankIdAndBranchIdAndIsDeleted(long bankId, long branchId, int isDeleted);

	@Modifying
	@Query("update BankVo set isDeleted=1, alterBy = ?2, modifiedOn = ?3 where bankId=?1")
	void delete(long bankId, long alterBy, String modifiedOn);	
	
	BankVo findByBankId(long bankId);
	
	@Query("SELECT accountCustomVo FROM BankVo where bank_id=?1")
	AccountCustomVo findAccountCustomVoByBankId(long bankId);
}
