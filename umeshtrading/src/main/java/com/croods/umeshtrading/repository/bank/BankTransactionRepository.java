package com.croods.umeshtrading.repository.bank;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.bank.BankTransactionVo;

@Repository
public interface BankTransactionRepository extends JpaRepository<BankTransactionVo, Long> {
	
	List<BankTransactionVo> findByBranchIdAndIsDeletedAndTransactionDateBetween(long branchId, int isDeleted, Date transactionDateStart, Date transactionDateEnd);
	
	BankTransactionVo findByBankTransactionIdAndBranchIdAndIsDeleted(long bankTransactionId, long branchId, int isDeleted);
	
	@Modifying
	@Query("update BankTransactionVo set isDeleted=1, alterBy = ?2, modifiedOn = ?3 where bankTransactionId=?1")
	void delete(long bankTransactionId, long alterBy, String modifiedOn);
	
	@Query(value="select max(voucherNo) from BankTransactionVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxVoucherNo(long branchId, String prefix);
}
