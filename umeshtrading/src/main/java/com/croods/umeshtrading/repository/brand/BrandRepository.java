package com.croods.umeshtrading.repository.brand;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.brand.BrandVo;

@Repository
public interface BrandRepository extends JpaRepository<BrandVo, Long> {
	
	BrandVo findByBrandId(long brandId);
	
	@Modifying
	@Query("update BrandVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where brandId=?1")
	void deleteBrand(long brandId, long alterBy, String modifiedOn);
	
	List<BrandVo> findByCompanyIdAndIsDeletedOrderByBrandIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update BrandVo set isDefault=?3, alterBy=?2, modifiedOn=?4 where companyId=?1")
	void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	@Modifying
	@Query("update BrandVo set isDefault=?3, alterBy=?2, modifiedOn=?4 where brandId=?1")
	void updateIsDefaultByBrandId(long brandId, long alterBy, int isDefault, String modifiedOn);

}
