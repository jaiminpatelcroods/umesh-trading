package com.croods.umeshtrading.repository.casetype;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.casetype.CaseTypeVo;

@Repository
public interface CaseTypeRepository extends JpaRepository<CaseTypeVo, Long> {
	
	CaseTypeVo findByCaseTypeId(long caseTypeId);
	
	@Modifying
	@Query("update CaseTypeVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where caseTypeId=?1")
	void deleteCaseType(long caseTypeId, long alterBy, String modifiedOn);

	List<CaseTypeVo> findByCompanyIdAndIsDeletedOrderByCaseTypeIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update CaseTypeVo set isDefault=?3, alterBy=?2, modifiedOn=?4 where companyId=?1")
	void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	@Modifying
	@Query("update CaseTypeVo set isDefault=?3, alterBy=?2, modifiedOn=?4  where caseTypeId=?1")
	void updateIsDefaultByCaseTypeId(long caseTypeId, long alterBy, int isDefault, String modifiedOn);

}
