package com.croods.umeshtrading.repository.category;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.category.CategoryVo;

@Repository
public interface CategoryRepository extends CrudRepository<CategoryVo, Long> {
	
	CategoryVo findByCategoryId(long categoryId);
	
	@Modifying
	@Query("update CategoryVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where categoryId=?1")
	void deleteCategory(long categoryId, long alterBy, String modifiedOn);

	List<CategoryVo> findByCompanyIdAndIsDeletedOrderByCategoryIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update CategoryVo set isDefault=?3, alterBy=?2, modifiedOn=?4 where companyId=?1")
	void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	@Modifying
	@Query("update CategoryVo set isDefault=?3, alterBy=?2, modifiedOn=?4  where categoryId=?1")
	void updateIsDefaultByCategoryId(long categoryId, long alterBy, int isDefault, String modifiedOn);

}
