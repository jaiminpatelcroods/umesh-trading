package com.croods.umeshtrading.repository.color;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.color.ColorVo;

@Repository
public interface ColorRepository extends JpaRepository<ColorVo, Long>{

	ColorVo findByColorId(long colorId);
	
	@Modifying
	@Query("update ColorVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where colorId=?1")
	void delete(long colorId, long alterBy, String modifiedOn);

	List<ColorVo> findByCompanyIdAndIsDeletedOrderByColorIdDesc(long companyId, int isDeleted);

}
