package com.croods.umeshtrading.repository.complain;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.complain.ComplainVo;

@Repository
public interface ComplainRepository extends JpaRepository<ComplainVo, Long>, DataTablesRepository<ComplainVo, Long>{

	@Query(value="select max(complainNo) from ComplainVo where isDeleted=0 and branchId=?1 and prefix=?2")
	String findMaxComplainNo(long branchId, String prefix);

	ComplainVo findByComplainIdAndBranchIdAndIsDeleted(long complainId, long branchId, int isDeleted);

	@Modifying
	@Query(value="UPDATE ComplainVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3  WHERE complainId = ?1")
	void delete(long complainId, long alterBy, String modifiedOn);

}
