package com.croods.umeshtrading.repository.contact;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.croods.umeshtrading.vo.contact.ContactOtherVo;

public interface ContactOtherRepository extends JpaRepository<ContactOtherVo, Long> {

	@Modifying
	@Query("DELETE ContactOtherVo WHERE contactOtherId in (?1)")
	void deleteContactOtherByIdIn(List<Long> contactOtherIds);
}
