package com.croods.umeshtrading.repository.contact;

import java.util.List;

import org.hibernate.annotations.Subselect;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.contact.ContactDTO;
import com.croods.umeshtrading.vo.contact.ContactVo;
import com.querydsl.core.annotations.QueryProjection;

@Repository
public interface ContactRepository extends JpaRepository<ContactVo, Long>, DataTablesRepository<ContactVo, Long> {

	ContactVo findByContactIdAndBranchIdAndTypeAndIsDeleted(long contactId, long branchId, String type, int isDeleted);

	List<ContactVo> findByBranchIdAndTypeAndIsDeletedOrderByContactIdDesc(long branchId, String type, int isDeleted);
	
	@Query("SELECT accountCustomVo FROM ContactVo WHERE contact_id=?1")
	AccountCustomVo findAccountCustomVoByContactId(long contactId);

	@Modifying
	@Query("UPDATE ContactVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE contactId = ?1")
	void deleteContact(long contactId, long userId, String modifiedOn);


	/*
	 * @Query("SELECT new com.croods.umeshtrading.vo.contact.ContactVo(c.contactId, c.companyName) FROM ContactVo c WHERE c.contactId = ?1"
	 * ) ContactVo findCompanyNameByContactId(long contactId);
	 */
	 
	 List<ContactVo> findByCompanyIdAndTypeAndIsDeleted(long companyId, String type, int i);

	long countByCompanyIdAndTypeAndIsDeleted(long companyId, String contactType, int i);

	long countByCompanyIdAndContactTypeAndIsDeleted(long companyId, String contactCategory, int isDeleted);
}
