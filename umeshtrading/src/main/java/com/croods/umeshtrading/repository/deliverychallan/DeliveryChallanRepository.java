package com.croods.umeshtrading.repository.deliverychallan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.deliverychallan.DeliveryChallanVo;

@Repository
public interface DeliveryChallanRepository extends JpaRepository<DeliveryChallanVo, Long>{

	@Query(value="SELECT MAX(deliveryChallanNo) FROM DeliveryChallanVo WHERE isDeleted=0 AND branchId=?1 AND prefix=?2")
	String findMaxDeliveryChallanNo(long branchId, String prefix);

	DeliveryChallanVo findByDeliveryChallanIdAndBranchIdAndIsDeleted(long deliveryChallanId, long branchId, int isDeleted);

}
