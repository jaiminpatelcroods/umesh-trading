package com.croods.umeshtrading.repository.department;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.department.DepartmentVo;

@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentVo, Long> {

	DepartmentVo findByDepartmentId(long departmentId);
	
	@Modifying
	@Query("update DepartmentVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where departmentId=?1")
	void deleteDepartment(long departmentId, long alterBy, String modifiedOn);

	List<DepartmentVo> findByCompanyIdAndIsDeletedOrderByDepartmentIdDesc(long companyId, int isDeleted);
	
}
