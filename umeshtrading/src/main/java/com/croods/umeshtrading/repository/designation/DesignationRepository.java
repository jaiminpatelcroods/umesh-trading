package com.croods.umeshtrading.repository.designation;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.designation.DesignationVo;

@Repository
public interface DesignationRepository extends JpaRepository<DesignationVo, Long> {

	DesignationVo findByDesignationId(long designationId);
	
	@Modifying
	@Query("update DesignationVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where designationId=?1")
	void deleteDesignation(long designationId, long alterBy, String modifiedOn);

	List<DesignationVo> findByCompanyIdAndIsDeletedOrderByDesignationIdDesc(long companyId, int isDeleted);
	
}
