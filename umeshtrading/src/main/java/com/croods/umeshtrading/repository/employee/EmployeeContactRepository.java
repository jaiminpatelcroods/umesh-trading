package com.croods.umeshtrading.repository.employee;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.employee.EmployeeContactVo;

@Repository
public interface EmployeeContactRepository extends JpaRepository<EmployeeContactVo, Long> {

	@Modifying
	@Query("DELETE FROM EmployeeContactVo where employeeContactId in (?1)")
	void deleteEmployeeContactByIdIn(List<Long> employeeContactIds);

}
