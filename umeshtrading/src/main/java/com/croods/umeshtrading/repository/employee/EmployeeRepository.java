package com.croods.umeshtrading.repository.employee;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.employee.EmployeeVo;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeVo, Long> {

	EmployeeVo findByEmployeeIdAndBranchIdAndIsDeleted(long employeeId, long branchId, int isDeleted);
	
	List<EmployeeVo> findByBranchIdAndIsDeletedOrderByEmployeeIdDesc(long branchId, int isDeleted);
	
	@Modifying
	@Query("UPDATE EmployeeVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE employeeId = ?1")
	void deleteEmployee(long contactId, long userId, String modifiedOn);
}
