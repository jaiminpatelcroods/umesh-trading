package com.croods.umeshtrading.repository.fabric;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.fabric.FabricVo;

@Repository
public interface FabricRepository extends JpaRepository<FabricVo, Long>{

	FabricVo findByFabricId(long fabricId);
	
	@Modifying
	@Query("update FabricVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where fabricId=?1")
	void delete(long fabricId, long alterBy, String modifiedOn);

	List<FabricVo> findByCompanyIdAndIsDeletedOrderByFabricIdDesc(long companyId, int isDeleted);

}
