package com.croods.umeshtrading.repository.financial;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.financial.FinancialMonth;

@Repository
public interface FinancialMonthRepository extends JpaRepository<FinancialMonth, Long> {
	
	@Query("select monthIntervalValue from FinancialMonth where monthInterval =:monthInterval")
	public String findByMonthInterval(@Param("monthInterval")String monthInterval);
	
}
