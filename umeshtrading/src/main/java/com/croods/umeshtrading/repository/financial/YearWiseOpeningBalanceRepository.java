package com.croods.umeshtrading.repository.financial;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.financial.YearWiseOpeningBalanceVo;

@Repository
public interface YearWiseOpeningBalanceRepository extends JpaRepository<YearWiseOpeningBalanceVo, Long> {
	
	YearWiseOpeningBalanceVo findByBranchIdAndAccountCustomVoAccountCustomIdAndYearInterval(long branchId,long accountCustomId,String yearInterval);
	
	YearWiseOpeningBalanceVo findByOpeningBalanceId(long Id);
}
