package com.croods.umeshtrading.repository.floor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.floor.FloorVo;
import com.croods.umeshtrading.vo.place.PlaceVo;

@Repository
public interface FloorRepository extends JpaRepository<FloorVo, Long> {

	FloorVo findByFloorId(long floorId);
	
	@Modifying
	@Query("update FloorVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where floorId=?1")
	void delete(long floorId, long alterBy, String modifiedOn);

	List<FloorVo> findByCompanyIdAndIsDeletedOrderByFloorIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update FloorVo set isDefault=?3 , alterBy=?2, modifiedOn=?4 where companyId=?1")
	void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	@Modifying
	@Query("update FloorVo set isDefault=?3, alterBy=?2, modifiedOn=?4 where floorId=?1")
	void updateIsDefaultByFloorId(long floorId, long alterBy, int isDefault, String modifiedOn);

	//FloorVo findByPlaceId(long placeId);

	//FloorVo findByPlaceVo(long id);

	List<FloorVo> findByPlaceVoPlaceId(long id);

}
