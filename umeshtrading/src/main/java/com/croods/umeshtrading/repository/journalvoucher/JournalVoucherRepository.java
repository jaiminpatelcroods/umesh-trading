package com.croods.umeshtrading.repository.journalvoucher;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.journalvoucher.JournalVoucherVo;

@Repository
public interface JournalVoucherRepository extends JpaRepository<JournalVoucherVo, Long> {
	
	List<JournalVoucherVo> findByBranchIdAndIsDeletedAndVoucherDateBetween(long branchId, int isDeleted, Date voucherDateStart, Date voucherDateEnd);
	
	@Query(value="select max(voucherNo) from JournalVoucherVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxVoucherNo(long branchId, String prefix);
	
	JournalVoucherVo findByJournalVoucherIdAndBranchIdAndIsDeleted(long journalVoucherId, long branchId, int isDeleted);

	@Modifying
	@Query("update JournalVoucherVo set isDeleted=1, alterBy = ?2, modifiedOn = ?3 where journalVoucherId=?1")
	void delete(long id, long alterBy, String modifiedOn);

	
}
