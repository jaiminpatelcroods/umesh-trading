package com.croods.umeshtrading.repository.lead;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.croods.umeshtrading.vo.lead.LeadContactVo;

public interface LeadContactRepository extends JpaRepository<LeadContactVo, Long> {

	@Modifying
	@Query("DELETE LeadContactVo WHERE leadContactId in (?1)")
	void deleteLeadContactByIdIn(List<Long> leadContactIds);
}
