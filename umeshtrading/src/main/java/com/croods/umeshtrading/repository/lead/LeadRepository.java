package com.croods.umeshtrading.repository.lead;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.lead.LeadVo;

@Repository
public interface LeadRepository extends JpaRepository<LeadVo, Long>, DataTablesRepository<LeadVo, Long> {

	LeadVo findByLeadIdAndBranchIdAndIsDeleted(long leadId, long branchId, int isDeleted);

	List<LeadVo> findByBranchIdAndIsDeletedOrderByLeadIdDesc(long branchId, int isDeleted);
	
	@Modifying
	@Query("UPDATE LeadVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE leadId = ?1")
	void deleteLead(long leadId, long userId, String modifiedOn);
}
