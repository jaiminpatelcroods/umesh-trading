package com.croods.umeshtrading.repository.location;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.location.CityVo;

@Repository
public interface CityRepository extends JpaRepository<CityVo, Long> {
	
	@Query("from CityVo where stateCode =:id")
	List<CityVo> findByStateCode(@Param("id")String stateCode);
	
	@Query("SELECT cityName FROM CityVo WHERE cityCode = ?1")
	String findCityNameByCityCode(String cityCode);
}
