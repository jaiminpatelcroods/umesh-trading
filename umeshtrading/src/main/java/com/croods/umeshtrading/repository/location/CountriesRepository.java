package com.croods.umeshtrading.repository.location;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.location.CountriesVo;

@Repository
public interface CountriesRepository extends JpaRepository<CountriesVo, Long> {
	
	@Query("SELECT countriesName FROM CountriesVo WHERE countriesCode = ?1")
	String findCountriesNameByCountriesCode(String countriesCode);
	
}
