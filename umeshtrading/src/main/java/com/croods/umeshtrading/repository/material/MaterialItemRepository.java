package com.croods.umeshtrading.repository.material;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.material.MaterialItemVo;

@Repository
public interface MaterialItemRepository  extends JpaRepository<MaterialItemVo, Long> {

	@Modifying
	@Query("delete from MaterialItemVo  where materialItemId in (?1)")
	public void deleteMaterialItem(List<Long> l);
	
	@Query("from MaterialItemVo  where materialVo.materialId = ?2")
	public MaterialItemVo findByMaterialId(long materialId);

	@Query(value="select sum(qty)from material_item  where material_id in(select material_id from Material where is_deleted=0 and type='materialorder' and company_id=?1 and material_date BETWEEN  ?2 AND ?3)",nativeQuery=true)
	public long getTotalMaterialOrderQty(long companyId, Date startDate, Date endDate);
 
}

