package com.croods.umeshtrading.repository.material;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.material.MaterialVo;

@Repository
public interface MaterialRepository  extends DataTablesRepository<MaterialVo, Long>,JpaRepository<MaterialVo, Long> {
  

	@Query(value="select max(material_no) from material where is_deleted=0 and company_id=?1 and type=?2 and prefix=?3",nativeQuery=true)
	public String findMaxMaterialNo(Long companyId, String type, String prefix);

	@Modifying
	@Query("update MaterialVo set isDeleted=1 where materialId=?1")
	public void deleteMaterial(Long id);
	
	public List<MaterialVo> findByTypeAndCompanyIdAndIsDeletedAndMaterialDateBetween(String type, long companyId, int isDeleted,
			Date materialDateStart, Date materialDateEnd);
	
	@Query(value="from MaterialVo where isDeleted = 0 AND contactVo.contactId = ?5 AND type IN (?1) AND companyId = ?2 AND materialDate BETWEEN ?3 AND ?4")
	public List<MaterialVo> findByTypesAndContactAndCompanyIdAndMaterialDateBetween(List<String> types, long companyId,Date materialDateStart, Date materialDateEnd, long contactId);
	
	public MaterialVo findByMaterialIdAndCompanyId(long materialId, long companyId);
	
	@Query("from MaterialVo where type=?1 And companyId=?2 And isDeleted=?3 And materialDate between ?4 And ?5 And contactVo.contactId=?6")
	public List<MaterialVo> getListOfAllUnpaidBillEdit(String type, long companyId, int isDeleted, Date startDate,Date enddate, long contactId);
	
	@Query("from MaterialVo where type=?1 And companyId=?2 And isDeleted=?3 And materialDate between ?4 And ?5 And contactVo.contactId=?6 And paidAmount!=total")
	public List<MaterialVo> getListOfAllUnpaidBill(String type, long companyId, int isDeleted, Date startDate, Date endDate,
			long contactId);
	
	/*@Query(value="SELECT contactVo.contactId,SUM(total-paidAmount) FROM MaterialVo WHERE isDeleted = 0 AND type IN (?1) AND companyId = ?2 AND materialDate BETWEEN ?3 AND ?4 GROUP BY contactVo.contactId")
	public List getDueAmountByContactId(List<String> types, long companyId,Date materialDateStart, Date materialDateEnd);*/
	
	@Query(value="SELECT contactVo.contactId as contactId ,SUM(total-paidAmount) as dueamount  FROM MaterialVo WHERE isDeleted = 0 AND type IN (?1) AND companyId = ?2 AND materialDate BETWEEN ?3 AND ?4 GROUP BY contactVo.contactId")
	public List<Map<Long, Double>> getDueAmountByContactId(List<String> types, long companyId,Date materialDateStart, Date materialDateEnd);
	
	@Query(value="SELECT SUM(total) as totalamount,SUM(paidAmount) as paidAmount  FROM MaterialVo WHERE isDeleted = 0 AND type IN (?1) AND companyId = ?2 AND materialDate BETWEEN ?3 AND ?4 And contactVo.contactId=?5")
	public List<Map<Double, Double>> getPaidAndTotalAmountByContactId(List<String> types, long companyId,Date materialDateStart, Date materialDateEnd,long ContactId);
	
	@Query(value="SELECT SUM(total) as totalamount,SUM(paidAmount) as paidAmount  FROM MaterialVo WHERE isDeleted = 0 AND type IN (?1) AND companyId = ?2 AND materialDate BETWEEN ?3 AND ?4 ")
	public List<Map<Double, Double>> getPaidAndTotalAmountAll(List<String> types, long companyId,Date materialDateStart, Date materialDateEnd);

	@Query(value="SELECT MAX(total) as total,materialDate FROM (select sum(total) as total, material_date as materialDate from material where DATE(material_date)<=current_date and material_date+0>=current_date-15 and is_deleted=0 and company_id=?1 group by material_date UNION ALL select  0 as total ,i\\:\\:date as materialDate from generate_series(current_date-14, current_date, '1 day'\\:\\:interval) i) AS a group by materialDate order by materialDate",nativeQuery=true)
	public List<Map<Double, String>> get15daySalesAll(long companyId);
	
}
