package com.croods.umeshtrading.repository.material;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface MaterialsAdditionalChargeRepository   {

	@Modifying
	@Query("delete from JobworkAdditionalChargeVo  where jobworkAdditionalChargeId in (?1)")
	public void deleteJobworkAdditionalItem(List<Long> l);
	
}
