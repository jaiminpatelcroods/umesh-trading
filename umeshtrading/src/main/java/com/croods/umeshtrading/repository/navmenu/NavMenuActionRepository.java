package com.croods.umeshtrading.repository.navmenu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.navmenu.NavMenuActionVo;

@Repository
public interface NavMenuActionRepository extends JpaRepository<NavMenuActionVo, Long> {

}
