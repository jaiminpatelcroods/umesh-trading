package com.croods.umeshtrading.repository.navmenu;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.navmenu.NavSubMenuVo;

@Repository
public interface NavSubMenuRepository extends JpaRepository<NavSubMenuVo, Long> {
	
	@Query(nativeQuery=true,value="SELECT * FROM nav_sub_menu WHERE nav_sub_menu_id IN (SELECT nav_sub_menu_id FROM nav_menu_permission WHERE user_front_id=?1)  GROUP BY nav_sub_menu_id ORDER BY ordering ASC")
	List<NavSubMenuVo> findNavSubMenuByUserFrontId(long userFrontId);
	
	public NavSubMenuVo findByNavSubMenuId(long navSubMenuId);
	
	@Query(nativeQuery=true,value="SELECT * FROM nav_sub_menu WHERE nav_sub_menu_id IN (SELECT nav_sub_menu_id FROM nav_menu_permission WHERE user_role_id=?1)  GROUP BY nav_sub_menu_id ORDER BY ordering ASC")
	List<NavSubMenuVo> findNavSubMenuByUserRoleId(long userRoleId);
}
