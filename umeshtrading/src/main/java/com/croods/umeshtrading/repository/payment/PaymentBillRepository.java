package com.croods.umeshtrading.repository.payment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.payment.PaymentBillVo;

@Repository
public interface PaymentBillRepository extends JpaRepository<PaymentBillVo, Long>{

	@Modifying
	@Query("delete from PaymentBillVo  where paymentBillId in (?1)")
	void deletePaymentBill(List<Long> paymentBillIds);
}
