package com.croods.umeshtrading.repository.paymentterm;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.paymentterm.PaymentTermVo;

@Repository
public interface PaymentTermRepository extends JpaRepository<PaymentTermVo, Long> {

	List<PaymentTermVo> findByBranchIdAndIsDeletedOrderByPaymentTermIdDesc(long branchid, int delete);

	PaymentTermVo findByPaymentTermId(long id);
	
	@Modifying
	@Query("update PaymentTermVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where paymentTermId=?1")
	void delete(long id, long alterBy, String modifiedOn);
}
