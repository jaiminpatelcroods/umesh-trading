package com.croods.umeshtrading.repository.place;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.place.PlaceVo;

@Repository
public interface PlaceRepository extends JpaRepository<PlaceVo, Long> {

	PlaceVo findByPlaceId(long placeId);
	
	@Modifying
	@Query("update PlaceVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where placeId=?1")
	void delete(long placeId, long alterBy, String modifiedOn);

	List<PlaceVo> findByCompanyIdAndIsDeletedOrderByPlaceIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update PlaceVo set isDefault=?3 , alterBy=?2, modifiedOn=?4 where companyId=?1")
	void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	@Modifying
	@Query("update PlaceVo set isDefault=?3, alterBy=?2, modifiedOn=?4 where placeId=?1")
	void updateIsDefaultByPlaceId(long placeId, long alterBy, int isDefault, String modifiedOn);

}
