package com.croods.umeshtrading.repository.product;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.product.ProductVo;

@Repository
public interface ProductRepository extends JpaRepository<ProductVo, Long>, DataTablesRepository<ProductVo, Long> {

	ProductVo findByProductIdAndCompanyIdAndIsDeleted(long productId, long companyId, int isDeleted);

	List<ProductVo> findByCompanyIdAndIsDeleted(long companyId, int i);

	@Modifying
	@Query("update ProductVo set isDeleted=1 where productId=?1")
	void deleteProduct(Long id);

	long countByCompanyIdAndIsDeleted(long companyId, int i);
}
