package com.croods.umeshtrading.repository.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.product.ProductVariantVo;

@Repository
public interface ProductVariantRepository extends JpaRepository<ProductVariantVo, Long>{

	List<ProductVariantVo> findByCompanyIdAndIsDeleted(long companyId, int isDeleted);

	ProductVariantVo findByProductVariantIdAndCompanyIdAndIsDeleted(long productVariantId, long companyId, int isDeleted);
	
	@Query("from ProductVariantVo where (LOWER(variantName) like LOWER(concat('%', ?1,'%')) OR LOWER(productVo.name) like LOWER(concat('%', ?1,'%')) OR LOWER(productVo.categoryVo.categoryName) like LOWER(concat('%', ?1,'%')) OR LOWER(productVo.brandVo.brandName) like LOWER(concat('%', ?1,'%'))) AND isDeleted=0 AND companyId=?2")
	List<ProductVariantVo> findProductVariants(String searchKey,long companyId);
}
