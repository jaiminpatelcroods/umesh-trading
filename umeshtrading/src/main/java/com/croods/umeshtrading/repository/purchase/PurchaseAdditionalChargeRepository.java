package com.croods.umeshtrading.repository.purchase;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.purchase.PurchaseAdditionalChargeVo;

@Repository
public interface PurchaseAdditionalChargeRepository extends JpaRepository<PurchaseAdditionalChargeVo, Long> {

	@Modifying
	@Query("delete from PurchaseAdditionalChargeVo  where purchaseAdditionalChargeId in (?1)")	
	void deletePurchaseAdditionalChargeByIdIn(List<Long> purchaseAdditionalChargeIds);
}
