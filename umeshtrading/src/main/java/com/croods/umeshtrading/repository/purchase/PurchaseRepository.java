package com.croods.umeshtrading.repository.purchase;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.purchase.PurchaseVo;
import com.croods.umeshtrading.vo.purchaserequest.PurchaseRequestVo;

@Repository
public interface PurchaseRepository extends JpaRepository<PurchaseVo, Long>, DataTablesRepository<PurchaseVo, Long> {

	@Query(value="select max(purchaseNo) from PurchaseVo where isDeleted=0 and branchId=?1 and type=?2 and prefix=?3")
	String findMaxPurchaseNo(long branchId, String type, String prefix);

	PurchaseVo findByPurchaseIdAndBranchIdAndIsDeleted(long purchaseId, long branchId, int isDeleted);

	@Modifying
	@Query("UPDATE PurchaseVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE purchaseId = ?1")
	void delete(long purchaseId, long userId, String modifiedOn);

	@Query("SELECT purchaseId FROM PurchaseVo WHERE isDeleted = 0 AND branchId = ?1 AND type = ?2 AND prefix = ?3 AND purchaseNo = ?4")
	String isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo);
	
	@Query("SELECT purchaseId FROM PurchaseVo WHERE isDeleted = 0 AND branchId = ?1 AND type = ?2 AND prefix = ?3 AND purchaseNo = ?4 AND purchaseId != ?5")
	String isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo, long purchaseId);

	@Query("from PurchaseVo where type=?1 And branchId=?2 And isDeleted=0 And purchaseDate between ?4 And ?5 And contactVo.contactId=?3 And paidAmount!=total")
	List<PurchaseVo> findByTypeAndBranchIdAndContactVoContactIdAndIsDeletedAndPurchaseDateBetween(String type,
			long branchId, long contactId, Date startDate, Date endDate);
	
	@Modifying
	@Query("UPDATE PurchaseVo SET paidAmount=?2 WHERE purchaseId=?1")
	void updatePaidAmount(long purchaseId, double amount);
	
	@Modifying
	@Query("UPDATE PurchaseVo SET paidAmount = paidAmount + ?2, alterBy = ?3, modifiedOn = ?4 WHERE purchaseId=?1")
	void addPaidAmountByPurchaseId(long purchaseId, double payment, long userId, String modifiedOn);
}
