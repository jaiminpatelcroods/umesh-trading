package com.croods.umeshtrading.repository.purchaserequest;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.purchaserequest.PurchaseRequestItemVo;

@Repository
public interface PurchaseRequestItemRepository extends JpaRepository<PurchaseRequestItemVo, Long> {

	@Modifying
	@Query("delete from PurchaseRequestItemVo  where purchaseRequestItemId in (?1)")
	void deletePurchaseRequestItemByIdIn(List<Long> purchaseRequestItemIds);

}
