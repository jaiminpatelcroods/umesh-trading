package com.croods.umeshtrading.repository.purchaserequest;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.purchaserequest.PurchaseRequestVo;

@Repository
public interface PurchaseRequestRepository extends JpaRepository<PurchaseRequestVo, Long>, DataTablesRepository<PurchaseRequestVo, Long> {

	@Query(value="select max(requestNo) from PurchaseRequestVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxRequestNo(long branchId, String prefix);

	PurchaseRequestVo findByPurchaseRequestIdAndBranchIdAndIsDeleted(long purchaseRequestId, long branchId, int isDeleted);

	@Modifying
	@Query("UPDATE PurchaseRequestVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE purchaseRequestId = ?1")
	void delete(long purchaseRequestId, long userId, String modifiedOn);

}
