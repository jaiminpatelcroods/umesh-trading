package com.croods.umeshtrading.repository.stock;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.stock.StockVo;



@Repository
public interface StockRepository extends DataTablesRepository<StockVo, Long>,JpaRepository<StockVo, Long> {

	//Jaimin public List<StockVo> findByProductVoProductIdAndBatchNoAndCompanyIdAndContactVoContactIdIsNull(long productId, String batchNo, long companyId);
	
	//Jaimin public List<StockVo> findByProductVoProductIdAndBatchNoAndCompanyIdAndContactVoContactId(long productId, String batchNo, long companyId, long contactId);
	
	@Modifying
	@Query(value = "update StockVo set quantity = ((quantity - ?3) + ?4)  where productVo.productId=?1 AND rackVo.rackId = ?2 AND companyId = ?5 ")
	public void updateStockWithOutContact(long productId, long rackId, double oldQuantity, double newQuantity, long companyId);
	
	
	/*@Modifying
	@Query(value = "update StockVo set quantity = quantity - ?3 + ?4  where productVo.productId=?1 AND rackVo.rackId=?2 AND contactVo.contactId = ?5 AND companyId = ?6")
	public void updateStockWithContact(long productId, long rackId, double oldQuantity, double newQuantity, long contactId, long companyId);*/

	/*@Query("select RackVo.rackId FROM StockVo where RackVo.rackId!=null AND productVo.productId=?1 AND quantity > 0.0")
	public List getProductBatchList(Long productId);*/
	
	public List<StockVo> findByProductVoProductIdAndRackVoRackIdAndCompanyId(long productId, long rackId, long companyId);

	@Query(value="select SUM(quantity) as total_qty FROM StockVo where rackVo.rackId!=null AND productVo.productId=?1 AND companyId = ?2 ")
	public Double availableProductQty(long productId, long companyId);
	
	@Query(value="select CAST(stock_master.place_id AS varchar) placeId,MAX(place.place_code) AS placeCode FROM stock_master LEFT JOIN place ON place.place_id = stock_master.place_id  where stock_master.quantity > 0.0 AND stock_master.product_id=?1 AND stock_master.company_id = ?2 GROUP BY stock_master.place_id",nativeQuery=true)
	public List<Map<String, String>> availableProductPlace(long productId, long companyId);

	@Query(value="select CAST(stock_master.floor_id AS varchar) floorId,MAX(floor.floor_code) AS floorCode FROM stock_master LEFT JOIN floor ON floor.floor_id = stock_master.floor_id  where stock_master.quantity > 0.0 AND stock_master.place_id=?1 AND stock_master.product_id=?2 AND stock_master.company_id = ?3 GROUP BY stock_master.floor_id",nativeQuery=true)
	public List<Map<String, String>> availableProductFloor(long placeId, long productId, long companyId2);

	@Query(value="select CAST(stock_master.rack_id AS varchar) rackId,MAX(rack.rack_code) AS rackCode FROM stock_master LEFT JOIN rack ON rack.rack_id = stock_master.rack_id  where stock_master.quantity > 0.0 AND stock_master.floor_id=?1 AND stock_master.product_id=?2 AND stock_master.company_id = ?3 GROUP BY stock_master.rack_id",nativeQuery=true)
	public List<Map<String, String>> availableProductRack(long floorId, long productId, long companyId);

	@Query(value="select SUM(quantity) as total_qty FROM StockVo where placeVo.placeId=?2 AND productVo.productId=?1 AND companyId = ?3 ")
	public Double availableProductQtyOnPlace(long productId, long placeId, long companyId);
	
}
