package com.croods.umeshtrading.repository.stock;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.stock.StockTransactionVo;

@Repository
public interface StockTransactionRepository extends DataTablesRepository<StockTransactionVo, Long>, JpaRepository<StockTransactionVo, Long> {

	@Modifying
	@Query(value = "delete from StockTransactionVo where companyId=?1 and typeId=?2 and type=?3")
	public void deleteStockTransaction(Long companyId, Long typeId, String type);

	/*Jaimin @Query("select SUM(inQuantity-outQuantity) FROM StockTransactionVo WHERE companyId = ?1 AND productVo.productId = ?2 AND stockTransactionDate BETWEEN ?3 AND ?4 AND yearInterval = ?5")
	public double getTotalQty(long companyId, long productId, Date startDate, Date endDate, String yearInterval);
 
	@Modifying
	@Query(value = "delete from StockTransactionVo where stockTransactionId=?1")
	public void deleteOpeningStock(Long transactionId);
	
	@Query("select type,SUM(inQuantity) FROM StockTransactionVo WHERE companyId = ?1 AND type IN('production_qty','production_scrap')  AND yearInterval = ?2 group by type")
	public List getTotalProductionStock(long companyId, String yearInterval);

	@Query("select batchNo FROM StockTransactionVo WHERE batchNo!=null  AND companyId = ?2 AND productVo.productId=?1 AND yearInterval = ?3 AND type in (?4) group by batchNo")
	public List getTotalProductionStock(Long productId, long companyId, String yearInterval, List<String> types);*/
  
	
	@Query(value="select SUM((in_quantity*product_price)-(out_quantity*product_price)) \n" + 
			"FROM stock_transaction \n" + 
			"WHERE \n" + 
			"company_id = ?1  AND \n" + 
			"stock_transaction_date BETWEEN ?2\\:\\:date AND ?3\\:\\:date \n" + 
			"AND year_interval = ?4",nativeQuery=true)
	public double getTotalAllInPriceQty(long companyId, Date startDate, Date endDate, String yearInterval);

	@Query(nativeQuery=true,value="SELECT product_price \n" + 
			"FROM stock_transaction\n" + 
			"WHERE type='materialin' AND product_id=?1 AND company_id=?2 AND create_date_time = (\n" + 
			"  SELECT MAX(create_date_time)\n" + 
			"  FROM stock_transaction "
			+ "WHERE type='materialin' AND product_id=?1 AND company_id=?2 \n" + 
			");")
	public Double getLastPucrchasePrice(long productId,long companyId);
}
