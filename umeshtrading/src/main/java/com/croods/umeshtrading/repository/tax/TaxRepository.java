package com.croods.umeshtrading.repository.tax;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.tax.TaxVo;

@Repository
public interface TaxRepository extends JpaRepository<TaxVo, Long> {
	
	TaxVo findByTaxId(long id);
	
	@Modifying
	@Query("update TaxVo set isDeleted=1, alterBy = ?2, modifiedOn = ?3 where taxId=?1")
	void delete(long taxId, long alterBy, String modifiedOn) ;
	
	List<TaxVo> findByCompanyIdAndIsDeletedOrderByTaxIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update TaxVo set isDefault=?2 where branchId=?1")
	void updateIsDefaultByCompanyId(long companyId, int isDefault);

	@Modifying
	@Query("update TaxVo set isDefault=?2 where taxId=?1")
	void updateIsDefaultByTaxId(long taxId, int isDefault);
}
