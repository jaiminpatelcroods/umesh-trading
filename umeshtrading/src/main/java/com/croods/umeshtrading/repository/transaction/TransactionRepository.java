package com.croods.umeshtrading.repository.transaction;

import java.util.Date;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.transaction.TransactionVo;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionVo, Long>, DataTablesRepository<TransactionVo, Long> {

	@Modifying
	@Query(value="delete from TransactionVo where branchId=?1 and voucherId=?2 and voucherType=?3")
	public void deleteTransaction(long branchId, long voucherId, String voucherType);
	
	@Query(nativeQuery=true,value="select case when account_group_id in (1,3,6,7,8,12,15,18,19) then sum(debit_amount)-sum(credit_amount) else sum(credit_amount)-sum(debit_amount) end as op from transaction tm where account_custom_id=?1 and tm.transaction_date between ?2 and ?3 group by account_group_id")
	double getBalance(long accountCustomId, Date startDate, Date fromDate);
	
	@Query(nativeQuery=true,value="select case when account_group_id in (1,3,6,7,8,12,15,18,19) then sum(debit_amount)-sum(credit_amount) else sum(credit_amount)-sum(debit_amount) end " + 
			"	from (select debit_amount, credit_amount, account_group_id from transaction tm where account_custom_id=?1 and tm.transaction_date between ?2 and ?3 ORDER BY tm.transaction_date asc, tm.transaction_id asc limit ?4 offset ?5) AS x group By account_group_id ")
	double getBalancePageable(long accountCustomId, Date startDate, Date fromDate, int start, int limit);
	
}
