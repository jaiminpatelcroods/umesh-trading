package com.croods.umeshtrading.repository.userfront;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.userfront.UserFrontVo;

@Repository
public interface UserFrontRepository extends JpaRepository<UserFrontVo, Long> {
	
	public UserFrontVo findByUserName(String userName);

	public UserFrontVo findByUserFrontId(long userFrontId);

	@Modifying
	@Query("update UserFrontVo set defaultYearInterval = ?2, modifiedOn = ?3 where userFrontId = ?1")
	public void updateDefaultYearIntervalByUserFrontId(long userFrontId, String yearInterval, String modifiedOn);
	
}
