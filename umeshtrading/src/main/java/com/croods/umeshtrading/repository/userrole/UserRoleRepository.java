package com.croods.umeshtrading.repository.userrole;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.userrole.UserRoleVo;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRoleVo, Long> {

	UserRoleVo findByUserRoleId(long userRoleId);
	
	List<UserRoleVo> findByBranchId(long branchId);

}
