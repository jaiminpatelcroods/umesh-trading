package com.croods.umeshtrading.service.Payment;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.umeshtrading.vo.payment.PaymentVo;

public interface PaymentService {

	public long findMaxPaymentNo(long companyId, long branchId, long userId, String type, String prefix);

	public PaymentVo save(PaymentVo paymentVo);
	
	public void deletePaymentBillVoByIn(List<Long> paymentBillIds);

	public PaymentVo findByPaymentIdAndBranchId(long paymentId, long parseLong);

	public DataTablesOutput<PaymentVo> findAll(@Valid DataTablesInput input, Specification<PaymentVo> additionalSpecification,
			Specification<PaymentVo> specification);

}
