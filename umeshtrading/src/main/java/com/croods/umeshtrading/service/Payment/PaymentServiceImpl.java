package com.croods.umeshtrading.service.Payment;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.payment.PaymentBillRepository;
import com.croods.umeshtrading.repository.payment.PaymentRepository;
import com.croods.umeshtrading.repository.purchase.PurchaseRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.payment.PaymentBillVo;
import com.croods.umeshtrading.vo.payment.PaymentVo;
import com.croods.umeshtrading.vo.prefix.PrefixVo;

@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	PaymentRepository paymentRepository;
	
	@Autowired
	PaymentBillRepository paymentBillRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	PurchaseRepository purchaseRepository;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public long findMaxPaymentNo(long companyId, long branchId, long userId, String type, String prefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(prefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = paymentRepository.findMaxPaymentNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public PaymentVo save(PaymentVo paymentVo) {
		
		paymentRepository.saveAndFlush(paymentVo);
		entityManager.refresh(paymentVo);
		return paymentVo;
	}
	
	@Override
	public void deletePaymentBillVoByIn(List<Long> paymentBillIds) {
		
		List<PaymentBillVo> paymentBillVo=paymentBillRepository.findAllById(paymentBillIds);
		
		for(PaymentBillVo billVo:paymentBillVo)
		{	
			double minus=billVo.getPurchaseVo().getPaidAmount()-billVo.getPayment();
			
			purchaseRepository.updatePaidAmount(billVo.getPurchaseVo().getPurchaseId(),minus);
		}
		
		paymentBillRepository.deletePaymentBill(paymentBillIds);
	}

	@Override
	public PaymentVo findByPaymentIdAndBranchId(long paymentId, long branchId) {
		return paymentRepository.findByPaymentIdAndBranchIdAndIsDeleted(paymentId, branchId, 0);
	}

	@Override
	public DataTablesOutput<PaymentVo> findAll(@Valid DataTablesInput input,
			Specification<PaymentVo> additionalSpecification, Specification<PaymentVo> specification) {
		return paymentRepository.findAll(input, additionalSpecification, specification);
	}

}
