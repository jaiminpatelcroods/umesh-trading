package com.croods.umeshtrading.service.account;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.account.AccountVo;
import com.croods.umeshtrading.vo.product.ProductVo;

public interface AccountService {
	
	// AccountCustomVo  Method
	public AccountCustomVo insertAccount(AccountCustomVo accountCustomVo);

	public List<AccountCustomVo> findByBranchIdAndAccounType(long parseLong, String string);
	
	public AccountCustomVo findAccountCustombyId(long accountCustomId);

	public  List<AccountCustomVo> findByBranchIdAndAccounTypes(long branchId,List<String> list);

	public AccountCustomVo findByAccountNameAndBranchId(String accountName, long branchId);
	
	public void deleteAccountCustom(long accountCustomId, long userId, String modifiedOn);
	
	public List<AccountCustomVo> findByBranchIdAndGroup(long branchId, long groupId);
	
	public List<AccountCustomVo> findByBranchIdAndIsDeletedOrderByGroup(long branchId);
	
	List<AccountCustomVo> findByBranchIdAndAccounTypesNotIn(long branchId,List<String> list);
	
	public DataTablesOutput<AccountCustomVo> findAllAccountDatatable(DataTablesInput input,
			Specification<AccountCustomVo> additionalSpecification, Specification<AccountCustomVo> specification);
	
	//Account Group Method	
	public List<AccountGroupVo> findAllAccountGroup();
	
	public AccountGroupVo findByAccountGroupId(long accountGroupId);
	
	//AccountVo Method
	public List<AccountVo> findAllAccount();
	
}
