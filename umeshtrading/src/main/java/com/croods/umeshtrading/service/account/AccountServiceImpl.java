package com.croods.umeshtrading.service.account;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.account.AccountCustomRepository;
import com.croods.umeshtrading.repository.account.AccountGroupRepository;
import com.croods.umeshtrading.repository.account.AccountRepository;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.account.AccountVo;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	AccountCustomRepository accountCustomRepository;
	
	@Autowired
	AccountGroupRepository accountGroupRepository;
	
	//AccountCustomVo Method implementation
	@Override
	public AccountCustomVo insertAccount(AccountCustomVo accountCustomVo) {
		return accountCustomRepository.save(accountCustomVo);
	}

	@Override
	public List<AccountCustomVo> findByBranchIdAndAccounType(long branchId, String type) {
		return accountCustomRepository.findByBranchIdAndAccounTypeAndIsDeletedOrderByAccountCustomIdDesc(branchId, type, 0);
	}

	@Override
	public AccountCustomVo findAccountCustombyId(long accountCustomId) {
		return accountCustomRepository.findById(accountCustomId).orElse(null);
	}

	@Override
	public List<AccountCustomVo> findByBranchIdAndAccounTypes(long branchId, List<String> list) {
		//Old fromAccount()
		return accountCustomRepository.findByBranchIdAndAccounTypes(branchId, list);
	}

	@Override
	public AccountCustomVo findByAccountNameAndBranchId(String accountName, long branchId) {
		return accountCustomRepository.findByAccountNameAndBranchId(accountName, branchId);
	}
	
	@Override
	public List<AccountCustomVo> findByBranchIdAndGroup(long branchId, long groupId) {
		return accountCustomRepository.findByBranchIdAndGroupAccountGroupIdAndIsDeletedOrderByAccountCustomIdDesc(branchId, groupId, 0);
	}

	@Override
	public List<AccountCustomVo> findByBranchIdAndIsDeletedOrderByGroup(long branchId) {
		return accountCustomRepository.findByBranchIdAndIsDeletedOrderByGroupAccountGroupIdDescAccountCustomIdDesc(branchId, 0);
	}
	
	@Override
	public List<AccountCustomVo> findByBranchIdAndAccounTypesNotIn(long branchId,List<String> list) {
		return accountCustomRepository.findByBranchIdAndAccounTypesNotIn(branchId, list);
	}
	
	@Override
	public DataTablesOutput<AccountCustomVo> findAllAccountDatatable(DataTablesInput input,
			Specification<AccountCustomVo> additionalSpecification, Specification<AccountCustomVo> specification) {
		return accountCustomRepository.findAll(input, null, specification);
	}
	
	//Account Group Method implementation
	
	@Override
	public List<AccountGroupVo> findAllAccountGroup() {
		
		return accountGroupRepository.findAll();
	}

	@Override
	public AccountGroupVo findByAccountGroupId(long accountGroupId) {
		return accountGroupRepository.findById(accountGroupId).orElse(null);
	}

	//AccountVo Method
	@Override
	public List<AccountVo> findAllAccount() {
		return accountRepository.findAll();
	}
	
	@Override
	public void deleteAccountCustom(long accountCustomId, long userId, String modifiedOn) {
		accountCustomRepository.delete(accountCustomId, userId, modifiedOn);
	}
}
