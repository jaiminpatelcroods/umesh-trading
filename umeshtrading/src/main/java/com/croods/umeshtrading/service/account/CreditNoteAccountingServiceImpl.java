package com.croods.umeshtrading.service.account;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.repository.account.CreditNoteAccountingRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.transaction.TransactionService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.util.TransactionAmount;
import com.croods.umeshtrading.vo.account.CreditNoteAccountingVo;
import com.croods.umeshtrading.vo.prefix.PrefixVo;
import com.croods.umeshtrading.vo.transaction.TransactionVo;

@Service
@Transactional
public class CreditNoteAccountingServiceImpl implements CreditNoteAccountingService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	CreditNoteAccountingRepository creditNoteAccountingRepository;
	
	@Autowired
	TransactionService transactionService;
	
	@Override
	public List<CreditNoteAccountingVo> findByBranchIdAndTransactionDateBetween(long branchId, Date transctionDateStart,
			Date transctionDateEnd) {
		return creditNoteAccountingRepository.findByBranchIdAndIsDeletedAndTransactionDateBetween(branchId, 0, transctionDateStart, transctionDateEnd);
	}

	@Override
	public CreditNoteAccountingVo findByCreditNoteAccountIdAndBranchId(long creditNoteAccountId, long branchId) {
		return creditNoteAccountingRepository.findByCreditNoteAccountIdAndBranchIdAndIsDeleted(creditNoteAccountId, branchId, 0);
	}

	@Override
	public void delete(long id, long alterBy, String modifiedOn) {
		creditNoteAccountingRepository.delete(id, alterBy, modifiedOn);
		
	}

	@Override
	public long findMaxVoucherNo(String type, long companyId, long branchId, long userId, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = creditNoteAccountingRepository.findMaxVoucherNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public CreditNoteAccountingVo save(CreditNoteAccountingVo creditNoteAccountingVo) {
		creditNoteAccountingVo = creditNoteAccountingRepository.saveAndFlush(creditNoteAccountingVo);
		entityManager.refresh(creditNoteAccountingVo);
		
		return creditNoteAccountingVo;
	}

	@Override
	public void saveTransation(CreditNoteAccountingVo creditNoteAccountingVo) {
		
		transactionService.deleteTransaction(creditNoteAccountingVo.getBranchId(), creditNoteAccountingVo.getCreditNoteAccountId(), Constant.ACCOUNTING_CREDIT_NOTE);
		
		TransactionVo transactionVo = new TransactionVo();
		
		//Transaction Entry For From Account
        transactionVo.setTransactionDate(creditNoteAccountingVo.getTransactionDate());
        transactionVo.setBranchId(creditNoteAccountingVo.getBranchId());
        transactionVo.setCompanyId(creditNoteAccountingVo.getCompanyId());

        transactionVo.setVoucherType(Constant.ACCOUNTING_CREDIT_NOTE);
        transactionVo.setVoucherId(creditNoteAccountingVo.getCreditNoteAccountId());
        transactionVo.setVoucherNo(String.valueOf(creditNoteAccountingVo.getVoucherNo()));
        transactionVo.setDescription(""+creditNoteAccountingVo.getPrefix()+creditNoteAccountingVo.getVoucherNo());
       
        transactionVo.setAccountCustomVo(creditNoteAccountingVo.getFromAccountCustomVo());
        transactionVo.setAccountGroupVo(creditNoteAccountingVo.getFromAccountCustomVo().getGroup());
        
        transactionService.save(transactionVo, creditNoteAccountingVo.getAmount(), TransactionAmount.DEBIT);
        //Transaction Entry For From Account
        
        //Transaction Entry For To Account
        transactionVo = new TransactionVo();
        
        transactionVo.setTransactionDate(creditNoteAccountingVo.getTransactionDate());
        transactionVo.setBranchId(creditNoteAccountingVo.getBranchId());
        transactionVo.setCompanyId(creditNoteAccountingVo.getCompanyId());

        transactionVo.setVoucherType(Constant.ACCOUNTING_CREDIT_NOTE);
        transactionVo.setVoucherId(creditNoteAccountingVo.getCreditNoteAccountId());
        transactionVo.setVoucherNo(String.valueOf(creditNoteAccountingVo.getVoucherNo()));
        transactionVo.setDescription(""+creditNoteAccountingVo.getPrefix()+creditNoteAccountingVo.getVoucherNo());
       
        transactionVo.setAccountCustomVo(creditNoteAccountingVo.getFromAccountCustomVo());
        transactionVo.setAccountGroupVo(creditNoteAccountingVo.getFromAccountCustomVo().getGroup());
        
        transactionService.save(transactionVo, creditNoteAccountingVo.getAmount(), TransactionAmount.CREDIT);
        //Transaction Entry For To Account
		
	}
	
}
