package com.croods.umeshtrading.service.account;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.account.DebitNoteAccountingVo;

public interface DebitNoteAccountingService {
	
	public DebitNoteAccountingVo save(DebitNoteAccountingVo debitNoteAccountingVo);
	
	public List<DebitNoteAccountingVo> findByBranchIdAndTransactionDateBetween(long branchId, Date transctionDateStart, Date transctionDateEnd);
	
	public DebitNoteAccountingVo findByDebitNoteAccountIdAndBranchId(long debitNoteAccountId, long branchId);
	
	public void delete(long id, long alterBy, String modifiedOn);
	
	public long findMaxVoucherNo(String type, long companyId, long branchId, long userId, String defaultPrefix);
	
	public void saveTransation(DebitNoteAccountingVo debitNoteAccountingVo);
}
