package com.croods.umeshtrading.service.account;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.repository.account.DebitNoteAccountingRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.transaction.TransactionService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.util.TransactionAmount;
import com.croods.umeshtrading.vo.account.DebitNoteAccountingVo;
import com.croods.umeshtrading.vo.prefix.PrefixVo;
import com.croods.umeshtrading.vo.transaction.TransactionVo;

@Service
@Transactional
public class DebitNoteAccountingServiceImpl implements DebitNoteAccountingService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	DebitNoteAccountingRepository debitNoteAccountingRepository;
	
	@Autowired
	TransactionService transactionService;
	
	@Override
	public List<DebitNoteAccountingVo> findByBranchIdAndTransactionDateBetween(long branchId, Date transctionDateStart,
			Date transctionDateEnd) {
		return debitNoteAccountingRepository.findByBranchIdAndIsDeletedAndTransactionDateBetween(branchId, 0, transctionDateStart, transctionDateEnd);
	}

	@Override
	public DebitNoteAccountingVo findByDebitNoteAccountIdAndBranchId(long debitNoteAccountId, long branchId) {
		return debitNoteAccountingRepository.findByDebitNoteAccountIdAndBranchIdAndIsDeleted(debitNoteAccountId, branchId, 0);
	}

	@Override
	public void delete(long id, long alterBy, String modifiedOn) {
		debitNoteAccountingRepository.delete(id, alterBy, modifiedOn);
		
	}

	@Override
	public DebitNoteAccountingVo save(DebitNoteAccountingVo debitNoteAccountingVo) {
		debitNoteAccountingVo = debitNoteAccountingRepository.saveAndFlush(debitNoteAccountingVo);
		entityManager.refresh(debitNoteAccountingVo);
		
		return debitNoteAccountingVo;
	}

	@Override
	public long findMaxVoucherNo(String type, long companyId, long branchId, long userId, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = debitNoteAccountingRepository.findMaxVoucherNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public void saveTransation(DebitNoteAccountingVo debitNoteAccountingVo) {
		
		transactionService.deleteTransaction(debitNoteAccountingVo.getBranchId(), debitNoteAccountingVo.getDebitNoteAccountId(), Constant.ACCOUNTING_DEBIT_NOTE);
        
		TransactionVo transactionVo = new TransactionVo();
        //Transaction Entry For From Account
        transactionVo.setTransactionDate(debitNoteAccountingVo.getTransactionDate());
        transactionVo.setBranchId(debitNoteAccountingVo.getBranchId());
        transactionVo.setCompanyId(debitNoteAccountingVo.getCompanyId());

        transactionVo.setVoucherType(Constant.ACCOUNTING_DEBIT_NOTE);
        transactionVo.setVoucherId(debitNoteAccountingVo.getDebitNoteAccountId());
        transactionVo.setVoucherNo(String.valueOf(debitNoteAccountingVo.getVoucherNo()));

      
        transactionVo.setAccountCustomVo(debitNoteAccountingVo.getFromAccountCustomVo());
        transactionVo.setAccountGroupVo(debitNoteAccountingVo.getFromAccountCustomVo().getGroup());
        transactionVo.setDescription(""+debitNoteAccountingVo.getPrefix()+debitNoteAccountingVo.getVoucherNo());
        
        transactionService.save(transactionVo, debitNoteAccountingVo.getAmount(), TransactionAmount.CREDIT);
        //End Transaction Entry For From Account
        
        //Transaction Entry For To Account
        transactionVo = new TransactionVo();
        
        transactionVo.setTransactionDate(debitNoteAccountingVo.getTransactionDate());
        transactionVo.setBranchId(debitNoteAccountingVo.getBranchId());
        transactionVo.setCompanyId(debitNoteAccountingVo.getCompanyId());

        transactionVo.setVoucherType(Constant.ACCOUNTING_DEBIT_NOTE);
        transactionVo.setVoucherId(debitNoteAccountingVo.getDebitNoteAccountId());
        transactionVo.setVoucherNo(String.valueOf(debitNoteAccountingVo.getVoucherNo()));

      
        transactionVo.setAccountCustomVo(debitNoteAccountingVo.getToAccountCustomVo());
        transactionVo.setAccountGroupVo(debitNoteAccountingVo.getToAccountCustomVo().getGroup());
        transactionVo.setDescription(""+debitNoteAccountingVo.getPrefix()+debitNoteAccountingVo.getVoucherNo());
        transactionService.save(transactionVo, debitNoteAccountingVo.getAmount(), TransactionAmount.DEBIT);
        //End Transaction Entry For To Account
	}

}
