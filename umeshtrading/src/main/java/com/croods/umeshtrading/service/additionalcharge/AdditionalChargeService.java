package com.croods.umeshtrading.service.additionalcharge;

import java.util.List;

import com.croods.umeshtrading.vo.additionalcharge.AdditionalChargeVo;

public interface AdditionalChargeService {
	
	public void save(AdditionalChargeVo additionalChargeVo);
	
	AdditionalChargeVo findByAdditionalChargeId(long additionalChargeId);
	
	List<AdditionalChargeVo> findByCompanyId(long companyId);
	
	void delete(long additionalChargeId, long alterBy, String modifiedOn);
	
}
