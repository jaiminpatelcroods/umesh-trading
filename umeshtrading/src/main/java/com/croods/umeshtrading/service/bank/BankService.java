package com.croods.umeshtrading.service.bank;

import java.util.Date;
import java.util.List;

import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.bank.BankTransactionVo;
import com.croods.umeshtrading.vo.bank.BankVo;

public interface BankService {
	
	//BankVo Method
	public BankVo saveBank(BankVo bankVo);
	
	public List<BankVo> findByBranchId(long branchId);
	
	public BankVo findByBankIdAndBranchId(long bankId, long branchId);
	
	public void deleteBank(long bankId, long alterBy, String modifiedOn);	
	
	public BankVo findByBankId(long bankId);
	
	public AccountCustomVo findAccountCustomVoByBankId(long bankId);
	
	//BankTransactionVo Method
	public BankTransactionVo saveBankTransaction(BankTransactionVo bankTransactionVo);
	
	public List<BankTransactionVo> findByBranchIdAndTransactionDateBetween(long branchId, Date transactionDateStart, Date transactionDateEnd);
	
	public BankTransactionVo findByBankTransactionIdAndBranchId(long bankTransactionId, long branchId);
	
	public void deleteBankTransaction(long bankTransactionId, long alterBy, String modifiedOn);
	
	public long findMaxVoucherNo(String type, long companyId, long branchId, long userId, String defaultPrefix);
	
	public void saveTransaction(BankTransactionVo bankTransactionVo);
}
