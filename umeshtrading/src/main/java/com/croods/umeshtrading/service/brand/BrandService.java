package com.croods.umeshtrading.service.brand;

import java.util.List;

import com.croods.umeshtrading.vo.brand.BrandVo;

public interface BrandService {
	
	public void save(BrandVo brandVo);
	
	public BrandVo findByBrandId(long brandId);
	
	public void deleteBrand(long brandId, long alterBy, String modifiedOn);
	
	public List<BrandVo> findByCompanyId(long companyId);

	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	public void updateIsDefaultByBrandId(long brandId, long alterBy, int isDefault, String modifiedOn);

}
