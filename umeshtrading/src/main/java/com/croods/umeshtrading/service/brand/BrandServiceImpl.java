package com.croods.umeshtrading.service.brand;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.brand.BrandRepository;
import com.croods.umeshtrading.vo.brand.BrandVo;

@Service
@Transactional
public class BrandServiceImpl implements BrandService {

	@Autowired
	BrandRepository brandRepository;
	
	@Override
	public BrandVo findByBrandId(long brandId) {
		return brandRepository.findByBrandId(brandId);
	}

	@Override
	public void deleteBrand(long brandId, long alterBy, String modifiedOn) {
		brandRepository.deleteBrand(brandId, alterBy, modifiedOn);
	}

	@Override
	public List<BrandVo> findByCompanyId(long companyId) {
		return brandRepository.findByCompanyIdAndIsDeletedOrderByBrandIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn) {
		brandRepository.updateIsDefaultByCompanyId(companyId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void updateIsDefaultByBrandId(long brandId, long alterBy, int isDefault, String modifiedOn) {
		brandRepository.updateIsDefaultByBrandId(brandId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void save(BrandVo brandVo) {
		brandRepository.save(brandVo);
	}
	
	

}
