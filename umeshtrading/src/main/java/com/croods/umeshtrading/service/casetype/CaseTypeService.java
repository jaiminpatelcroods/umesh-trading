package com.croods.umeshtrading.service.casetype;

import java.util.List;

import com.croods.umeshtrading.vo.casetype.CaseTypeVo;

public interface CaseTypeService {

	void save(CaseTypeVo caseTypeVo);
	
	CaseTypeVo findByCaseTypeId(long caseTypeId);
	
	void deleteCaseType(long caseTypeId, long alterBy, String modifiedOn);

	List<CaseTypeVo> findByCompanyId(long companyId);
	
	void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);
	
	void updateIsDefaultByCaseTypeId(long caseTypeId, long alterBy, int isDefault, String modifiedOn);
	
}
