package com.croods.umeshtrading.service.casetype;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.casetype.CaseTypeRepository;
import com.croods.umeshtrading.vo.casetype.CaseTypeVo;

@Service
@Transactional
public class CaseTypeServiceImpl implements CaseTypeService {

	@Autowired
	CaseTypeRepository caseTypeRepository;
	
	@Override
	public void save(CaseTypeVo caseTypeVo) {
		caseTypeRepository.save(caseTypeVo);
	}

	@Override
	public CaseTypeVo findByCaseTypeId(long caseTypeId) {
		return caseTypeRepository.findByCaseTypeId(caseTypeId);
	}

	@Override
	public void deleteCaseType(long caseTypeId, long alterBy, String modifiedOn) {
		caseTypeRepository.deleteCaseType(caseTypeId, alterBy, modifiedOn);
	}

	@Override
	public List<CaseTypeVo> findByCompanyId(long companyId) {
		return caseTypeRepository.findByCompanyIdAndIsDeletedOrderByCaseTypeIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn) {
		caseTypeRepository.updateIsDefaultByCompanyId(companyId, alterBy, isDefault, modifiedOn);
	}

	@Override
	public void updateIsDefaultByCaseTypeId(long caseTypeId, long alterBy, int isDefault, String modifiedOn) {
		caseTypeRepository.updateIsDefaultByCaseTypeId(caseTypeId, alterBy, isDefault, modifiedOn);
	}

}
