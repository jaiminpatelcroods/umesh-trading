package com.croods.umeshtrading.service.category;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.category.CategoryRepository;
import com.croods.umeshtrading.vo.category.CategoryVo;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	
	@Override
	public CategoryVo findByCategoryId(long categoryid) {
		return categoryRepository.findByCategoryId(categoryid);
	}

	@Override
	public void deleteCategory(long categoryId, long alterBy, String modifiedOn) {
		categoryRepository.deleteCategory(categoryId, alterBy, modifiedOn);
		
	}

	@Override
	public List<CategoryVo> findByCompanyId(long companyId) {
		return categoryRepository.findByCompanyIdAndIsDeletedOrderByCategoryIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn) {
		categoryRepository.updateIsDefaultByCompanyId(companyId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void updateIsDefaultByCategoryId(long categoryId, long alterBy, int isDefault, String modifiedOn) {
		categoryRepository.updateIsDefaultByCategoryId(categoryId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void save(CategoryVo categoryVo) {
		categoryRepository.save(categoryVo);
	}
	
}
