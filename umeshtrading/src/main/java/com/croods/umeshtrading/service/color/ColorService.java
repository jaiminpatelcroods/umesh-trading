package com.croods.umeshtrading.service.color;

import java.util.List;

import com.croods.umeshtrading.vo.color.ColorVo;

public interface ColorService {
	
	public void save(ColorVo colorVo);
	
	ColorVo findByColorId(long colorId);
	
	void delete(long colorId, long alterBy, String modifiedOn);

	List<ColorVo> findByCompanyId(long companyId);
}
