package com.croods.umeshtrading.service.color;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.color.ColorRepository;
import com.croods.umeshtrading.vo.color.ColorVo;

@Service
@Transactional
public class ColorServiceImpl implements ColorService {

	@Autowired
	ColorRepository colorRepository;
	
	@Override
	public void save(ColorVo colorVo) {
		colorRepository.save(colorVo);
	}
	@Override
	public ColorVo findByColorId(long colorId) {
		return colorRepository.findByColorId(colorId);
	}

	@Override
	public void delete(long colorId, long alterBy, String modifiedOn) {
		colorRepository.delete(colorId, alterBy, modifiedOn);
	}

	@Override
	public List<ColorVo> findByCompanyId(long companyId) {
		return colorRepository.findByCompanyIdAndIsDeletedOrderByColorIdDesc(companyId, 0);
	}

}
