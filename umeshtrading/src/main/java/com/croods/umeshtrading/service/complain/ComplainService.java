package com.croods.umeshtrading.service.complain;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.umeshtrading.vo.complain.ComplainVo;

public interface ComplainService {

	public ComplainVo save(ComplainVo complainVo);
	
	long findMaxComplainNo(long companyId, long branchId, long userId, String type, String prefix);

	public ComplainVo findByComplainIdAndBranchId(long complainId, long branchId);
	
	public DataTablesOutput<ComplainVo> findAll(DataTablesInput input,
			Specification<ComplainVo> additionalSpecification, Specification<ComplainVo> specification);

	public void delete(long complainId, long alterBy, String modifiedOn);
}
