package com.croods.umeshtrading.service.contact;

import java.util.List;

import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.contact.ContactAddressVo;
import com.croods.umeshtrading.vo.contact.ContactVo;

public interface ContactService {

	public void saveContact(ContactVo contact);
	
	public List<ContactVo> findByBranchIdAndType(long branchId, String type);
	
	public ContactVo findByContactIdAndBranchIdAndType(long contactId, long branchId, String type);
	
	public AccountCustomVo findAccountCustomVoByContactId(long contactId);

	public void deleteContactAddressByIdIn(List<Long> contactAddressIds);
	
	public void deleteContactOtherByIdIn(List<Long> contactOtherIds);

	public void deleteContact(long contactId, long userId, String modifiedOn);

	public void changeDefaultAddress(long contactId, long contactAddressId);
	
	public ContactAddressVo findAddressByContactAddressId(long contactAddressId);
	
	public List<ContactVo> contactList(long companyId, String type) ;

	
	
	public long countByCompanyIdAndType(long companyId, String contactType);

	public long countByCompanyIdAndContactTypeAndIsDeleted(long companyId, String contactCategory, int isDeleted);
	
}
