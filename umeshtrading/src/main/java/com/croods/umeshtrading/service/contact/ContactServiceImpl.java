package com.croods.umeshtrading.service.contact;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.contact.ContactAddressRepository;
import com.croods.umeshtrading.repository.contact.ContactOtherRepository;
import com.croods.umeshtrading.repository.contact.ContactRepository;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.contact.ContactAddressVo;
import com.croods.umeshtrading.vo.contact.ContactVo;

@Service
@Transactional
public class ContactServiceImpl implements ContactService {

	@Autowired
	ContactRepository contactRepository;
	
	@Autowired
	ContactAddressRepository contactAddressRepository;
	
	@Autowired
	ContactOtherRepository contactOtherRepository;
	
	@Override
	public void saveContact(ContactVo contactVo) {
		contactRepository.save(contactVo);
	}

	@Override
	public List<ContactVo> findByBranchIdAndType(long branchId, String type) {
		return contactRepository.findByBranchIdAndTypeAndIsDeletedOrderByContactIdDesc(branchId, type, 0);
	}

	@Override
	public ContactVo findByContactIdAndBranchIdAndType(long contactId, long branchId, String type) {
		return contactRepository.findByContactIdAndBranchIdAndTypeAndIsDeleted(contactId, branchId, type, 0);
	}

	@Override
	public AccountCustomVo findAccountCustomVoByContactId(long contactId) {
		return contactRepository.findAccountCustomVoByContactId(contactId);
	}

	@Override
	public void deleteContactAddressByIdIn(List<Long> contactAddressIds) {
		contactAddressRepository.deleteContactAddressByIdIn(contactAddressIds);
	}
	
	@Override
	public void deleteContactOtherByIdIn(List<Long> contactOtherIds) {
		contactOtherRepository.deleteContactOtherByIdIn(contactOtherIds);
	}

	@Override
	public void deleteContact(long contactId, long userId, String modifiedOn) {
		contactRepository.deleteContact(contactId, userId, modifiedOn);
		
	}

	@Override
	public void changeDefaultAddress(long contactId, long contactAddressId) {
		contactAddressRepository.updateIsDefaultByContactId(contactId);
		contactAddressRepository.updateIsDefaultByContactIdAndContactAddressId(contactId, contactAddressId);
	}

	@Override
	public ContactAddressVo findAddressByContactAddressId(long contactAddressId) {
		return contactAddressRepository.findByContactAddressId(contactAddressId);
	}
	
	@Override
	public List<ContactVo> contactList(long CompanyId, String type) {
		return contactRepository.findByCompanyIdAndTypeAndIsDeleted(CompanyId, type, 0);
	}

	@Override
	public long countByCompanyIdAndType(long companyId, String contactType) {
		return contactRepository.countByCompanyIdAndTypeAndIsDeleted(companyId,contactType,0);
	}

	@Override
	public long countByCompanyIdAndContactTypeAndIsDeleted(long companyId, String contactCategory, int isDeleted) {
		return contactRepository.countByCompanyIdAndContactTypeAndIsDeleted(companyId,contactCategory,isDeleted);
	}
	
}
