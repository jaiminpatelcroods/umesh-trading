package com.croods.umeshtrading.service.deliverychallan;

import java.util.List;

import com.croods.umeshtrading.vo.deliverychallan.DeliveryChallanVo;

public interface DeliveryChallanService {

	public long findMaxDeliveryChallanNo(long companyId, long branchId, long userId, String type, String prefix);

	public DeliveryChallanVo save(DeliveryChallanVo deliveryChallanVo);

	public DeliveryChallanVo findByDeliveryChallanIdAndBranchId(long deliveryChallanId, long branchId);

	public void deleteDeliveryChallanItemIds(List<Long> l);
}
