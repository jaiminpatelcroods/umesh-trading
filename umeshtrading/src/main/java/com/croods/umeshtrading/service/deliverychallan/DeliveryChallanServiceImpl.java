package com.croods.umeshtrading.service.deliverychallan;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.deliverychallan.DeliveryChallanItemRepository;
import com.croods.umeshtrading.repository.deliverychallan.DeliveryChallanRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.deliverychallan.DeliveryChallanVo;
import com.croods.umeshtrading.vo.prefix.PrefixVo;

@Service
@Transactional
public class DeliveryChallanServiceImpl implements DeliveryChallanService {

	@Autowired
	DeliveryChallanRepository deliveryChallanRepository;
	
	@Autowired
	DeliveryChallanItemRepository deliveryChallanItemRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public long findMaxDeliveryChallanNo(long companyId, long branchId, long userId, String type, String prefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(prefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = deliveryChallanRepository.findMaxDeliveryChallanNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public DeliveryChallanVo save(DeliveryChallanVo deliveryChallanVo) {
		return deliveryChallanRepository.save(deliveryChallanVo);
	}

	@Override
	public DeliveryChallanVo findByDeliveryChallanIdAndBranchId(long deliveryChallanId, long branchId) {
		return deliveryChallanRepository.findByDeliveryChallanIdAndBranchIdAndIsDeleted(deliveryChallanId, branchId, 0);
	}

	@Override
	public void deleteDeliveryChallanItemIds(List<Long> l) {
		deliveryChallanItemRepository.deleteDeliveryChallanItemIds(l);
		
	}
}
