package com.croods.umeshtrading.service.department;

import java.util.List;

import com.croods.umeshtrading.vo.department.DepartmentVo;

public interface DepartmentService {

	public void save(DepartmentVo departmentVo);
	
	public DepartmentVo findByDepartmentId(long departmentId);
	
	public void deleteDepartment(long departmentId, long alterBy, String modifiedOn);

	public List<DepartmentVo> findByCompanyId(long companyId);
	
}
