package com.croods.umeshtrading.service.department;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.department.DepartmentRepository;
import com.croods.umeshtrading.vo.department.DepartmentVo;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired
	DepartmentRepository departmentRepository;
	
	@Override
	public DepartmentVo findByDepartmentId(long departmentId) {
		return departmentRepository.findByDepartmentId(departmentId);
	}

	
	@Override
	public void deleteDepartment(long departmentId, long alterBy, String modifiedOn) {
		departmentRepository.deleteDepartment(departmentId, alterBy, modifiedOn);
		
	}

	@Override
	public List<DepartmentVo> findByCompanyId(long companyId) {
		return departmentRepository.findByCompanyIdAndIsDeletedOrderByDepartmentIdDesc(companyId, 0);
	}


	@Override
	public void save(DepartmentVo departmentVo) {
		departmentRepository.save(departmentVo);
	}

}
