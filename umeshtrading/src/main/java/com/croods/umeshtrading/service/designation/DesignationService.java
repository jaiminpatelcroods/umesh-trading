package com.croods.umeshtrading.service.designation;

import java.util.List;

import com.croods.umeshtrading.vo.designation.DesignationVo;

public interface DesignationService {

	public void save(DesignationVo designationVo);
	
	DesignationVo findByDesignationId(long designationId);
	
	void deleteDesignation(long designationId, long alterBy, String modifiedOn);

	List<DesignationVo> findByCompanyId(long companyId);
	
}
