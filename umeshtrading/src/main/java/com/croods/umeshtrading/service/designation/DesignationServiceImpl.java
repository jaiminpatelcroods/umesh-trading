package com.croods.umeshtrading.service.designation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.designation.DesignationRepository;
import com.croods.umeshtrading.vo.designation.DesignationVo;

@Service
@Transactional
public class DesignationServiceImpl implements DesignationService{

	@Autowired
	DesignationRepository designationRepository;
	
	@Override
	public DesignationVo findByDesignationId(long designationId) {
		return designationRepository.findByDesignationId(designationId);
	}

	
	@Override
	public void deleteDesignation(long designationId, long alterBy, String modifiedOn) {
		designationRepository.deleteDesignation(designationId, alterBy, modifiedOn);
		
	}

	@Override
	public List<DesignationVo> findByCompanyId(long companyId) {
		return designationRepository.findByCompanyIdAndIsDeletedOrderByDesignationIdDesc(companyId, 0);
	}

	@Override
	public void save(DesignationVo designationVo) {
		designationRepository.save(designationVo);
	}
}
