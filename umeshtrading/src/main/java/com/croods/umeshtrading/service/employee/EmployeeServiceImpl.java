package com.croods.umeshtrading.service.employee;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.employee.EmployeeContactRepository;
import com.croods.umeshtrading.repository.employee.EmployeeRepository;
import com.croods.umeshtrading.vo.employee.EmployeeVo;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	EmployeeContactRepository employeeContactRepository;
	
	@Override
	public EmployeeVo findByEmployeeIdAndBranchId(long employeeId, long branchId) {
		return employeeRepository.findByEmployeeIdAndBranchIdAndIsDeleted(employeeId, branchId, 0);
	}

	@Override
	public List<EmployeeVo> findByBranchId(long branchId) {
		return employeeRepository.findByBranchIdAndIsDeletedOrderByEmployeeIdDesc(branchId, 0);
	}

	@Override
	public void deleteEmployee(long employeeId, long userId, String modifiedOn) {
		employeeRepository.deleteEmployee(employeeId, userId, modifiedOn);
	}

	@Override
	public void save(EmployeeVo employeeVo) {
		employeeRepository.save(employeeVo);
	}

	@Override
	public void deleteEmployeeContactByIdIn(List<Long> employeeContactIds) {
		employeeContactRepository.deleteEmployeeContactByIdIn(employeeContactIds);
	}

}
