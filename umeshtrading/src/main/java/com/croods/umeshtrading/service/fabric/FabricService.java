package com.croods.umeshtrading.service.fabric;

import java.util.List;

import com.croods.umeshtrading.vo.fabric.FabricVo;

public interface FabricService {
	
	public void save(FabricVo fabricVo);
	
	public FabricVo findByFabricId(long fabricId);
	
	public void delete(long fabricId, long alterBy, String modifiedOn);

	public List<FabricVo> findByCompanyId(long companyId);
}
