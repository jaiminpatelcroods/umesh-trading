package com.croods.umeshtrading.service.financial;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.croods.umeshtrading.vo.financial.FinancialMonth;
import com.croods.umeshtrading.vo.financial.FinancialYear;
import com.croods.umeshtrading.vo.financial.YearWiseOpeningBalanceVo;

public interface FinancialService {

	//MonthInterval Method
	public String findByMonthInterval(@Param("monthInterval")String monthInterval);
	
	public List<FinancialMonth> findAllFinancialMonth();
	
	//FinancialYear Method
	public List<FinancialYear> findAllFinancialYear();
	
	//YearWiseOpeningBalanceVo Method
	YearWiseOpeningBalanceVo findByBranchIdAndAccountCustomIdAndYearInterval(long branchId,long accountCustomId,String yearInterval);
	
	YearWiseOpeningBalanceVo findByOpeningBalanceId(long Id);
	
	YearWiseOpeningBalanceVo saveYearWiseOpeningBalance(YearWiseOpeningBalanceVo yearWiseOpeningBalanceVo);
}
