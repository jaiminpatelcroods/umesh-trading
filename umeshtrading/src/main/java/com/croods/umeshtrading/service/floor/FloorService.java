package com.croods.umeshtrading.service.floor;

import java.util.List;

import com.croods.umeshtrading.vo.floor.FloorVo;

public interface FloorService {

	public FloorVo findByFloorId(long floorId);
	
	public List<FloorVo> findByCompanyId(long companyId);

	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	public void updateIsDefaultByFloorId(long floorId, long alterBy, int isDefault, String modifiedOn);
	
	public void save(FloorVo floorVo);
	
	public void delete(long floorId, long alterBy, String modifiedOn);

	//public FloorVo findByPlaceId(long placeId);

	//public FloorVo findByPlaceVo(long id);

	public List<FloorVo> findByPlaceVoPlaceId(long id);
}
