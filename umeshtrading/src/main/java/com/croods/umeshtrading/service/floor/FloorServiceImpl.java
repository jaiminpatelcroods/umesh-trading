package com.croods.umeshtrading.service.floor;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.floor.FloorRepository;
import com.croods.umeshtrading.vo.floor.FloorVo;

@Service
@Transactional
public class FloorServiceImpl implements FloorService {

	@Autowired
	FloorRepository floorRepository;
	
	@Override
	public FloorVo findByFloorId(long floorId) {
		return floorRepository.findByFloorId(floorId);
	}

	@Override
	public List<FloorVo> findByCompanyId(long companyId) {
		return floorRepository.findByCompanyIdAndIsDeletedOrderByFloorIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn) {
		floorRepository.updateIsDefaultByCompanyId(companyId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void updateIsDefaultByFloorId(long floorId, long alterBy, int isDefault, String modifiedOn) {
		floorRepository.updateIsDefaultByFloorId(floorId, alterBy, isDefault, modifiedOn);
		
	}
	
	@Override
	public void save(FloorVo floorVo) {
		floorRepository.save(floorVo);
	}
	
	@Override
	public void delete(long floorId, long alterBy, String modifiedOn) {
		floorRepository.delete(floorId, alterBy, modifiedOn);
	}
	
	/*
	 * @Override public FloorVo findByPlaceId(long placeId) { return
	 * floorRepository.findByPlaceId(placeId); }
	 */

	/*@Override
	public FloorVo findByPlaceVo(long id) {
		return floorRepository.findByPlaceVo(id);
	}*/

	@Override
	public List<FloorVo> findByPlaceVoPlaceId(long id) {
		return floorRepository.findByPlaceVoPlaceId(id);
	}
	
}
