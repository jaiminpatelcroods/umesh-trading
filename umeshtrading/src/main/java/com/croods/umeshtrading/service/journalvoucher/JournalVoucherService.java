package com.croods.umeshtrading.service.journalvoucher;

import java.util.Date;
import java.util.List;

import com.croods.umeshtrading.vo.journalvoucher.JournalVoucherVo;

public interface JournalVoucherService {

	//JournalVoucherVo Method
	public JournalVoucherVo save(JournalVoucherVo journalVoucherVo);
	
	List<JournalVoucherVo> findByBranchIdAndVoucherDateBetween(long branchId, Date voucherDateStart, Date voucherDateEnd);
	
	public long findMaxVoucherNo(String type, long companyId, long branchId, long userId, String defaultPrefix);
	
	JournalVoucherVo findByJournalVoucherIdAndBranchId(long journalVoucherId, long branchId);
	
	void delete(long id, long alterBy, String modifiedOn);
	
	//JournalVoucherAccountVo Method
	void deleteJournalVoucherAccountByIdIn(List<Long> journalVoucherAccountIds);
	
	public void saveTransation(JournalVoucherVo journalVoucher);
}
