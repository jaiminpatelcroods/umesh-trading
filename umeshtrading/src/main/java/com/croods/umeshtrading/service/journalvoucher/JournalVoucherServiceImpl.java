package com.croods.umeshtrading.service.journalvoucher;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.repository.journalvoucher.JournalVoucherAccountRepository;
import com.croods.umeshtrading.repository.journalvoucher.JournalVoucherRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.transaction.TransactionService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.util.TransactionAmount;
import com.croods.umeshtrading.vo.journalvoucher.JournalVoucherAccountVo;
import com.croods.umeshtrading.vo.journalvoucher.JournalVoucherVo;
import com.croods.umeshtrading.vo.prefix.PrefixVo;
import com.croods.umeshtrading.vo.transaction.TransactionVo;

@Service
@Transactional
public class JournalVoucherServiceImpl implements JournalVoucherService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	JournalVoucherRepository journalVoucherRepository;
	
	@Autowired
	JournalVoucherAccountRepository journalVoucherAccountRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	TransactionService transactionService;
	
	@Override
	public JournalVoucherVo save(JournalVoucherVo journalVoucherVo) {
		
		journalVoucherVo = journalVoucherRepository.saveAndFlush(journalVoucherVo);
		entityManager.refresh(journalVoucherVo);
		
		return journalVoucherVo;
	}

	@Override
	public List<JournalVoucherVo> findByBranchIdAndVoucherDateBetween(long branchId, Date voucherDateStart,
			Date voucherDateEnd) {
		return journalVoucherRepository.findByBranchIdAndIsDeletedAndVoucherDateBetween(branchId, 0, voucherDateStart, voucherDateEnd);
	}

	@Override
	public long findMaxVoucherNo(String type, long companyId, long branchId, long userId, String defaultPrefix)  {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = journalVoucherRepository.findMaxVoucherNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public JournalVoucherVo findByJournalVoucherIdAndBranchId(long journalVoucherId, long branchId) {
		return journalVoucherRepository.findByJournalVoucherIdAndBranchIdAndIsDeleted(journalVoucherId, branchId, 0);
	}

	@Override
	public void delete(long id, long alterBy, String modifiedOn) {
		journalVoucherRepository.delete(id, alterBy, modifiedOn);
		
	}

	@Override
	public void deleteJournalVoucherAccountByIdIn(List<Long> journalVoucherAccountIds) {
		journalVoucherAccountRepository.deleteJournalVoucherAccountByIdIn(journalVoucherAccountIds);
	}

	@Override
	public void saveTransation(JournalVoucherVo journalVoucher) {

		TransactionVo transactionVo = new TransactionVo();
		JournalVoucherAccountVo journalVoucherAccountVo;
		
		transactionService.deleteTransaction(journalVoucher.getBranchId(), journalVoucher.getJournalVoucherId(), Constant.ACCOUNTING_JOURNAL);
		
		for (int i = 0; i < journalVoucher.getJournalVoucherAccountVos().size(); i++) {
			
			journalVoucherAccountVo=journalVoucher.getJournalVoucherAccountVos().get(i);
			
			transactionVo = new TransactionVo();
			
			transactionVo.setTransactionDate(journalVoucher.getVoucherDate());
			transactionVo.setBranchId(journalVoucher.getBranchId());
			transactionVo.setCompanyId(journalVoucher.getCompanyId());
			transactionVo.setDescription(""+journalVoucher.getPrefix()+journalVoucher.getVoucherNo());
			transactionVo.setVoucherType(Constant.ACCOUNTING_JOURNAL);
			transactionVo.setVoucherId(journalVoucher.getJournalVoucherId());
			transactionVo.setVoucherNo(""+journalVoucher.getVoucherNo());
			
			transactionVo.setDescription(""+journalVoucher.getPrefix()+journalVoucher.getVoucherNo());
			
			transactionVo.setAccountGroupVo(journalVoucherAccountVo.getFromAccountCustomVo().getGroup());
			transactionVo.setAccountCustomVo(journalVoucherAccountVo.getFromAccountCustomVo());
			
			if(journalVoucherAccountVo.getDebit() ==  0) {
				transactionService.save(transactionVo, journalVoucherAccountVo.getCredit(), TransactionAmount.CREDIT);
			} else {
				transactionService.save(transactionVo, journalVoucherAccountVo.getDebit(), TransactionAmount.DEBIT);
			}
			
		}
		
	}

}
