package com.croods.umeshtrading.service.lead;

import java.util.List;

import com.croods.umeshtrading.vo.lead.LeadAddressVo;
import com.croods.umeshtrading.vo.lead.LeadVo;

public interface LeadService {

	public void saveLead(LeadVo leadVo);
	
	public List<LeadVo> findByBranchId(long branchId);
	
	public LeadVo findByLeadIdAndBranchId(long leadId, long branchId);
	
	public void deleteLead(long leadId, long userId, String modifiedOn);
	
	public void deleteLeadAddressByIdIn(List<Long> leadAddressIds);
	
	public void deleteLeadContactByIdIn(List<Long> leadContactIds);

	public void changeDefaultAddress(long leadId, long leadAddressId);
	
	public LeadAddressVo findAddressByLeadAddressId(long leadAddressId);
	
}
