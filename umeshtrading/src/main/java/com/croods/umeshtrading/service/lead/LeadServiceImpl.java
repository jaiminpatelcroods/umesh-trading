package com.croods.umeshtrading.service.lead;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.lead.LeadAddressRepository;
import com.croods.umeshtrading.repository.lead.LeadContactRepository;
import com.croods.umeshtrading.repository.lead.LeadRepository;
import com.croods.umeshtrading.vo.lead.LeadAddressVo;
import com.croods.umeshtrading.vo.lead.LeadVo;

@Service
@Transactional
public class LeadServiceImpl implements LeadService {

	@Autowired
	LeadRepository leadRepository;
	
	@Autowired
	LeadAddressRepository leadAddressRepository;
	
	@Autowired
	LeadContactRepository leadContactRepository;

	@Override
	public void saveLead(LeadVo leadVo) {
		leadRepository.save(leadVo);
	}

	@Override
	public List<LeadVo> findByBranchId(long branchId) {
		return leadRepository.findByBranchIdAndIsDeletedOrderByLeadIdDesc(branchId, 0);
	}

	@Override
	public LeadVo findByLeadIdAndBranchId(long leadId, long branchId) {
		return leadRepository.findByLeadIdAndBranchIdAndIsDeleted(leadId, branchId, 0);
	}

	@Override
	public void deleteLead(long leadId, long userId, String modifiedOn) {
		leadRepository.deleteLead(leadId, userId, modifiedOn);
	}

	@Override
	public void deleteLeadAddressByIdIn(List<Long> leadAddressIds) {
		leadAddressRepository.deleteLeadAddressByIdIn(leadAddressIds);
	}

	@Override
	public void deleteLeadContactByIdIn(List<Long> leadContactIds) {
		leadContactRepository.deleteLeadContactByIdIn(leadContactIds);
	}

	@Override
	public void changeDefaultAddress(long leadId, long leadAddressId) {
		leadAddressRepository.updateIsDefaultByLeadId(leadId);
		leadAddressRepository.updateIsDefaultByLeadIdAndLeadAddressId(leadId, leadAddressId);
	}

	@Override
	public LeadAddressVo findAddressByLeadAddressId(long leadAddressId) {
		return leadAddressRepository.findByLeadAddressId(leadAddressId);
	}
	
}
