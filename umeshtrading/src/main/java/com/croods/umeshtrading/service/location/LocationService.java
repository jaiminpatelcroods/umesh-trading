package com.croods.umeshtrading.service.location;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.croods.umeshtrading.vo.location.CityVo;
import com.croods.umeshtrading.vo.location.CountriesVo;
import com.croods.umeshtrading.vo.location.StateVo;

public interface LocationService {

	//CityVo Method
	public List<CityVo> findByStateCode(@Param("id")String stateCode);
	
	public String findCityNameByCityCode(String cityCode);
	
	//CountriesVo Method
	public List<CountriesVo> findAllCountries();
	
	public String findCountriesNameByCountriesCode(String countriesCode);
	
	//StateVo Method
	public List<StateVo> findByCountriesCode(@Param("id")String countriesCode);

	public String findStateNameByStateCode(String stateCode);
	
	//public CountriesVo findCountriesByCountriesCode(String countriesCode);
	
}
