package com.croods.umeshtrading.service.location;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.location.CityRepository;
import com.croods.umeshtrading.repository.location.CountriesRepository;
import com.croods.umeshtrading.repository.location.StateRepository;
import com.croods.umeshtrading.vo.location.CityVo;
import com.croods.umeshtrading.vo.location.CountriesVo;
import com.croods.umeshtrading.vo.location.StateVo;

@Service
public class LocationServiceImpl implements LocationService {

	@Autowired
	CountriesRepository countriesRepository;
	
	@Autowired
	StateRepository stateRepository;
	
	@Autowired
	CityRepository cityRepository;

	//CityVo Method Implementation
	@Override
	public List<CityVo> findByStateCode(String stateCode) {
		return cityRepository.findByStateCode(stateCode);
	}

	@Override
	public String findCityNameByCityCode(String cityCode) {
		return cityRepository.findCityNameByCityCode(cityCode);
	}

	//CountriesVo Method Implementation
	@Override
	public List<CountriesVo> findAllCountries() {
		return countriesRepository.findAll();
	}

	@Override
	public String findCountriesNameByCountriesCode(String countriesCode) {
		return countriesRepository.findCountriesNameByCountriesCode(countriesCode);
	}
	

	//StateVo Method Implementation
	@Override
	public List<StateVo> findByCountriesCode(String countriesCode) {
		return stateRepository.findByCountriesCode(countriesCode);
	}

	@Override
	public String findStateNameByStateCode(String stateCode) {
		return stateRepository.findStateNameByStateCode(stateCode);
	}

	
}
