package com.croods.umeshtrading.service.material;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.croods.umeshtrading.vo.material.MaterialItemVo;
import com.croods.umeshtrading.vo.material.MaterialVo;

public interface MaterialService {
  
	
	  public long findMaxMaterialNo(long companyId, String type, String prefix,long
	  userId);
	  
	  public MaterialVo save(MaterialVo materialVo);
	  
	  public MaterialVo findByMaterialIdAndCompanyId(long materialId, long
	  companyId);
	  
	  void insertMaterialOutTransaction(MaterialVo materialVo, String yearInterval);
	  
	  public void deleteMaterialItem(long companyId, List<Long> l);
	  
//	  public void deleteMaterialAdditionalItem(List<Long> l);
	  
	  public void deleteMaterial(long companyId, long materialId, String type);
	  
//	  public List<MaterialVo>
//	  findByTypeAndCompanyIdAndIsDeletedAndMaterialDateBetween(String type, long
//	  companyId, int isDeleted, Date materialDateStart, Date materialDateEnd);
	  
//	  public List<MaterialVo>
//	  findByTypesAndContactAndCompanyIdAndMaterialDateBetween(List<String> types,
//	  long companyId, Date materialDateStart, Date materialDateEnd, long
//	  contactId);
	  
	  public MaterialItemVo findByMaterialId(long materialId);
	  
	  public void insertMaterialInTransaction(MaterialVo materialVo, String yearInterval);
	  
	  //public void updatePaidAmountMinus(List<Long> l);
	  
	  /*	  public void updatePaidAmountPlus(long materialId, double paidAmount);
	  
	  public List<MaterialVo> getListOfAllUnpaidBillEdit(String type, long
	  companyId, int isDeleted, Date startDate, Date endDate, long contactId);
	  
	  public List<MaterialVo> getListOfAllUnpaidBill(String type, long companyId,
	  int isDeleted, Date startDate, Date endDate, long contactId);
	  
	  public List<Map<Long, Double>> getDueAmountByContactId(List<String> types,
	  long companyId,Date materialDateStart, Date materialDateEnd);
	  
	  public List<Map<Double, Double>>
	  getPaidAndTotalAmountByContactId(List<String> types, long companyId,Date
	  materialDateStart, Date materialDateEnd,long ContactId);
	  
//	  public List<Map<Double, Double>> getPaidAndTotalAmountAll(List<String>
//	  materialTypes, long companyId, Date startDate, Date endDate);
//	  
//	  public long getTotalMaterialOrderQty(long companyId, Date startDate, Date
//	  endDate);
*/	 
	
}
