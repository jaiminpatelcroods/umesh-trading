package com.croods.umeshtrading.service.material;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.repository.material.MaterialItemRepository;
import com.croods.umeshtrading.repository.material.MaterialRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.service.stock.StockTransactionService;
import com.croods.umeshtrading.service.transaction.TransactionService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.account.AccountGroupVo;
import com.croods.umeshtrading.vo.material.MaterialItemVo;
import com.croods.umeshtrading.vo.material.MaterialVo;
import com.croods.umeshtrading.vo.prefix.PrefixVo;
import com.croods.umeshtrading.vo.transaction.TransactionVo;

@Service
@Transactional
public class MaterialServiceImpl implements MaterialService {

	@Autowired
	PrefixService prefixService;

	@Autowired
	MaterialRepository materialRepository;

	@Autowired
	MaterialItemRepository materialItemRepository;

	/*
	 * @Autowired MaterialAdditionalChargeRepository
	 * materialAdditionalChargeRepository;
	 */

	@Autowired
	TransactionService transactionService;

	/*
	 * @Autowired AccountCustomService accountCustomService;
	 */ 
	@Autowired StockTransactionService stockTransactionService;
	 

	@PersistenceContext
	EntityManager entityManager;

	/*
	 * @Autowired ReceiptBillRepository receiptBillRepository;
	 */
	
	  @Override 
	  public long findMaxMaterialNo(long branchId, String type, String prefix, long userId) 
	  {
		  List<PrefixVo> prefixList = prefixService.findByPrefixTypeAndBranchId(type,branchId);
		  PrefixVo prefixVo;
		  if (prefixList.size() == 0 || prefixList == null) 
		  {
			  prefixVo = new PrefixVo();
			  prefixVo.setAlterBy(userId);
			  prefixVo.setCreatedBy(userId);
			  prefixVo.setBranchId(branchId);
			  prefixVo.setPrefix(prefix);
			  prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			  prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			  prefixVo.setSequenceNo(1L);
			  prefixVo.setPrefixType(type);
			  prefixService.save(prefixVo); 
		  }else{
			  prefixVo = prefixList.get(0); 
		  }
		  String maxNumber = materialRepository.findMaxMaterialNo(branchId, type, prefix);
		  if (maxNumber == null || prefixVo.getIsChangeSequnce() == 1) 
		  {
			  return prefixVo.getSequenceNo(); 
		  } else { 
			  long materialNo = Long.parseLong(maxNumber); 
			  materialNo++;
			  return materialNo; 
		  }
	  }
	  
	  @Override
	  public MaterialVo save(MaterialVo materialVo) { 
		  materialVo = materialRepository.saveAndFlush(materialVo);
	      entityManager.refresh(materialVo);
	      return materialVo; 
	  }
	  
	  @Override 
	  public MaterialVo findByMaterialIdAndCompanyId(long materialId, long companyId) {
		  return materialRepository.findByMaterialIdAndCompanyId(materialId, companyId); 
	  }
	  
	  
	  
	  @Override
	  public void deleteMaterialItem(long companyId, List<Long> materialItemIds) {
		  materialItemRepository.deleteMaterialItem(materialItemIds); 
	  }
	  
	@Override
	public void deleteMaterial(long companyId, long materialId, String type) {
		
		//transactionService.deleteTransaction(companyId, materialId, type);
		/*if (type.equals(Constant.MATERIAL_OUT)) {
			stockTransactionService.deleteStockTransactionMaterial(companyId, materialId, "materialout");
		} else if (type.equals(Constant.MATERIAL_IN)) {
			stockTransactionService.deleteStockTransactionMaterial(companyId, materialId, "materialin");
		}*/

		materialRepository.deleteMaterial(materialId);

	}

//	@Override
//	public List<MaterialVo> findByTypeAndCompanyIdAndIsDeletedAndMaterialDateBetween(String type, long companyId,
//			int isDeleted, Date materialDateStart, Date materialDateEnd) {
//		return materialRepository.findByTypeAndCompanyIdAndIsDeletedAndMaterialDateBetween(type, companyId, isDeleted,
//				materialDateStart, materialDateEnd);
//	}

	@Override public void insertMaterialOutTransaction(MaterialVo materialVo, String yearInterval) 
	{
	  
	  AccountCustomVo accountCustomVo;
	  TransactionVo transactionVo;
	  AccountGroupVo accountGroupVo;
	  
	  // Update Stock
	  stockTransactionService.deleteStockTransactionMaterial(materialVo.getCompanyId(), materialVo.getMaterialId(),Constant.MATERIAL_OUT);
	  stockTransactionService.saveStockFromMaterialOut(materialVo, yearInterval); 
	  //End Update Stock 
	  
	}

//	@Override
//	public List<MaterialVo> findByTypesAndContactAndCompanyIdAndMaterialDateBetween(List<String> types, long companyId,
//			Date materialDateStart, Date materialDateEnd, long contactId) {
//
//		return materialRepository.findByTypesAndContactAndCompanyIdAndMaterialDateBetween(types, companyId,
//				materialDateStart, materialDateEnd, contactId);
//	}

	@Override
	public MaterialItemVo findByMaterialId(long materialId) {
		return materialItemRepository.findByMaterialId(materialId);
	}

	@Override public void insertMaterialInTransaction(MaterialVo materialVo, String yearInterval) 
	{	
	  AccountCustomVo accountCustomVo; 
	  TransactionVo transactionVo;
	  AccountGroupVo accountGroupVo;
	  
	  // Update Stock
	  stockTransactionService.deleteStockTransactionMaterial(materialVo.getCompanyId(), materialVo.getMaterialId(), Constant.MATERIAL_IN);
	  stockTransactionService.saveStockFromMaterialIn(materialVo, yearInterval); 
	  // End Update Stock
	  
	  }
//
//	@Override public void updatePaidAmountMinus(List<Long> l) {
//	  List<ReceiptBillVo> receiptBillVo=receiptBillRepository.findAllById(l);
//	  for(ReceiptBillVo billVo:receiptBillVo) { double
//	  minus=billVo.getMaterialVo().getPaidAmount()-billVo.getTotalPayment();
//	  MaterialVo materialVo=billVo.getMaterialVo();
//	  materialVo.setPaidAmount(minus); materialRepository.save(materialVo);
//	  //purchaseRepository.updatePaidAmountMinus(billVo.getPurchaseVo().
//	  getPurchaseId(),minus); } }

/*	@Override
	public void updatePaidAmountPlus(long materialId, double paidAmount) {

		MaterialVo materialVo = materialRepository.findById(materialId).orElse(new MaterialVo());
		materialVo.setPaidAmount(materialVo.getPaidAmount() + paidAmount);
		materialRepository.save(materialVo);
	}

	@Override
	public List<MaterialVo> getListOfAllUnpaidBillEdit(String receipt, long companyId, int isdeleted, Date StartDate,
			Date endDate, long contactId) {
		return materialRepository.getListOfAllUnpaidBillEdit(receipt, companyId, isdeleted, StartDate, endDate,
				contactId);
	}

	@Override
	public List<MaterialVo> getListOfAllUnpaidBill(String type, long companyId, int isDeleted, Date startDate,
			Date endDate, long contactId) {
		return materialRepository.getListOfAllUnpaidBill(type, companyId, isDeleted, startDate, endDate, contactId);
	}

	@Override
	public List<Map<Long, Double>> getDueAmountByContactId(List<String> types, long companyId, Date materialDateStart,
			Date materialDateEnd) {
		return materialRepository.getDueAmountByContactId(types, companyId, materialDateStart, materialDateEnd);
	}

	@Override
	public List<Map<Double, Double>> getPaidAndTotalAmountByContactId(List<String> types, long companyId,
			Date materialDateStart, Date materialDateEnd, long ContactId) {
		return materialRepository.getPaidAndTotalAmountByContactId(types, companyId, materialDateStart, materialDateEnd,
				ContactId);
	}
*/
//	@Override
//	public List<Map<Double, Double>> getPaidAndTotalAmountAll(List<String> materialTypes, long companyId,Date startDate, Date endDate) {
//		materialRepository.getPaidAndTotalAmountAll(materialTypes, companyId, startDate, endDate);
//	}
//
//	@Override
//	public long getTotalMaterialOrderQty(long companyId, Date startDate, Date endDate) {
//		materialItemRepository.getTotalMaterialOrderQty(companyId, startDate, endDate);
//	}
	 

}
