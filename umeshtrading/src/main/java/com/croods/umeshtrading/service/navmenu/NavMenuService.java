package com.croods.umeshtrading.service.navmenu;

import java.util.List;

import com.croods.umeshtrading.vo.navmenu.NavMenuActionVo;
import com.croods.umeshtrading.vo.navmenu.NavMenuPermissionVo;
import com.croods.umeshtrading.vo.navmenu.NavMenuVo;
import com.croods.umeshtrading.vo.navmenu.NavSubMenuVo;

public interface NavMenuService {
	
	public List<NavMenuVo> findNavMenuByUserFrontId(long userFrontId);
	
	public List<NavSubMenuVo> findNavSubMenuByUserFrontId(long userFrontId);
	
	public List<NavMenuVo> findNavMenuByUserRoleId(long userFrontId);
	
	public List<NavSubMenuVo> findNavSubMenuByUserRoleId(long userFrontId);
	
	public NavSubMenuVo findByNavSubMenuId(long navSubMenuId);
	
	public List<NavMenuActionVo> findAllNavMenuAction();
	
	public List<NavMenuPermissionVo> findByUserfrontId(long userFrontId);
	
	public int deleteByNavMenuPermissionIdIn(List<Long> navMenuPermissionIds);
	
	public void saveNavMenuPermission(NavMenuPermissionVo navMenuPermissionVo);
	
	public List<NavMenuPermissionVo> findByUserRoleId(long userRoleId);
	
	public void saveNavSubMenu(NavSubMenuVo navSubMenuVo);
	
	public void saveNavMenu(NavMenuVo navMenuVo);
	
	public List<NavMenuVo> findAllNavMenu();
	
	public List<NavSubMenuVo> findAllNavSubMenu();
}
