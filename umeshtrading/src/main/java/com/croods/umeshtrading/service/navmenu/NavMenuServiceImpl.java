package com.croods.umeshtrading.service.navmenu;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.navmenu.NavMenuActionRepository;
import com.croods.umeshtrading.repository.navmenu.NavMenuPermissionRepository;
import com.croods.umeshtrading.repository.navmenu.NavMenuRepository;
import com.croods.umeshtrading.repository.navmenu.NavSubMenuRepository;
import com.croods.umeshtrading.vo.navmenu.NavMenuActionVo;
import com.croods.umeshtrading.vo.navmenu.NavMenuPermissionVo;
import com.croods.umeshtrading.vo.navmenu.NavMenuVo;
import com.croods.umeshtrading.vo.navmenu.NavSubMenuVo;

@Service
@Transactional
public class NavMenuServiceImpl implements NavMenuService {

	@Autowired
	NavMenuRepository navMenuRepository;
	
	@Autowired
	NavSubMenuRepository navSubMenuRepository;
	
	@Autowired
	NavMenuActionRepository navMenuActionRepository;
	
	@Autowired
	NavMenuPermissionRepository navMenuPermissionRepository;

	@Override
	public List<NavMenuVo> findNavMenuByUserFrontId(long userFrontId) {
		return navMenuRepository.findNavMenuByUserFrontId(userFrontId);
	}

	@Override
	public List<NavSubMenuVo> findNavSubMenuByUserFrontId(long userFrontId) {
		return navSubMenuRepository.findNavSubMenuByUserFrontId(userFrontId);
	}
	
	@Override
	public List<NavMenuVo> findNavMenuByUserRoleId(long userRoleId) {
		return navMenuRepository.findNavMenuByUserRoleId(userRoleId);
	}

	@Override
	public List<NavSubMenuVo> findNavSubMenuByUserRoleId(long userRoleId) {
		return navSubMenuRepository.findNavSubMenuByUserRoleId(userRoleId);
	}
	
	@Override
	public NavSubMenuVo findByNavSubMenuId(long navSubMenuId) {
		return navSubMenuRepository.findByNavSubMenuId(navSubMenuId);
	}

	@Override
	public List<NavMenuActionVo> findAllNavMenuAction() {
		return navMenuActionRepository.findAll();
	}

	@Override
	public List<NavMenuPermissionVo> findByUserfrontId(long userFrontId) {
		return navMenuPermissionRepository.findByUserfrontId(userFrontId);
	}

	@Override
	public int deleteByNavMenuPermissionIdIn(List<Long> navMenuPermissionIds) {
		return navMenuPermissionRepository.deleteByNavMenuPermissionIdIn(navMenuPermissionIds);
	}

	@Override
	public void saveNavMenuPermission(NavMenuPermissionVo navMenuPermissionVo) {
		navMenuPermissionRepository.save(navMenuPermissionVo);
	}
	
	@Override
	public List<NavMenuPermissionVo> findByUserRoleId(long userRoleId) {
		return navMenuPermissionRepository.findByUserRoleId(userRoleId);
	}
	
	@Override
	public void saveNavMenu(NavMenuVo navMenuVo) {
		navMenuRepository.save(navMenuVo);
	}
	
	@Override
	public void saveNavSubMenu(NavSubMenuVo navSubMenuVo) {
		navSubMenuRepository.save(navSubMenuVo);
	}
	
	public List<NavMenuVo> findAllNavMenu() {
		return navMenuRepository.findAll();
	}

	@Override
	public List<NavSubMenuVo> findAllNavSubMenu() {
		// TODO Auto-generated method stub
		return navSubMenuRepository.findAll();
	}
}
