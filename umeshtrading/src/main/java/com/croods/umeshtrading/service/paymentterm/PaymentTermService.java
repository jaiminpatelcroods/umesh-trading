package com.croods.umeshtrading.service.paymentterm;

import java.util.List;

import com.croods.umeshtrading.vo.paymentterm.PaymentTermVo;

public interface PaymentTermService {

	public void save(PaymentTermVo paymentTermVo);
	
	public List<PaymentTermVo> findByBranchId(long branchid);

	public PaymentTermVo findByPaymentTermId(long id);
	
	public void delete(long id, long alterBy, String modifiedOn);
}
