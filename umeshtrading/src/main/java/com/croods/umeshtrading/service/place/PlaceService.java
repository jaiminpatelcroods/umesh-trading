package com.croods.umeshtrading.service.place;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.umeshtrading.vo.place.PlaceVo;

public interface PlaceService {

	public PlaceVo findByPlaceId(long placeId);
	
	public List<PlaceVo> findByCompanyId(long companyId);

	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	public void updateIsDefaultByPlaceId(long placeId, long alterBy, int isDefault, String modifiedOn);
	
	public void save(PlaceVo placeVo);
	
	public void delete(long placeId, long alterBy, String modifiedOn);

}
