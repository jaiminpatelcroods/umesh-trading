package com.croods.umeshtrading.service.place;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.place.PlaceRepository;
import com.croods.umeshtrading.vo.place.PlaceVo;

@Service
@Transactional
public class PlaceServiceImpl implements PlaceService {

	@Autowired
	PlaceRepository placeRepository;
	
	@Override
	public PlaceVo findByPlaceId(long placeId) {
		return placeRepository.findByPlaceId(placeId);
	}

	@Override
	public List<PlaceVo> findByCompanyId(long companyId) {
		return placeRepository.findByCompanyIdAndIsDeletedOrderByPlaceIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn) {
		placeRepository.updateIsDefaultByCompanyId(companyId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void updateIsDefaultByPlaceId(long placeId, long alterBy, int isDefault, String modifiedOn) {
		placeRepository.updateIsDefaultByPlaceId(placeId, alterBy, isDefault, modifiedOn);
		
	}
	
	@Override
	public void save(PlaceVo unitOfPlaceVo) {
		placeRepository.save(unitOfPlaceVo);
	}
	
	@Override
	public void delete(long placeId, long alterBy, String modifiedOn) {
		placeRepository.delete(placeId, alterBy, modifiedOn);
	}
	
}
