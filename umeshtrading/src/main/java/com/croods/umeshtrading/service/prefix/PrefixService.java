package com.croods.umeshtrading.service.prefix;

import java.util.List;

import com.croods.umeshtrading.vo.prefix.PrefixVo;

public interface PrefixService {
	
	public List<PrefixVo> findByBranchId(long branchId);

	public void save(PrefixVo prefixVo);
	
	public PrefixVo findByPrefixId(long prefixId);

	public List<PrefixVo> findByPrefixTypeAndBranchId(String prefixType,long branchId);
}
