package com.croods.umeshtrading.service.product;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.umeshtrading.vo.product.ProductVariantVo;
import com.croods.umeshtrading.vo.product.ProductVo;

public interface ProductService {
	
	public ProductVo saveProduct(ProductVo productVo);

	public ProductVo findByProductIdAndCompanyId(long productId, long companyId);
	
	public DataTablesOutput<ProductVo> findAll(DataTablesInput input,
			Specification<ProductVo> additionalSpecification, Specification<ProductVo> specification);

	public List<ProductVo> findByCompanyId(long companyId);
	
	public List<ProductVariantVo> findProductVariantByCompanyId(long companyId);

	public ProductVariantVo findProductVariantByIdAndCompanyId(long productVariantId, long companyId);

	public List<ProductVariantVo> findProductVariants(String searchKey, long companyId);

	public void deleteProduct(Long id);

	public long countByCompanyIdAndIsDeleted(long companyId, int i);
}
