package com.croods.umeshtrading.service.product;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.product.ProductRepository;
import com.croods.umeshtrading.repository.product.ProductVariantRepository;
import com.croods.umeshtrading.vo.product.ProductVariantVo;
import com.croods.umeshtrading.vo.product.ProductVo;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductVariantRepository productVariantRepository;
	
	@Override
	public ProductVo saveProduct(ProductVo productVo) {
		return productRepository.save(productVo);
	}
	
	@Override
	public ProductVo findByProductIdAndCompanyId(long productId, long companyId) {
		return productRepository.findByProductIdAndCompanyIdAndIsDeleted(productId, companyId, 0);
	}

	@Override
	public DataTablesOutput<ProductVo> findAll(DataTablesInput input, Specification<ProductVo> additionalSpecification,
			Specification<ProductVo> specification) {
		
		return productRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public List<ProductVo> findByCompanyId(long companyId) {
		return productRepository.findByCompanyIdAndIsDeleted(companyId, 0);
	}
	
	@Override
	public List<ProductVariantVo> findProductVariantByCompanyId(long companyId) {
		return productVariantRepository.findByCompanyIdAndIsDeleted(companyId, 0);
	}

	@Override
	public ProductVariantVo findProductVariantByIdAndCompanyId(long productVariantId, long companyId) {
		return productVariantRepository.findByProductVariantIdAndCompanyIdAndIsDeleted(productVariantId, companyId, 0);
	}

	@Override
	public List<ProductVariantVo> findProductVariants(String searchKey, long companyId) {
		return productVariantRepository.findProductVariants(searchKey, companyId);
	}

	@Override
	public void deleteProduct(Long id) {
		productRepository.deleteProduct(id);
	}

	@Override
	public long countByCompanyIdAndIsDeleted(long companyId, int i) {
		return productRepository.countByCompanyIdAndIsDeleted(companyId,0);
	}
}
