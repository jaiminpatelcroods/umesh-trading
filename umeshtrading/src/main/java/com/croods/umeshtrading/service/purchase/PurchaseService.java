package com.croods.umeshtrading.service.purchase;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.umeshtrading.vo.purchase.PurchaseVo;

public interface PurchaseService {
	
	long findMaxPurchaseNo(long companyId, long branchId, long userId, String type, String prefix);

	PurchaseVo findByPurchaseIdAndBranchId(long purchaseId, long branchId);
	
	void delete(long purchaseId, long userId, String modifiedOn);
	
	void deletePurchaseItemByIdIn(List<Long> purchaseItemIds);

	DataTablesOutput<PurchaseVo> findAll(@Valid DataTablesInput input, Specification<PurchaseVo> additionalSpecification,
			Specification<PurchaseVo> preFilteringSpecification);

	boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo);

	boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo, long purchaseId);
	
	void deletePurchaseAdditionalChargeByIdIn(List<Long> purchaseAdditionalChargeIds);

	public PurchaseVo save(PurchaseVo purchaseVo);

	List<PurchaseVo> findByTypeAndBranchIdAndContactIdAndPurchaseDateBetween(String type, long branchId,
			long contactId, Date startDate, Date endDate);

	void addPaidAmountByPurchaseId(long purchaseId, double payment, long userId, String modifiedOn);
}
