package com.croods.umeshtrading.service.purchase;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.purchase.PurchaseAdditionalChargeRepository;
import com.croods.umeshtrading.repository.purchase.PurchaseItemRepository;
import com.croods.umeshtrading.repository.purchase.PurchaseRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.prefix.PrefixVo;
import com.croods.umeshtrading.vo.purchase.PurchaseVo;

@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService {

	@Autowired
	PurchaseRepository purchaseRepository;
	
	@Autowired
	PurchaseItemRepository purchaseItemRepository;
	
	@Autowired
	PurchaseAdditionalChargeRepository purchaseAdditionalChargeRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public long findMaxPurchaseNo(long companyId, long branchId, long userId, String type, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = purchaseRepository.findMaxPurchaseNo(branchId, type, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
		
	}

	@Override
	public PurchaseVo findByPurchaseIdAndBranchId(long purchaseId, long branchId) {
		return purchaseRepository.findByPurchaseIdAndBranchIdAndIsDeleted(purchaseId, branchId, 0);
	}

	@Override
	public void delete(long purchaseId, long userId, String modifiedOn) {
		purchaseRepository.delete(purchaseId, userId, modifiedOn);
	}

	@Override
	public void deletePurchaseItemByIdIn(List<Long> purchaseItemIds) {
		purchaseItemRepository.deletePurchaseItemByIdIn(purchaseItemIds);
	}

	@Override
	public DataTablesOutput<PurchaseVo> findAll(@Valid DataTablesInput input,
			Specification<PurchaseVo> additionalSpecification, Specification<PurchaseVo> preFilteringSpecification) {
		return purchaseRepository.findAll(input, additionalSpecification, preFilteringSpecification);
	}

	@Override
	public boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo) {
		
		if(purchaseRepository.isExistPurchaesNo(branchId, type, prefix, purchaseNo) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo, long purchaseId) {
		
		if(purchaseRepository.isExistPurchaesNo(branchId, type, prefix, purchaseNo, purchaseId) == null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void deletePurchaseAdditionalChargeByIdIn(List<Long> purchaseAdditionalChargeIds) {
		purchaseAdditionalChargeRepository.deletePurchaseAdditionalChargeByIdIn(purchaseAdditionalChargeIds);
	}

	@Override
	public PurchaseVo save(PurchaseVo purchaseVo) {
		purchaseVo = purchaseRepository.saveAndFlush(purchaseVo);
		entityManager.refresh(purchaseVo);
		return purchaseVo;
	}

	@Override
	public List<PurchaseVo> findByTypeAndBranchIdAndContactIdAndPurchaseDateBetween(String type, long branchId,
			long contactId, Date startDate, Date endDate) {
		return purchaseRepository.findByTypeAndBranchIdAndContactVoContactIdAndIsDeletedAndPurchaseDateBetween(type, branchId, contactId, startDate, endDate);
	}

	@Override
	public void addPaidAmountByPurchaseId(long purchaseId, double payment, long userId, String modifiedOn) {
		purchaseRepository.addPaidAmountByPurchaseId(purchaseId, payment, userId, modifiedOn);
	}

}
