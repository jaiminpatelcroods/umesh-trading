package com.croods.umeshtrading.service.purchaserequest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.purchaserequest.PurchaseRequestVo;

public interface PurchaseRequestService {

	public long findMaxRequestNo(String purchaseRequest, long companyId, long branchId, long userId, String prefix);

	public void save(PurchaseRequestVo purchaseRequestVo);

	public PurchaseRequestVo findByPurchaseRequestIdAndBranchId(long purchaseRequestId, long branchId);

	public void deletePurchaseRequestItemByIdIn(List<Long> purchaseRequestItemIds);

	public void delete(long purchaseRequestId, long userId, String modifiedOn);

	public DataTablesOutput<PurchaseRequestVo> findAll(DataTablesInput input,
			Specification<PurchaseRequestVo> additionalSpecification, Specification<PurchaseRequestVo> specification);

}
