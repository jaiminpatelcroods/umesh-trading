package com.croods.umeshtrading.service.purchaserequest;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.purchaserequest.PurchaseRequestItemRepository;
import com.croods.umeshtrading.repository.purchaserequest.PurchaseRequestRepository;
import com.croods.umeshtrading.service.prefix.PrefixService;
import com.croods.umeshtrading.util.CurrentDateTime;
import com.croods.umeshtrading.vo.prefix.PrefixVo;
import com.croods.umeshtrading.vo.purchaserequest.PurchaseRequestVo;

@Service
@Transactional
public class PurchaseRequestServiceImpl implements PurchaseRequestService {

	@Autowired
	PurchaseRequestRepository purchaseRequestRepository;
	
	@Autowired
	PurchaseRequestItemRepository purchaseRequestItemRepository;
	
	@Autowired
	PrefixService prefixService;

	@Override
	public long findMaxRequestNo(String type, long companyId, long branchId, long userId, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = purchaseRequestRepository.findMaxRequestNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
		
	}

	@Override
	public void save(PurchaseRequestVo purchaseRequestVo) {
		purchaseRequestRepository.save(purchaseRequestVo);
	}

	@Override
	public PurchaseRequestVo findByPurchaseRequestIdAndBranchId(long purchaseRequestId, long branchId) {
		return purchaseRequestRepository.findByPurchaseRequestIdAndBranchIdAndIsDeleted(purchaseRequestId, branchId, 0);
	}

	@Override
	public void deletePurchaseRequestItemByIdIn(List<Long> purchaseRequestItemIds) {
		purchaseRequestItemRepository.deletePurchaseRequestItemByIdIn(purchaseRequestItemIds);
	}

	@Override
	public void delete(long purchaseRequestId, long userId, String modifiedOn) {
		purchaseRequestRepository.delete(purchaseRequestId, userId, modifiedOn);
	}

	@Override
	public DataTablesOutput<PurchaseRequestVo> findAll(DataTablesInput input,
			Specification<PurchaseRequestVo> additionalSpecification, Specification<PurchaseRequestVo> specification) {
		return purchaseRequestRepository.findAll(input, additionalSpecification, specification);
	}
	
}
