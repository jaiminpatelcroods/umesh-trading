package com.croods.umeshtrading.service.rack;

import java.util.List;

import com.croods.umeshtrading.vo.rack.RackVo;

public interface RackService {

	public void save(RackVo rackVo);
	
	public RackVo findByRackId(long rackId);
	
	public void delete(long rackId, long alterBy, String modifiedOn);

	public List<RackVo> findByBranchId(long branchId);

	public List<RackVo> findByFloorVoFloorId(long id);
}
