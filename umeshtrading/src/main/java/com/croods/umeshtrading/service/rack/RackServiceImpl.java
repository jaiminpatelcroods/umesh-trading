package com.croods.umeshtrading.service.rack;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.rack.RackRepository;
import com.croods.umeshtrading.vo.rack.RackVo;

@Service
@Transactional
public class RackServiceImpl implements RackService {

	@Autowired
	RackRepository rackRepository;
	
	@Override
	public void save(RackVo rackVo) {
		rackRepository.save(rackVo);
	}

	@Override
	public RackVo findByRackId(long rackId) {
		return rackRepository.findByRackId(rackId);
	}

	@Override
	public void delete(long rackId, long alterBy, String modifiedOn) {
		rackRepository.delete(rackId, alterBy, modifiedOn);
	}

	@Override
	public List<RackVo> findByBranchId(long branchId) {
		return rackRepository.findByBranchIdAndIsDeletedOrderByRackIdDesc(branchId, 0);
	}

	@Override
	public List<RackVo> findByFloorVoFloorId(long id) {
		return rackRepository.findByFloorVoFloorId(id);
	}

}
