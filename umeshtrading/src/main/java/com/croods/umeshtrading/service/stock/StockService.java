package com.croods.umeshtrading.service.stock;

import java.util.List;
import java.util.Map;

import com.croods.umeshtrading.vo.contact.ContactVo;
import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.rack.RackVo;
import com.croods.umeshtrading.vo.stock.StockVo;


public interface StockService 
{
	
	//public List<StockVo> findByProductVoProductIdAndBatchNoAndCompanyId(long productId, String batchNo, long companyId);
	
	//public List<StockVo> findByProductVoProductIdAndBatchNoAndCompanyIdAndContactVoContactId(long productId, String batchNo, long companyId, long contactId);
	
	public void updateStockWithoutContact(ProductVo productVo,RackVo rackVo,double oldQuantity,double newQuantity, long companyId);
	
	//public void updateStockWithContact(ProductVo productVo,RackVo rackVo,double oldQuantity,double newQuantity,ContactVo contactVo, long companyId);

	public void save(StockVo stockVo);

	//public List getProductBatchList(Long id);
	
	public List<StockVo> findByProductVoProductIdAndRackVoRackIdAndCompanyId(long productId, long rackId, long companyId);

	public Double availableProductQty(long productId, long companyId);
	
	public List<Map<String, String>> availableProductPlace(long productId, long companyId);

	public List<Map<String, String>> availableProductFloor(long placeId, long producId, long companyId);

	public List<Map<String, String>> availableProductRack(long floorId, long productId, long company);

	public Double availableProductQtyOnPlace(long productId, long placeId, long companyId);
	  
}
