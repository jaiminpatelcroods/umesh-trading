package com.croods.umeshtrading.service.stock;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.stock.StockRepository;
import com.croods.umeshtrading.vo.contact.ContactVo;
import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.rack.RackVo;
import com.croods.umeshtrading.vo.stock.StockVo;


@Service
@Transactional
public class StockServiceImpl implements StockService {

	@Autowired
	StockRepository stockRepository;

	@Override
	public void updateStockWithoutContact(ProductVo productVo, RackVo rackVo, double oldQuantity, double newQuantity,long companyId) {
		stockRepository.updateStockWithOutContact(productVo.getProductId(),rackVo.getRackId(), oldQuantity, newQuantity, companyId);
	}

	/*@Override
	public void updateStockWithContact(ProductVo productVo, RackVo rackVo, double oldQuantity, double newQuantity,ContactVo contactVo, long companyId) {
		stockRepository.updateStockWithContact(productVo.getProductId(),rackVo.getRackId(), oldQuantity, newQuantity, contactVo.getContactId(),companyId);
	}*/
	
	/*public List<StockVo> findByProductVoProductIdAndBatchNoAndCompanyId(long productId, String batchNo, long companyId){
		return stockRepository.findByProductVoProductIdAndBatchNoAndCompanyIdAndContactVoContactIdIsNull(productId, batchNo,companyId);
	}

	@Override
	public List<StockVo> findByProductVoProductIdAndBatchNoAndCompanyIdAndContactVoContactId(long productId,
			String batchNo, long companyId, long contactId) {
		
		return stockRepository.findByProductVoProductIdAndBatchNoAndCompanyIdAndContactVoContactId(productId, batchNo,companyId,contactId);
	}*/

	@Override
	public void save(StockVo stockVo) {
		stockRepository.save(stockVo);
	}

	/*@Override
	public List getProductBatchList(Long id) {
		return stockRepository.getProductBatchList(id);
	}*/

	@Override
	public List<StockVo> findByProductVoProductIdAndRackVoRackIdAndCompanyId(long productId,
			long rackId, long companyId) {
		return stockRepository.findByProductVoProductIdAndRackVoRackIdAndCompanyId( productId,
				rackId, companyId);
	}

	@Override
	public Double availableProductQty(long productId, long companyId) {
		return stockRepository.availableProductQty(productId,companyId);
	}
	
	public List<Map<String, String>> availableProductPlace(long productId, long companyId) {
		return stockRepository.availableProductPlace(productId,companyId);
	}
	
	public List<Map<String, String>> availableProductFloor(long placeId, long productId, long companyId) {
		return stockRepository.availableProductFloor(placeId,productId,companyId);
	}

	@Override
	public List<Map<String, String>> availableProductRack(long floorId, long productId, long companyId) {
		return stockRepository.availableProductRack(floorId,productId,companyId);
	}

	@Override
	public Double availableProductQtyOnPlace(long productId, long placeId, long companyId) {
		return stockRepository.availableProductQtyOnPlace(productId, placeId, companyId);
	}

}
