package com.croods.umeshtrading.service.stock;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.umeshtrading.vo.material.MaterialVo;
import com.croods.umeshtrading.vo.purchase.PurchaseVo;
import com.croods.umeshtrading.vo.stock.StockTransactionVo;



public interface StockTransactionService {

	/*public double getTotalQty(long companyId, long productId, Date startDate, Date endDate, String yearInterval);

	public void deleteStockTransactionPurchase(long companyId, long purchaseId, String string);

	public void saveStockFromPurchase(PurchaseVo purchaseVo, String yearIterval);
	
	public void saveStockFromPurchaseReturn(PurchaseVo purchaseVo, String yearIterval);

	

	
	public StockTransactionVo saveStockFromSales(SalesVo salesVo, String yearInterval);

	public void deleteStockTransactionSales(Long branchId, Long typeId, String type);
 
	public void saveStockFromSalesReturn(SalesVo salesVo, String yearInterval);
  
	public void saveStockFromStockTransfer(List<StockTransferItemVo> stockTransferItemVos, String yearInterval,
			String status);

	 
	public void saveOpeningStock(List<StockTransactionVo> stockTransactionVos);

	DataTablesOutput<StockTransactionVo> findAll(DataTablesInput input, Specification<StockTransactionVo> additionalSpecification, Specification<StockTransactionVo> specification);
	
	
	public StockTransactionVo saveStockFromJobwork(JobworkVo jobworkVo, String yearInterval);

	public void deleteStockTransactionJobwork(Long branchId, Long typeId, String type);
 
	public void saveStockFromJobworkIn(JobworkVo jobworkVo, String yearInterval);
	
	
	public StockTransactionVo saveStockFromStore(StoreVo storeVo, String yearInterval);

	public void deleteStockTransactionStore(Long companyId, Long typeId, String type);
 
	public void saveStockFromStoreReturn(StoreVo storeVo, String yearInterval);

	public void deleteStockTransactionProduction(Long companyId, Long productionId, String type);

	public void saveStockFromProduction(ProductionVo productionVo2, String yearInterval);

	public void saveStockFromProductionScrap(ProductionVo productionVo2, String yearInterval);

	public void deleteOpeningStock(Long id);
 
	public List getTotalProductionStock(long companyId, String yearInterval);

	public List getProductBatchList(Long productId, long companyId, String yearInterval, List<String> types);*/

	
	
	public void deleteStockTransactionMaterial(Long branchId, Long typeId, String type);
	public void saveStockFromMaterialIn(MaterialVo materialVo, String yearInterval);
	public void saveStockFromMaterialOut(MaterialVo materialVo, String yearInterval);
	public double getTotalAllInPriceQty(long companyId, Date startDate, Date endDate, String string);
	  
}
