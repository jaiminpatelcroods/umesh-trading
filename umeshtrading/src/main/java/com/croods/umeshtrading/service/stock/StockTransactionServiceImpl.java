package com.croods.umeshtrading.service.stock;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.constant.Constant;
import com.croods.umeshtrading.repository.product.ProductRepository;
import com.croods.umeshtrading.repository.stock.StockTransactionRepository;
import com.croods.umeshtrading.vo.material.MaterialItemVo;
import com.croods.umeshtrading.vo.material.MaterialVo;
import com.croods.umeshtrading.vo.purchase.PurchaseItemVo;
import com.croods.umeshtrading.vo.purchase.PurchaseVo;
import com.croods.umeshtrading.vo.stock.StockTransactionVo;



@Service
@Transactional
public class StockTransactionServiceImpl implements StockTransactionService {


	@Autowired
	StockTransactionRepository stockTransactionRepository;

	@Autowired
	ProductRepository productRepository;
	
	/*@Override
	public double getTotalQty(long companyId, long productId, Date startDate, Date endDate, String yearInterval) {

		try {
			return stockTransactionRepository.getTotalQty(companyId, productId, startDate, endDate, yearInterval);
		} catch (Exception e) {
			return 0;
		}
	}

	
	
	@Override
	public void deleteStockTransactionPurchase(long companyId, long typeId, String type) {
		stockTransactionRepository.deleteStockTransaction(companyId, typeId, type);
	}

	public void saveStockFromPurchase(PurchaseVo purchaseVo, String yearInterval) {

		com.croods.parasenterprise.vo.stock.StockTransactionVo stockTransactionVo;

		for (PurchaseItemVo purchaseItemVo : purchaseVo.getPurchaseItemVos()) {
			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(purchaseVo.getCompanyId());
			stockTransactionVo.setDescription(purchaseVo.getPrefix()+""+purchaseVo.getPurchaseNo());
			stockTransactionVo.setBatchNo(purchaseItemVo.getBatchNo());
			stockTransactionVo.setInQuantity(purchaseItemVo.getQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(purchaseItemVo.getPrice());
			stockTransactionVo.setType("purchase");
			stockTransactionVo.setTypeId(purchaseItemVo.getPurchaseVo().getPurchaseId());
			stockTransactionVo.setStockTransactionDate(purchaseItemVo.getPurchaseVo().getPurchaseDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(purchaseItemVo.getProductVo());
		
	//		stockTransactionVo.setContactId(purchaseVo.getContactVo().getContactId());
			
			stockTransactionRepository.save(stockTransactionVo);

		}

	}
	
	public void saveStockFromPurchaseReturn(PurchaseVo purchaseVo, String yearInterval) {

		com.croods.parasenterprise.vo.stock.StockTransactionVo stockTransactionVo;

		for (PurchaseItemVo purchaseItemVo : purchaseVo.getPurchaseItemVos()) {
			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(purchaseVo.getCompanyId());
			stockTransactionVo.setDescription(purchaseVo.getPrefix()+purchaseVo.getPurchaseNo());
			stockTransactionVo.setBatchNo(purchaseItemVo.getBatchNo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(purchaseItemVo.getQty());
			stockTransactionVo.setProductPrice(purchaseItemVo.getPrice());
			stockTransactionVo.setType("purchase_return");
			stockTransactionVo.setTypeId(purchaseItemVo.getPurchaseVo().getPurchaseId());
			stockTransactionVo.setStockTransactionDate(purchaseItemVo.getPurchaseVo().getPurchaseDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(purchaseItemVo.getProductVo());
			
		
		//	stockTransactionVo.setContactId(purchaseVo.getContactVo().getContactId());
			stockTransactionRepository.save(stockTransactionVo);

		}

	}
	  
	@Override
	public void deleteStockTransactionSales(Long companyId, Long typeId, String type) {
		stockTransactionRepository.deleteStockTransaction(companyId, typeId, type);
	}
   
	@Override
	public StockTransactionVo saveStockFromSales(SalesVo salesVo, String yearInterval) {
		StockTransactionVo stockTransactionVo;
		 
		for (SalesItemVo salesItemVo : salesVo.getSalesItemVos()) {

			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(salesItemVo.getSalesVo().getCompanyId());
			stockTransactionVo.setCompanyId(salesItemVo.getSalesVo().getCompanyId());
			stockTransactionVo
					.setDescription(salesItemVo.getSalesVo().getPrefix() + "" + salesItemVo.getSalesVo().getSalesNo());
			stockTransactionVo.setBatchNo(salesItemVo.getBatchNo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(salesItemVo.getQty());
			stockTransactionVo.setProductPrice(salesItemVo.getPrice());
			stockTransactionVo.setType("sales");
			stockTransactionVo.setTypeId(salesItemVo.getSalesVo().getSalesId());
			stockTransactionVo.setStockTransactionDate(salesItemVo.getSalesVo().getSalesDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(salesItemVo.getProductVo());

		
	//		stockTransactionVo.setContactId(salesVo.getContactVo().getContactId());
			
			stockTransactionRepository.save(stockTransactionVo);
		}
  
		return null;

	}

	@Override
	public void saveStockFromSalesReturn(SalesVo salesVo, String yearInterval) {

		StockTransactionVo stockTransactionVo; 

		for (SalesItemVo salesItemVo : salesVo.getSalesItemVos()) {

			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(salesItemVo.getSalesVo().getCompanyId());
			stockTransactionVo.setCompanyId(salesItemVo.getSalesVo().getCompanyId());
			stockTransactionVo
					.setDescription(salesItemVo.getSalesVo().getPrefix() + "" + salesItemVo.getSalesVo().getSalesNo());
			stockTransactionVo.setBatchNo(salesItemVo.getBatchNo());
			stockTransactionVo.setInQuantity(salesItemVo.getQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(salesItemVo.getPrice());
			stockTransactionVo.setType("sales_return");
			stockTransactionVo.setTypeId(salesItemVo.getSalesVo().getSalesId());
			stockTransactionVo.setStockTransactionDate(salesItemVo.getSalesVo().getSalesDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(salesItemVo.getProductVo());
			//Ronak -- 06/02/2019
//			stockTransactionVo.setContactId(salesVo.getContactVo().getContactId());
			stockTransactionRepository.save(stockTransactionVo);
		}
 

	}

 
	@Override
	public DataTablesOutput<StockTransactionVo> findAll(DataTablesInput input,
			Specification<StockTransactionVo> additionalSpecification,
			Specification<StockTransactionVo> specification) {
		return stockTransactionRepository.findAll(input, null, specification);
	}
	
	

	 
		
	   
		@Override
		public StockTransactionVo saveStockFromJobwork(JobworkVo jobworkVo, String yearInterval) {
			StockTransactionVo stockTransactionVo;
			 
			for (JobworkItemVo jobworkItemVo : jobworkVo.getJobworkItemVos()) {

				stockTransactionVo = new StockTransactionVo();

				stockTransactionVo.setCompanyId(jobworkItemVo.getJobworkVo().getCompanyId());
				stockTransactionVo.setCompanyId(jobworkItemVo.getJobworkVo().getCompanyId());
				stockTransactionVo
						.setDescription(jobworkItemVo.getJobworkVo().getPrefix() + "" + jobworkItemVo.getJobworkVo().getJobworkNo());
				stockTransactionVo.setBatchNo(jobworkItemVo.getBatchNo());
				stockTransactionVo.setInQuantity(0);
				stockTransactionVo.setOutQuantity(jobworkItemVo.getQty());
				stockTransactionVo.setProductPrice(jobworkItemVo.getPrice());
				stockTransactionVo.setType("materialout");
				stockTransactionVo.setTypeId(jobworkItemVo.getJobworkVo().getJobworkId());
				stockTransactionVo.setStockTransactionDate(jobworkItemVo.getJobworkVo().getJobworkDate());
				stockTransactionVo.setYearInterval(yearInterval);
				stockTransactionVo.setProductVo(jobworkItemVo.getProductVo());
				//Ronak -- 06/02/2019
				stockTransactionVo.setContactId(jobworkVo.getContactVo().getContactId());
				
				stockTransactionRepository.save(stockTransactionVo);
			}
	  
			return null;

		}

		@Override
		public void saveStockFromJobworkIn(JobworkVo jobworkVo, String yearInterval) {

			StockTransactionVo stockTransactionVo; 

			for (JobworkItemVo jobworkItemVo : jobworkVo.getJobworkItemVos()) {

				stockTransactionVo = new StockTransactionVo();

				stockTransactionVo.setCompanyId(jobworkItemVo.getJobworkVo().getCompanyId());
				stockTransactionVo.setCompanyId(jobworkItemVo.getJobworkVo().getCompanyId());
				stockTransactionVo
						.setDescription(jobworkItemVo.getJobworkVo().getPrefix() + "" + jobworkItemVo.getJobworkVo().getJobworkNo());
				stockTransactionVo.setBatchNo(jobworkItemVo.getBatchNo());
				stockTransactionVo.setInQuantity(jobworkItemVo.getQty());
				stockTransactionVo.setOutQuantity(0);
				stockTransactionVo.setProductPrice(jobworkItemVo.getPrice());
				stockTransactionVo.setType("materialin");
				stockTransactionVo.setTypeId(jobworkItemVo.getJobworkVo().getJobworkId());
				stockTransactionVo.setStockTransactionDate(jobworkItemVo.getJobworkVo().getJobworkDate());
				stockTransactionVo.setYearInterval(yearInterval);
				stockTransactionVo.setProductVo(jobworkItemVo.getProductVo());
				//Ronak -- 06/02/2019
				stockTransactionVo.setContactId(jobworkVo.getContactVo().getContactId());
				stockTransactionRepository.save(stockTransactionVo);
			}
	 

		}
		
		
		
		
	@Override
	public void deleteStockTransactionStore(Long companyId, Long typeId, String type) {
		stockTransactionRepository.deleteStockTransaction(companyId, typeId, type);
	}
   
	@Override
	public StockTransactionVo saveStockFromStore(StoreVo storeVo, String yearInterval) {
		StockTransactionVo stockTransactionVo;
		 
		for (StoreItemVo storeItemVo : storeVo.getStoreItemVos()) {

			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(storeItemVo.getStoreVo().getCompanyId());
			stockTransactionVo.setCompanyId(storeItemVo.getStoreVo().getCompanyId());
			stockTransactionVo
					.setDescription(storeItemVo.getStoreVo().getPrefix() + "" + storeItemVo.getStoreVo().getStoreNo());
			stockTransactionVo.setBatchNo(storeItemVo.getBatchNo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(storeItemVo.getQty());
			stockTransactionVo.setProductPrice(0);
			stockTransactionVo.setType("material_issue");
			stockTransactionVo.setTypeId(storeItemVo.getStoreVo().getStoreId());
			stockTransactionVo.setStockTransactionDate(storeItemVo.getStoreVo().getStoreDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(storeItemVo.getProductVo());
			//Ronak -- 06/02/2019
			try{
			if(storeVo.getJobworkVo() != null){
				stockTransactionVo.setContactId(storeVo.getJobworkVo().getContactVo().getContactId());
			}else{
				stockTransactionVo.setContactId(0);
			}
			}catch(Exception e){
				stockTransactionVo.setContactId(0);
			}
			stockTransactionRepository.save(stockTransactionVo);
		}
  
		return null;

	}

	@Override
	public void saveStockFromStoreReturn(StoreVo storeVo, String yearInterval) {

		StockTransactionVo stockTransactionVo; 

		for (StoreItemVo storeItemVo : storeVo.getStoreItemVos()) {

			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(storeItemVo.getStoreVo().getCompanyId());
			stockTransactionVo.setCompanyId(storeItemVo.getStoreVo().getCompanyId());
			stockTransactionVo
					.setDescription(storeItemVo.getStoreVo().getPrefix() + "" + storeItemVo.getStoreVo().getStoreNo());
			stockTransactionVo.setBatchNo(storeItemVo.getBatchNo());
			stockTransactionVo.setInQuantity(storeItemVo.getQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(0);
			stockTransactionVo.setType("material_return");
			stockTransactionVo.setTypeId(storeItemVo.getStoreVo().getStoreId());
			stockTransactionVo.setStockTransactionDate(storeItemVo.getStoreVo().getStoreDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(storeItemVo.getProductVo());
			//Ronak -- 06/02/2019
			try{
			if(storeVo.getJobworkVo() != null){
				stockTransactionVo.setContactId(storeVo.getJobworkVo().getContactVo().getContactId());
			}else{
				stockTransactionVo.setContactId(0);
			}
			}catch(Exception e){
				stockTransactionVo.setContactId(0);
			}
			stockTransactionRepository.save(stockTransactionVo);
		}
 

	}

	@Override
	public void deleteStockTransactionProduction(Long companyId, Long typeId, String type) {
		// TODO Auto-generated method stub
		stockTransactionRepository.deleteStockTransaction(companyId, typeId, type);
	}

	@Override
	public void saveStockFromProduction(ProductionVo productionVo2, String yearInterval) {
		// TODO Auto-generated method stub
		StockTransactionVo stockTransactionVo;
		  
		stockTransactionVo = new StockTransactionVo();

		stockTransactionVo.setCompanyId(productionVo2.getCompanyId());
		stockTransactionVo.setCompanyId(productionVo2.getCompanyId());
		stockTransactionVo
				.setDescription(productionVo2.getPrefix() + "" + productionVo2.getProductionNo()); 
		stockTransactionVo.setInQuantity(productionVo2.getProductionQty());
		stockTransactionVo.setOutQuantity(0);
		stockTransactionVo.setProductPrice(0);
		stockTransactionVo.setType("production_qty");
		stockTransactionVo.setTypeId(productionVo2.getProductionId());
		stockTransactionVo.setStockTransactionDate(productionVo2.getProductionDate());
		stockTransactionVo.setYearInterval(yearInterval);
		stockTransactionVo.setProductVo(productionVo2.getProductVo());
		//Ronak -- 06/02/2019
		try{
		if(productionVo2.getJobworkVo() != null){
			stockTransactionVo.setContactId(productionVo2.getJobworkVo().getContactVo().getContactId());
		}else{
			stockTransactionVo.setContactId(0);
		}
		}catch(Exception e){
			stockTransactionVo.setContactId(0);
		}
		stockTransactionRepository.save(stockTransactionVo);
	   
	}

	@Override
	public void saveStockFromProductionScrap(ProductionVo productionVo2, String yearInterval) {
		// TODO Auto-generated method stub
		StockTransactionVo stockTransactionVo1;
		  
		stockTransactionVo1 = new StockTransactionVo();

		stockTransactionVo1.setCompanyId(productionVo2.getCompanyId());
		stockTransactionVo1
				.setDescription(productionVo2.getPrefix() + "" + productionVo2.getProductionNo()); 
		stockTransactionVo1.setInQuantity(productionVo2.getProductionScrap());
		stockTransactionVo1.setOutQuantity(0);
		stockTransactionVo1.setProductPrice(0);
		stockTransactionVo1.setType("production_scrap");
		stockTransactionVo1.setTypeId(productionVo2.getProductionId());
		stockTransactionVo1.setStockTransactionDate(productionVo2.getProductionDate());
		stockTransactionVo1.setYearInterval(yearInterval);
		stockTransactionVo1.setProductVo(productRepository.findByNameAndCompanyId("Scrap",productionVo2.getCompanyId()));
		//Ronak -- 06/02/2019
				try{
				if(productionVo2.getJobworkVo() != null){
					stockTransactionVo1.setContactId(productionVo2.getJobworkVo().getContactVo().getContactId());
				}else{
					stockTransactionVo1.setContactId(0);
				}
				}catch(Exception e){
					stockTransactionVo1.setContactId(0);
				}
		stockTransactionRepository.save(stockTransactionVo1);
	   
	}
 

	@Override
	public void saveOpeningStock(List<StockTransactionVo> stockTransactionVos) {

		for (StockTransactionVo stockTransactionVo : stockTransactionVos) { 
			stockTransactionRepository.save(stockTransactionVo); 
		}

	}
	 
	@Override
	public void deleteOpeningStock(Long transactionId) {
		stockTransactionRepository.deleteOpeningStock(transactionId);
	}
 
	@Override
	public List getTotalProductionStock(long companyId, String yearInterval) {
		return  stockTransactionRepository.getTotalProductionStock(companyId,yearInterval);
	}

 
	public List getProductBatchList(Long productId, long companyId, String yearInterval, List<String> types){
		return  stockTransactionRepository.getTotalProductionStock(productId,companyId,yearInterval,types);
	} */

	
	
	@Override
	public void deleteStockTransactionMaterial(Long companyId, Long typeId, String type) {
		stockTransactionRepository.deleteStockTransaction(companyId, typeId, type);
	}
	
	@Override
	public void saveStockFromMaterialIn(MaterialVo materialVo, String yearInterval) 
	{

		StockTransactionVo stockTransactionVo; 

		for (MaterialItemVo materialItemVo : materialVo.getMaterialItemVos()) {

			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(materialItemVo.getMaterialVo().getCompanyId());
			stockTransactionVo.setDescription(materialItemVo.getMaterialVo().getPrefix() + "" + materialItemVo.getMaterialVo().getMaterialNo());
			stockTransactionVo.setPlaceVo(materialItemVo.getPlaceVo());
			stockTransactionVo.setFloorVo(materialItemVo.getFloorVo());
			stockTransactionVo.setRackVo(materialItemVo.getRackVo());
			stockTransactionVo.setInQuantity(materialItemVo.getQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(materialItemVo.getPrice());
			stockTransactionVo.setType(Constant.MATERIAL_IN);
			stockTransactionVo.setTypeId(materialItemVo.getMaterialVo().getMaterialId());
			stockTransactionVo.setStockTransactionDate(materialItemVo.getMaterialVo().getMaterialDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(materialItemVo.getProductVo());
			stockTransactionVo.setContactVo(materialVo.getContactVo());
			stockTransactionRepository.save(stockTransactionVo);
		}
	}
	
	@Override
	public void saveStockFromMaterialOut(MaterialVo materialVo, String yearInterval) 
	{

		StockTransactionVo stockTransactionVo; 

		for (MaterialItemVo materialItemVo : materialVo.getMaterialItemVos()) {

			stockTransactionVo = new StockTransactionVo();

			stockTransactionVo.setCompanyId(materialItemVo.getMaterialVo().getCompanyId());
			stockTransactionVo.setDescription(materialItemVo.getMaterialVo().getPrefix() + "" + materialItemVo.getMaterialVo().getMaterialNo());
			stockTransactionVo.setPlaceVo(materialItemVo.getPlaceVo());
			stockTransactionVo.setFloorVo(materialItemVo.getFloorVo());
			stockTransactionVo.setRackVo(materialItemVo.getRackVo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(materialItemVo.getQty());
			stockTransactionVo.setProductPrice(materialItemVo.getPrice());
			stockTransactionVo.setType(Constant.MATERIAL_OUT);
			stockTransactionVo.setTypeId(materialItemVo.getMaterialVo().getMaterialId());
			stockTransactionVo.setStockTransactionDate(materialItemVo.getMaterialVo().getMaterialDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVo(materialItemVo.getProductVo());
			stockTransactionVo.setContactVo(materialVo.getContactVo());
			stockTransactionRepository.save(stockTransactionVo);
		}
	}

	@Override
	public double getTotalAllInPriceQty(long companyId, Date startDate, Date endDate, String yearInterval) {
		double dd=0.0;
		try {
			dd=stockTransactionRepository.getTotalAllInPriceQty(companyId, startDate, endDate, yearInterval);
		} catch (Exception e) {
			dd=0;
		}
		return dd;
	}
	
 }

