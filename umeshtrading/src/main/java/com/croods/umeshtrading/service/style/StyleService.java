package com.croods.umeshtrading.service.style;

import java.util.List;

import com.croods.umeshtrading.vo.style.StyleVo;

public interface StyleService {
	
	public void save(StyleVo styleVo);
	
	public StyleVo findByStyleId(long styleId);
	
	public void delete(long styleId, long alterBy, String modifiedOn);

	public List<StyleVo> findByCompanyId(long companyId);
}
