package com.croods.umeshtrading.service.style;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.style.StyleRepository;
import com.croods.umeshtrading.vo.style.StyleVo;

@Service
@Transactional
public class StyleServiceImpl implements StyleService {

	@Autowired
	StyleRepository styleRepository;
	
	@Override
	public void save(StyleVo styleVo) {
		styleRepository.save(styleVo);
	}

	@Override
	public StyleVo findByStyleId(long styleId) {
		return styleRepository.findByStyleId(styleId);
	}

	@Override
	public void delete(long styleId, long alterBy, String modifiedOn) { 
		styleRepository.delete(styleId, alterBy, modifiedOn);
	}

	@Override
	public List<StyleVo> findByCompanyId(long companyId) {
		return styleRepository.findByCompanyIdAndIsDeletedOrderByStyleIdDesc(companyId, 0);
	}

}
