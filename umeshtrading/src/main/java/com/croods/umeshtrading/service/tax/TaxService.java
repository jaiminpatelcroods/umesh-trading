package com.croods.umeshtrading.service.tax;


import java.util.List;

import com.croods.umeshtrading.vo.tax.TaxVo;

public interface TaxService {
	
	public TaxVo findByTaxId(long id);
	
	public void taxSave(TaxVo taxVo);
	
	public void delete(long taxId, long alterBy, String modifiedOn);

	public List<TaxVo> findByCompanyId(long companyId);

	public void updateIsDefaultByCompanyId(long companyId, int isDefault);
	
	public void updateIsDefaultByTaxId(long taxId, int isDefault);
}
