package com.croods.umeshtrading.service.termsandcondition;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.termsandcondition.TermsAndConditionRepository;
import com.croods.umeshtrading.vo.termsandcondition.TermsAndConditionVo;

@Service
@Transactional
public class TermsAndConditionServiceImpl implements TermsAndConditionService {

	@Autowired
	TermsAndConditionRepository termsAndConditionRepository;

	@Override
	public void save(TermsAndConditionVo termsAndConditionVo) {
		termsAndConditionRepository.save(termsAndConditionVo);
	}

	@Override
	public TermsAndConditionVo findByTermsandConditionId(long termsandConditionId) {
		return termsAndConditionRepository.findByTermsandConditionId(termsandConditionId);
	}

	@Override
	public List<TermsAndConditionVo> findByCompanyId(long companyId) {
		return termsAndConditionRepository.findByCompanyIdAndIsDeletedOrderByTermsandConditionIdDesc(companyId, 0);
	}

	@Override
	public List<TermsAndConditionVo> findByCompanyIdAndIsDefault(long companyId, int isDefault) {
		return termsAndConditionRepository.findByCompanyIdAndIsDefaultAndIsDeletedOrderByTermsandConditionIdDesc(companyId, isDefault, 0);
	}

	@Override
	public List<TermsAndConditionVo> findByTermsandConditionIdIn(List<Long> termsandConditionIds) {
		return termsAndConditionRepository.findByTermsandConditionIdIn(termsandConditionIds);
	}

	@Override
	public void delete(long termsandConditionId, long alterBy, String modifiedOn) {
		termsAndConditionRepository.delete(termsandConditionId, alterBy, modifiedOn);
		
	}
	
	@Override
	public List<TermsAndConditionVo> findByCompanyIdAndIsDefaultAndModules(long companyId, int isDefault, String modules) {
		return termsAndConditionRepository.findByCompanyIdAndIsDefaultAndModules(companyId, isDefault, modules);
	}
	
	@Override
	public List<TermsAndConditionVo> findByCompanyIdAndModules(long companyId, String modules) {
		return termsAndConditionRepository.findByCompanyIdAndModules(companyId, modules);
	}

}
