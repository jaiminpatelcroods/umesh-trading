package com.croods.umeshtrading.service.transaction;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.croods.umeshtrading.repository.transaction.TransactionRepository;
import com.croods.umeshtrading.service.financial.FinancialService;
import com.croods.umeshtrading.util.TransactionAmount;
import com.croods.umeshtrading.vo.financial.YearWiseOpeningBalanceVo;
import com.croods.umeshtrading.vo.transaction.TransactionVo;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	FinancialService financialService;
	
	@Override
	public TransactionVo save(TransactionVo transactionVo,double amount, TransactionAmount transactionAmount) {
		if(TransactionAmount.DEBIT == transactionAmount) {
			transactionVo.setDebitAmount(amount);
			transactionVo.setCreditAmount(0.0);
		} else {
			transactionVo.setDebitAmount(0.0);
			transactionVo.setCreditAmount(amount);
		}
		return transactionRepository.save(transactionVo);
	}

	@Override
	public List<TransactionVo> saveAll(List<TransactionVo> transactionVos) {
		
		return transactionRepository.saveAll(transactionVos);
	}
	
	@Override
	public void deleteTransaction(long branchId, long voucherId, String voucherType) {
		
		transactionRepository.deleteTransaction(branchId, voucherId, voucherType);
	}

	@Override
	public double getOpeningBalance(long branchId, long accountCustomId, Date startDate, Date fromDate,String yearInterval) {
		
		YearWiseOpeningBalanceVo yearWiseOpeningBalanceVo = financialService
				.findByBranchIdAndAccountCustomIdAndYearInterval(branchId, accountCustomId, yearInterval);
		
		double openingBalance = 0.0;
		
		if(yearWiseOpeningBalanceVo != null) {
			 String[] accountGroupIds={"1","3","6","7","8","12","15","18","19"};
			 if(Arrays.asList(accountGroupIds).contains(String.valueOf(yearWiseOpeningBalanceVo.getAccountCustomVo().getGroup().getAccountGroupId()))) {
				 openingBalance=yearWiseOpeningBalanceVo.getDebitAmount()-yearWiseOpeningBalanceVo.getCreditAmount();
			 } else {
				 openingBalance=yearWiseOpeningBalanceVo.getCreditAmount()-yearWiseOpeningBalanceVo.getDebitAmount();
			 }
		 }
		
		double balance = 0;
		
		try {
			balance = transactionRepository.getBalance(accountCustomId, startDate, fromDate);
		} catch(Exception e) {
			balance = 0;
		}
		
		return openingBalance+balance;
	}

	@Override
	public DataTablesOutput<TransactionVo> findAll(DataTablesInput input,
			Specification<TransactionVo> additionalSpecification, Specification<TransactionVo> specification) {
		return transactionRepository.findAll(input, null, specification);
	}

	@Override
	public double getOpeningBalancePageable(long branchId, long accountCustomId, Date startDate, Date fromDate, int start, int limit,String yearInterval) {
		
		YearWiseOpeningBalanceVo yearWiseOpeningBalanceVo = financialService
				.findByBranchIdAndAccountCustomIdAndYearInterval(branchId, accountCustomId, yearInterval);
		
		double openingBalance = 0.0;
		
		if(yearWiseOpeningBalanceVo != null) {
			 String[] accountGroupIds={"1","3","6","7","8","12","15","18","19"};
			 if(Arrays.asList(accountGroupIds).contains(String.valueOf(yearWiseOpeningBalanceVo.getAccountCustomVo().getGroup().getAccountGroupId()))) {
				 openingBalance=yearWiseOpeningBalanceVo.getDebitAmount()-yearWiseOpeningBalanceVo.getCreditAmount();
			 } else {
				 openingBalance=yearWiseOpeningBalanceVo.getCreditAmount()-yearWiseOpeningBalanceVo.getDebitAmount();
			 }
		 }
		
		double balance = 0;
		
		try {
			balance = transactionRepository.getBalancePageable(accountCustomId, startDate, fromDate, start, limit);
		} catch(Exception e) {
			balance = 0;
		}
		
		return balance+openingBalance;
	}

	
}
