package com.croods.umeshtrading.service.unitofmeasurement;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.unitofmeasurement.UnitOfMeasurementRepository;
import com.croods.umeshtrading.vo.unitofmeasurement.UnitOfMeasurementVo;

@Service
@Transactional
public class UnitOfMeasurementServiceImpl implements UnitOfMeasurementService {

	@Autowired
	UnitOfMeasurementRepository unitOfMeasurementRepository;
	
	@Override
	public UnitOfMeasurementVo findByMeasurementId(long measurementId) {
		return unitOfMeasurementRepository.findByMeasurementId(measurementId);
	}

	@Override
	public List<UnitOfMeasurementVo> findByCompanyId(long companyId) {
		return unitOfMeasurementRepository.findByCompanyIdAndIsDeletedOrderByMeasurementIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn) {
		unitOfMeasurementRepository.updateIsDefaultByCompanyId(companyId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void updateIsDefaultByMeasurementId(long measurementId, long alterBy, int isDefault, String modifiedOn) {
		unitOfMeasurementRepository.updateIsDefaultByMeasurementId(measurementId, alterBy, isDefault, modifiedOn);
		
	}
	
	@Override
	public void save(UnitOfMeasurementVo unitOfMeasurementVo) {
		unitOfMeasurementRepository.save(unitOfMeasurementVo);
	}
	
	@Override
	public void delete(long measurementId, long alterBy, String modifiedOn) {
		unitOfMeasurementRepository.delete(measurementId, alterBy, modifiedOn);
	}
	
}
