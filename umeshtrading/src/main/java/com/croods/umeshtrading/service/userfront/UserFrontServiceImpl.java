package com.croods.umeshtrading.service.userfront;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.userfront.UserFrontRepository;
import com.croods.umeshtrading.vo.userfront.UserFrontVo;

@Service
@Transactional
public class UserFrontServiceImpl implements UserFrontService {
	
	@Autowired
	UserFrontRepository userFrontRepository;

	@Override
	public void save(UserFrontVo userFrontVo) {
		userFrontRepository.save(userFrontVo);
	}
	
	@Override
	public UserFrontVo findByUserFrontId(long userFrontId) {
		return userFrontRepository.findByUserFrontId(userFrontId);
	}

	@Override
	public void setDefaultYearInterval(long userFrontId, String yearInterval, String modifiedOn) {
		userFrontRepository.updateDefaultYearIntervalByUserFrontId(userFrontId, yearInterval, modifiedOn);
	}
}
