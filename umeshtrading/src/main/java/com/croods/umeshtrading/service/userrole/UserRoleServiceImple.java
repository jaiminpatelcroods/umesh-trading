package com.croods.umeshtrading.service.userrole;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.umeshtrading.repository.userrole.UserRoleRepository;
import com.croods.umeshtrading.vo.userrole.UserRoleVo;

@Service
@Transactional
public class UserRoleServiceImple implements UserRoleService {

	@Autowired
	UserRoleRepository userRoleRepository;

	@Override
	public UserRoleVo findByUserRoleId(long userRoleId) {
		return userRoleRepository.findByUserRoleId(userRoleId);
	}

	@Override
	public void save(UserRoleVo userRoleVo) {
		userRoleRepository.save(userRoleVo);
	}

	@Override
	public List<UserRoleVo> findByBranchId(long branchId) {
		return userRoleRepository.findByBranchId(branchId);
	}
}
