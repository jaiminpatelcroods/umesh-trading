package com.croods.umeshtrading.util;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import javax.activation.DataSource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

public class JasperExporter {
	
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	File file;
	JRXlsExporter xlsExporter;
	@Autowired
	DataSource dataSource;
	

	public void jasperExporterPDF(HashMap jasperParameter,String jrxmlpath ,String fileName,  HttpServletResponse response) throws IOException
	{
		
		try
		{
			
			outputStream = response.getOutputStream();
	        jasperReport = JasperCompileManager.compileReport(jrxmlpath);
	           
            jasperPrint = JasperFillManager.fillReport(jasperReport,jasperParameter, DBConnection.getConnection()); 
           
            file = File.createTempFile("output.", ".pdf");
            
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition","inline; filename="+fileName+".pdf");
            
          
            
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
          
        }
	 	catch (Exception e) {
	     	e.printStackTrace();
	 	}
		
		
	}
	
	public void jasperExporterEXCEL(HashMap jasperParameter,String jrxmlpath ,String fileName,  HttpServletResponse response) throws IOException
	{
		
		try
		{
			
			outputStream = response.getOutputStream();
	        jasperReport = JasperCompileManager.compileReport(jrxmlpath);
	           
            jasperPrint = JasperFillManager.fillReport(jasperReport,jasperParameter, DBConnection.getConnection()); 
           
            file = File.createTempFile("output.", ".xls");
            
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","inline; filename="+fileName+".xls");
            
            //JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
            
            xlsExporter = new JRXlsExporter();
            xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
            xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
            xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            xlsExporter.exportReport();
            
		
	           
        }
	 	catch (Exception e) {
	 		e.printStackTrace();
	 	}
	}
	

	public void jasperExporterHTML(HashMap jasperParameter,String jrxmlpath ,String fileName,  HttpServletResponse response) throws IOException
	{
		
		try
		{
			
			outputStream = response.getOutputStream();
	        jasperReport = JasperCompileManager.compileReport(jrxmlpath);
	        // con = ((DBConnection) dataSource).getConnection();
            jasperPrint = JasperFillManager.fillReport(jasperReport,jasperParameter, DBConnection.getConnection()); 
           
            
            file = File.createTempFile("output.", ".html");
            
            response.setContentType("text/html");
            
            response.setHeader("Content-disposition","inline; filename="+fileName+".html");
            
            //JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
            
            //JasperExportManager.exportReportToHtmlFile(jasperPrint, "output.html");
            
            /*JRExporter  exporter = new JRHtmlExporter();*/
            HtmlExporter exporter = new HtmlExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
            
            exporter.exportReport();
        }
	 	catch (Exception e) {
	     	e.printStackTrace();
	 	}
		/*finally {
			try {
				if(!con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Log.error("Connection Close error");
				e.printStackTrace();
			}
		}*/
		
	}


}
