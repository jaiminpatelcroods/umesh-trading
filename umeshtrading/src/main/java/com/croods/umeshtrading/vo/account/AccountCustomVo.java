package com.croods.umeshtrading.vo.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="account_custom")
public class AccountCustomVo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="account_custom_id",length=10)
	private long accountCustomId;
	
	@ManyToOne
	@JoinColumn(name = "account_group_id", referencedColumnName = "account_group_id")
	private AccountGroupVo group;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="account_name",length=100)
	private String accountName;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="is_updatable",length=1,columnDefinition="int default 0",updatable=false)
	private int isUpdatable;
	
	@Column(name="account_type",length=100)
	private String accounType;

	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;	

	@Transient
	double creditAmount = 0.0;
	
	@Transient
	double debitAmount = 0.0;
	
	public long getAccountCustomId() {
		return accountCustomId;
	}

	public void setAccountCustomId(long accountCustomId) {
		this.accountCustomId = accountCustomId;
	}

	public AccountGroupVo getGroup() {
		return group;
	}

	public void setGroup(AccountGroupVo group) {
		this.group = group;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getIsUpdatable() {
		return isUpdatable;
	}

	public void setIsUpdatable(int isUpdatable) {
		this.isUpdatable = isUpdatable;
	}

	public String getAccounType() {
		return accounType;
	}

	public void setAccounType(String accounType) {
		this.accounType = accounType;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public double getCreditAmount() {
		return creditAmount;
	}
	
	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}
}


