package com.croods.umeshtrading.vo.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;


@Entity
@Table(name="account_group")
public class AccountGroupVo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="account_group_id",length=10)
	private long accountGroupId;
	
	@Column(name="account_group_name",length=100)
	private String accountGroupName;
	
	@Column(name="group_type",length=50)
	private String groupType;
	
	public long getAccountGroupId() {
		return accountGroupId;
	}

	public void setAccountGroupId(long accountGroupId) {
		this.accountGroupId = accountGroupId;
	}
	
	public String getAccountGroupName() {
		return accountGroupName;
	}

	public void setAccountGroupName(String accountGroupName) {
		this.accountGroupName = accountGroupName;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	
}
