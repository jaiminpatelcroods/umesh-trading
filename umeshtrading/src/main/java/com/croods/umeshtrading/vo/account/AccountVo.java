package com.croods.umeshtrading.vo.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="account")
public class AccountVo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="account_id",length=10)
	private long accountId;
	
	@ManyToOne
	@JoinColumn(name = "account_group_id", referencedColumnName = "account_group_id")
	private AccountGroupVo group;
	
	@Column(name="account_name",length=100)
	private String accountName;

	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public AccountGroupVo getGroup() {
		return group;
	}

	public void setGroup(AccountGroupVo group) {
		this.group = group;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
}
