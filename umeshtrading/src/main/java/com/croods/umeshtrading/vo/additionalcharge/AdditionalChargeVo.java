package com.croods.umeshtrading.vo.additionalcharge;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.croods.umeshtrading.vo.tax.TaxVo;


@Entity
@Table(name="additional_charge")
@DynamicUpdate(value=true)
public class AdditionalChargeVo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="additional_charge_id",length=10)
	private long additionalChargeId;
	
	@Column(name="additional_charge")
	private String additionalCharge;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="default_amount")
	private double defaultAmount=0;
	
	@Column(name="hsn_code",length=50)
	private String hsnCode;
	
	@ManyToOne
	@JoinColumn(name="tax_id",referencedColumnName = "tax_id")
	private TaxVo taxVo;	
	
	@ManyToOne
	@JoinColumn(name="account_custom_id",referencedColumnName = "account_custom_id")
	private AccountCustomVo accountCustomVo;	
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby",length=10,updatable=false)
	private long createdBy;

	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;

	public long getAdditionalChargeId() {
		return additionalChargeId;
	}

	public void setAdditionalChargeId(long additionalChargeId) {
		this.additionalChargeId = additionalChargeId;
	}

	public String getAdditionalCharge() {
		return additionalCharge;
	}

	public void setAdditionalCharge(String additionalCharge) {
		this.additionalCharge = additionalCharge;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public double getDefaultAmount() {
		return defaultAmount;
	}

	public void setDefaultAmount(double defaultAmount) {
		this.defaultAmount = defaultAmount;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public TaxVo getTaxVo() {
		return taxVo;
	}

	public void setTaxVo(TaxVo taxVo) {
		this.taxVo = taxVo;
	}

	public AccountCustomVo getAccountCustomVo() {
		return accountCustomVo;
	}

	public void setAccountCustomVo(AccountCustomVo accountCustomVo) {
		this.accountCustomVo = accountCustomVo;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
}