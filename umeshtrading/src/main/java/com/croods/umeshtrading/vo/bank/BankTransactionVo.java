package com.croods.umeshtrading.vo.bank;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.croods.umeshtrading.vo.bank.BankVo;

@Entity
@Table(name="bank_transaction")
public class BankTransactionVo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bank_transaction_id",length=10)
	private long bankTransactionId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id", updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="debit_amount",length=100)
	private double debitAmount;
	
	@Column(name="credit_amount",length=100)
	private double creditAmount;
	
	@ManyToOne
	@JoinColumn(name="from_bank_id",referencedColumnName="bank_id")
	private BankVo fromBankVo;
	
	@ManyToOne
	@JoinColumn(name="to_bank_id",referencedColumnName="bank_id")
	private BankVo toBankVo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "transaction_date")
	private Date transactionDate;
	
	@Column(name="description",length=300,columnDefinition="text")
	private String description;
	
	@Column(name="transaction_type",length=50, updatable=false)
	private String transactionType;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "voucher_no")
	private long voucherNo;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;

	public long getBankTransactionId() {
		return bankTransactionId;
	}

	public void setBankTransactionId(long bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public BankVo getFromBankVo() {
		return fromBankVo;
	}

	public void setFromBankVo(BankVo fromBankVo) {
		this.fromBankVo = fromBankVo;
	}

	public BankVo getToBankVo() {
		return toBankVo;
	}

	public void setToBankVo(BankVo toBankVo) {
		this.toBankVo = toBankVo;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public long getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(long voucherNo) {
		this.voucherNo = voucherNo;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
}
