package com.croods.umeshtrading.vo.bank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.croods.umeshtrading.vo.account.AccountCustomVo;

@Entity
@Table(name="bank")
public class BankVo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bank_id",length=10)
	private long bankId;

	@Column(name="bank_acno",length=20)
	private String bankAcNo;
	
	@Column(name="bank_name",length=50)
	private String bankName;
	
	@Column(name="bank_branch",length=50)
	private String bankBranch;
	
	@Column(name="bank_ifsc",length=20)
	private String ifscCode;
	
	@Column(name="swift_code",length=20)
	private String swiftCode;
	
	@Column(name="account_holder_name",length=100)
	private String accountHolderName;
	
	@Column(name="address_line_1",length=300,columnDefinition="text")
	private String addressLine1;
	
	@Column(name="address_line_2",length=300,columnDefinition="text")
	private String addressLine2;
	
	@Column(name="countries_code",length=50)
	private String countriesCode;
	
	@Column(name="state_code",length=50)
	private String stateCode;
	
	@Column(name="city_code",length=50)
	private String cityCode;
	
	@Transient
	private String countriesName;
	
	@Transient
	private String stateName;
	
	@Transient
	private String cityName;
	
	@Column(name="pin_code",length=6)
	private String pinCode;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@ManyToOne
	@JoinColumn(name = "account_custom_id", referencedColumnName = "account_custom_id")
	private AccountCustomVo accountCustomVo;

	public long getBankId() {
		return bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public String getBankAcNo() {
		return bankAcNo;
	}

	public void setBankAcNo(String bankAcNo) {
		this.bankAcNo = bankAcNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCountriesCode() {
		return countriesCode;
	}

	public void setCountriesCode(String countriesCode) {
		this.countriesCode = countriesCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountriesName() {
		return countriesName;
	}

	public void setCountriesName(String countriesName) {
		this.countriesName = countriesName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public AccountCustomVo getAccountCustomVo() {
		return accountCustomVo;
	}

	public void setAccountCustomVo(AccountCustomVo accountCustomVo) {
		this.accountCustomVo = accountCustomVo;
	}
	
}
