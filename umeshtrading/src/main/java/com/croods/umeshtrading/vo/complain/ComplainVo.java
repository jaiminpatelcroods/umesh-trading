package com.croods.umeshtrading.vo.complain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.croods.umeshtrading.vo.casetype.CaseTypeVo;
import com.croods.umeshtrading.vo.contact.ContactVo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="complain")
@Getter @Setter
public class ComplainVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "complain_id", length = 10)
	private long complainId;
	
	@ManyToOne
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "complain_no", length = 30)
	private long complainNo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "complain_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date complainDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "due_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dueDate;
	
	@Column(name = "case_channel", length = 50)
	private String caseChannel;
	
	@ManyToOne
	@JoinColumn(name="case_type_id",referencedColumnName="case_type_id")
	private CaseTypeVo caseTypeVo;
	
	@Column(name = "remark", length = 200)
	private String remark;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
}
