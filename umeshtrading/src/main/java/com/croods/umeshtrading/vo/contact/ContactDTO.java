package com.croods.umeshtrading.vo.contact;

import lombok.Data;

public class ContactDTO {

	private long contactId;
	
	private String companyName;

	public ContactDTO(long contactId, String companyName) {
		this.contactId = contactId;
		this.companyName = companyName;
	}
	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
}

/*package com.croods.bssgroup.vo.contact;

import org.springframework.beans.factory.annotation.Value;

import lombok.Data;

//@Projection(name = "deadline", types = { ContactVo.class })
public interface ContactDTO {

	@Value("#{target.contactId}")
	public long getContactId();

	@Value("#{target.companyName}")
	public String getCompanyName();
	
	
}*/
