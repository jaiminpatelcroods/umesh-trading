package com.croods.umeshtrading.vo.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="contact_other")
@Getter @Setter
public class ContactOtherVo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="contact_other_id",length=10)
	private long contactOtherId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@Column(name="name",length=100)
	private String name;
	
	@Column(name="mobileno", length=12)
	private String mobileno;
	
	@Column(name="email", length=50)
	private String email;

}
