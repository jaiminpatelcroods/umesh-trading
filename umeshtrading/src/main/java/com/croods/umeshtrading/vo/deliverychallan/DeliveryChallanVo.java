package com.croods.umeshtrading.vo.deliverychallan;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.umeshtrading.vo.contact.ContactVo;
import com.croods.umeshtrading.vo.employee.EmployeeVo;

import lombok.Data;

@Entity
@Table(name = "delivery_challan")
@DynamicUpdate(value = true)
@Data
public class DeliveryChallanVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "delivery_challan_id", length = 10)
	private long deliveryChallanId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_challan_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date deliveryChallanDate;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "delivery_challan_no", length = 30)
	private long deliveryChallanNo;
	
	@Column(name = "reference_no", length = 50)
	private String referenceNo;
	
	@ManyToOne
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@Column(name="note",length=300,columnDefinition="text")
	private String note;
	
	@ManyToOne
	@JoinColumn(name="received_by",referencedColumnName="employee_id")
	private EmployeeVo receivedBy;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "deliveryChallanVo", cascade = CascadeType.ALL)
	private List<DeliveryChallanItemVo> deliveryChallanItemVos;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
}
