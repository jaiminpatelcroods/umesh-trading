package com.croods.umeshtrading.vo.designation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="designation")
public class DesignationVo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "designation_id", length = 10)
	private long designationId;
	
	@Column(name="designation_name",length=50)
	private String designationName;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;

	public long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(long designationId) {
		this.designationId = designationId;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
}
