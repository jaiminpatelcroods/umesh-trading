package com.croods.umeshtrading.vo.employee;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.croods.umeshtrading.vo.contact.ContactOtherVo;
import com.croods.umeshtrading.vo.department.DepartmentVo;
import com.croods.umeshtrading.vo.designation.DesignationVo;
import com.croods.umeshtrading.vo.userfront.UserFrontVo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name="employee")
@Data
public class EmployeeVo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="employee_id",length=10)
	private long employeeId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="employee_name", length=100)
	private String employeeName;
	
	@Column(name="employee_mobileno", length=12)
	private String employeeMobileno;
	
	@Column(name="alternative_mobileno", length=12)
	private String alternativeMobileno;
	
	@Column(name="employee_email", length=50)
	private String employeeEmail;
	
	@Column(name="alternative_email", length=50)
	private String alternativeEmail;
	
	@Column(name="pan_no",length=20)
	private String panNo;
	
	@Column(name="ctc",length=20)
	private double ctc = 0.0;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "anniversary_date")
	private Date anniversaryDate;
	
	@ManyToOne
	@JoinColumn(name = "department_id", referencedColumnName = "department_id")
	private DepartmentVo departmentVo;
	
	@ManyToOne
	@JoinColumn(name = "designation_id", referencedColumnName = "designation_id")
	private DesignationVo designationVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_front_id", referencedColumnName = "user_front_id")
	private UserFrontVo userFrontVo;
	
	@Column(name="address_line_1",length=300,columnDefinition="text")
	private String addressLine1;
	
	@Column(name="address_line_2",length=300,columnDefinition="text")
	private String addressLine2;
	
	@Column(name="countries_code",length=50)
	private String countriesCode;
	
	@Transient
	String countriesName;
	
	@Column(name="state_code",length=50)
	private String stateCode;
	
	@Transient
	String stateName;
	
	@Column(name="city_code",length=50)
	private String cityCode;
	
	@Transient
	String cityName;
	
	@Column(name="pin_code",length=6)
	private String pinCode;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "employeeVo",cascade = CascadeType.ALL)
	private List<EmployeeContactVo> employeeContactVos;
 }