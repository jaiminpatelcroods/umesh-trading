package com.croods.umeshtrading.vo.farmotype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="farmo_type")
@Data
public class FarmoTypeVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "farmo_id", length = 10)
	private long farmoId;
	
	@Column(name="front_length",columnDefinition = "float default 0.0")
	private float frontLength;
	
	@Column(name="front_seat",columnDefinition = "float default 0.0")
	private float frontSeat;
	
	@Column(name="front_thai",columnDefinition = "float default 0.0")
	private float frontThai;
	
	@Column(name="front_langot",columnDefinition = "float default 0.0")
	private float frontLangot;
	
	@Column(name="front_knee",columnDefinition = "float default 0.0")
	private float frontKnee;
	
	@Column(name="front_mori_munda",columnDefinition = "float default 0.0")
	private float frontMoriMunda;
	
	@Column(name="front_jati",columnDefinition = "float default 0.0")
	private float frontJati;
	
	@Column(name="back_length",columnDefinition = "float default 0.0")
	private float backLength;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="is_default",length=1,columnDefinition="int default 0")
	private int isDefault;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
}
