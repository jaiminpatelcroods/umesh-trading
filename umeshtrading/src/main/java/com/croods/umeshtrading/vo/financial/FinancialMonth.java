package com.croods.umeshtrading.vo.financial;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "financial_month")
public class FinancialMonth {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "financial_month_id",length=10)
	private long financialMonth;
	
	@Column(name="month_interval",length=100)
	private String monthInterval;
	
	@Column(name="month_interval_value",length=100)
	private String monthIntervalValue;

	public long getFinancialMonth() {
		return financialMonth;
	}

	public void setFinancialMonth(long financialMonth) {
		this.financialMonth = financialMonth;
	}

	public String getMonthInterval() {
		return monthInterval;
	}

	public void setMonthInterval(String monthInterval) {
		this.monthInterval = monthInterval;
	}

	public String getMonthIntervalValue() {
		return monthIntervalValue;
	}

	public void setMonthIntervalValue(String monthIntervalValue) {
		this.monthIntervalValue = monthIntervalValue;
	}
	
	
}
