package com.croods.umeshtrading.vo.financial;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "financial_year")
public class FinancialYear {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "financial_year_id",length=10)
	private long financialYearId;
	
	@Column(name="year_interval",length=100)
	private String yearInterval;

	public long getFinancialYearId() {
		return financialYearId;
	}

	public void setFinancialYearId(long financialYearId) {
		this.financialYearId = financialYearId;
	}

	public String getYearInterval() {
		return yearInterval;
	}

	public void setYearInterval(String yearInterval) {
		this.yearInterval = yearInterval;
	}
	
	
}
