package com.croods.umeshtrading.vo.financial;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.umeshtrading.vo.account.AccountCustomVo;

@Entity
@Table(name = "year_wise_opening_balance")
public class YearWiseOpeningBalanceVo {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "opening_balance_id",length=10)
	private long openingBalanceId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="debit_amount",length=50)
	private double debitAmount=0.0;
	
	@Column(name="credit_amount",length=50)
	private double creditAmount=0.0;
	
	@ManyToOne
	@JoinColumn(name = "account_custom_id", referencedColumnName = "account_custom_id")
	private AccountCustomVo accountCustomVo;
	
	@Column(name="year_interval",length=100)
	private String yearInterval;

	public long getOpeningBalanceId() {
		return openingBalanceId;
	}

	public void setOpeningBalanceId(long openingBalanceId) {
		this.openingBalanceId = openingBalanceId;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public AccountCustomVo getAccountCustomVo() {
		return accountCustomVo;
	}

	public void setAccountCustomVo(AccountCustomVo accountCustomVo) {
		this.accountCustomVo = accountCustomVo;
	}

	public String getYearInterval() {
		return yearInterval;
	}

	public void setYearInterval(String yearInterval) {
		this.yearInterval = yearInterval;
	}
}
