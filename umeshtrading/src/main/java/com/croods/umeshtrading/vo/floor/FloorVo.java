package com.croods.umeshtrading.vo.floor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.croods.umeshtrading.vo.place.PlaceVo;

import lombok.Data;

@Entity
@Table(name="floor")
@Data
public class FloorVo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "floor_id", length = 10)
	private long floorId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	//@CreationTimestamp
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	//@UpdateTimestamp
	@Column(name="modified_on",length=50)
	private String modifiedOn;

	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;

	@Column(name="floor_name",length=150)
	private String floorName;
	
	@Column(name="floor_code",length=50)
	private String floorCode;
	
	@Column(name = "description", length = 300, columnDefinition = "text")
	private String description;
	
	@ManyToOne
	@JoinColumn(name="place_id",referencedColumnName="place_id")
	private PlaceVo placeVo;
}
