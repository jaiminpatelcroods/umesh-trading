package com.croods.umeshtrading.vo.journalvoucher;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.umeshtrading.vo.account.AccountCustomVo;

@Entity
@Table(name = "journal_voucher_account")
public class JournalVoucherAccountVo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "journal_voucher_account_id", length = 10)
	private long journalVoucherAccountId;
	
	@ManyToOne
	@JoinColumn(name = "journal_voucher_id", referencedColumnName = "journal_voucher_id")
	private JournalVoucherVo journalVoucherVo;
	
	@ManyToOne
	@JoinColumn(name = "account_custom_id", referencedColumnName = "account_custom_id")
	private AccountCustomVo fromAccountCustomVo;
	
	@Column(name = "credit", length =30)
	private double credit;
	
	@Column(name = "debit", length =30)
	private double debit;

	public long getJournalVoucherAccountId() {
		return journalVoucherAccountId;
	}

	public void setJournalVoucherAccountId(long journalVoucherAccountId) {
		this.journalVoucherAccountId = journalVoucherAccountId;
	}

	public JournalVoucherVo getJournalVoucherVo() {
		return journalVoucherVo;
	}

	public void setJournalVoucherVo(JournalVoucherVo journalVoucherVo) {
		this.journalVoucherVo = journalVoucherVo;
	}

	public AccountCustomVo getFromAccountCustomVo() {
		return fromAccountCustomVo;
	}

	public void setFromAccountCustomVo(AccountCustomVo fromAccountCustomVo) {
		this.fromAccountCustomVo = fromAccountCustomVo;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}
}
