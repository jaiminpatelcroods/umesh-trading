package com.croods.umeshtrading.vo.lead;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.croods.umeshtrading.vo.account.AccountCustomVo;
import com.querydsl.core.annotations.QueryProjection;

import lombok.Data;


@Entity
@Table(name="lead")
@Data
public class LeadVo
 {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="lead_id",length=10)
	private long leadId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="lead_source",length=50)
	private String leadSource;
	
	@Column(name="reference_name",length=50)
	private String referenceName;
	
	@Column(name="reference_mobileno",length=15)
	private String referenceMobileno;
	
	@Column(name="compny_name", length=100)
	private String companyName;
	
	@Column(name="company_mobileno", length=12)
	private String companyMobileno;
	
	@Column(name="company_email", length=50)
	private String companyEmail;
	
	@Column(name="company_telephone",length=17)
	private String companyTelephone;
	
	@Column(name="owner_name", length=100)
	private String ownerName;
	
	@Column(name="owner_mobileno", length=12)
	private String ownerMobileno;
	
	@Column(name="concern_person_name", length=100)
	private String concernPersonName;
	
	@Column(name="concern_person_mobileno", length=12)
	private String concernPersonMobileno;
	
	@Column(name="lead_type",length=50)
	private String leadType;
	
	@Column(name="gst_type",length=50)
	private String gstType;
	
	@Column(name="gstin",length=20)
	private String gstin;
	
	@Column(name="pan_no",length=20)
	private String panNo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "anniversary_date")
	private Date anniversaryDate;
	
	@Column(name="bank_name",length=50)
	private String bankName;
	
	@Column(name="bank_branch",length=50)
	private String bankBranch;
	
	@Column(name="bank_accountno",length=20)
	private String bankAccountNo;
	
	@Column(name="bank_ifsc",length=20)
	private String bankIfsc;
	
	@Column(name="no_of_shops", columnDefinition="int default 0")
	private int noOfShops;
	
	@Column(name="previous_supplier",length=50)
	private String previousSupplier;
	
	@Column(name="payment_type",length=20)
	private String paymentType;
	
	@Column(name="credit_days", columnDefinition="int default 0")
	private int creditDays;
	
	@Column(name="maximum_credit_limit", columnDefinition="double precision default 0")
	private double maximumCreditLimit;
	
	@Column(name="interested_product_ids",length=100)
	private String interestedProductIds;
	
	//quantity OR amount
	@Column(name="interested_product_requirements",length=70)
	private String interestedProductRequirements;
	
	@Column(name="interested_product_value",length=50)
	private String interestedProductValue;
	
	@Column(name="agent_id",length=10)
	private long agentId;
	
	@Column(name="agent_commission", columnDefinition="float default 0.0")
	private float agentCommission;
	
	@Column(name="working_time",length=50)
	private String workingTime;
	
	@Column(name="service_available",length=50)
	private String serviceAvailable;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "leadVo",cascade = CascadeType.ALL)
	private List<LeadAddressVo> leadAddressVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "leadVo",cascade = CascadeType.ALL)
	private List<LeadContactVo> leadContactVos;
	
 }