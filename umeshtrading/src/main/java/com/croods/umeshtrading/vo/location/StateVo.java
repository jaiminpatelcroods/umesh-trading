package com.croods.umeshtrading.vo.location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="loc_state")
public class StateVo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "state_id", length = 10)
	private long stateId;
	
	@Column(name="state_code",length=20)
	private String stateCode;
	
	@Column(name="state_name",length=50)
	private String stateName;
	
	@Column(name="countries_code",length=10)
	private String countriesCode;
	
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountriesCode() {
		return countriesCode;
	}

	public void setCountriesCode(String countriesCode) {
		this.countriesCode = countriesCode;
	}

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}
}
