package com.croods.umeshtrading.vo.material;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.croods.umeshtrading.vo.floor.FloorVo;
import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.rack.RackVo;
import com.croods.umeshtrading.vo.tax.TaxVo;

@Entity
@Table(name = "material_item")
public class MaterialItemVo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "material_item_id", length = 10)
	private long materialItemId;

	@ManyToOne
	@JoinColumn(name = "material_id", referencedColumnName = "material_id")
	MaterialVo materialVo;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	private ProductVo productVo;

	@Column(name = "qty", length = 10)
	private float qty;

	@Column(name = "product_description", length = 500)
	private String productDescription;

	@Column(name = "price", length = 10)
	private double price = 0.0;

	@Column(name = "tax_amount", length = 10)
	private double taxAmount = 0.0;

	@Column(name = "tax_rate", length = 10)
	private double taxRate = 0.0;

	@ManyToOne
	@JoinColumn(name = "tax_id", referencedColumnName = "tax_id")
	private TaxVo taxVo;

	@Column(name = "discount", length = 6)
	private double discount = 0.0;

	@Column(name = "discount_type", length = 20)
	String discountType;

	/*@Column(name = "batch_no", length = 50)
	private String batchNo;*/
	
	/*@Transient
	private String oldProductBatchnoQty;*/
	
	@ManyToOne
	@JoinColumn(name = "place_id", referencedColumnName = "place_id")
	private PlaceVo placeVo;
	
	@ManyToOne
	@JoinColumn(name = "floor_id", referencedColumnName = "floor_id")
	private FloorVo floorVo;
	
	@ManyToOne
	@JoinColumn(name = "rack_id", referencedColumnName = "rack_id")
	private RackVo rackVo;
	
	@Column(name="type",length=50)
	private String type;
	
	@Transient
	private String oldProductRackQty;

	public long getMaterialItemId() {
		return materialItemId;
	}

	public void setMaterialItemId(long materialItemId) {
		this.materialItemId = materialItemId;
	}

	public MaterialVo getMaterialVo() {
		return materialVo;
	}

	public void setMaterialVo(MaterialVo materialVo) {
		this.materialVo = materialVo;
	}

	public ProductVo getProductVo() {
		return productVo;
	}

	public void setProductVo(ProductVo productVo) {
		this.productVo = productVo;
	}

	public float getQty() {
		return qty;
	}

	public void setQty(float qty) {
		this.qty = qty;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public TaxVo getTaxVo() {
		return taxVo;
	}

	public void setTaxVo(TaxVo taxVo) {
		this.taxVo = taxVo;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public RackVo getRackVo() {
		return rackVo;
	}

	public void setRackVo(RackVo rackVo) {
		this.rackVo = rackVo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PlaceVo getPlaceVo() {
		return placeVo;
	}

	public void setPlaceVo(PlaceVo placeVo) {
		this.placeVo = placeVo;
	}

	public FloorVo getFloorVo() {
		return floorVo;
	}

	public void setFloorVo(FloorVo floorVo) {
		this.floorVo = floorVo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOldProductRackQty() {
		return oldProductRackQty;
	}

	public void setOldProductRackQty(String oldProductRackQty) {
		this.oldProductRackQty = oldProductRackQty;
	}

}