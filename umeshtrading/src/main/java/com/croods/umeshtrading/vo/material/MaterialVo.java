package com.croods.umeshtrading.vo.material;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.croods.umeshtrading.vo.contact.ContactVo;



@Entity
@Table(name = "material")
@DynamicUpdate(value=true)
public class MaterialVo {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "material_id", length = 10)
	private long materialId;
	
	@Column(name = "material_no")
	private long materialNo;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "material_date")
	private Date materialDate;
	
	/*@Temporal(TemporalType.DATE)
	@Column(name = "due_date")
	private Date dueDate;*/
	
	@Column(name="sez",columnDefinition="int default 0")
	private int sez;
 
	/*@ManyToOne
	@JoinColumn(name="parent_id",referencedColumnName="jobwork_id")
	private JobworkVo jobworkVo;*/
	
	@ManyToOne
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@Column(name="company_id",length=10)
	private long companyId;	
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="type",length=50)
	private String type;
	
	@Column(name="tax_type",columnDefinition="int default 0")
	private int taxType;
	
	@Column(name = "total", length = 10)
	private double total;
	
	@Column(name = "roundoff", length = 10, columnDefinition = "float default 0.0")
	private float roundoff;
	
	@Column(name="status",length=50)
	private String status;
	
	@Column(name = "paid_amount", length = 10)
	private double paidAmount;
	
	@Column(name = "reverse_charge", length = 10, columnDefinition = "int default 0")
	private int reverseCharge;

	/**Billing Address**/
	
	@Column(name="billing_compny_name")
	private String billingCompanyName;
	
	@Column(name="billing_name")
	private String billingName; 
	
	@Column(name="billing_address_line_1",length=300,columnDefinition="text")
	private String billingAddressLine1;
	
	@Column(name="billing_address_line_2",length=300,columnDefinition="text")
	private String billingAddressLine2;
	
	@Column(name="billing_countries_code",length=50)
	private String billingCountriesCode;
	
	@Transient
	String billingCountriesName;
	
	@Column(name="billing_state_code",length=50)
	private String billingStateCode;
	
	@Transient
	String billingStateName;
	
	@Column(name="billing_city_code",length=50)
	private String billingCityCode;
	
	@Transient
	String billingCityName;
	
	@Column(name="billing_pin_code",length=6)
	private String billingPinCode;
	
	/**Shipping Address**/
	
	@Column(name="shipping_compny_name")
	private String shippingCompanyName;
	
	@Column(name="shipping_name")
	private String shippingName; 
	
	@Column(name="shipping_address_line_1",length=300,columnDefinition="text")
	private String shippingAddressLine1;
	
	@Column(name="shipping_address_line_2",length=300,columnDefinition="text")
	private String shippingAddressLine2;
	
	@Column(name="shipping_countries_code",length=50)
	private String shippingCountriesCode;
	
	@Transient
	String shippingCountriesName;
	
	@Column(name="shipping_state_code",length=50)
	private String shippingStateCode;
	
	@Transient
	String shippingStateName;
	
	@Column(name="shipping_city_code",length=50)
	private String shippingCityCode;
	
	@Transient
	String shippingCityName;
	
	@Column(name="shipping_pin_code",length=6)
	private String shippingPinCode;
	
	/*@Column(name="terms_and_condition_ids",length=80)
	String termsAndConditionIds;*/

	@Column(name="note",length=300)
	String note;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "materialVo", cascade = CascadeType.ALL)
	private List<MaterialItemVo> materialItemVos;

	public long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(long materialId) {
		this.materialId = materialId;
	}

	public long getMaterialNo() {
		return materialNo;
	}

	public void setMaterialNo(long materialNo) {
		this.materialNo = materialNo;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Date getMaterialDate() {
		return materialDate;
	}

	public void setMaterialDate(Date materialDate) {
		this.materialDate = materialDate;
	}

	public int getSez() {
		return sez;
	}

	public void setSez(int sez) {
		this.sez = sez;
	}

	public ContactVo getContactVo() {
		return contactVo;
	}

	public void setContactVo(ContactVo contactVo) {
		this.contactVo = contactVo;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTaxType() {
		return taxType;
	}

	public void setTaxType(int taxType) {
		this.taxType = taxType;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public float getRoundoff() {
		return roundoff;
	}

	public void setRoundoff(float roundoff) {
		this.roundoff = roundoff;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public int getReverseCharge() {
		return reverseCharge;
	}

	public void setReverseCharge(int reverseCharge) {
		this.reverseCharge = reverseCharge;
	}

	public String getBillingCompanyName() {
		return billingCompanyName;
	}

	public void setBillingCompanyName(String billingCompanyName) {
		this.billingCompanyName = billingCompanyName;
	}

	public String getBillingName() {
		return billingName;
	}

	public void setBillingName(String billingName) {
		this.billingName = billingName;
	}

	public String getBillingAddressLine1() {
		return billingAddressLine1;
	}

	public void setBillingAddressLine1(String billingAddressLine1) {
		this.billingAddressLine1 = billingAddressLine1;
	}

	public String getBillingAddressLine2() {
		return billingAddressLine2;
	}

	public void setBillingAddressLine2(String billingAddressLine2) {
		this.billingAddressLine2 = billingAddressLine2;
	}

	public String getBillingCountriesCode() {
		return billingCountriesCode;
	}

	public void setBillingCountriesCode(String billingCountriesCode) {
		this.billingCountriesCode = billingCountriesCode;
	}

	public String getBillingCountriesName() {
		return billingCountriesName;
	}

	public void setBillingCountriesName(String billingCountriesName) {
		this.billingCountriesName = billingCountriesName;
	}

	public String getBillingStateCode() {
		return billingStateCode;
	}

	public void setBillingStateCode(String billingStateCode) {
		this.billingStateCode = billingStateCode;
	}

	public String getBillingStateName() {
		return billingStateName;
	}

	public void setBillingStateName(String billingStateName) {
		this.billingStateName = billingStateName;
	}

	public String getBillingCityCode() {
		return billingCityCode;
	}

	public void setBillingCityCode(String billingCityCode) {
		this.billingCityCode = billingCityCode;
	}

	public String getBillingCityName() {
		return billingCityName;
	}

	public void setBillingCityName(String billingCityName) {
		this.billingCityName = billingCityName;
	}

	public String getBillingPinCode() {
		return billingPinCode;
	}

	public void setBillingPinCode(String billingPinCode) {
		this.billingPinCode = billingPinCode;
	}

	public String getShippingCompanyName() {
		return shippingCompanyName;
	}

	public void setShippingCompanyName(String shippingCompanyName) {
		this.shippingCompanyName = shippingCompanyName;
	}

	public String getShippingName() {
		return shippingName;
	}

	public void setShippingName(String shippingName) {
		this.shippingName = shippingName;
	}

	public String getShippingAddressLine1() {
		return shippingAddressLine1;
	}

	public void setShippingAddressLine1(String shippingAddressLine1) {
		this.shippingAddressLine1 = shippingAddressLine1;
	}

	public String getShippingAddressLine2() {
		return shippingAddressLine2;
	}

	public void setShippingAddressLine2(String shippingAddressLine2) {
		this.shippingAddressLine2 = shippingAddressLine2;
	}

	public String getShippingCountriesCode() {
		return shippingCountriesCode;
	}

	public void setShippingCountriesCode(String shippingCountriesCode) {
		this.shippingCountriesCode = shippingCountriesCode;
	}

	public String getShippingCountriesName() {
		return shippingCountriesName;
	}

	public void setShippingCountriesName(String shippingCountriesName) {
		this.shippingCountriesName = shippingCountriesName;
	}

	public String getShippingStateCode() {
		return shippingStateCode;
	}

	public void setShippingStateCode(String shippingStateCode) {
		this.shippingStateCode = shippingStateCode;
	}

	public String getShippingStateName() {
		return shippingStateName;
	}

	public void setShippingStateName(String shippingStateName) {
		this.shippingStateName = shippingStateName;
	}

	public String getShippingCityCode() {
		return shippingCityCode;
	}

	public void setShippingCityCode(String shippingCityCode) {
		this.shippingCityCode = shippingCityCode;
	}

	public String getShippingCityName() {
		return shippingCityName;
	}

	public void setShippingCityName(String shippingCityName) {
		this.shippingCityName = shippingCityName;
	}

	public String getShippingPinCode() {
		return shippingPinCode;
	}

	public void setShippingPinCode(String shippingPinCode) {
		this.shippingPinCode = shippingPinCode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<MaterialItemVo> getMaterialItemVos() {
		return materialItemVos;
	}

	public void setMaterialItemVos(List<MaterialItemVo> materialItemVos) {
		this.materialItemVos = materialItemVos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
