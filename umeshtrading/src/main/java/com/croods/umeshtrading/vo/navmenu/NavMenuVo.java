package com.croods.umeshtrading.vo.navmenu;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table(name="nav_menu")
public class NavMenuVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "nav_menu_id", length = 10)
	private long navMenuId;
	
	@Column(name="title",length=50)
	private String title;

	@Column(name="menu_url",length=50)
	private String menuURL;

	@Column(name="icon_class",length=50)
	private String iconClass;

	@Column(name="status",length=50)
	private String status;

	@Column(name="ordering")
	private int ordering;

	public long getNavMenuId() {
		return navMenuId;
	}

	public void setNavMenuId(long navMenuId) {
		this.navMenuId = navMenuId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMenuURL() {
		return menuURL;
	}

	public void setMenuURL(String menuURL) {
		this.menuURL = menuURL;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getOrdering() {
		return ordering;
	}

	public void setOrdering(int ordering) {
		this.ordering = ordering;
	}

	
}
