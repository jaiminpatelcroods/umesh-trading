package com.croods.umeshtrading.vo.payment;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.umeshtrading.vo.bank.BankVo;
import com.croods.umeshtrading.vo.contact.ContactVo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "payment")
@Getter @Setter
public class PaymentVo {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "payment_id",length=10)
	private long paymentId;
	
	@ManyToOne
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@ManyToOne
	@JoinColumn(name="bank_id",referencedColumnName="bank_id")
	private BankVo bankVo;
	
	@Column(name="payment_mode",length=50)
	private String paymentMode;
	
	@Column(name="type",length=50)
	private String type;
	
	@Column(name="description",length=300,columnDefinition="text")
	private String description;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date paymentDate;
	
	@Column(name = "payment_no",length=10)
	private long paymentNo;
		
	@Column(name = "total_payment",length=10)
	private double totalPayment;
	
	@Column(name = "prefix",length=50)
	private String prefix;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "paymentVo", cascade = CascadeType.ALL)
	List<PaymentBillVo> paymentBillVos;
	
	@Column(name="company_id",length=10,updatable=false)
	private long companyId;	
	
	@Column(name="branch_id",length=10,updatable=false)
	private long branchId;	
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
}
