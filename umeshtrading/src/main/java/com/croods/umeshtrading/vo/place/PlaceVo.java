package com.croods.umeshtrading.vo.place;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "place")
@Data
public class PlaceVo {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "place_id", length = 10)
	private long placeId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name = "place_name", length = 150)
	private String placeName;
	
	@Column(name="place_code",length=50)
	private String placeCode;
	
	@Column(name = "description", length = 300, columnDefinition = "text")
	private String description;
	
	@Column(name = "is_default", length = 1, columnDefinition = "int default 0")
	private int isDefault;
	
}
