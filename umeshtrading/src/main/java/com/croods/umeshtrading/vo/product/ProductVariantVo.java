package com.croods.umeshtrading.vo.product;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "product_variant")
@Data
public class ProductVariantVo  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_variant_id", length = 10)
	private long productVariantId;
	
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	private ProductVo productVo;
	
	@Column(name = "variant_name", length = 150)
	private String variantName;
	
	@Column(name = "position", length = 150)
	private String position;
	
	@Column(name = "purchase_price", columnDefinition="double precision default 0")
	private double purchasePrice;
	
	@Column(name = "retailer_price", columnDefinition="double precision default 0")
	private double retailerPrice;
	
	@Column(name = "wholesaler_price", columnDefinition="double precision default 0")
	private double wholesalerPrice;	
	
	@Column(name = "other_price", columnDefinition="double precision default 0")
	private double otherPrice;
	
	@Column(name = "attribute_value_1", length = 150)
	private String attributeValue1;
	
	@Column(name = "attribute_value_2", length = 150)
	private String attributeValue2;
	
	@Column(name = "attribute_value_3", length = 150)
	private String attributeValue3;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Transient
	private double qty;
}
