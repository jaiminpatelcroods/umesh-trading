package com.croods.umeshtrading.vo.product;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.croods.umeshtrading.vo.brand.BrandVo;
import com.croods.umeshtrading.vo.category.CategoryVo;
import com.croods.umeshtrading.vo.tax.TaxVo;
import com.croods.umeshtrading.vo.unitofmeasurement.UnitOfMeasurementVo;

import lombok.Data;

@Entity
@Table(name = "product")
@Data
public class ProductVo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id", length = 10)
	private long productId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name = "name", length = 150)
	private String name;
	
	@Column(name = "display_name", length = 150)
	private String displayName;
	
	@Column(name = "description", length = 300, columnDefinition = "text")
	private String description;
	
	@Column(name = "hsn_code", length = 100)
	private String hsnCode;
	
	@ManyToOne
	@JoinColumn(name="category_id",referencedColumnName="category_id")
	private CategoryVo categoryVo;	
	
	@ManyToOne
	@JoinColumn(name="brand_id",referencedColumnName="brand_id")
	private BrandVo brandVo;
	
	@ManyToOne
	@JoinColumn(name = "measurement_id", referencedColumnName = "measurement_id")
	private UnitOfMeasurementVo unitOfMeasurementVo;
	
	@ManyToOne
	@JoinColumn(name = "purchase_tax_id", referencedColumnName = "tax_id")
	private TaxVo purchaseTaxVo;
	
	@ManyToOne
	@JoinColumn(name = "sales_tax_id", referencedColumnName = "tax_id")
	private TaxVo salesTaxVo;
	
	@Column(name = "purchase_tax_included", columnDefinition = "int default 0")
	private int purchaseTaxIncluded;
	
	@Column(name = "sales_tax_included", columnDefinition = "int default 0")
	private int salesTaxIncluded;
	
	@Column(name = "purchase_price", columnDefinition="double precision default 0")
	private double purchasePrice;
	
	@Column(name = "sale_price", columnDefinition="double precision default 0")
	private double salePrice;
	
	@Column(name = "market_price", columnDefinition="double precision default 0")
	private double marketPrice;	
	
	@Column(name = "discount_type", length = 20)
	String discountType;
	
	@Column(name = "discount", length = 6)
	private double discount = 0.0; //discount will work only for Purchase (not for sale)

	@Column(name = "min_stock", columnDefinition="double precision default 0")
	private double minStock;
	
	@Column(name = "max_stock", columnDefinition="double precision default 0")
	private double maxStock;
	
	@Column(name = "have_variation", length = 1, columnDefinition = "int default 0")
	private int haveVariation;
	
	@Column(name = "selling_margin", length = 6, columnDefinition="double precision default 0")
	private double sellingMargin = 0.0;
	
	/* Jaimin @LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "productVo",cascade = CascadeType.ALL)
	private List<ProductVariantVo> productVariantVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "productVo",cascade = CascadeType.ALL)
	private List<ProductAttributeVo> productAttributeVos;
	
	@Column(name = "have_design_no", columnDefinition = "int default 0")
	private int haveDesignno;
	
	@Column(name = "have_bale_no", columnDefinition = "int default 0")
	private int haveBaleno;*/
	
	@Column(name = "applicable_type", columnDefinition = "int default 0")
	private int applicableType;
}
