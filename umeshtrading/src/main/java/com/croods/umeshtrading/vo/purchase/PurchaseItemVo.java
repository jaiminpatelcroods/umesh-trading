package com.croods.umeshtrading.vo.purchase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.umeshtrading.vo.product.ProductVariantVo;
import com.croods.umeshtrading.vo.tax.TaxVo;

import lombok.Data;

@Entity
@Table(name = "purchase_item")
@Data
public class PurchaseItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "purchase_item_id", length = 10)
	private long purchaseItemId;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "purchase_id", referencedColumnName = "purchase_id")
	private PurchaseVo purchaseVo;
	
	@Column(name = "qty",length=10)
	private float qty;
	
	@Column(name = "product_description",length=50)
	private String productDescription;
	
	@Column(name = "price",length=10)
	private double price = 0.0;
	
	@Column(name = "tax_amount",length=10)
	private double taxAmount=0.0;
	
	@Column(name = "tax_rate",length=10)
	private double taxRate=0.0;
	
	@ManyToOne
	@JoinColumn(name="tax_id",referencedColumnName="tax_id")
	private TaxVo taxVo;
	
	@Column(name="discount",length=6)
	private double discount=0.0;
	
	@Column(name="discount_type",length=20)
	String discountType;
	
	@Column(name="design_no",length=50)
	private String designNo;
	
	@Column(name="bale_no",length=50)
	private String baleNo;
}
