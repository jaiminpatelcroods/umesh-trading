package com.croods.umeshtrading.vo.purchaserequest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.umeshtrading.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "purchase_request_item")
@Data
public class PurchaseRequestItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "purchase_request_item_id", length = 10)
	private long purchaseRequestItemId;
	
	@Column(name="qty")
	private double qty=0.0;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "purchase_request_id", referencedColumnName = "purchase_request_id")
	private PurchaseRequestVo purchaseRequestVo;
}
