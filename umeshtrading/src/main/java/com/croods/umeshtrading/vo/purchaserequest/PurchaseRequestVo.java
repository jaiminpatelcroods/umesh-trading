package com.croods.umeshtrading.vo.purchaserequest;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.umeshtrading.vo.employee.EmployeeVo;
import com.croods.umeshtrading.vo.journalvoucher.JournalVoucherAccountVo;
import com.croods.umeshtrading.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "purchase_request")
@Data
public class PurchaseRequestVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "purchase_request_id", length = 10)
	private long purchaseRequestId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "purchase_request_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date requestDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "require_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date requireDate;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "request_no", length = 30)
	private long requestNo;
	
	@Column(name="note",length=300,columnDefinition="text")
	private String note;
	
	@Column(name="status",length=50)
	private String status;
	
	@ManyToOne
	@JoinColumn(name="approveby_id",referencedColumnName="employee_id")
	private EmployeeVo approveBy;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "purchaseRequestVo", cascade = CascadeType.ALL)
	private List<PurchaseRequestItemVo> purchaseRequestItemVos;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
}
