package com.croods.umeshtrading.vo.rack;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.umeshtrading.vo.floor.FloorVo;

import lombok.Data;

@Entity
@Table(name="rack")
@Data
public class RackVo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rack_id", length = 10)
	private long rackId;
	
	/*@Column(name="no_of_row",columnDefinition="int default 0")
	private int noOfRow;
	
	@Column(name="no_of_column",columnDefinition="int default 0")
	private int noOfColumn;*/
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="rack_name",length=150)
	private String rackName;
	
	@Column(name="rack_code",length=50)
	private String rackCode;
	
	@Column(name = "description", length = 300, columnDefinition = "text")
	private String description;
	
	@ManyToOne
	@JoinColumn(name="floor_id",referencedColumnName="floor_id")
	private FloorVo floorVo;
}
