package com.croods.umeshtrading.vo.stock;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.croods.umeshtrading.vo.contact.ContactVo;
import com.croods.umeshtrading.vo.floor.FloorVo;
import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.rack.RackVo;

import lombok.Data;

@Entity
@Table(name = "stock_transaction")
@Data
public class StockTransactionVo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "stock_transaction_id", length = 10)
	private long stockTransactionId;

	@Column(name = "stock_transaction_date")
	Date stockTransactionDate;

	@Column(name = "company_id", length = 10)
	private long companyId;

	@Column(name = "type", length = 100)
	private String type;

	@Column(name = "type_id", length = 100)
	private long typeId;

	@Column(name = "in_quantity", length = 50)
	private double inQuantity;

	@Column(name = "out_quantity", length = 50)
	private double outQuantity;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	private ProductVo productVo;

	@Column(name = "year_interval", length = 100)
	private String yearInterval;

	@Column(name = "product_price", length = 50)
	private double productPrice;

	@Column(name = "description", length = 100)
	private String description;

	/*@Column(name = "batch_no", length = 100)
	private String batchNo;*/

	@ManyToOne
	@JoinColumn(name = "place_id", referencedColumnName = "place_id")
	private PlaceVo placeVo;
	
	@ManyToOne
	@JoinColumn(name = "floor_id", referencedColumnName = "floor_id")
	private FloorVo floorVo;
	
	@ManyToOne
	@JoinColumn(name = "rack_id", referencedColumnName = "rack_id")
	private RackVo rackVo;
	
	@ManyToOne
	@JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
	private ContactVo contactVo;
	
	@CreationTimestamp
	@Column
	private LocalDateTime createDateTime;
}
