package com.croods.umeshtrading.vo.stock;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.umeshtrading.vo.contact.ContactVo;
import com.croods.umeshtrading.vo.floor.FloorVo;
import com.croods.umeshtrading.vo.place.PlaceVo;
import com.croods.umeshtrading.vo.product.ProductVo;
import com.croods.umeshtrading.vo.rack.RackVo;

import lombok.Data;

@Entity
@Table(name = "stock_master")
@Data
public class StockVo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "stock_master_id", length = 10)
	private long stockMasterId;

	@Column(name = "company_id", length = 10)
	private long companyId;

	@Column(name = "quantity", length = 50)
	private double quantity;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	private ProductVo productVo;

	/*@ManyToOne
	@JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
	private ContactVo contactVo;*/

	/*@Column(name = "batch_no", length = 100)
	private String batchNo;*/

	@ManyToOne
	@JoinColumn(name = "place_id", referencedColumnName = "place_id")
	private PlaceVo placeVo;
	
	@ManyToOne
	@JoinColumn(name = "floor_id", referencedColumnName = "floor_id")
	private FloorVo floorVo;
	
	@ManyToOne
	@JoinColumn(name = "rack_id", referencedColumnName = "rack_id")
	private RackVo rackVo;
	
}
