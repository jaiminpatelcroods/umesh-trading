package com.croods.umeshtrading.vo.termsandcondition;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="termandcondition_master")
public class TermsAndConditionVo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="termandcondition_id",length=10)
	private long termsandConditionId;

	@Column(name="terms_condition")
	private String termsCondition;

	@Column(name="modules",length=100)
	private String modules;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="is_default",length=1,columnDefinition="int default 0")
	private int isDefault;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;

	public long getTermsandConditionId() {
		return termsandConditionId;
	}

	public void setTermsandConditionId(long termsandConditionId) {
		this.termsandConditionId = termsandConditionId;
	}

	public String getTermsCondition() {
		return termsCondition;
	}

	public void setTermsCondition(String termsCondition) {
		this.termsCondition = termsCondition;
	}

	public String getModules() {
		return modules;
	}

	public void setModules(String modules) {
		this.modules = modules;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
}
