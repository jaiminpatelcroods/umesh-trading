package com.croods.umeshtrading.vo.userfront;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.croods.umeshtrading.vo.userrole.UserRoleVo;

@Entity
@Table(name="user_front")
public class UserFrontVo {
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "user_front_id", length = 10)
		private long userFrontId;
		
		@ManyToOne
		@JoinColumn(name="parent_id",referencedColumnName = "user_front_id")
		private UserFrontVo userFrontVo;	
		
		@ManyToMany
		@JoinTable(name = "user_front_role", joinColumns = @JoinColumn(name = "user_front_id"), inverseJoinColumns = @JoinColumn(name = "user_role_id"))   
		private List<UserRoleVo> roles;
		 
		@Column(name = "countries_code", length = 20)
		private String countriesCode;
		
		@Transient
		private String countriesName;
		
		@Column(name = "state_code", length = 20)
		private String stateCode;
		
		@Transient
		private String stateName;
		
		@Column(name = "city_code", length = 20)
		private String cityCode;
		
		@Transient
		private String cityName;
		
		@Column(name = "name", length = 150)
		private String name;
		
		@Column(name = "email", length = 80)
		private String email;
		
		@Column(name = "address", length = 200)
		private String address;
		
		@Column(name = "pincode", length = 6)
		private String pincode;
		
		@Column(name = "contact_name", length = 100)
		private String contactName;
		
		@Column(name = "contact_no", length = 30)
		private String contactNo;
		
		@Column(name = "user_name", length = 50)
		private String userName;
		
		@Column(name = "password")
		private String password;
		
		@Column(name = "bank_acholder_name", length = 50)
		private String bankAcholderName;
		
		@Column(name = "bank_acno", length = 30)
		private String bankAcno;
		
		@Column(name = "bank_branch", length = 50)
		private String bankBranch;
		
		@Column(name = "bank_ifsc", length = 20)
		private String bankIFSC;
		
		@Column(name = "bank_name", length = 50)
		private String bankName;
		
		@Column(name = "bank_swift_code", length = 50)
		private String bankSwiftCode;
		
		@Column(name = "bank_iban_no", length = 30)
		private String ibanNo;
		
		@Column(name = "gst", length = 20)
		private String gst;
		
		@Column(name = "pan_no", length = 30)
		private String panNo;
		
		@Column(name = "telephone", length = 20)
		private String telephone;
		
		@Column(name = "website", length = 100)
		private String website;
		
		@Column(name = "status", length = 20)
		private String status;
		
		@Column(name = "is_deleted", length = 1, columnDefinition = "int default 0")
		private int isDeleted;
		
		@Column(name = "created_on", length = 50,updatable=false)
		private String createdOn;
		
		@Column(name = "modified_on", length = 50)
		private String modifiedOn;
		
		@Column(name = "tocken_no", length = 100)
		private String tockenNo;

		@Column(name = "sender_id", length = 10)
		private String senderId;
		
		@Column(name="logo",length=50)
		private String logo;
		
		@Column(name="gst_registration_type",length=100)
		private String gstRegistrationType;
		
		@Temporal(TemporalType.DATE)
		@Column(name = "financial_year")
		private Date financialYear;
		
		@Column(name="month_interval",length=100)
		private String monthInterval;
		
		@Column(name="default_year_interval",length=100)
		private String defaultYearInterval;

		public long getUserFrontId() {
			return userFrontId;
		}

		public void setUserFrontId(long userFrontId) {
			this.userFrontId = userFrontId;
		}

		public UserFrontVo getUserFrontVo() {
			return userFrontVo;
		}

		public void setUserFrontVo(UserFrontVo userFrontVo) {
			this.userFrontVo = userFrontVo;
		}

		public List<UserRoleVo> getRoles() {
			return roles;
		}

		public void setRoles(List<UserRoleVo> roles) {
			this.roles = roles;
		}

		public String getCountriesCode() {
			return countriesCode;
		}

		public void setCountriesCode(String countriesCode) {
			this.countriesCode = countriesCode;
		}

		public String getCountriesName() {
			return countriesName;
		}

		public void setCountriesName(String countriesName) {
			this.countriesName = countriesName;
		}

		public String getStateCode() {
			return stateCode;
		}

		public void setStateCode(String stateCode) {
			this.stateCode = stateCode;
		}

		public String getStateName() {
			return stateName;
		}

		public void setStateName(String stateName) {
			this.stateName = stateName;
		}

		public String getCityCode() {
			return cityCode;
		}

		public void setCityCode(String cityCode) {
			this.cityCode = cityCode;
		}

		public String getCityName() {
			return cityName;
		}

		public void setCityName(String cityName) {
			this.cityName = cityName;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getPincode() {
			return pincode;
		}

		public void setPincode(String pincode) {
			this.pincode = pincode;
		}

		public String getContactName() {
			return contactName;
		}

		public void setContactName(String contactName) {
			this.contactName = contactName;
		}

		public String getContactNo() {
			return contactNo;
		}

		public void setContactNo(String contactNo) {
			this.contactNo = contactNo;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getBankAcholderName() {
			return bankAcholderName;
		}

		public void setBankAcholderName(String bankAcholderName) {
			this.bankAcholderName = bankAcholderName;
		}

		public String getBankAcno() {
			return bankAcno;
		}

		public void setBankAcno(String bankAcno) {
			this.bankAcno = bankAcno;
		}

		public String getBankBranch() {
			return bankBranch;
		}

		public void setBankBranch(String bankBranch) {
			this.bankBranch = bankBranch;
		}

		public String getBankIFSC() {
			return bankIFSC;
		}

		public void setBankIFSC(String bankIFSC) {
			this.bankIFSC = bankIFSC;
		}

		public String getBankName() {
			return bankName;
		}

		public void setBankName(String bankName) {
			this.bankName = bankName;
		}

		public String getBankSwiftCode() {
			return bankSwiftCode;
		}

		public void setBankSwiftCode(String bankSwiftCode) {
			this.bankSwiftCode = bankSwiftCode;
		}

		public String getIbanNo() {
			return ibanNo;
		}

		public void setIbanNo(String ibanNo) {
			this.ibanNo = ibanNo;
		}

		public String getGst() {
			return gst;
		}

		public void setGst(String gst) {
			this.gst = gst;
		}

		public String getPanNo() {
			return panNo;
		}

		public void setPanNo(String panNo) {
			this.panNo = panNo;
		}

		public String getTelephone() {
			return telephone;
		}

		public void setTelephone(String telephone) {
			this.telephone = telephone;
		}

		public String getWebsite() {
			return website;
		}

		public void setWebsite(String website) {
			this.website = website;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public int getIsDeleted() {
			return isDeleted;
		}

		public void setIsDeleted(int isDeleted) {
			this.isDeleted = isDeleted;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}

		public String getTockenNo() {
			return tockenNo;
		}

		public void setTockenNo(String tockenNo) {
			this.tockenNo = tockenNo;
		}

		public String getSenderId() {
			return senderId;
		}

		public void setSenderId(String senderId) {
			this.senderId = senderId;
		}

		public String getLogo() {
			return logo;
		}

		public void setLogo(String logo) {
			this.logo = logo;
		}

		public String getGstRegistrationType() {
			return gstRegistrationType;
		}

		public void setGstRegistrationType(String gstRegistrationType) {
			this.gstRegistrationType = gstRegistrationType;
		}

		public Date getFinancialYear() {
			return financialYear;
		}

		public void setFinancialYear(Date financialYear) {
			this.financialYear = financialYear;
		}

		public String getMonthInterval() {
			return monthInterval;
		}

		public void setMonthInterval(String monthInterval) {
			this.monthInterval = monthInterval;
		}

		public String getDefaultYearInterval() {
			return defaultYearInterval;
		}

		public void setDefaultYearInterval(String defaultYearInterval) {
			this.defaultYearInterval = defaultYearInterval;
		}
		
}
