/**
 * This File is For Account New Modal
 * Account Ajax Insert
 * Account Validation
 */

$(document).ready(function(){
	
	//----------Department------------
	$("#account_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveaccount",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			accountName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			"group.accountGroupId":{
				validators: {
					notEmpty: {
						message: 'Account Group is Required'
					}
				}
			},
			debit:{
				validators: {
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			},
			credit:{
				validators: {
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 var $form = $(e.target);
		 fv    = $form.data('formValidation');
		 
		 $.post("/account/save", {
			 accountName: $("#accountName").val(),
			 group: $("#accountGroup").val(),
			 debit: $("#debit").val(),
			 credit: $("#credit").val(),
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#account_new_modal').modal('toggle');
			 
			var t = $('#account_table').DataTable();
			t.row.add([
				 "0",
				 data.accountName,
				 data.group.accountGroupName,
				 '<a  data-toggle="modal" data-target="#account_update_modal" onclick="updateAccount(this,'+data.accountCustomId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteAccount(this,'+data.accountCustomId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#account_new_modal').on('shown.bs.modal', function() {
		$('#account_new_form').formValidation('resetForm', true);
		$("#accountGroup").select2({dropdownParent: $("#accountGroup").parent(),placeholder:"Select Account Group"});
	});
	
	$("#saveaccount").click(function() {
		$('#account_new_form').data('formValidation').validate();
	});
	
	//----------End Department------------
	
});