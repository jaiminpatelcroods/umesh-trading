/**
 * This File is For Account Update Modal
 * Account Ajax Insert
 * Account Validation
 */

$(document).ready(function(){
	
	//----------Account------------
	$("#account_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updateaccount",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			accountName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			"group.accountGroupId":{
				validators: {
					notEmpty: {
						message: 'Account Group is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/account/update", {
			 accountName: $("#updateAccountName").val(),
			 group: $("#updateAccountGroup").val(),
			 accountCustomId: $("#accountCustomId").val(),
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			 
			 $('#account_update_modal').modal('toggle');
			 
			 location.reload(true);
			
		 });
	});
	
	$("#updateaccount").click(function() {
		$('#account_update_form').data('formValidation').validate();
	});
	
	$('#account_update_modal').on('shown.bs.modal', function() {
		$("#updateAccountGroup").select2({dropdownParent: $("#updateAccountGroup").parent(),placeholder:"Select Account Group"});
	});
	//----------End Account------------
	
});

//----------Account Function------------
function updateAccount(row,id)
{  		
	$('#account_update_form').formValidation('resetForm', true);
	
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var group = $(crow).find('td:eq(2)').attr("data-group-id");
	var rowindex = $(crow).find('td:eq(0)').text();
	
	$('#updateAccountName').val(name);
  	$('#accountCustomId').val(id); 
  	$('#updateAccountGroup').val(group).trigger('change');
    $("#dataindex").val(rowindex);
    
}
//----------End Account Function------------

