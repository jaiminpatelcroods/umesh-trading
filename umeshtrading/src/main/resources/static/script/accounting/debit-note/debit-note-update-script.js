/**
 * This File is For Credit Note New
 * Credit Note Validation
 */

$(document).ready(function(){
	
	$("#debit_note_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatedebitnote",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			transactionDate:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			"fromAccountCustomVo.accountCustomId":{
				validators: {
					notEmpty: {
						message: 'From Account is Required'
					}
				}
			},
			"toAccountCustomVo.accountCustomId":{
				validators: {
					notEmpty: {
						message: 'To Account is Required'
					}
				}
			},
			amount:{
				validators: {
					notEmpty: {
						message: 'Amount is Required'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			},
			description:{
				validators: {
					
				}
			}
		}
	});
	
	$('#transactionDate').datepicker({
		format: 'dd/mm/yyyy',autoclose: true
	}).on('changeDate', function(e) {
		$('#debit_note_update_form').formValidation('revalidateField', 'transactionDate'); 
	});
});