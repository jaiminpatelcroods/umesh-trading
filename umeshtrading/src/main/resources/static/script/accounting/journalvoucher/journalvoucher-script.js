/**
 * This File is For Journal Voucher
 * Journal Voucher Validation
 */
	var tableIndex = 0;
	var _accountData;
	
	var amountValidator = {
			validators : {
				notEmpty : {
					message : 'required'
				},
				stringLength : {
					max : 20,
					message : 'invalid'
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,2})?))$/,
					message : 'invalid'
				}
			}
		},
		accountCustomValidator = {
			validators : {
				notEmpty : {
					message : 'required'
				}
			}
		};
	$(document).ready(function(){
		getAccount();
		
		$("#journalvoucher_new_form").formValidation({
			framework : 'bootstrap',
			live:'disabled',
			excluded : ":disabled",
			button:{
				selector : "#savejournalvoucher",
				disabled : "disabled",
			},
			icon : null,
			fields : {
				voucherDate:{
					validators: {
						notEmpty: {
							message: 'Voucher Date is Required'
						}
					}
				},
				description:{
					validators: {
						
					}
				}
			}
		});
		
		$('#voucherDate').datepicker({
			format: 'dd/mm/yyyy',autoclose: true
		}).on('changeDate', function(e) {
			$('#journalvoucher_new_form').formValidation('revalidateField', 'voucherDate'); 
		});
		
		$("#journalvoucher_table").on("click",'button[data-item-remove]',function(e) {
			
			e.preventDefault();
			var i = $(this).closest("[data-table-item]").attr("data-table-item");
			
			$('#journalvoucher_new_form').formValidation('removeField',"journalVoucherAccountVos["+i+"].fromAccountCustomVo.accountCustomId");
			$('#journalvoucher_new_form').formValidation('removeField',"journalVoucherAccountVos["+i+"].credit");
			$('#journalvoucher_new_form').formValidation('removeField',"journalVoucherAccountVos["+i+"].debit");
			$(this).closest("[data-table-item]").remove();
			
			changeTotal();
		});
		
		$("#savejournalvoucher").click(function() {
			
			toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-center",
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000","extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
			}
			
			if ($('#journalvoucher_new_form').data('formValidation').isValid() == null) {
				$('#journalvoucher_new_form').data('formValidation').validate();
			}
			
			if ($("#journalvoucher_table").find("[data-table-item]").not(".m--hide").length == 0){
				toastr.error("","add minimum two account");
				return false;
			} else if(parseFloat($('#debit_total').text()) != parseFloat($('#credit_total').text())) {
				toastr.error("","Debit and credit amount total should be same");
				return false;
			} else if($('#journalvoucher_new_form').data('formValidation').isValid() == true) {
				$("#journalvoucher_table").find("[data-table-item='template']").remove();
			}
			
		});
		
	});
	
	function getAccount() {
		
		mApp.blockPage({overlayColor:"#000000",type:"loader",state:"danger",message:"Loading Account"});
		
		$.post("/account/typenotin/json", {
			
		}, function( data,status ) {
			if(status == 'success') {
				_accountData = data;
				
				if($("#journalvoucher_table").find("[data-table-item]").not(".m--hide").length == 0) {
					addTableItem();
				} else {

					var $tableList=$("#journalvoucher_table").find("[data-table-item]").not(".m--hide");
					
					$tableList.each(function (){
						
						index = $(this).attr("data-table-item");
						
						$("#fromAccountCustomVo"+index).append($('<option></option>').val("").html("Select Account"));
						
						$.each(_accountData, function (key, value) {
							$("#fromAccountCustomVo"+index).append($('<option></option>').val(value.accountCustomId).html(value.accountName));
						});
						
						$("#fromAccountCustomVo"+index).addClass("m-select2");
						$("#fromAccountCustomVo"+index).select2({placeholder:"Select Account",allowClear:0});
						
						if($("#fromAccountCustomVo"+index).data("default") != undefined)
							$("#fromAccountCustomVo"+index).val($("#fromAccountCustomVo"+index).data("default")).trigger('change') ;
					});
					
				}
				
				mApp.unblockPage()
			}
		});
	}
	
	function addTableItem() {
		
		var	$tableItemTemplate = $("#journalvoucher_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
		});
		
		$("#journalvoucher_table").find("[data-table-list]").append($tableItemTemplate);
		$("#fromAccountCustomVo"+tableIndex).append($('<option></option>').val("").html("Select Account"));
		$.each(_accountData, function (key, value) {
			$("#fromAccountCustomVo"+tableIndex).append($('<option></option>').val(value.accountCustomId).html(value.accountName));
		});
		
		$("#fromAccountCustomVo"+tableIndex).addClass("m-select2");
		$("#fromAccountCustomVo"+tableIndex).select2({placeholder:"Select Account",allowClear:0});
		
		$('#journalvoucher_new_form').formValidation('addField',"journalVoucherAccountVos["+tableIndex+"].fromAccountCustomVo.accountCustomId", accountCustomValidator);
		$('#journalvoucher_new_form').formValidation('addField',"journalVoucherAccountVos["+tableIndex+"].credit", amountValidator);
		$('#journalvoucher_new_form').formValidation('addField',"journalVoucherAccountVos["+tableIndex+"].debit", amountValidator);
		tableIndex++;
		
		setdTableSrNo();
		
		changeTotal();
	}
	
	function setdTableSrNo() {
		var $tableItemTemplate=$("#journalvoucher_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function changeAmount(id,type)
	{	
		if(type=='d' && ($('#debit'+id).val() != '' && $('#debit'+id).val() != 0)) {
			$('#credit'+id).val(0)
		} else if(type=='c' && ($('#credit'+id).val() != '' && parseFloat($('#credit'+id).val()) != 0)) {
			$('#debit'+id).val(0)
		}
		
		changeTotal();
	}
	
	function changeTotal()
	{
		var totalDebit=0.00;
		var totalCredit=0.00;
		
		var $tableItem=$("#journalvoucher_table").find("[data-table-item]").not(".m--hide");
		
		$tableItem.each(function (){
			if($('#debit'+$(this).attr("data-table-item")).val() != '') {
				totalDebit +=parseFloat($('#debit'+$(this).attr("data-table-item")).val());
			}
			if($('#credit'+$(this).attr("data-table-item")).val() != '') {
				totalCredit +=parseFloat($('#credit'+$(this).attr("data-table-item")).val());
			}
			
		});
		
		$('#debit_total').text(totalDebit.toFixed(2));	
		$('#credit_total').text(totalCredit.toFixed(2));
		
		$('#total').val(totalDebit.toFixed(2));
	}
	
	function setTableIndex(id) {
		tableIndex = id;
	}
	
	