/**
 * This File is For Additional Charge New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#additionalcharge_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveadditionalcharge",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			additionalCharge:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			defaultAmount:{
				validators: {
					notEmpty: {
						message: 'The Amount is reqired'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					},
				}
			},
			"taxVo.taxId":{
				validators: {
					notEmpty: {
						message: 'The Tax is reqired'
					}
				}
			},
			hsnCode:{
				validators: {
					notEmpty: {
						message: 'The Hsn Code is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/additionalcharge/save", {
			 additionalCharge: $('#additionalCharge').val(),
			 defaultAmount:$('#defaultAmount').val(),    
			 hsnCode:$('#hsnCode').val(),
			 tax:$('#taxVo').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#additionalcharge_new_modal').modal('toggle');
			 
			var t = $('#additionalcharge_table').DataTable();
			t.row.add([
				"0",
				data.additionalCharge,
				data.defaultAmount,
				data.taxVo.taxName,
				data.hsnCode,
				'<a  data-toggle="modal" data-target="#additionalcharge_update_modal" onclick="updateAdditionalCharge(this,'+data.additionalChargeId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteAdditionalCharge(this,'+data.additionalChargeId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#additionalcharge_new_modal').on('shown.bs.modal', function() {
		
		$('#additionalcharge_new_form').formValidation('resetForm', true);
		$("#taxVo").select2({dropdownParent: $("#taxVo").parent()});
		$("#defaultAmount").val(0);
		$("#additionalCharge").focus();
	});
	
	$("#saveadditionalcharge").click(function() {
		$('#additionalcharge_new_form').data('formValidation').validate();
	});
	
});