/**
 * This File is For Bank New
 * Bank Validation
 */

$(document).ready(function(){
	
	$("#bank_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savebank",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			bankName:{
				validators: {
					notEmpty : {
						message : 'The bank name is required'
					},
					stringLength : {
						max : 50,
						message : 'The bank name must be less than 50 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
						message : 'The bank name can only consist of alphabetical, number and underscore'
					}
				}
			},
			bankBranch:{
				validators: {
					notEmpty : {
						message : 'The branch name is required'
					},
					stringLength : {
						max : 50,
						message : 'The branch name must be less than 50 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
						message : 'The branch name can only consist of alphabetical, number and underscore'
					}
				}
			},
			accountHolderName:{
				validators: {
					stringLength : {
						max : 50,
						message : 'The account holder name name must be less than 50 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
						message : 'The account holder name name can only consist of alphabetical, number and underscore'
					}
				}
			},
			bankAcNo:{
				validators: {
					stringLength : {
						max : 20,
						message : 'The bank aacount no must be 20 digit long'
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The bank aacount no can only consist of number'
					}
				}
			},
			ifscCode:{
				validators: {
					stringLength : {
						max : 50,
						message : 'The ifsc code must be less than 50 characters long'
					},
					regexp : {
						regexp: /[a-zA-Z0-9]$/,
						message : 'The ifsc code can only consist of alphabetical and number'
					}
				}
			},
			swiftCode:{
				validators: {
					stringLength : {
						max : 50,
						message : 'The swift code must be less than 50 characters long'
					},
					regexp : {
						regexp: /[a-zA-Z0-9]$/,
						message : 'The swift code can only consist of alphabetical and number'
					}
				}
			},
			creditBalance:{
				validators: {
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			},
			debitBalance:{
				validators: {
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			},
			addressLine1:{
				validators: {
					stringLength : {
						max : 80,
						message : 'The address line 1 must be less than 80 characters long'
					}
				}
			},
			addressLine2:{
				validators: {
					stringLength : {
						max : 80,
						message : 'The address line 2 must be less than 80 characters long'
					}
				}
			},
			countriesCode:{
				validators: {
					notEmpty : {
						message : 'select country'
					}
				}
			},
			stateCode:{
				validators: {
					notEmpty : {
						message : 'select state'
					}
				}
			},
			cityCode:{
				validators: {
					notEmpty : {
						message : 'select city'
					}
				}
			},
			pinCode:{
				validators: {
					regexp : {
						regexp : /^\d{6}$/,
						message : 'The ZIP/Postal code can only consist of number'
					}
				}
			},
			
		}
	});
	
	$(document).ready(function() {
		getAllCountryAjax("countriesCode")
	});
});