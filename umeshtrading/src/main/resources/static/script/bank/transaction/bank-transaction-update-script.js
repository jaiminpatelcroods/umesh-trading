/**
 * This File is For Bank Transaction Update
 * Bank Transaction Validation
 */

$(document).ready(function(){
	
	//transaction form validation	
	$("#bank_transaction_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savebanktransaction",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			transactionDate:{
				validators: {
					notEmpty: {
						message: 'The date is Required'
					}
				}
			},
			"fromBankVo.bankId":{
				validators: {
					notEmpty: {
						message: 'From Bank is Required'
					}
				}
			},
			"toBankVo.bankId":{
				validators: {
					notEmpty: {
						message: 'TO Bank is Required'
					}
				}
			},
			debitAmount:{
				validators: {
					notEmpty: {
						message: 'Amount is Required'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			},
			creditAmount:{
				validators: {
					notEmpty: {
						message: 'Amount is Required'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			},
			description:{
				validators: {
					
				}
			}
		}
	});
	
	if($("#debitAmount").val() == undefined) {
		$("#bank_transaction_form").formValidation('removeField',"debitAmount");
	}
	if($("#creditAmount").val() == undefined) {
		$("#bank_transaction_form").formValidation('removeField',"creditAmount");
	}
	
	if($("#toBank").val() == undefined) {
		$("#bank_transaction_form").formValidation('removeField',"toBankVo.bankId");
	}
	if($("#fromBank").val() == undefined) {
		$("#bank_transaction_form").formValidation('removeField',"fromBankVo.bankId");
	}
	
	$('#transactionDate').datepicker({
		format: 'dd/mm/yyyy',autoclose: true
	}).on('changeDate', function(e) {
		$('#bank_transaction_form').formValidation('revalidateField', 'transactionDate'); 
	});
});