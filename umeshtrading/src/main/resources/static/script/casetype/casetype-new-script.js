/**
 * This File is For Case Type New Modal
 * Case Type Ajax Insert
 * Case Type Validation
 */

$(document).ready(function(){
	
	//----------Category------------
	$("#casetype_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecasetype",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			caseName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			caseDescription:{
				validators: {
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/casetype/save", {
			 caseName: $('#caseName').val(),
			 caseDescription:$('#caseDescription').val()
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#casetype_new_modal').modal('toggle');
			 
			var t = $('#casetype_table').DataTable();
			t.row.add([
				 "0",
				 data.caseName,
				 data.caseDescription,
				 '<a  data-toggle="modal" data-target="#casetype_update_modal" onclick="updateCaseType(this,'+data.caseTypeId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteCaseType(this,'+data.caseTypeId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#casetype_new_modal').on('show.bs.modal', function() {
		$('#casetype_new_form').formValidation('resetForm', true);
	});
	
	$("#savecasetype").click(function() {
		$('#casetype_new_form').data('formValidation').validate();
	});
	
	//----------End Category------------
});