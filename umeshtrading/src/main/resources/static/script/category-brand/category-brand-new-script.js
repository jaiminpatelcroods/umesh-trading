/**
 * This File is For Category and Brand New Modal
 * Category and Brand Ajax Insert
 * Category and Brand Validation
 */

$(document).ready(function(){
	
	//----------Category------------
	$("#category_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecategory",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			category:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			description:{
				validators: {
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("categorybrand/category/save", {
			 name: $('#category').val(),
			 description:$('#description').val()
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#category_new_modal').modal('toggle');
			 
			var t = $('#category_table').DataTable();
			t.row.add([
				 "0",
				 data.categoryName,
				 data.categoryDescription,
				 '<a  data-toggle="modal" data-target="#category_update_modal" onclick="updateCategory(this,'+data.categoryId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteCategory(this,'+data.categoryId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#category_new_modal').on('show.bs.modal', function() {
		$('#category_new_form').formValidation('resetForm', true);
	});
	
	$("#savecategory").click(function() {
		$('#category_new_form').data('formValidation').validate();
	});
	
	//----------End Category------------
	
	//----------Brand------------
	$("#brand_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savebrand",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			brandname:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			branddesc:{
				validators: {
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("categorybrand/brand/save", {
			 name: $('#brandname').val(),
			 description:$('#branddescription').val()
		 }, function( data,status ) {
			
			toastr["success"]("Record Inserted....");
			
			$('#brand_new_modal').modal('toggle');
			 
			var t = $('#brand_table').DataTable();
			t.row.add([
				 "0",
				 data.brandName,
				 data.brandDescription,
				 '<a  data-toggle="modal" data-target="#brand_update_modal" onclick="updateBrand(this,'+data.brandId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteBrand(this,'+data.brandId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#brand_new_modal').on('show.bs.modal', function() {
		$('#brand_new_form').formValidation('resetForm', true);
	});
	
	$("#savebrand").click(function() {
		$('#brand_new_form').data('formValidation').validate();
	});
	
	//----------End Brand------------
});