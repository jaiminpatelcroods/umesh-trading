/**
 * This File is For Color New Modal
 * Color Ajax Insert
 * Color Validation
 */

$(document).ready(function(){
	$("#color_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			colorName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			colorCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/color/save", {
			 colorName: $('#colorName').val(),
			 colorCode:$('#colorCode').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#color_new_modal').modal('toggle');
			 
			var t = $('#color_table').DataTable();
			t.row.add([
				"0",
				data.colorName,
				data.colorCode,
				'<a  data-toggle="modal" data-target="#color_update_modal" onclick="updateColor(this,'+data.colorId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteColor(this,'+data.colorId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#color_new_modal').on('show.bs.modal', function() {
		$('#color_new_form').formValidation('resetForm', true);
	});
	
	$("#savecolor").click(function() {
		$('#color_new_form').data('formValidation').validate();
	});
	
});