/**
 * This File is For Complain
 * Complain Validation
 */

	$(document).ready(function(){
		if($("#complainId").val() == undefined) {
			try {
	  			$('#complainDate').datepicker('setDate','today');
	  			
	  		} catch(e) {
	  			$('#complainDate').datepicker('setDate','<%=session.getAttribute("firstDateFinancialYear")%>');
	  		}
		}
  		//-------------------- Purchase Validation -------------------------------
		$('#complain_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savecomplain",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				"contactVo.contactId" : {
					validators : {
						notEmpty : {
							message : 'please select customer'
						}
					}
				},
				complainDate : {
					validators : {
						notEmpty : {
							message : 'The complain date is required'
						}
					}
				},
				"caseTypeVo.caseTypeId" : {
					validators : {
						notEmpty : {
							message : 'please select case type'
						}
					}
				},
				"caseChannel" : {
					validators : {
						notEmpty : {
							message : 'please select case channel'
						}
					}
				},
			}
		});
		//-------------------- End Purchase Validation ---------------------------
		
		$("#savecomplain").click(function (){
			if ($('#complain_form').data('formValidation').isValid() == null) {
				$('#complain_form').data('formValidation').validate();
			}
			
			if($('#complain_form').data('formValidation').isValid() == true) {
				$("#dueDate").remove();
			}
		});
		
		getContactInfo()
	});
	
	function getContactInfo() {
		var id=$("#contactVo").val();
		
		if($("#contactVo").val() != '') {
			$.post("/contact/customers/"+id+"/json", {
				
			}, function (data, status) {
				
				
				if(status == 'success') {
					
					$("#lblContactGSTIN").text(data.gstin=='' ? "-" : data.gstin);
					
					address=data.contactAddressVos;
					
					$.each(data.contactAddressVos, function( key, value ) {
						
						if(key == 0) {
							
							$("#purchase_billing_address").find("[data-address-name]").html(value.companyName).end()
								.find("[data-address-line-1]").html(value.addressLine1).end()
								.find("[data-address-line-2]").html(value.addressLine2).end()
								.find("[data-address-pincode]").html(value.pinCode).end()
								.find("[data-address-city]").html(value.cityName).end()
								.find("[data-address-state]").html(value.stateName).end()
								.find("[data-address-country]").html(value.countriesName).end()
								.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
								.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
								.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
								.find("[data-address-phoneNo]").html(data.companyMobileno == "" ? "Mobile no. is not provided": data.companyMobileno).end()
								.removeClass("m--hide").end();
								$("#purchase_billing_address").parent().find("[data-address-message]").addClass("m--hide").end();
							
							$("#lblPlaceofSupply").text(value.stateName);
						}
						
					});
					
				} else {
					console.log("status: "+status);
				}
			});
		}
	}
	
	function getCaseTypeInfo() {
		
		var id=$("#caseTypeVo").val();
		
		$.post("/casetype/"+id+"/json", {
			
		}, function (data, status) {
			
			
			if(status == 'success') {
				if(data.caseDescription != '') {
					$("#case_type_div").find("span").html(data.caseDescription);
					$("#case_type_div").removeClass("m--hide");
				} else {
					$("#case_type_div").addClass("m--hide");
				}
				
			} else {
				console.log("status: "+status);
			}
		});
	}
		