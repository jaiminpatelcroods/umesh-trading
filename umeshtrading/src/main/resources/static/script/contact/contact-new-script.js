/**
 * This File is For Contact New
 * Contact Validation
 */

var companyNameValidator = {
		validators : {
			notEmpty : {
				message: 'The Company Name is Required'
			},
			stringLength : {
				max : 100,
				message : 'The Company Name Not More than 100 characters long'
			},
			regexp : {
				regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
				message : 'The Company Name can only consist of alphabetical, numberical value'
			}
		}
	},
	addressValidator = {
		validators : {
			stringLength : {
				max : 100,
				message : 'The Address Line Not More than 100 characters long'
			},
			regexp : {
				regexp : /^[a-zA-Z0-9_-\s-.,()&/ ]+$/,
				message : 'The Address Line can only consist of alphabetical, numberical value'
			}
		}
	},
	countryValidator = {
		validators : {
			notEmpty : {
				message: 'The Country is Required'
			},
		}
	},
	stateValidator = {
		validators : {
			notEmpty : {
				message: 'The State is Required'
			},
		}
	},
	cityValidator = {
		validators : {
			notEmpty : {
				message: 'The City is Required'
			},
		}
	},
	pincodeValidator = {
		validators : {
			stringLength : {
				min : 6,
				max : 6,
				message : 'The Pincode must be 6 digit long'
			},
			regexp : {
				regexp : /^\d{6}$/,
				message : 'The Pincode can only consist of numerical value'
			}
		}
	},
	nameValidator = {
		validators: {
			stringLength : {
				max : 100,
				message : 'The Name Not More  than 100 characters long'
			},
			regexp : {
				regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
				message : 'The Name can only consist of alphabetical, numberical value'
			},
		}
	},
	mobilenoValidator = {
		validators : {
			stringLength : {
				max : 15,
				message : 'The Mobile no. must be 15 digit long'
			},
			regexp : {
				regexp : /^[0-9+]+$/,
				message : 'The Mobile no. can only consist numerical value'
			}
		}
	},
	emailValidator = {
		validators : {
			regexp : {
				regexp : /^[a-z|A-Z|]+([.|-]?[a-z|A-Z|0-9|_]+)*@[a-z|A-Z]+[a-z|A-Z|0-9]*[\\.]+[a-z|A-Z]+([.]?[a-z|A-Z]+)$/,
				message : 'The Email address is not valid'
			},
		}
	};
			
$(document).ready(function(){
	$("#contact_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecontact",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			companyName:{
				validators: {
					notEmpty: {
						message: 'The Company Name is Required'
					},
					stringLength : {
						max : 100,
						message : 'The Company Name Not More than 100 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
						message : 'The Company Name can only consist of alphabetical, numberical value'
					},
				}
			}, 
			companyMobileno : {
				validators : {
					stringLength : {
						max : 15,
						message : 'The Mobile no. must be 15 digit long'
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The Mobile no. can only consist numerical value'
					}
				}
			},
			companyTelephone : {
				validators : {
					stringLength : {
						
						max : 17,
						message : 'The Telehphone No must be 17 digit long'
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The Telephone No consist only numeric digit'
					}
				}
			},
			companyEmail : {
				verbose : false,
				validators : {
					regexp : {
						regexp : /^[a-z|A-Z|]+([.|-]?[a-z|A-Z|0-9|_]+)*@[a-z|A-Z]+[a-z|A-Z|0-9]*[\\.]+[a-z|A-Z]+([.]?[a-z|A-Z]+)$/,
						message : 'The Email address is not valid'
					},
				}
			},
			ownerName:{
				validators: {
					stringLength : {
						max : 100,
						message : 'The Owner Not More  than 100 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
						message : 'The Owner Name can only consist of alphabetical, numberical value'
					},
				}
			},
			ownerMobileno : {
				validators : {
					stringLength : {
						max : 15,
						message : 'The Mobile no. must be 15 digit long'
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The Mobile no. can only consist numerical value'
					}
				}
			},
			concernPersonName:{
				validators: {
					stringLength : {
						max : 100,
						message : 'The Concern Person Not More  than 100 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
						message : 'The Concern Person Name can only consist of alphabetical, numberical value'
					},
				}
			},
			concernPersonMobileno : {
				validators : {
					stringLength : {
						max : 15,
						message : 'The Mobile no. must be 15 digit long'
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The Mobile no. can only consist numerical value'
					}
				}
			},
			gstType : {
				validators : {
					notEmpty : {
						message : 'The GST Type is required'
					},

				}
			},
			gstin : {
				validators : {
					notEmpty : {
						message : 'GSTIN/UIN is required'
					},
					stringLength : {
						max : 15,
						message : 'GSTIN/UIN is not more than 15 digit long'
					},
					regexp : {

						regexp : /^\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d{1}[A-Z]{1}[A-Z0-9]{1}$/,
						message : 'The GSTIN/UIN is not valid'
					}
				}
			},
			panNo : {
				validators : {
					stringLength : {
						max : 10,
						message : 'The Pan No not more than 10 digit long'
					},
					regexp : {

						regexp : /^[A-Z]{5}\d{4}[A-Z]{1}$/,
						message : 'The Pan No can only consist 5 capital alphabatic  value , 4 digit and 1 capital alphabate'
					}
				}
			},
			noOfShops : {
				validators : {
					stringLength : {
						max : 4,
						message : 'The No. of Shops must be 4 digit long'
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The No. of Shops can only consist numerical value'
					}
				}
			},
			paymentType : {
				validators : {
					notEmpty : {
						message : 'The Payment Type is required'
					},

				}
			},
			creditDays : {
				validators : {
					stringLength : {
						max : 10,
						message : 'The Credit Days must be 10 digit long'
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The Credit Days can only consist numerical value'
					}
				}
			},
			maximumCreditLimit : {
				validators : {
					stringLength : {
						max : 10,
						message : 'The Max. Credit Limit must be 10 digit long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
			        	message: 'The Max. Credit Limit can contain only numeric value and not more than three decimal value'
					},
				}
			},
			agentCommission : {
				validators : {
					stringLength : {
						max : 5,
						message : 'The Agent Commission must be 6 digit long'
					},
					regexp : {
						regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
						message: 'The Agent Commission can contain only numeric value and not more than two decimal value'
					},
				}
			},
			bankName : {
				validators : {
					stringLength : {
						max : 50,
						message : 'The Bank Name not more than 50 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-, ]+$/,
						message : 'The Bank Name can only consist of alphabetic value'
					}
				}
			},
			bankBranch : {
				validators : {
					stringLength : {
						max : 50,
						message : 'The Bank Branch not more than 50 characters long'
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-, ]+$/,
						message : 'The Bank Branch can only consist of alphabetic value'
					}
				}
			},
			bankAccountNo : {
				validators : {
					stringLength : {
						max : 20,
						message : 'The Account No. not more than 20 digit long'
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The Account No can only consist of numerical value'
					}
				}
			},
			bankIfsc : {
				validators : {
					stringLength : {
						max : 20,
						message : 'The IFSC code must be 20 character long'
					},
					regexp : {
						regexp : /[a-zA-Z0-9]$/,
						message : 'The IFSC code can only consist of alphabetical and Numerical'
					}
				}
			},
		}
	});
	
	if($('#serviceAvailable').val() != undefined) {
		$('#serviceAvailable').tagsinput({
			tagClass:'labeldanger'
		});
	}
	
	// Payment Type On Change
	$("#paymentType").change(function() {

		if($(this).val() == 'Credit') {
			$("#payment_type_credit").collapse('show');
			$('#contact_form').formValidation('enableFieldValidators', 'creditDays', true);
			$('#contact_form').formValidation('enableFieldValidators', 'maximumCreditLimit', true);
		}
		else {
			$("#payment_type_credit").collapse('hide');
			$('#contact_form').formValidation('enableFieldValidators', 'creditDays', false);
			$('#contact_form').formValidation('enableFieldValidators', 'maximumCreditLimit', false);
		}
		
	});
	
	// Agent 
	$("#agentId").change(function() {

		if($(this).val() == '') {
			$("#agent_commission").collapse('hide');
			$('#contact_form').formValidation('enableFieldValidators', 'agentCommission', false);
			
		}
		else {
			$("#agent_commission").collapse('show');
			$('#contact_form').formValidation('enableFieldValidators', 'agentCommission', true);
		}
		
	});
	
	$("#gstType").change(function() {
		
		if($("#gstType").val()=="UnRegistered") {
			$('#contact_form').formValidation('resetField','gstin');
			$("#gstin").val();
			$("#gstin").prop('disabled', true);	
			$('#contact_form').formValidation('enableFieldValidators', 'gstin', false);
  		} else {
  			$('#contact_form').formValidation('resetField','gstin');
  			$("#gstin").prop('disabled', false);
  			$('#contact_form').formValidation('enableFieldValidators', 'gstin', true);
  		 }
	});
	$('input[name="interestedProductRequirements"]').click(function(){
		if($(this).val() == 'Quantity Based'){
            $("#interestedProductValue").attr('placeholder',"Quantity");
        }
        else {
        	$("#interestedProductValue").attr('placeholder',"Amount");
        }
    });
	
	/**
	 * Contact Other Repeater 
	 */
	
	$("#contact_other_repeater").repeater({
		initEmpty: $("#contact_other_repeater").find("input[id*='contactOtherId']").length==0 ? true : false,	
        isFirstItemUndeletable: false,
		/* defaultValues: {
            "text-input": "foo"
        }, */
        show: function() {
            
        	index=$(this).index();
        	
        	$('#contact_form').formValidation('addField',"contactOtherVos["+index+"].name", nameValidator);
        	$('#contact_form').formValidation('addField',"contactOtherVos["+index+"].mobileno", mobilenoValidator);
        	$('#contact_form').formValidation('addField',"contactOtherVos["+index+"].email", emailValidator);
        	
        	$(this).slideDown();

        },
        hide: function(e) {
        	index =$(this).index();
        	
        	$('#contact_form').formValidation('removeField',"contactOtherVos["+index+"].name");
        	$('#contact_form').formValidation('removeField',"contactOtherVos["+index+"].mobileno");
        	$('#contact_form').formValidation('removeField',"contactOtherVos["+index+"].email");
        	
        	if($("#deleted-contact-other").val() != undefined) {
        		if($("#contactOtherId"+index).val() != '') {
        			$("#deleted-contact-other").val($("#deleted-contact-other").val() + $("#contactOtherId"+index).val() + ",");
        		}
        	}
        	$(this).slideUp();
            //$(this).remove()
        },
        
        ready: function (setIndexes) {
        	/* $dragAndDrop.on('drop', setIndexes); */
        },
        
    });
	
	$("#contact_other_repeater").find("[data-repeater-item]").each(function() {
		
		index=$(this).index();
    	
		$('#contact_form').formValidation('addField',"contactOtherVos["+index+"].name", nameValidator);
    	$('#contact_form').formValidation('addField',"contactOtherVos["+index+"].mobileno", mobilenoValidator);
    	$('#contact_form').formValidation('addField',"contactOtherVos["+index+"].email", emailValidator);
	});
	/**
	 * Address Repeater 
	 */
	
	$("#address_repeater").repeater({
		initEmpty: false,	
        isFirstItemUndeletable: true,
		/* defaultValues: {
            "text-input": "foo"
        }, */
        show: function() {
            
        	index=$(this).index();
        	
        	$(this).find("select").each(function() {
            	$(this).addClass("m-select2");
            	
            	placeholder="Select...";
            	if($(this).attr("placeholder")) {
            		placeholder=$(this).attr("placeholder");
            	}
            	
            	allowClear=0;
            	if($(this).data('allow-clear')) {
            		allowClear=	!0;
            	}
            	$(this).select2({placeholder:placeholder,allowClear:allowClear});
            	
            });
        	
        	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].companyName", companyNameValidator);
        	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].addressLine1", addressValidator);
        	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].addressLine2", addressValidator);
        	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].countriesCode", countryValidator);
        	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].stateCode", stateValidator);
        	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].cityCode", cityValidator);
        	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].pinCode", pincodeValidator);
        	
        	$("#companyName"+index).val($("#companyName").val());
        	//$('#defaultAddress'+index).val("0");
        	getAllCountryAjax("countriesCode"+index);
        	
        	$(this).slideDown();

        },
        hide: function(e) {
        	index =$(this).index();
        	
        	$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].companyName");
        	$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].addressLine1");
        	$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].addressLine2");
        	$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].countriesCode");
        	$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].stateCode");
        	$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].cityCode");
        	$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].pinCode");
        	
        	if($("#deleted-address").val() != undefined) {
        		if($("#contactAddressId"+index).val() != '') {
        			$("#deleted-address").val($("#deleted-address").val() + $("#contactAddressId"+index).val() + ",");
        		}
        	}
        	
            $(this).remove()
        },
        
        ready: function (setIndexes) {
        	
        },
        
    });
	
	$("#address_repeater").find("[data-repeater-item]").each(function() {
		
		index=$(this).index();
    	
    	$(this).find("select").each(function() {
        	$(this).addClass("m-select2");
        	
        	placeholder="Select...";
        	if($(this).attr("placeholder")) {
        		placeholder=$(this).attr("placeholder");
        	}
        	
        	allowClear=0;
        	if($(this).data('allow-clear')) {
        		allowClear=	!0;
        	}
        	$(this).select2({placeholder:placeholder,allowClear:allowClear});
        	
        });
    	
    	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].companyName", companyNameValidator);
    	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].addressLine1", addressValidator);
    	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].addressLine2", addressValidator);
    	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].countriesCode", countryValidator);
    	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].stateCode", stateValidator);
    	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].cityCode", cityValidator);
    	$('#contact_form').formValidation('addField',"contactAddressVos["+index+"].pinCode", pincodeValidator);
    
    	//$('#defaultAddress'+index).val("0");
    	getAllCountryAjax("countriesCode"+index);
    	
		
	});
	
	//$("#address_repeater").find("[data-repeater-create]").click();
	
	/*For Remote Multiple Select2*/
	$("#interestedProductIds").select2({
        placeholder: "Search Product",
        allowClear: !0,
        ajax: {
            url: '/product/variant/json',
            dataType: "json",
            type: "GET",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
				return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        minimumInputLength: 1,
        templateResult: function(e) {
            if (e.loading) return e.text;
            return e.text 
        },
        templateSelection: function(e) {
            return e.full_name || e.text
        }
    });
	/*For Remote Multiple Select2*/
	
	
	
	
	
});