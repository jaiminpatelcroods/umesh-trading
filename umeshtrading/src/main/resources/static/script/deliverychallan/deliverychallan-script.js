/**
 * This File is For Delivery Challan
 * Delivery Challan Validation
 */

	var tableIndex = 0;
	
	$(document).ready(function(){
		
  		//-------------------- Delivery Challan Validation -------------------------------
		$('#deliverychallan_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savedeliverychallan",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				"contactVo.contactId" : {
					validators : {
						notEmpty : {
							message : 'please select customer'
						}
					}
				},
				deliveryChallanDate : {
					validators : {
						notEmpty : {
							message : 'The Delivery Challan date is required'
						}
					}
				},
				"receivedBy.employeeId" : {
					validators : {
						notEmpty : {
							message : 'please select Received By'
						}
					}
				},
			}
		});
		//-------------------- End Delivery Challan Validation ---------------------------
		
		$("#product_table").on("click",'button[data-item-remove]',function(e) {
			
			e.preventDefault();
			var i = $(this).closest("[data-table-item]").attr("data-table-item");
			
			$("#deleteDeliveryChallanIds").val($("#deleteDeliveryChallanIds").val()+$("#deliveryChallanItemId"+i).val()+ ",");
			
			
			/* $('#journalvoucher_new_form').formValidation('removeField',"journalVoucherAccountVos["+i+"].fromAccountCustomVo.accountCustomId");
			$('#journalvoucher_new_form').formValidation('removeField',"journalVoucherAccountVos["+i+"].credit");
			$('#journalvoucher_new_form').formValidation('removeField',"journalVoucherAccountVos["+i+"].debit"); */
			$(this).closest("[data-table-item]").remove();
			
			setdTableSrNo();
		});
		
		$("#savedeliverychallan").click(function (){
			if ($('#deliverychallan_form').data('formValidation').isValid() == null) {
				$('#deliverychallan_form').data('formValidation').validate();
			}
			
			if($('#deliverychallan_form').data('formValidation').isValid() == true) {
				$("#product_table").find("[data-table-item='template']").remove();
			}
		});
		
		getContactInfo();
	});
	
	function addTableItem() {
		
		var	$tableItemTemplate = $("#product_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
		});
		
		$("#product_table").find("[data-table-list]").append($tableItemTemplate);
		
		$("#productVariantId"+tableIndex).addClass("m-select2");
		$("#productVariantId"+tableIndex).select2({placeholder:"Select Product",allowClear:0});
		
		/* $('#journalvoucher_new_form').formValidation('addField',"journalVoucherAccountVos["+tableIndex+"].fromAccountCustomVo.accountCustomId", accountCustomValidator);
		$('#journalvoucher_new_form').formValidation('addField',"journalVoucherAccountVos["+tableIndex+"].credit", amountValidator);
		$('#journalvoucher_new_form').formValidation('addField',"journalVoucherAccountVos["+tableIndex+"].debit", amountValidator); */
		
		tableIndex++;
		
		setdTableSrNo();
	}
	
	function setdTableSrNo() {
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function getProductVariantInfo(id) {
		
		if($("#productVariantId"+id).val() != "") {
			$.post("/product/variant/"+$("#productVariantId"+id).val()+"/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					
					$tableItem=$("#product_table").find('[data-table-item="'+id+'"]');
					
					if(data.productVo.haveDesignno == 1) {
						$("#designNo"+id).attr("disabled",false);
					} else {
						$("#designNo"+id).val("");
						$("#designNo"+id).attr("disabled",true);
					}
					
					if(data.productVo.haveBaleno == 1) {
						$("#baleNo"+id).attr("disabled",false);
					} else {
						$("#baleNo"+id).val("");
						$("#baleNo"+id).attr("disabled",true);
					}
					$("#qty"+id).val("0");
					$tableItem.find("[data-item-uom]").html(data.productVo.unitOfMeasurementVo.measurementCode);
				}
			});
		}
	}
	
	function getContactInfo() {
		var id=$("#contactVo").val();
		
		$.post("/contact/suppliers/"+id+"/json", {
			
		}, function (data, status) {
			
			
			if(status == 'success') {
				
				$("#lblContactGSTIN").text(data.gstin=='' ? "-" : data.gstin);
				
				address=data.contactAddressVos;
				
				
				$.each(data.contactAddressVos, function( key, value ) {
					
					if(key == 0) {
						
						$("#purchase_billing_address").find("[data-address-name]").html(value.companyName).end()
							.find("[data-address-line-1]").html(value.addressLine1).end()
							.find("[data-address-line-2]").html(value.addressLine2).end()
							.find("[data-address-pincode]").html(value.pinCode).end()
							.find("[data-address-city]").html(value.cityName).end()
							.find("[data-address-state]").html(value.stateName).end()
							.find("[data-address-country]").html(value.countriesName).end()
							.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
							.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
							.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
							.find("[data-address-phoneNo]").html(data.companyMobileno == "" ? "Mobile no. is not provided": data.companyMobileno).end()
							.removeClass("m--hide").end()
							.find("[data-address-message]").addClass("m--hide").end();
						
						$("#lblPlaceofSupply").text(value.stateName);
						$("#purchase_billing_address").parent().find("[data-address-message]").addClass("m--hide").end();
					}
				});
				
			} else {
				console.log("status: "+status);
			}
		});
	}
	function setTableIndex(id){
		
		tableIndex=id;
	}
		