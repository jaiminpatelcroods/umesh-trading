/**
 * This File is For Department New Modal
 * Department Ajax Insert
 * Department Validation
 */

$(document).ready(function(){
	
	//----------Department------------
	$("#department_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savedepartment",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			departmentName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/department/save", {
			 name: $('#departmentName').val(),
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#department_new_modal').modal('toggle');
			 
			var t = $('#department_table').DataTable();
			t.row.add([
				 "0",
				 data.departmentName,
				 '<a  data-toggle="modal" data-target="#department_update_modal" onclick="updateDepartment(this,'+data.departmentId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteDepartment(this,'+data.departmentId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#department_new_modal').on('show.bs.modal', function() {
		$('#department_new_form').formValidation('resetForm', true);
	});
	
	$("#savedepartment").click(function() {
		$('#department_new_form').data('formValidation').validate();
	});
	
	//----------End Department------------
	
});