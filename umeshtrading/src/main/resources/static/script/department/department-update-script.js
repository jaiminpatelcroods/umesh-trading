/**
 * This File is For Department Update Modal
 * Department Ajax Insert
 * Department Validation
 */

$(document).ready(function(){
	
	//----------Department------------
	$("#department_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatedepartment",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			departmentName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $.post("/department/update", {
			 name: $('#updateDepartmentName').val(),
			 id:$('#updateDepartmentId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			 
			 $('#department_update_modal').modal('toggle');
			 
			 location.reload(true);
			
		 });
	});
	
	$("#updatedepartment").click(function() {
		$('#department_update_form').data('formValidation').validate();
	});
	//----------End Department------------
	
});

//----------Department Function------------
function updateDepartment(row,id)
{  		
	$('#department_update_form').formValidation('resetForm', true);
	
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();

	$('#updateDepartmentName').val(name);
	$('#updateDepartmentId').val(id);

}
//----------End Department Function------------

