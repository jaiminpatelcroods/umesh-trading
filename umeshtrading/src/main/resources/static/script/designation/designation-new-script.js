/**
 * This File is For Designation New Modal
 * Designation Ajax Insert
 * Designation Validation
 */

$(document).ready(function(){
	
	//----------Designation------------
	$("#designation_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savedesignation",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			designationName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/designation/save", {
			 name: $('#designationName').val(),
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#designation_new_modal').modal('toggle');
			 
			var t = $('#designation_table').DataTable();
			t.row.add([
				 "0",
				 data.designationName,				 
				 '<a  data-toggle="modal" data-target="#designation_update_modal" onclick="updateDesignation(this,'+data.designationId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteDesignation(this,'+data.designationId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#designation_new_modal').on('show.bs.modal', function() {
		$('#designation_new_form').formValidation('resetForm', true);
	});
	
	$("#savedesignation").click(function() {
		$('#designation_new_form').data('formValidation').validate();
	});
	
	//----------End Designation------------
	
});