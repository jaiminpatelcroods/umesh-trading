/**
 * This File is For Fabric New Modal
 * Fabric Ajax Insert
 * Fabric Validation
 */

$(document).ready(function(){
	$("#fabric_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			fabricName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/fabric/save", {
			 fabricName: $('#fabricName').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#fabric_new_modal').modal('toggle');
			 
			var t = $('#fabric_table').DataTable();
			t.row.add([
				"0",
				data.fabricName,
				'<a  data-toggle="modal" data-target="#fabric_update_modal" onclick="updateFabric(this,'+data.fabricId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteFabric(this,'+data.fabricId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#fabric_new_modal').on('show.bs.modal', function() {
		$('#fabric_new_form').formValidation('resetForm', true);
	});
	
	$("#savefabric").click(function() {
		$('#fabric_new_form').data('formValidation').validate();
	});
	
});