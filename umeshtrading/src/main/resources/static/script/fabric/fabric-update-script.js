/**
 * This File is For Fabric New Modal
 * Fabric Ajax Insert
 * Fabric Validation
 */

$(document).ready(function(){
	$("#fabric_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatefabric",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			fabricName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $.post("/fabric/update", {
			 fabricName: $('#updateFabricName').val(),
			 fabricId:$('#fabricId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#fabric_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#fabric_update_modal').on('hide.bs.modal', function() {
		$('#fabric_update_form').formValidation('resetForm', true);
	});
	
	$("#updatefabric").click(function() {
		$('#fabric_update_form').data('formValidation').validate();
	});
	
});

function updateFabric(row,id)
{  		//console.log(row);
	
	$('#fabric_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var rowindex = $(crow).find('td:eq(0)').text();

	$('#updateFabricName').val(name);
	$('#fabricId').val(id);
	
}