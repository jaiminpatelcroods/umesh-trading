/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	
	$("#placeVo").select2();
	
	$("#floor_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savefloor",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			floorName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			floorCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
			description:{
				validators: {
					notEmpty: {
						message: 'Description is reqired'
					},
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/floor/save", {
			 placeVo: $('#placeVo').val(),
			 floorName: $('#floorName').val(),
			 floorCode:$('#floorCode').val(),
			 description:$('#description').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#floor_new_modal').modal('toggle');
			 
			var t = $('#floor_table').DataTable();
			t.row.add([
				"0", 
				data.placeVo.placeCode + ' ('+data.placeVo.placeName+')' ,
				data.floorCode,
				data.floorName,
				data.description,
				'<a  data-toggle="modal" data-target="#floor_update_modal" onclick="updateFloor(this,'+data.floorId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteFloor(this,'+data.floorId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#floor_new_modal').on('show.bs.modal', function() {
		$('#floor_new_form').formValidation('resetForm', true);
	});
	
	$("#savefloor").click(function() {
		$('#floor_new_form').data('formValidation').validate();
	});
	
});