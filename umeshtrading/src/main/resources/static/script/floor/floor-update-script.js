/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	
	$("#updatePlaceVo").select2();
	
	$("#floor_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatefloor",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			floorName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			floorCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
			description:{
				validators: {
					notEmpty: {
						message: 'Description is reqired'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		
		 e.preventDefault();//stop the from action methods
		 $.post("/floor/update", {
			 floorId:$('#floorId').val(),
			 placeVo : $('#updatePlaceVo').val(),
			 floorName: $('#updateFloorName').val(),
			 floorCode:$('#updateFloorCode').val(),
			 description:$('#updateDescription').val()
		}, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#floor_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#floor_update_modal').on('hide.bs.modal', function() {
		$('#floor_update_form').formValidation('resetForm', true);
	});
	
	$("#updatefloor").click(function() {
		$('#floor_update_form').data('formValidation').validate();
	});
	
});

function updateFloor(row,id)
{  	
	$('#floor_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var rowindex = $(crow).find('td:eq(0)').text();
	var placeid=$(crow).find('td:eq(1) input:hidden').val();
	var code = $(crow).find('td:eq(2)').text();
	var name=$(crow).find('td:eq(3)').text();
	var desc = $(crow).find('td:eq(4)').text();
	$('#updatePlaceVo').val(placeid); // Change the value or make some change to the internal state
	$('#updatePlaceVo').trigger('change.select2'); // Notify only Select2 of changes
	$('#updateFloorCode').val(code);
	$('#updateFloorName').val(name);
	$('#floorId').val(id);
	$('#updateDescription').val(desc);
	$("#DataIndex").val(rowindex);
}