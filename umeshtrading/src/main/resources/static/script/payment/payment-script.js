/**
 * This File is For Payment
 * Payment Validation
 */

	var _billData,_bankData;
	var index = 0, billId=0;
	
	var amountValidator = {
			validators : {
				notEmpty : {message : 'The amount is required'},
				stringLength : {max : 20, message : 'The amount must be less than 20 characters long'},
				regexp : {regexp:/^((\d+)((\.\d{0,2})?))$/, message : 'The amount is invalid'}
			}
		},
		billValidator = {
			validators : {
				notEmpty : {message : 'The Bill is required'},
			}
		};
	
	$(document).ready(function(){
		
  		$("input[name=paymentMode]").change(function() {
			if($("#paymentModeBank").prop('checked')) {
				$("#bank_div").show();
				$('#payment_form').formValidation('enableFieldValidators', 'bankVo.bankId', true);
				loadBank();
			} else {
				$("#bank_div").hide();
				$('#payment_form').formValidation('enableFieldValidators', 'bankVo.bankId', false);
	        }
		});
  		
  		$("input[name=type]").change(function() {
			
			if($("#onAccount").prop('checked')){
				$("#bill_details").hide();
				$("#totalPayment").prop("readonly", false);
			}
			
			if($("#advancePayment").prop('checked')){
				$("#bill_details").hide();
				$("#totalPayment").prop("readonly", false);
			}
			
			if($("#againstBill").prop('checked')){
				$("#bill_details").show();
				$("#totalPayment").val(0)
				$("#totalPayment").prop("readonly", true);
				
				loadBill(true);	
			}
			$("#bill_table").find("[data-bill-item]").not(".m--hide").remove();
			
		});
  		
  		$(document).on("click",'#add_Bill',function(e) {
			loadBill(true);
			addBillRow();
		});
		
		$("#bill_table").on("click",'button[data-item-remove]',function(e) {
			var i=$(this).closest("[data-bill-item]").attr("data-bill-item");
			alert($(this).find("input[id*='paymentBillId']").val());
			if($(this).find("input[id*='paymentBillId']").val() != undefined) {
				$("#deleteBillIds").val($("#deleteBillIds").val() + $(this).find("input[id*='paymentBillId']").val() + ",");
			}
			
			$(this).closest("[data-bill-item]").remove();
			
			$('#payment_form').formValidation('removeField',"paymentBillVos["+i+"].purchaseVo.purchaseId");
			$('#payment_form').formValidation('removeField',"paymentBillVos["+i+"].kasar");
			$('#payment_form').formValidation('removeField',"paymentBillVos["+i+"].payment");
			alert($("#deleteBillIds").val());
			setSrNo();
			setSubTotal();
			
			return false;
		});
		
		//-------------------- Payment Validation -------------------------------
		$('#payment_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			live:'disabled',
			button: {
				selector: "#savepayment",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				"contactVo.contactId" : {
					validators : {
						notEmpty : {
							message : 'Select Supplier'
						},
					}
				},
				totalPayment : {
					validators : {
						notEmpty : {
							message : 'The Total Payment is required'
						}
					}
				},
				paymentDate : {
					validators : {
						notEmpty : {
							message : 'The Payment Date is required'
						}
					}
				},
				type : {
					validators : {
						notEmpty : {
							message : 'The Payment Type is required'
						}
					}
				},
				paymentMode : {
					validators : {
						notEmpty : {
							message : 'The Payment Mode is required'
						}
					}
				},
				"bankVo.bankId" : {
					validators : {
						notEmpty : {
							message : 'Select Bank'
						},
					}
				},
				
			}
		});
		
		$("#savepayment").click(function (){
			
			if ($('#payment_form').data('formValidation').isValid() == null) {
				$('#payment_form').data('formValidation').validate();
			}
			
			if($('#payment_form').data('formValidation').isValid() == true) {
				
				if($("#againstBill").prop('checked')){
					
					if($("#bill_table").find("[data-bill-item]").not(".m--hide").length===0){
						toastr.error("Minimum One Bill Is require");	
						return false;
					}
				}
				
				if($("#payment_sub_total").html()<=$("#totalPayment").val()){
					
					if($("#paymentModeCash").prop('checked')) {
						$('#bankVo').val(0);
					}
					
					$("#bill_table").find("[data-bill-item='template']").remove();
				}
				else {
					toastr.error("","total bill amount should be less than total payment");
					return false;
				}
				
			}
		});
		
		if($("#paymentModeBank").prop('checked')) {
			$("#bank_div").show();
			$('#payment_form').formValidation('enableFieldValidators', 'bankVo.bankId', true);
			loadBank();
		} else {
			$('#payment_form').formValidation('enableFieldValidators', 'bankVo.bankId', false);
			$("#bank_div").hide();
		}
		
		if($("#onAccount").prop('checked')){
			$("#bill_details").hide();
			$("#totalPayment").prop("readonly", false);
		}
		
		if($("#advancePayment").prop('checked')){
			$("#bill_details").hide();
			$("#totalPayment").prop("readonly", false);
		}
		
		if($("#againstBill").prop('checked')){
			$("#bill_details").show();
			$("#totalPayment").prop("readonly", true);
			
			loadBill(false);	
		}
	});
	
	function changeContact() {
		_billData = undefined;
		loadBill(true);
	}
	
	function loadBill(isEmpty) {
		if($("#contactVo").val()!="" && $("#againstBill").prop('checked')==true ){
			if(_billData == undefined) {
				var id = $("#contactVo").val();
				$.post("/purchase/bill/"+$("#contactVo").val()+"/pending/json", {
									
				}, function (data, status) {
					if(status == 'success') {
						_billData=data;
						if(isEmpty == true) {
							$("#bill_table").find("[data-bill-item]").not(".m--hide").remove();
						}
						
					}
				});
			}
		}			
	}
	
	function loadBank() {
		if(_bankData == undefined) {
			$.post("/bank/json", {
								
			}, function (data, status) {
				if(status == 'success') {	
					_bankData = data;
					loadBank();
				}
			});
		}
		else {
			$("#bankVo").empty();
			$("#bankVo").append("<option value=''>Select Bank</option>");
			$.each(_bankData, function (key, value) {
				$("#bankVo").append($('<option></option>').val(value.bankId).html(value.bankName+""+value.bankBranch));
			});
			
			if($('#bankVo').data("default") != undefined)
  				$('#bankVo').val($('#bankVo').data("default")).trigger('change') ;
		}
	}
	
	function addBillRow() {
		if(_billData != undefined) {
			
			$billTemplate=$("#bill_table").find("[data-bill-item='template']").clone();
			$billTemplate.removeClass("m--hide").attr("data-bill-item",billId);
			
			$billTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,billId)) : "";
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,billId)) : "";
				
				$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,billId)) : "";
			});
			
			$("#bill_table").find("[data-bill-list]").append($billTemplate);

			$.each(_billData, function (key, value) {
				$("#bill"+billId).append($('<option></option>').val(value.purchaseId).html(value.billNo));
			});
			
			$("#bill"+billId).addClass("m-select2");
			$("#bill"+billId).select2({placeholder:"Select Bill",allowClear:0});
			
			$('#payment_form').formValidation('addField',"paymentBillVos["+billId+"].purchaseVo.purchaseId", billValidator);
			$('#payment_form').formValidation('addField',"paymentBillVos["+billId+"].kasar", amountValidator);
			$('#payment_form').formValidation('addField',"paymentBillVos["+billId+"].payment", amountValidator);
			billId++;
			
			//setAdditionalChargeSrNo();
		}
		 setSrNo();
	}
	
	function getBillInfo(index){
		if($('#bill'+index).val() == "") {
			return;
		}
		var $billItem=$("#bill_table").find("[data-bill-item]").not(".m--hide"), flag = false;
	
		$billItem.each(function (){
			if($(this).attr("data-bill-item") != index && $(this).find(".m-select2").val()==$('#bill'+index).val()){
				flag = true;
				return;
			}		
					
		});
		
		if(flag) {
			toastr.error("please select different bill");
			$('#bill'+index).val($("#bill"+index+"option:first").val()).trigger('change');
			return;
		}
		var id=$('#bill'+index).val();
		$.post("/purchase/bill/"+id+"/json", {		
		}, function (data, status) {
			if(status == 'success') {
				$billItem=$("#bill_table").find("[data-bill-item='"+index+"']");
				$billItem.find("[data-item-original]").text(data.total);
				$billItem.find("[data-item-paid]").text(data.paidAmount);
				$billItem.find("[data-item-pending]").text(data.total-data.paidAmount);
				$billItem.find("[data-item-amount]").val(data.total-data.paidAmount);
				$billItem.find("[data-item-kasar]").val(0);
			}
			setSubTotal();
		});
	}
	
	function setSrNo() {
		var $billItem=$("#bill_table").find("[data-bill-item]").not(".m--hide"), subTotal=0.0;
		var i = 0;
		$billItem.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function setSubTotal() {
		var $billItem=$("#bill_table").find("[data-bill-item]").not(".m--hide"), subTotal=0.0;
		
		$billItem.each(function (){
			
			subTotal += parseFloat($(this).find("[data-item-amount]").val());
		});
		
		$("#payment_sub_total").html(subTotal.toFixed(2));
		if($("#againstBill").prop('checked')){
			$("#totalPayment").val(subTotal.toFixed(2))	
		}
		
	}
	
	function checkKasar() {
		var $billItem=$("#bill_table").find("[data-bill-item]").not(".m--hide"), subTotal=0.0;
		
		$billItem.each(function () {
			if($(this).find("[data-item-kasar]").val()*1>0){
				if($(this).find("[data-item-kasar]").val()*1>$(this).find("[data-item-pending]").text()){
					$(this).find("[data-item-kasar]").val($(this).find("[data-item-pending]").text());
					$(this).find("[data-item-amount]").val($(this).find("[data-item-kasar]").val()-$(this).find("[data-item-pending]").text())
				} else {
					$(this).find("[data-item-amount]").val($(this).find("[data-item-pending]").text()-$(this).find("[data-item-kasar]").val())
				}
			}
			else{
				$(this).find("[data-item-amount]").val($(this).find("[data-item-pending]").text())
			}					
		});
		
		
		setSubTotal();
	}
	
	function checkAmount() {
		var $billItem=$("#bill_table").find("[data-bill-item]").not(".m--hide"), subTotal=0.0;
	
		$billItem.each(function (){
			
			if(($(this).find("[data-item-amount]").val()*1)>($(this).find("[data-item-pending]").text()*1)) {
				$(this).find("[data-item-amount]").val($(this).find("[data-item-pending]").text()-$(this).find("[data-item-kasar]").val())
			}
			
			if(($(this).find("[data-item-kasar]").val()*1)>0) {
				$(this).find("[data-item-amount]").val($(this).find("[data-item-pending]").text()-$(this).find("[data-item-kasar]").val())
			}
			
			if(($(this).find("[data-item-amount]").val()*1)<0) {
				$(this).find("[data-item-amount]").val($(this).find("[data-item-pending]").text()-$(this).find("[data-item-kasar]").val())
			}					
		});
		
		setSubTotal();
		
	}
	function check(e, c) {
        var allowedKeyCodesArr = [9, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 37, 39, 109, 189, 46, 110, 190];  // allowed keys
        
        if ($.inArray(e.keyCode, allowedKeyCodesArr) === -1) {  // if event key is not in array and its not Ctrl+V (paste) return false;
            e.preventDefault();
        } else if ($.trim($(c).val()).indexOf('.') > -1 && $.inArray(e.keyCode, [110, 190]) !== -1) {  // if float decimal exists and key is not backspace return fasle;
            e.preventDefault();
        } else {
            return true;
        }
    }
	
	function setBillId(i) {
		alert(i);
		billId = i;
	}
	