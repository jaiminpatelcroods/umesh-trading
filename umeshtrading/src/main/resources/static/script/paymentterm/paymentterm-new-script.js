/**
 * This File is For Payment Term_new_form New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#paymentterm_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savepaymentterm",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			paymentTermName:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Name is reqired'
					}
				}
			},
			paymentTermDay:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Day is Required'
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The Payment Term Day can only consist of numeric value'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/paymentterm/save", {
			 name: $('#paymentTermName').val(),
			 day:$('#paymentTermDay').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#paymentterm_new_modal').modal('toggle');
			 
			var t = $('#paymentterm_table').DataTable();
			t.row.add([
				"0",
				data.paymentTermName,
				data.paymentTermDay,
				'<a  data-toggle="modal" data-target="#paymentterm_update_modal" onclick="updatePaymentTerm(this,'+data.paymentTermId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deletePaymentTerm(this,'+data.paymentTermId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#paymentterm_new_modal').on('show.bs.modal', function() {
		$('#paymentterm_new_form').formValidation('resetForm', true);
	});
	
	$("#savepaymentterm").click(function() {
		$('#paymentterm_new_form').data('formValidation').validate();
	});
	
});