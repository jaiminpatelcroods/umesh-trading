/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#place_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveplace",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			placeName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			placeCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
			description:{
				validators: {
					notEmpty: {
						message: 'Description is reqired'
					},
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/place/save", {
			 placeName: $('#placeName').val(),
			 placeCode:$('#placeCode').val(),
			 description:$('#description').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#place_new_modal').modal('toggle');
			 
			var t = $('#place_table').DataTable();
			t.row.add([
				"0",
				data.placeCode,
				data.placeName,
				data.description,
				'<a  data-toggle="modal" data-target="#place_update_modal" onclick="updatePlace(this,'+data.placeId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deletePlace(this,'+data.placeId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#place_new_modal').on('show.bs.modal', function() {
		$('#place_new_form').formValidation('resetForm', true);
	});
	
	$("#saveplace").click(function() {
		$('#place_new_form').data('formValidation').validate();
	});
	
});