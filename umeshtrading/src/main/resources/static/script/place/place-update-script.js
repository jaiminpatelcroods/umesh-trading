/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#place_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updateplace",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			placeName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			placeCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
			description:{
				validators: {
					notEmpty: {
						message: 'Description is reqired'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $.post("/place/update", {
			 placeName: $('#updatePlaceName').val(),
			 placeCode:$('#updatePlaceCode').val(),
			 description:$('#updateDescription').val(),
			 id:$('#placeId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#place_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#place_update_modal').on('hide.bs.modal', function() {
		$('#place_update_form').formValidation('resetForm', true);
	});
	
	$("#updateplace").click(function() {
		$('#place_update_form').data('formValidation').validate();
	});
	
});

function updatePlace(row,id)
{  		//console.log(row);
	
	$('#place_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var code = $(crow).find('td:eq(1)').text();
	var name=$(crow).find('td:eq(2)').text();
	var desc = $(crow).find('td:eq(3)').text();
	var rowindex = $(crow).find('td:eq(0)').text();
	$('#updatePlaceName').val(name);
	$('#updatePlaceCode').val(code);
	$('#placeId').val(id);
	$('#updateDescription').val(desc);
	$("#DataIndex").val(rowindex);
}