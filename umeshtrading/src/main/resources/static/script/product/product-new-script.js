/**
 * This File is For Product New
 * Product Validation
 */

	
	var priceValidator = {
			validators : {
				notEmpty : {
					message : 'The price is required'
				},
				stringLength : {
					max : 20,
					message : 'The price must be less than 20 characters long'
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,2})?))$/,
					message : 'The price is invalid'
				}
			}
		};
	
	$(document).ready(function(){
		
		
		
		$("#name").change(function() {
			$('#displayName').val($("#name").val());
			$('#product_form').formValidation('revalidateField', 'displayName');
		});
		
		/*
		* whene click on Tax Included checkbox value is change 0 or 1
		*/
		$('input[name="salesTaxIncluded"],input[name="purchaseTaxIncluded"]').click(function(){
	        if($(this).prop("checked") == true){
	            $(this).val(1);
	        }
	        else if($(this).prop("checked") == false){
	        	  $(this).val(0);
	        }
	    });
		
		/**
		 * 
		 * 
		 */
		
		if($("#discountType").val() == 'amount') {
			$('#btn_percentage').slideUp()
		} else {
			$('#btn_amount').slideUp()
		}
		
		
		$('#btn_percentage').click(function (){
			$('#btn_percentage').slideUp();
			$('#btn_amount').slideDown();
			$("#modalDiscount").attr("placeholder", 'Amount');
  		});
		
		$('#btn_amount').click(function (){
			$('#btn_percentage').slideDown();
			$('#btn_amount').slideUp()
			$("#modalDiscount").attr("placeholder", 'Percentage');
  		});
		
		$('#product_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#save_product",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				name : {
					validators : {
						notEmpty : {
							message : 'The product name is required'
						},
						stringLength : {
							max : 50,
							message : 'The product name must be less than 50 characters long'
						},
						regexp : {
							regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
							message : 'The product name can only consist of alphabetical, number and underscore'
						}
					}
				},
				displayName : {
					validators : {
						notEmpty : {
							message : 'The print name is required'
						},
						stringLength : {
							max : 50,
							message : 'The print name must be less than 50 characters long'
						},
						regexp : {
							regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
							message : 'The print name can only consist of alphabetical, number and underscore'
						}
					}
				},
				'unitOfMeasurementVo.measurementId' : {
					validators : {
						notEmpty : {
							message : 'select Unit of Measurement'
						}
					}
				},
				'salesTaxVo.taxId' : {
					validators : {
						notEmpty : {
							message : 'select sales tax'
						}
					}
				},
				'purchaseTaxVo.taxId' : {
					validators : {
						notEmpty : {
							message : 'select purchase tax'
						}
					}
				},
				hsnCode : {
					validators : {
						stringLength : {
							max : 8,
							message : 'The hsn code must be 8 characters long'
						},
						regexp : {
							regexp : /^[0-9]+$/,
							message : 'The hsn can only consist of number'
						}
					}
				},
				'categoryVo.categoryId' : {
					validators : {
						notEmpty : {
							message : 'select Category'
						}
					}
				},
				'brandVo.brandId' : {
					validators : {
						notEmpty : {
							message : 'select Brand'
						}
					}
				}
			}
		});
	});

	