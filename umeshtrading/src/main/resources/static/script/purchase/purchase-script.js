/**
 * This File is For Purchase
 * Purchase Validation
 */

	var index = 0, purchaseNoVerifyURL = '', additionalChargeId=0, purchaseType;
	var _additionalChargeData, _termsAndConditionData;
	
	var priceValidator = {
			validators : {
				notEmpty : {message : 'The price is required'},
				stringLength : {max : 20,message : 'The price must be less than 20 characters long'},
				regexp : {regexp:/^((\d+)((\.\d{0,2})?))$/,message : 'The price is invalid'}
			}
		},
		additionalChargeValidator = {
			validators: {
				notEmpty: {message: 'Select Additional Charge'},
			}
		},
		additionalChargeAmountValidator = {
			validators : {
				notEmpty : {message : 'The Amount is required'},
				stringLength : {max : 20,message : 'The Amount must be less than 20 characters long'},
				regexp : {regexp:/^((\d+)((\.\d{0,2})?))$/,message : 'The amount is invalid'}
			}
		};
	
	$(document).ready(function(){
		
		$("#billing_address_btn").attr("data-content",$("#address").html());
		
		//Close All Address Popover
		$(document).on("click", ".popover a[data-popover-close]" , function(){
			$(this).parents(".popover").popover('hide');
			return false;
		});
		
		//Change Billing & Shipping Address
		$("body").on("click",'button[data-change-address]',function() {
			var id,$selectedAddress=$(this).closest('[data-address-item]');
			
			if($(this).attr("data-change-address") == 'billing') {
				id='#purchase_billing_address';
				$("#billingAddressId").val($selectedAddress.attr("data-address-item"));
			} else {
				id='#purchase_shipping_address';
				$("#lblPlaceofSupply").text($selectedAddress.find("[data-address-state]").text());
				$("#shippingAddressId").val($selectedAddress.attr("data-address-item"));
			}
			
			$(id).find("[data-address-name]").html($selectedAddress.find("[data-address-name]").text()).end()
				.find("[data-address-line-1]").html($selectedAddress.find("[data-address-line-1]").text()).end()
				.find("[data-address-line-2]").html($selectedAddress.find("[data-address-line-2]").text()).end()
				.find("[data-address-pincode]").html($selectedAddress.find("[data-address-pincode]").text()).end()
				.find("[data-address-city]").html($selectedAddress.find("[data-address-city]").text()).end()
				.find("[data-address-state]").html($selectedAddress.find("[data-address-state]").text()).end()
				.find("[data-address-country]").html($selectedAddress.find("[data-address-country]").text()).end()
				.find("[data-address-city]").attr("data-address-city",$selectedAddress.find("[data-address-city]").attr("data-address-city")).end()
				.find("[data-address-state]").attr("data-address-state",$selectedAddress.find("[data-address-state]").attr("data-address-state")).end()
				.find("[data-address-country]").attr("data-address-country",$selectedAddress.find("[data-address-country]").attr("data-address-country")).end()
				.find("[data-address-phoneNo]").html($selectedAddress.find("[data-address-phoneNo]").text()).end()
				.removeClass("m--hide").end()
				.find("[data-address-message]").addClass("m--hide").end();
			  
			  $selectedAddress.closest(".popover").find("a[data-popover-close]").click();
			  
			  changeTaxType();
		});
		
		$roundoffSection = $("#roundoff_section").clone();
		$roundoffSection.find("[name='roundoff']").attr("id","edit_roundoff");
		$("#roundoff_edit").attr("data-content",$roundoffSection.html());
		
		$('#roundoff_edit').popover().on('shown.bs.popover', function() {
			$("#edit_roundoff").val($("#round_off").text());
			$("#edit_roundoff").focus();
		});
		$('input[name="sez"]').click(function(){
	        if($(this).prop("checked") == true){
	            $(this).val(1);
	        }
	        else if($(this).prop("checked") == false){
	        	  $(this).val(0);
	        }
	    });
		
		/**
		 * Product
		 */
		
		$("#productId").change(function(){
			if($("#productId").val() != "") {
				$("#addProductModal").modal("show");
				$("#productIdModal").val($("#productId").val()).trigger('change');
				$("#productId").val("").trigger('change');
			}
		});
		
		$('#btn_percentage').click(function (){
			$('#btn_percentage').slideUp();
			$('#btn_amount').slideDown();
			$("#modalDiscount").attr("placeholder", 'Amount');
  		});
		$('#btn_amount').click(function (){
			$('#btn_percentage').slideDown();
			$('#btn_amount').slideUp()
			$("#modalDiscount").attr("placeholder", 'Percentage');
  		});
		
		$(document).on("click",'a[data-purchase-item-toggle]',function(e) {
			if($(this).closest('[data-purchase-item]').hasClass("m-table__row--brand")) {
				$(this).closest('[data-purchase-item]').removeClass("m-table__row--brand");
			} else {
				$(this).closest('[data-purchase-item]').addClass("m-table__row--brand");
			}
			e.stopPropagation();
		});
		
		$(document).on("click",'tr[data-purchase-item]',function() {
			
			var obj = new Object();
			
			obj.haveVariation=$(this).find("input[id*='haveVariant']").val();
			obj.haveDesignno = $(this).find("input[id*='haveDesignno']").val();
			obj.haveBaleno = $(this).find("input[id*='haveBaleno']").val();
			obj.productDescription = $(this).find("[data-item-description]").html();
			obj.name=$(this).find("[data-item-name]").html();
			obj.purchaseItemIndex = $(this).attr("data-purchase-item");
			var purchaseTaxVo = new Object();
			purchaseTaxVo.taxName=$(this).find("[data-item-tax-name]").html();
			
			var unitOfMeasurementVo = new Object();
			unitOfMeasurementVo.measurementCode = $(this).find("[data-item-uom]").html();
			unitOfMeasurementVo.noOfDecimalPlaces=$(this).find("input[id*='noOfDecimalPlaces']" ).val();
			obj.unitOfMeasurementVo = unitOfMeasurementVo;
			
			obj.purchaseTaxIncluded = $(this).find("input[id*='taxIncluded']" ).val();
			
			var productVariantVos =[]
			
			$variantItem=$("#product_table").find("[data-variant-row='"+$(this).attr("data-purchase-item")+"']").find("[data-variant-item]");
			
			$variantItem.each(function (){	
				
				if($(this).index() == 0) {
					obj.productId=$(this).find("input[id*='productId']" ).val();
					
					purchaseTaxVo.taxId=$(this).find("input[id*='taxId']" ).val();
					purchaseTaxVo.taxRate=$(this).find("input[id*='taxRate']" ).val();
					obj.designno = $(this).find("input[id*='designNo']").val();
					obj.baleno = $(this).find("input[id*='baleNo']").val();
				}
				var variant=new Object();
				variant.productVariantId=$(this).find("input[id*='productVariantId']" ).val();
				variant.variantName=$(this).find("[data-variant-name]" ).html();
				variant.purchasePrice=$(this).find("input[id*='price']" ).val();
				variant.designNo=$(this).find("input[id*='designNo']" ).val();
				variant.qty=$(this).find("input[id*='qty']" ).val();
				variant.discount=$(this).find("input[id*='discount']" ).val();
				variant.discountType=$(this).find("input[id*='discountType']" ).val();
				variant.variantItemIndex= $(this).attr("data-variant-item");
				productVariantVos.push(variant);
			});
			
			obj.purchaseTaxVo = purchaseTaxVo;
			
			obj.productVariantVos = productVariantVos;
			
			$("#addProductModal").modal("show");
			setProductOnModal(obj);
			
		});
		
		$("#product_table").on("click",'a[data-item-remove]',function(e) {
			var i=$(this).closest("[data-purchase-item]").attr("data-purchase-item");
			
			$("#product_table").find("[data-variant-row='"+i+"']").find("input[id*='purchaseItemId']").each(function() {
				$("#deletePurchaseItemIds").val($("#deletePurchaseItemIds").val() + $(this).val() + ",");
			});
			
			$(this).closest("[data-purchase-item]").remove();
			$("#product_table").find("[data-variant-row='"+i+"']").remove();
			setSubTotal();
			setSrNo();
			setTaxSummary();
			return false;
		});
		
		/**
		 * Additional Charge
		 */
		
		$(document).on("click",'#additional_charge_button',function(e) {
			getAdditionalCharge();
		});
		
		$(document).on("click",'#add_additional_charge',function(e) {
			e.preventDefault();
			addAdditionalCharge();
		});
		
		$(document).on("click",'#additional_charge_cancel',function(e) {
			
			var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
			
			$additionalChargeItem.each(function (){
				var i=$(this).attr("data-charge-item");
				$('#purchase_form').formValidation('removeField',"purchaseAdditionalChargeVos["+i+"].additionalChargeVo.additionalChargeId");
				$('#purchase_form').formValidation('removeField',"purchaseAdditionalChargeVos["+i+"].amount");
				
				if($(this).find("input[id*='purchaseAdditionalChargeId']" ).val()) {
					$("#deleteAdditionalChargeIds").val($("#deleteAdditionalChargeIds").val() + $(this).find("input[id*='purchaseAdditionalChargeId']" ).val() + ",");
				}
			});
			
			$("#additional_charge_table").find("[data-charge-item]").not(".m--hide").remove();
			$("#additional_charge_button").closest(".m-demo-icon").removeClass("m--hide");
			
			setAdditionalChargeSubTotal();
			setTaxSummary();
		});
		
		$("#additional_charge_table").on("click",'a[data-item-remove]',function(e) {
			//var i=$(this).closest("[data-charge-item]").attr("data-purchase-item");
			e.preventDefault();
			var i=$(this).closest("[data-charge-item]").attr("data-charge-item");
			$('#purchase_form').formValidation('removeField',"purchaseAdditionalChargeVos["+i+"].additionalChargeVo.additionalChargeId");
			$('#purchase_form').formValidation('removeField',"purchaseAdditionalChargeVos["+i+"].amount");
			
			if($(this).closest("[data-charge-item]").find("input[id*='purchaseAdditionalChargeId']" ).val()) {
				$("#deleteAdditionalChargeIds").val($("#deleteAdditionalChargeIds").val() + $(this).closest("[data-charge-item]").find("input[id*='purchaseAdditionalChargeId']" ).val() + ",");
			}
			
			$(this).closest("[data-charge-item]").remove();
			
			setAdditionalChargeSubTotal();
			setAdditionalChargeSrNo();
			setTaxSummary();
		});
		
		/**
		 * Term And Condition
		 */
		$(document).on("click",'#terms_condition_button',function(e) {
			getTermsAndCondition();
		});
		
		//-------------------- Purchase Validation -------------------------------
		$('#purchase_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savepurchase",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				"contactVo.contactId" : {
					validators : {
						notEmpty : {
							message : 'please select supplier'
						}
					}
				},
				purchaseDate : {
					validators : {
						notEmpty : {
							message : 'The Request Date is required'
						}
					}
				},
				prefix : {
					validators : {
						notEmpty : {
							message : 'The Prefix is required'
						},
						stringLength : {
							max : 20,
							message : 'The Prefix must be less than 20 characters long'
						},
						regexp : {
							regexp : /^[a-zA-Z0-9/_-\s-., ]+$/,
							message : 'Prefix is not valid'
						}
					}
				},
				purchaseNo : {
					verbose : false,
					validators : {
						notEmpty : {
							message : 'The Purchase No. is required'
						},
						stringLength : {
							max : 8,
							message : 'The Purchase No is required'
						},
						regexp : {
							regexp : /^[0-9]+$/,
							message : 'The Purchase No only consist of number'
						},
						remote : {
							message : 'This Purchase No is already exist',
							url : purchaseNoVerifyURL,
							type : 'POST',
							data: function(validator, $field, value) {
								 return {
									 prefix : $('#prefix').val(),
								 };
							},
							onSuccess: function(data, value) {
								$('#purchase_form').formValidation('validateField', 'prefix');
								if($('#purchase_form').data('formValidation').isValidField("prefix") ==false) {
									$('#purchase_form').formValidation('revalidateField', 'prefix');
								}
							}
						}
					}
				},
			}
		});
		//-------------------- End Purchase Validation ---------------------------
		
		$("#savepurchase").click(function (){
			
			if($("#product_table").find("[data-purchase-item]").not(".m--hide").length == 0) {
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-center",
				  "preventDuplicates": true,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};
				toastr.error("","add minimum one product");
				
				return false;
			}
			
			
			if($('#purchase_form').data('formValidation').isValid() == null || $('#purchase_form').data('formValidation').isValid() == true)
			{
				$("#product_table").find("[data-purchase-item='template']").remove();
				$("#product_table").find("[data-variant-row='template']").remove();
				$("#additional_charge_table").find("[data-charge-item='template']").remove();
				$("#total").val($("#net_amount").text());
				$("#roundoff").val($("#round_off").text());
				
				if($("#paymentTermVo").val() == "") {
					$("#paymentTermVo").remove();
				}
				
				if($("#transportId").val() == "") {
					$("#transportId").remove();
				}
				
				if($("#agentId").val() == "") {
					$("#agentId").remove();
				}
			}
		});
		
		$("body").on("change", "input[type='text']", function(){
			if($(this).attr('data-decimal')!=undefined && $(this).attr('data-decimal') != '') {			
				var fix=parseFloat($(this).val()).toFixed($(this).attr('data-decimal'));					
				$(this).val(fix);
			}	
		});
	});
	
	function getContactInfo(change) {
		var id=$("#contactVo").val();
		
		$.post("/contact/suppliers/"+id+"/json", {
			
		}, function (data, status) {
			
			
			if(status == 'success') {
				
				$("#lblContactGSTIN").text(data.gstin=='' ? "-" : data.gstin);
				
				address=data.contactAddressVos;
				
				var $template = $('#address_template'),
		        $clone = $template.clone()
		        		.removeClass('m--hide')
				        .removeAttr('id')
				        .attr('id', "billing_address");
				        //.insertBefore($template);
				
				$addressList=$clone.find("[data-address-list]").clone();
				$clone.find("[data-address-list]").html("");
				$.each(data.contactAddressVos, function( key, value ) {
					
					$addressItem=$addressList.clone();
					
					$addressItem.find("[data-address-item]").attr("data-address-item",value.contactAddressId).end()
								.find("[data-address-name]").html(value.companyName).end()
								.find("[data-address-line-1]").html(value.addressLine1).end()
								.find("[data-address-line-2]").html(value.addressLine2).end()
								.find("[data-address-pincode]").html(value.pinCode).end()
								.find("[data-address-city]").html(value.cityName).end()
								.find("[data-address-state]").html(value.stateName).end()
								.find("[data-address-country]").html(value.countriesName).end()
								.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
								.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
								.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
								.find("[data-address-phoneNo]").html(data.companyMobileno == "" ? "Mobile no. is not provided": data.companyMobileno).end();
					
					if(key == 0) {
						$addressItem.find(".m-divider").parent().remove();
						
						if(change == 0) {
							$("#purchase_billing_address,#purchase_shipping_address").find("[data-address-name]").html(value.companyName).end()
								.find("[data-address-line-1]").html(value.addressLine1).end()
								.find("[data-address-line-2]").html(value.addressLine2).end()
								.find("[data-address-pincode]").html(value.pinCode).end()
								.find("[data-address-city]").html(value.cityName).end()
								.find("[data-address-state]").html(value.stateName).end()
								.find("[data-address-country]").html(value.countriesName).end()
								.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
								.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
								.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
								.find("[data-address-phoneNo]").html(data.companyMobileno == "" ? "Mobile no. is not provided": data.companyMobileno).end()
								.removeClass("m--hide").end()
								.find("[data-address-message]").addClass("m--hide").end();
							
							$("#lblPlaceofSupply").text(value.stateName);
							$("#shippingAddressId").val(value.contactAddressId);
							$("#billingAddressId").val(value.contactAddressId);
						}
					}
					//console.log($addressItem.html());
					$clone.find("[data-address-list]").append($addressItem.html());	
					//console.log("key::"+key)
					//console.log($clone.find("[data-address-list]").html());
					
				});
				
				$clone.find("[data-change-address]").attr("data-change-address","billing");
				$("#billing_address_btn").attr("data-content",$clone.html());
				
				$clone.find("[data-change-address]").attr("data-change-address","shipping");
				$("#shipping_address_btn").attr("data-content",$clone.html());
				
				if(change == 0) {
					changeTaxType();
				}
				
			} else {
				console.log("status: "+status);
			}
		});
	}
	
	function changeDueDate(){
		
		if($('#paymentTermVo').val()==null || $('#paymentTermVo').val()=='')
		{
			var date2 = $('#purchaseDate').datepicker('getDate', '+1d'); 
			date2.setDate(date2.getDate()); 
			$('#purchaseDueDate').datepicker('setDate', date2);
		}
		else
		{
			day=$('#paymentTermVo option[value="'+$('#paymentTermVo').val()+'"]').attr("name");
			var date2 = $('#purchaseDate').datepicker('getDate', '+1d'); 
			var d=parseInt(day);
			date2.setDate(date2.getDate()+d); 
			$('#purchaseDueDate').datepicker('setDate', date2);	
		}
	}
	
	function setPurchaseNoVerifyURL(url) {
		purchaseNoVerifyURL = url;
	}
	
	function getProductInfo() {
		
		var id = $("#productIdModal").val();
		
		if(id != "") {
			$.post("/product/"+id+"/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					setProductOnModal(data);
				}
			});
		}
	}
	
	function setProductOnModal(data) {
		
		$("#variant_table").find("[data-variant-item]").not(".m--hide").remove();
		var $template = $("#variant_table").find("[data-variant-list]").clone();
		
		$("#modalTax").val(data.purchaseItemIndex ? data.purchaseTaxVo.taxName  : data.purchaseTaxVo.taxName +" ("+data.purchaseTaxVo.taxRate+" %)");
		$("#modalTaxId").val(data.purchaseTaxVo.taxId);
		$("#modalTaxIncluded").val(data.purchaseTaxIncluded);
		$("#modalTaxRate").val(data.purchaseTaxVo.taxRate);
		$("#modalDiscount").val("0");
		$("#modalDiscountType").val("percentage");
		$("#modalUOM").val(data.unitOfMeasurementVo.measurementCode);
		$("#modalNoOfDecimalPlaces").val(data.unitOfMeasurementVo.noOfDecimalPlaces);
		$("#modalDescription").val(data.productDescription ? data.productDescription : "");
		$("#modalHaveBaleno").val(data.haveBaleno);
		$("#modalDesignNo").val(data.designno);
		$("#modalBaleno").val(data.baleno);
		var haveDesignno = $("#modalHaveDesignno"), haveVariant = $("#modalHaveVariant");
		haveVariant.val(data.haveVariation);
		haveDesignno.val(data.haveDesignno ? data.haveDesignno : "0");
		
		$("#purchaseItemIndex").val(data.purchaseItemIndex ? data.purchaseItemIndex : "-1");
		
		$.each(data.productVariantVos, function( key, value ) {
			
			$clone = $template.clone();
			$clone.find("[data-variant-item]").removeClass('m--hide');
			$clone.find("[data-variant-item]").attr("data-variant-item", value.variantItemIndex ? value.variantItemIndex : "");
			
			if(data.haveVariation == 1) {
				
				//$clone.find("[data-variant-item]").find("[id='modalQty{index}']").val(value.qty ? value.qty : "0");
				$clone.find("[data-variant-item]").find("[id='modalProductVariantId{index}']").val(value.productVariantId);
				
				$clone.find("input[type='text'],input[type='hidden']").each(function (){
					n=$(this).attr("id");
					n ? $(this).attr("id",n.replace(/{index}/g,key)) : "";
				});
				
				$clone.find("[data-variant-name]").html(value.variantName);
				
				$("#variant_table").find("[data-variant-list]").append($clone.html());
				
				$("#modalQty"+key).val(value.qty ? value.qty : "");
			}
			
			if(key == 0) {
				$("#modalPurchasePrice").val(value.purchasePrice);
				$("#modalProductVariantId").val(value.productVariantId);
				$("#modalDesignNo").val(value.designNo ? value.designNo : "");
				$("#modalDiscount").val(value.discount ? value.discount : "0");
				$("#modalDiscountType").val(value.discountType ? value.discountType : "percentage");
				
				if($("#modalDiscountType").val() == 'percentage') {
					$("#btn_amount").click();
				} else {
					$("#btn_percentage").click();
				}
				$("#modalQty").val(value.qty ? value.qty : "1");
				
			}
		});
		
		if($("#designno_div").html() != undefined && haveDesignno.val() == 1
				&& $("#modalHaveBaleno").val() == 1) {
			$("#designno_div").removeClass("m--hide");
			$("#baleno_div").removeClass("m--hide");
			
			$("#baleno_div").removeClass("col-lg-12 col-md-12");
			$("#baleno_div").addClass("col-lg-6 col-md-6");
			
			$("#designno_div").removeClass("col-lg-12 col-md-12");
			$("#designno_div").addClass("col-lg-6 col-md-6");
			
		} else if($("#designno_div").html() != undefined && haveDesignno.val() == 1
				&& $("#modalHaveBaleno").val() == 0) {
			$("#baleno_div").addClass("m--hide");
			$("#designno_div").removeClass("m--hide");
			
			$("#designno_div").removeClass("col-lg-6 col-md-6");
			$("#designno_div").addClass("col-lg-12 col-md-12");
			
		} else if($("#designno_div").html() != undefined && haveDesignno.val() == 0
				&& $("#modalHaveBaleno").val() == 1) {
			$("#baleno_div").removeClass("m--hide");
			$("#designno_div").addClass("m--hide");
			
			$("#baleno_div").removeClass("col-lg-6 col-md-6");
			$("#baleno_div").addClass("col-lg-12 col-md-12");
			
		} else {
			$("#designno_div").addClass("m--hide");
			$("#baleno_div").addClass("m--hide");
			
		}
		
		if(haveVariant.val() == 1) {
			$("#qty_div").addClass("m--hide");
			$("#variant_div").removeClass("m--hide");
		} else {
			$("#variant_div").addClass("m--hide");
			$("#qty_div").removeClass("m--hide");
			
		}
		
		if(data.purchaseItemIndex) {
			$("#productIdModal").val(data.productId);
			$("#productIdModal").select2();
		}
	}
	
	function setProduct() {
		//modalDescription
		//designno = $("#modalDesignNo").val(),
		var taxName = $("#modalTax").val(), taxId = $("#modalTaxId").val(), taxRate = parseFloat($("#modalTaxRate").val()),
			discountType = $("#btn_amount").is(":visible") ? 'amount' : 'percentage',
			haveVariant = $("#modalHaveVariant").val(),
			haveDesignno = $("#modalHaveDesignno").val(),
			haveBaleno = $("#modalHaveBaleno").val(),
			designno = $("#modalDesignNo").val(),
			baleno = $("#modalBaleno").val(),
			productId = $('#productIdModal').val(),
			productVariantId=$("#modalProductVariantId").val(),
			uom = $("#modalUOM").val(),
			taxIncluded = $("#modalTaxIncluded").val(),
			purchaseItemIndex = $("#purchaseItemIndex").val(),
			noOfDecimalPlaces = $("#modalNoOfDecimalPlaces").val(),
			discount = parseFloat($("#modalDiscount").val()),
			price = parseFloat($("#modalPurchasePrice").val()),
			productName = $('#productIdModal option[value="'+$('#productIdModal').val()+'"]').html(),
			productDescription = $("#modalDescription").val();
			
			taxAmount = 0.0, qty = 0, productId=$("#productIdModal").val();
			//'  <i class="fa fa-clone m--regular-font-size-sm2 "></i>';
		var	$purchaseItemTemplate, $variantTableTemplate;
		var mainIndex=index;
		var selector = purchaseItemIndex==-1 ? "{index}" : purchaseItemIndex;
		
		if(purchaseItemIndex == -1) {
			$purchaseItemTemplate=$("#product_table").find("[data-purchase-item='template']").clone();
			$variantTableTemplate=$("#product_table").find("[data-variant-row='template']").clone();
		} else {
			$purchaseItemTemplate=$("#product_table").find("[data-purchase-item='"+purchaseItemIndex+"']");
			$variantTableTemplate=$("#product_table").find("[data-variant-row='"+purchaseItemIndex+"']");
			mainIndex = purchaseItemIndex;
		}
		
		//$purchaseItemTemplate.not("[data-purchase-item='template']").remove();
		$variantTableTemplate.attr("data-variant-row",mainIndex)
		//replace {index} to ID of variant TR
		$variantTableTemplate.attr("id", $variantTableTemplate.attr("id").replace(/{index}/g,index));
		$variantTableTemplate.find("[data-variant-item]").attr("data-variant-item",mainIndex)
		if(haveVariant == 0) {
			
			qty = $("#modalQty").val();
			
			$variantTableTemplate.find("[data-variant-name]").html("").end()
				.find("[data-variant-qty]").html(qty).end()
				.find("[name='purchaseItemVos["+selector+"].product.productId']").val(productId).end()
				.find("[name='purchaseItemVos["+selector+"].productVariantVo.productVariantId']").val(productVariantId).end()
				.find("[name='purchaseItemVos["+selector+"].qty']").val(qty).end()
				.find("[name='purchaseItemVos["+selector+"].price']").val(price).end()
				.find("[name='purchaseItemVos["+selector+"].productDescription']").val(productDescription).end()
				.find("[name='purchaseItemVos["+selector+"].taxAmount']").val(taxAmount).end()
				.find("[name='purchaseItemVos["+selector+"].taxRate']").val(taxRate).end()
				.find("[name='purchaseItemVos["+selector+"].taxVo.taxId']").val(taxId).end()
				.find("[name='purchaseItemVos["+selector+"].discount']").val(discount).end()
				.find("[name='purchaseItemVos["+selector+"].discountType']").val(discountType).end()
				.find("[name='purchaseItemVos["+selector+"].designNo']").val(designno).end()
				.find("[name='purchaseItemVos["+selector+"].baleNo']").val(baleno).end();
			$variantTableTemplate.find("input[type='hidden']").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,index)) : "";
				
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,index)) : "";
			});
			
		} else {
			
			qty = 0.0;
			
			if(purchaseItemIndex == -1) {
				$variantItemTemplate = $variantTableTemplate.find("[data-variant-item]").clone();
				$variantTableTemplate.find("[data-variant-table]").children('tbody').html("");
			} else {
				$variantItemTemplate = $variantTableTemplate.find("[data-variant-table]").children('tbody');
			}
			
			$("#variant_table").find("[data-variant-item]").not(".m--hide").each(function () {
				
				//var variantItemIndex = $variantItem.attr("data-variant-item") == ""? $variantItem.attr("data-variant-item");
				var variantSelector = "{index}";
				if(purchaseItemIndex == -1)	{
					$variantItem = $variantItemTemplate.clone();
					$variantItem.attr("data-variant-item",index);
				} else {
					$variantItem = $variantItemTemplate.find("[data-variant-item='"+$(this).attr("data-variant-item")+"']");
					variantSelector = $(this).attr("data-variant-item");
				}
				var i=($(this).index())-1;
				productVariantId = $("#modalProductVariantId"+i).val();
				
				if($("#modalQty"+($(this).index()-1)).val() != '') {
					qty += parseFloat($("#modalQty"+($(this).index()-1)).val());
				}
				
				$variantItem.find("[data-variant-name]").html($(this).find("[data-variant-name]").html()).end()
					.find("[data-variant-qty]").html($("#modalQty"+($(this).index()-1)).val() == ''? 0 : $("#modalQty"+($(this).index()-1)).val()).end()
					.find("[name='purchaseItemVos["+selector+"].product.productId']").val(productId).end()
					.find("[name='purchaseItemVos["+selector+"].productVariantVo.productVariantId']").val(productVariantId).end()
					.find("[name='purchaseItemVos["+selector+"].qty']").val($("#modalQty"+($(this).index()-1)).val() == ''? 0 : $("#modalQty"+($(this).index()-1)).val()).end()
					.find("[name='purchaseItemVos["+selector+"].price']").val(price).end()
					.find("[name='purchaseItemVos["+selector+"].productDescription']").val(productDescription).end()
					.find("[name='purchaseItemVos["+selector+"].taxAmount']").val(taxAmount).end()
					.find("[name='purchaseItemVos["+selector+"].taxRate']").val(taxRate).end()
					.find("[name='purchaseItemVos["+selector+"].taxVo.taxId']").val(taxId).end()
					.find("[name='purchaseItemVos["+selector+"].discount']").val(discount).end()
					.find("[name='purchaseItemVos["+selector+"].discountType']").val(discountType).end()
					.find("[name='purchaseItemVos["+selector+"].designNo']").val(designno).end()
					.find("[name='purchaseItemVos["+selector+"].baleNo']").val(baleno).end();
					
				$variantItem.find("input[type='hidden']").each(function (){
					n=$(this).attr("id");
					n ? $(this).attr("id",n.replace(/{index}/g,index)) : "";
					
					n=$(this).attr("name");
					n ? $(this).attr("name",n.replace(/{index}/g,index)) : "";
				});
				
				if(purchaseItemIndex == -1)	{
					$variantTableTemplate.find("[data-variant-table]").children('tbody').append($variantItem);
					index++;
				}
				
				
			});
		}
		
		if(haveVariant == 0) {
			$purchaseItemTemplate.find("[data-purchase-item-toggle]").removeAttr("data-purchase-item-toggle");
			index++;
		} else {
			productName += '  <i class="fa fa-clone m--regular-font-size-sm2 "></i>';
			$purchaseItemTemplate.find("[data-purchase-item-toggle]").attr("href",$purchaseItemTemplate.find("[data-purchase-item-toggle]").attr("href").replace(/{index}/g,mainIndex)).end();
		}
		
		$purchaseItemTemplate.attr("data-purchase-item",mainIndex)
			.removeClass("m--hide");
		$purchaseItemTemplate.find("[data-item-name]").html(productName).end()
			.find("[data-item-description]").html(productDescription).end()
			.find("[data-item-qty]").html(qty).end()
			.find("[data-item-uom]").html(uom).end()
			.find("[data-item-price]").html(price).end()
			.find("[data-item-discount]").html(discount).end()
			.find("[data-item-tax-name]").html(taxName).end()
			.find("[data-item-tax-amount]").html(taxAmount).end()
			.find("[data-item-amount]").html(0.0).end()
			.find("[id='haveDesignno"+selector+"']").val(haveDesignno).end()
			.find("[id='haveVariant"+selector+"']").val(haveVariant).end()
			.find("[id='taxIncluded"+selector+"']").val(taxIncluded).end()
			.find("[id='haveBaleno"+selector+"']").val(haveBaleno).end()
			.find("[id='noOfDecimalPlaces"+selector+"']").val(noOfDecimalPlaces).end();
		
		if($purchaseItemTemplate.find("[data-item-designno]").html() != undefined) {
			
			$purchaseItemTemplate.find("[data-item-designno]").html(designno).end()
				.find("[data-item-baleno]").html(baleno).end();
			
			if(haveDesignno == 0) {
				$purchaseItemTemplate.find("[data-item-designno]").addClass("m--hide");
			} else {
				$purchaseItemTemplate.find("[data-item-designno]").removeClass("m--hide");
			}
			
			if(haveBaleno == 0) {
				$purchaseItemTemplate.find("[data-item-baleno]").addClass("m--hide");
			} else {
				$purchaseItemTemplate.find("[data-item-baleno]").removeClass("m--hide");
			}
		}
		
		$purchaseItemTemplate.find("input[type='hidden']").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,index)) : "";
			
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,index)) : "";
		});
		
		if(purchaseItemIndex == -1) {
			$("#product_table").find("[data-purchase-list]").append($purchaseItemTemplate).append($variantTableTemplate);
		}
		
		setAmount(mainIndex)
		setSrNo();
		$("#addProductModal").modal("hide");
	}
	
	function setAmount(id) {
		
		var qty, totalQty=0;
  		var rate;
  		var discount, totalDiscount = 0.0;
		var taxRate=parseFloat("0.0");
		var taxableAmount,taxAmount = 0.0, totalTaxAmount = 0.0;
		var total=0.0,totalAmount= 0.0;
		var discountType;
		
		$variantItem=$("#product_table").find("[data-variant-row='"+id+"']").find("[data-variant-item]");
		
		$variantItem.each(function (){	
			
			if($(this).find("input[id*='qty']" ).val()=="") {
				qty=parseFloat("0.0");
			} else {
				qty = parseFloat($(this).find("input[id*='qty']" ).val());
			}
			
			if($(this).find("input[id*='taxRate']" ).val()=="") {
				taxRate=parseFloat("0.0");
			} else {
				taxRate = parseFloat($(this).find("input[id*='taxRate']" ).val());
			}
			
			if($(this).find("input[id*='price']" ).val()=="") {
				rate=parseFloat("0.0");
			} else {
				rate = parseFloat($(this).find("input[id*='price']" ).val());
			}
							
			if($(this).find("input[id*='discount']" ).val()=="") {
				discount=parseFloat("0.0");
			} else {
				discount = parseFloat($(this).find("input[id*='discount']" ).val());
			}
			
			discountType = $(this).find("input[id*='discountType']" ).val();
			
			if(discountType == "percentage") {
				discount = (rate * discount) / 100;
			}
			
			totalDiscount += (discount*1)*(qty*1);
			
			taxableValue=(rate*qty)-(discount*qty);
			
			taxAmount=(taxableValue*taxRate)/100;
			total=taxableValue+taxAmount;
			
			totalAmount += total;
			totalTaxAmount += taxAmount;
			totalQty += qty;
			
			/*amount=qty*rate;
			
			if(discountType == "amount") {
				taxableValue=amount-discount;
			} else {
				taxableValue=amount-((amount*discount)/100);
			}
			
			taxAmount=taxableValue*taxRate/100;
			total=taxableValue+taxAmount;
			
			totalAmount += total;
			totalTaxAmount += taxAmount;
			totalQty += qty;*/
			
			$(this).find("input[id*='taxAmount']").val(taxAmount.toFixed(2));
		});
		
		$purchaseItem=$("#product_table").find("[data-purchase-item='"+id+"']");
		
		$purchaseItem.find("[data-item-qty]").html(totalQty);
		$purchaseItem.find("[data-item-tax-amount]").html(totalTaxAmount.toFixed(2));
		$purchaseItem.find("[data-item-discount]").html(totalDiscount.toFixed(2));
		$purchaseItem.find("[data-item-amount]").html(totalAmount.toFixed(2));
		
		setTaxSummary();
		
		setSubTotal();
	}
	
	function setSubTotal() {
		var $purchaseItem=$("#product_table").find("[data-purchase-item]").not(".m--hide"), subTotal=0.0;
		
		$purchaseItem.each(function (){
			subTotal += parseFloat($(this).find("[data-item-amount]").html());
		});
		
		$("#product_sub_total").html(subTotal.toFixed(2));
		
		setAllTotal();
	}
	
	function setAllTotal() {
		var taxAmount = 0.0, totalAmount = 0.0, roundoff=0.0, netAmount = 0.0;
		
		totalAmount = parseFloat($("#product_sub_total").text()) + parseFloat($("#additional_charge_sub_total").text());
		
		var $purchaseItem=$("#product_table").find("[data-purchase-item]").not(".m--hide"), subTotal=0.0;
		
		$purchaseItem.each(function (){
			taxAmount += parseFloat($(this).find("[data-item-tax-amount]").html());
		});
		
		$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide")
		$additionalChargeItem.each(function (){
			taxAmount += parseFloat($(this).find("[data-item-tax-amount]").html());
		});
		$("#tax_amount").text(taxAmount.toFixed(2));
		$("#total_amount").text(totalAmount);
		$("#round_off").text(parseFloat(Math.round(totalAmount.toFixed(2))-totalAmount.toFixed(2)).toFixed(2));
  		//$("#roundoff").val(parseFloat(Math.round(totalAmount.toFixed(2))-totalAmount.toFixed(2)).toFixed(2));
  		$("#net_amount").text(Math.round(totalAmount.toFixed(2)));
  		
	}
	
	function setSrNo() {
		var $purchaseItem=$("#product_table").find("[data-purchase-item]").not(".m--hide"), subTotal=0.0;
		var i = 0;
		$purchaseItem.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	/**
	 * Additional Charge Methods
	 * @returns
	 */
	
	function addAdditionalCharge() {
		if(_additionalChargeData != undefined) {
			
			$additionalChargeItemTemplate=$("#additional_charge_table").find("[data-charge-item='template']").clone();
			$additionalChargeItemTemplate.removeClass("m--hide").attr("data-charge-item",additionalChargeId);
			
			$additionalChargeItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,additionalChargeId)) : "";
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,additionalChargeId)) : "";
				
				$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,additionalChargeId)) : "";
			});
			
			console.log($additionalChargeItemTemplate);
			
			$("#additional_charge_table").find("[data-charge-list]").append($additionalChargeItemTemplate);

			$.each(_additionalChargeData, function (key, value) {
				$("#additionalCharge"+additionalChargeId).append($('<option></option>').val(value.additionalChargeId).html(value.additionalCharge));
			});
			
			$("#additionalCharge"+additionalChargeId).addClass("m-select2");
			$("#additionalCharge"+additionalChargeId).select2({placeholder:"Select Additional",allowClear:0});
			
			$('#purchase_form').formValidation('addField',"purchaseAdditionalChargeVos["+additionalChargeId+"].additionalChargeVo.additionalChargeId", additionalChargeValidator);
			$('#purchase_form').formValidation('addField',"purchaseAdditionalChargeVos["+additionalChargeId+"].amount", additionalChargeAmountValidator);
			
			additionalChargeId++;
			
			setAdditionalChargeSrNo();
		}
	}
	
	function getAdditionalCharge() {
		//mApp.blockPage()
		
		if(_additionalChargeData == undefined) {
			$.post("/additionalcharge/json", {
								
			}, function (data, status) {
				if(status == 'success') {
					_additionalChargeData = data;
					$("#add_additional_charge").click();
				}
			});
		} else {
			$("#add_additional_charge").click();
		}
		
		$("#additional_charge_button").closest(".m-demo-icon").addClass("m--hide");
	}
	
	function setAdditionalChargeSrNo() {
		var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
		var i = 0;
		$additionalChargeItem.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function getAdditionalChargeInfo(i) {
		
		var id = $("#additionalCharge"+i).val();
		
		if(id != "") {
			var data = jQuery.map(_additionalChargeData, function(obj) {
				if (obj.additionalChargeId == id) return obj;
			});
			
			var taxAmount=parseFloat(data[0].defaultAmount) * parseFloat(data[0].taxVo.taxRate) / 100;
			$("#additionalChargeAmount"+i).val(data[0].defaultAmount);
			$("#additionalChargeTaxId"+i).val(data[0].taxVo.taxId);
			$("#additionalChargeTaxRate"+i).val(data[0].taxVo.taxRate);
			$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item='"+i+"']");
			$additionalChargeItem.find("[data-item-tax-name]").text(data[0].taxVo.taxName + " ("+data[0].taxVo.taxRate+" %)");
			
			setAdditionalChargeAmount(i);
		}
	}
	
	function setAdditionalChargeAmount(id) {
		
		var taxRate=0.0;
		var taxableAmount,taxAmount = 0.0;
		
		if($("#additionalChargeTaxRate"+id).val()=="") {
			taxRate=parseFloat("0.0");
		} else {
			taxRate=parseFloat($("#additionalChargeTaxRate"+id).val());
		}
		
		if($("#additionalChargeAmount"+id).val()=="") {
			taxableAmount=parseFloat("0.0");
		} else {
			taxableAmount = parseFloat($("#additionalChargeAmount"+id).val());
		}
		
		taxAmount=parseFloat(taxableAmount) * taxRate / 100;
		
		$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item='"+id+"']");
		
		$additionalChargeItem.find("[data-item-tax-amount]").text(taxAmount.toFixed(2));
		$additionalChargeItem.find("[data-item-amount]").text(parseFloat(taxableAmount+taxAmount).toFixed(2));
		$("#additionalChargeTaxAmount"+id).val(taxAmount.toFixed(2));
		
		setAdditionalChargeSubTotal();
		
		setTaxSummary();
	}
	
	function setAdditionalChargeSubTotal() {
		var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide"), subTotal=0.0;
		
		$additionalChargeItem.each(function (){
			
			if(!isNaN(parseFloat($(this).find("[data-item-amount]").html()))) {
				subTotal += parseFloat($(this).find("[data-item-amount]").html());
			}
		});
		
		$("#additional_charge_sub_total").html(subTotal.toFixed(2));
		
		setAllTotal();
	}
	
	function setTaxSummary() {
		var split=1;
		
		$("#tax_summary_table").children('tbody').find("[data-tax-item]").not(".m--hide").remove();
		
		var $purchaseItem = $("#product_table").find("[data-variant-item]"),
			$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
		
		$purchaseItem.each(function() {
			
			var purchaseItemId=$(this).closest("[data-variant-item]").attr("data-variant-item");
			
			if(purchaseItemId != "") {
				
				setTax($("#taxId"+purchaseItemId).val(), $("#taxAmount"+purchaseItemId).val(), $("#taxRate"+purchaseItemId).val());
			}
		});
		
		$additionalChargeItem.each(function() {
			
			var additionalChargeId=$(this).attr("data-charge-item");
			
			if(additionalChargeId != "template") {
				if($("#additionalChargeTaxId"+additionalChargeId).val() != '') {
					setTax($("#additionalChargeTaxId"+additionalChargeId).val(), $("#additionalChargeTaxAmount"+additionalChargeId).val(), $("#additionalChargeTaxRate"+additionalChargeId).val());
				}
			}
		});
		
	}
	
	function setTax(taxId, taxAmount, taxRate) {
		var isExist = 0,
			$taxItem = $("#tax_summary_table").children('tbody'), amount=0;
		$taxItem.find("[data-tax-item]").not(".m--hide").each(function () {
			
			if($(this).attr("data-tax-item") == taxId) {
				isExist=1;
				
				if($("#taxType").val() == 1) {
					amount =parseFloat(taxAmount);
					
				} else {
					amount =parseFloat(taxAmount)/2;
				}
				
				$(this).find('td').eq(2).text((amount*1 + parseFloat($(this).find('td').eq(2).text())).toFixed(2));
			}
		});
		
		if(isExist == 0) {
			$taxItemTemplate=$taxItem.find("[data-tax-item='template']").clone();
			$taxItemTemplate.removeClass("m--hide").attr("data-tax-item",taxId);
			
			if($("#taxType").val() == 1) {
				amount = parseFloat(taxAmount);
				taxRate = parseFloat(taxRate);
				
			} else {
				amount =parseFloat(taxAmount)/2;
				taxRate = parseFloat(taxRate)/2;
			}
			
			$taxItemTemplate.find('td').eq(1).text(taxRate + " %");
			$taxItemTemplate.find('td').eq(2).text(amount);
			
			if($("#taxType").val() == 1) {
				
				$taxItemTemplate.find('td').eq(0).text("IGST");
				$taxItem.append($taxItemTemplate);
				
			} else {
				
				$taxItemTemplate.find('td').eq(0).text("CGST");
				$taxItem.append($taxItemTemplate.clone());
				
				$taxItemTemplate.find('td').eq(0).text("SGST");
				$taxItem.append($taxItemTemplate);
			}
		}
	}
	
	function setTermsAndConditionIds(ids) {
		$("#termsAndConditionIds").val(ids);
	}
	
	function changeTaxType()
	{
		if($("#sez").prop("checked") == true){
			$("#taxType").val(1);
		} else {
			if($("#purchase_shipping_address").find("[data-address-state]").attr("data-address-state") == $("#state_code").val()){
				$("#taxType").val(0);
			}
			else
				$("#taxType").val(1);	
			
		}
		setTaxSummary();
	}
	
	function setRoundoff()
  	{
  		
		if($("#edit_roundoff").val()==="")
  			$("#edit_roundoff").val(0);
  		
  		$("#round_off").text($("#edit_roundoff").val());
  		$("#net_amount").text((parseFloat($("#total_amount").text())+parseFloat($("#edit_roundoff").val())).toFixed(2));
  		
  		$("#roundoff_edit").click();
  	}
	
	function setIndex(id) {
		index=id;
	}
	
	function setAdditionalChargeIndex(id) {
		additionalChargeId=id;
	}
	function setPurchaseType(t) {
		purchaseType = t;
	}
	function getTermsAndCondition() {
		if(_termsAndConditionData == undefined) {
			
			$.get('/termsandcondition/'+purchaseType+'/json', {
								
			}, function (data, status) {
				if(status == 'success') {
					
					_termsAndConditionData = data;
					
					setTermsAndConditionOnModal();
				}
			});
		} else {
			setTermsAndConditionOnModal();
		}
	}
	
	function setTermsAndConditionOnModal() {
		if(_termsAndConditionData != undefined) {
			
			$("#term_condition_edit_table").find("[data-terms-item]").not(".m--hide").remove();
			
			var $termConditionTemplate;
			
			$.each(_termsAndConditionData, function (key, value) {
				
				$termConditionTemplate = $("#term_condition_edit_table").find("[data-terms-item='template']").clone();
				
				$termConditionTemplate.attr("data-terms-item",value.termsandConditionId).removeClass("m--hide");
				
				$termConditionTemplate.find("input[type='checkbox']").attr("name",value.termsandConditionId);
				
				$termConditionTemplate.find("[data-terms-name]").html(value.termsCondition);
				
				var termsArray = $("#termsAndConditionIds").val().split(',');
				
				$.each(termsArray, function (i, val) {
					if(val==value.termsandConditionId) {
						$termConditionTemplate.find("input[type='checkbox']").prop("checked",true);
						return;
					}
				});
				
				$("#term_condition_edit_table").children('tbody').append($termConditionTemplate);
			});
		}
	}
	
	function setTermsAndCondition() {
		var termsAndConditionIds="";
		$("#terms_and_condition_table").find("[data-terms-item]").not(".m--hide").remove();
		var $termConditionTemplate, i=0;
		$("#term_condition_edit_table").find("[data-terms-item]").not(".m--hide").each(function() {
			if($(this).find("input[type='checkbox']").prop("checked")) {
				
				termsAndConditionIds += $(this).find("input[type='checkbox']").attr("name") +",";
				
				$termConditionTemplate = $("#terms_and_condition_table").find("[data-terms-item='template']").clone();
				
				$termConditionTemplate.removeClass("m--hide").attr("data-terms-item","");
				$termConditionTemplate.find("[data-terms-index]").html(++i);
				$termConditionTemplate.find("[data-terms-name]").html($(this).find("[data-terms-name]").html());
				
				$("#terms_and_condition_table").children('tbody').append($termConditionTemplate);
			}
			
		});
		
		$("#termsAndConditionIds").val(termsAndConditionIds);
		
		$("#terms_and_condition").modal("hide");
	}