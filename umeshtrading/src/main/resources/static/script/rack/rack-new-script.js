/**
 * This File is For Rack New Modal
 * Rack Ajax Insert
 * Rack Validation
 */

$(document).ready(function(){
	
	$("#placeVo").select2();
	$("#floorVo").select2();
	
	$("#rack_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			rackCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
			noOfRow:{
				validators: {
					notEmpty: {
						message: 'The No. of Row is reqired'
					},
					regexp: {
						regexp:  /^[0-9,/d{1}]$/,
						message: 'The No. of Row can only consist numberical value O to 9'
					}
				}
			},
			noOfColumn:{
				validators: {
					notEmpty: {
						message: 'The No. of Column is reqired'
					},
					regexp: {
						regexp:  /^[0-5,/d{1}]$/,
						message: 'The No. of Column can only consist numberical value O to 5'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/rack/save", {
			 floorVo:$('#floorVo').val(),
			 rackName:$('#rackName').val(),
			 rackCode:$('#rackCode').val(),
			 description:$('#description').val(),
			 
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#rack_new_modal').modal('toggle');
			 
			var t = $('#rack_table').DataTable();
			t.row.add([
				"0",
				data.floorVo.floorCode +' ('+data.floorVo.floorName+')',
				data.rackName,
				data.rackCode,
				data.description,
				'<a data-toggle="modal" data-target="#rack_update_modal" onclick="updateRack(this,'+data.rackId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteRack(this,'+data.rackId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#rack_new_modal').on('show.bs.modal', function() {
		$('#rack_new_form').formValidation('resetForm', true);
	});
	
	$("#saverack").click(function() {
		$('#rack_new_form').data('formValidation').validate();
	});
	
	  
	
});

function getAllFloorAjax(){	
	
	$.get('/floor/place/'+$("#placeVo").val(),		   
	    function(data, status){		      
	        $('#floorVo').empty();
	        
	        $("#floorVo").append($('<option>',{value :'', text :"Select Floor"}));
	        $.each( data, function( key, value) {				        	
  				$("#floorVo").append(	$('<option>',{value : value.floorId, text :value.floorCode+' ('+value.floorName+')',}));
  			});
  			
  			if($('#floorVo').data("default") != undefined)
  				$('#floorVo').val($('#floorVo').data("default")).trigger('change') ;
    	});
	
	
}