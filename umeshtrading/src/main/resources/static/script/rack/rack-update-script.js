/**
 * This File is For Rack New Modal
 * Rack Ajax Insert
 * Rack Validation
 */
var flooriduu,flag=0;
$(document).ready(function(){
	
	$("#updatePlaceVo").select2();
	$("#updateFloorVo").select2();
	
	$("#rack_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updaterack",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			rackName:{
				validators: {
					notEmpty: {
						message: 'Rack Name is Required'
					}
				}
			},
			rackCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $.post("/rack/update", {
			 rackId:$('#rackId').val(),
			 floorVo:$('#updateFloorVo').val(),
			 rackName:$('#updateRackName').val(),
			 rackCode:$('#updateRackCode').val(),
			 description:$('#updateDescription').val(),
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#rack_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#rack_update_modal').on('hide.bs.modal', function() {
		$('#rack_update_form').formValidation('resetForm', true);
	});
	
	$("#updaterack").click(function() {
		$('#rack_update_form').data('formValidation').validate();
	});
	
});

function getAllFloorAjaxUpdate(){
	
	$.get('/floor/place/'+$("#updatePlaceVo").val(),		   
	    function(data, status){		      
	        $('#updateFloorVo').empty();
	        $("#updateFloorVo").append($('<option>',{value :'', text :"Select Floor"}));
	        $.each( data, function( key, value) {				        	
  				$("#updateFloorVo").append(	$('<option>',{value : value.floorId, text :value.floorCode+' ('+value.floorName+')',}));
  			});
	        
	        if(flag==1){
	        	$('#updateFloorVo').val(flooriduu);
	        	$('#updateFloorVo').trigger('change.select2');
	        	flag=0;
	        }else{
	        	if($('#updateFloorVo').data("default") != undefined)
	  				$('#updateFloorVo').val($('#updateFloorVo').data("default")).trigger('change');
	        }
	    });
}

function updateRack(row,id)
{
	$('#rack_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var placeid = $(crow).find('td:eq(0) input:hidden').val();
	flooriduu=$(crow).find('td:eq(1) input:hidden').val();
	var racknmae=$(crow).find('td:eq(2)').text();
	var rackcode = $(crow).find('td:eq(3)').text();
	var desc = $(crow).find('td:eq(4)').text();
	flag=1;
	$('#updatePlaceVo').val(placeid);
	$('#updatePlaceVo').trigger('change.select2');
	$('#updateRackName').val(racknmae);
	$('#updateRackCode').val(rackcode);
	$('#updateDescription').val(desc);
	$('#rackId').val(id);
}

