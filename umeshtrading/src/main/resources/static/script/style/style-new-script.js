/**
 * This File is For Style New Modal
 * Style Ajax Insert
 * Style Validation
 */

$(document).ready(function(){
	$("#style_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			styleName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/style/save", {
			 styleName: $('#styleName').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#style_new_modal').modal('toggle');
			 
			var t = $('#style_table').DataTable();
			t.row.add([
				"0",
				data.styleName,
				'<a  data-toggle="modal" data-target="#style_update_modal" onclick="updateStyle(this,'+data.styleId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteStyle(this,'+data.styleId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#style_new_modal').on('show.bs.modal', function() {
		$('#style_new_form').formValidation('resetForm', true);
	});
	
	$("#savestyle").click(function() {
		$('#style_new_form').data('formValidation').validate();
	});
	
});