/**
 * This File is For Style New Modal
 * Style Ajax Insert
 * Style Validation
 */

$(document).ready(function(){
	$("#style_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatestyle",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			styleName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $.post("/style/update", {
			 styleName: $('#updateStyleName').val(),
			 styleId:$('#styleId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#style_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#style_update_modal').on('hide.bs.modal', function() {
		$('#style_update_form').formValidation('resetForm', true);
	});
	
	$("#updatestyle").click(function() {
		$('#style_update_form').data('formValidation').validate();
	});
	
});

function updateStyle(row,id)
{  		//console.log(row);
	
	$('#style_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var rowindex = $(crow).find('td:eq(0)').text();

	$('#updateStyleName').val(name);
	$('#styleId').val(id);
	
}