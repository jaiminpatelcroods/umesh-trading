/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#tax_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			tax:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			taxrate:{
				validators: {
					notEmpty: {
						message: 'The Rate is Required'
					},
					regexp: {
						regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
						message: 'The Tax Rate can contain only numeric value and not more than two decimal value'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/tax/create", {
			 name: $('#tax').val(),
			 rate:$('#rate').val()
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#tax_new_modal').modal('toggle');
			 
			var t = $('#m_table_1').DataTable();
			t.row.add([
				 "0",
				 data.taxName,
				 data.taxRate,
				 '<a  data-toggle="modal" data-target="#m_modal_7" onclick="updatetax(this,'+data.taxId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deletetax(this,'+data.taxId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
		 /*$('#tax_new_form').formValidation('resetForm', true);
		 $('#tax').val("");
		 $('#rate').val("");*/
	});
	
	$('#tax_new_modal').on('show.bs.modal', function() {
		$('#tax_new_form').formValidation('resetForm', true);
	});
	
	$("#savetaxnew").click(function() {
		$('#tax_new_form').data('formValidation').validate();
	});
	
});