/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#tax_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			taxupname:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			taxuprate:{
				validators: {
					notEmpty: {
						message: 'The Rate is Required'
					},
					regexp: {
						regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
						message: 'The Tax Rate can contain only numeric value and not more than two decimal value'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $.post("/tax/update", {
			 name: $('#uptaxname').val(),
			 rate:$('#uptaxrate').val(),
			 id:$('#uptaxid').val()
		 }, function( data,status ) {
			toastr["success"]("Record Updated....");
			$('#tax_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$("#updatetax").click(function() {
		$('#tax_update_form').data('formValidation').validate();
	});
	
});

function updatetax(row,id)
{  		//console.log(row);
	
	$('#tax_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var rate = $(crow).find('td:eq(2)').text();
	var rowindex = $(crow).find('td:eq(0)').text();

	$('#uptaxname').val(name);
	$('#uptaxrate').val(rate);
	$('#uptaxid').val(id);
	$("#dataindex").val(rowindex);
	
}