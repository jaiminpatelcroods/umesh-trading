/**
 * This File is For Terms and condition New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#termsandcondition_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetermsandcondition",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			termsCondition:{
				validators: {
					notEmpty: {
						message: 'The Terms And Condition is Required'
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 var checked=0;
		 
		 if($("#isDefault").prop('checked') == true) {
			 checked=1;
		 } 
         var isdefault="No"
		 $.post("/termsandcondition/save", {
			 termsCondition: $('#termsCondition').val(),
			 modules:$('#modules').val().join(","),
			 isDefault:checked
		 }, function( data,status ) {
			 
			 toastr["success"]("Record Inserted....");
			 
			 $('#termsandcondition_new_modal').modal('toggle');
			 
			 var t = $('#termsandcondition_table').DataTable();
			 
			 if(data.isDefault==1) {
				 isdefault = "Yes";
			 } else {
				 isdefault = "No";
			 }
			 
			t.row.add([
				"0",
				data.termsCondition,
				data.modules,
				isdefault,
				'<a  data-toggle="modal" data-target="#termsandcondition_update_modal" onclick="updateTermsandCondition(this,'+data.termsandConditionId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteTermsandCondition(this,'+data.termsandConditionId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#termsandcondition_new_modal').on('show.bs.modal', function() {
		$('#termsandcondition_new_form').formValidation('resetForm', true);
	});
	
	$("#savetermsandcondition").click(function() {
		$('#termsandcondition_new_form').data('formValidation').validate();
	});
	
});