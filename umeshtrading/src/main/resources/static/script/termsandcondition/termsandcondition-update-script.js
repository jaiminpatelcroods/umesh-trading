/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#termsandcondition_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatetermsandcondition",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			termsCondition:{
				validators: {
					notEmpty: {
						message: 'The Terms And Condition is Required'
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 var checked=0;
		 
		 if($("#updateisDefault").prop('checked') == true) {
			 checked=1;
		 } 
         var isdefault="No"
		 
		 $.post("/termsandcondition/update", {
			 termsCondition: $('#updatetermsCondition').val(),
			 modules:$('#updateModules').val().join(","),
			 id:$('#updatetermsId').val(),
			 isDefault:checked
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#termsandcondition_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#termsandcondition_update_modal').on('hide.bs.modal', function() {
		$('#termsandcondition_update_form').formValidation('resetForm', true);
	});
	
	$("#updatetermsandcondition").click(function() {
		$('#termsandcondition_update_form').data('formValidation').validate();
	});
	
});

function updateTermsandCondition(row,id)
{  		//console.log(row);
	
	$('#termsandcondition_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var rowindex = $(crow).find('td:eq(0)').text();
	
	$('#updatetermsCondition').val(name);
	
	$('#updateModules').selectpicker('val',$(crow).find('td:eq(2)').text().split(','));
	if($(crow).find('td:eq(3)').text()=="Yes"){
		$("#updateisDefault").prop('checked',true)
	} else {
		$("#updateisDefault").prop('checked',false)
	}
	
	$('#updatetermsId').val(id);
	$("#dataIndex").val(rowindex);
	
}