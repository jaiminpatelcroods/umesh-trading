/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#uom_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			Uom:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			UomCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
			noOfDecimalPlaces:{
				validators: {
					notEmpty: {
						message: 'The No. of Decimal is reqired'
					},
					regexp: {
						regexp:  /^[0-4,/d{1}]$/,
						message: 'The No. of Decimal Places can only consist numberical value O tO 4'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $.post("/unitofmeasurement/save", {
			 name: $('#Uom').val(),
			 code:$('#Code').val(),
			 decimal:$('#Nodp').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#uom_new_modal').modal('toggle');
			 
			var t = $('#unitofmeasurement_table').DataTable();
			t.row.add([
				"0",
				data.measurementName,
				data.measurementCode,
				data.noOfDecimalPlaces,
				'<a  data-toggle="modal" data-target="#uom_update_modal" onclick="updateUOM(this,'+data.measurementId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'+'<a href="JavaScript:Void(0)" onclick="deleteUOM(this,'+data.measurementId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></a>'
			]).draw( false );
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				});
			}).draw();
			
		 });
		 
	});
	
	$('#uom_new_modal').on('show.bs.modal', function() {
		$('#uom_new_form').formValidation('resetForm', true);
	});
	
	$("#saveuom").click(function() {
		$('#uom_new_form').data('formValidation').validate();
	});
	
});