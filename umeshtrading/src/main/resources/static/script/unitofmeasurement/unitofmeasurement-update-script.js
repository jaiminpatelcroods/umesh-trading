/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#uom_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updateuom",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			UomUpName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required'
					}
				}
			},
			UomupCode:{
				validators: {
					notEmpty: {
						message: 'The Code is Required'
					}
				}
			},
			NoofDescimalPlaces:{
				validators: {
					notEmpty: {
						message: 'The No. of Decimal is reqired'
					},
					regexp: {
						regexp:  /^[0-4,/d{1}]$/,
						message: 'The No. of Decimal Places can only consist numberical value O tO 4'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $.post("/unitofmeasurement/update", {
			 name: $('#UpUomName').val(),
			 code:$('#UpUomCode').val(),
			 noOfDecimalPlaces:$('#UpNodp').val(),
			 id:$('#UpUomId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#uom_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#uom_update_modal').on('hide.bs.modal', function() {
		$('#uom_update_form').formValidation('resetForm', true);
	});
	
	$("#updateuom").click(function() {
		$('#uom_update_form').data('formValidation').validate();
	});
	
});

function updateUOM(row,id)
{  		//console.log(row);
	
	$('#uom_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var code = $(crow).find('td:eq(2)').text();
	var nodp = $(crow).find('td:eq(3)').text();
	var rowindex = $(crow).find('td:eq(0)').text();

	$('#UpUomName').val(name);
	$('#UpUomCode').val(code);
	$('#UpUomId').val(id);
	$('#UpNodp').val(nodp);
	$("#DataIndex").val(rowindex);
	
}