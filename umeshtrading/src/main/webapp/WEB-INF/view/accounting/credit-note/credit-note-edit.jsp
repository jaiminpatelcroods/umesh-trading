<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
	
	<%@include file="../../header/head.jsp" %>
	
	<title>Edit | ${creditNoteAccountingVo.prefix}${creditNoteAccountingVo.voucherNo} | Credit Note</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${creditNoteAccountingVo.prefix}${creditNoteAccountingVo.voucherNo}</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/account/creditnote/${creditNoteAccountingVo.creditNoteAccountId}" class="m-nav__link">
										<span class="m-nav__link-text">${creditNoteAccountingVo.prefix}${creditNoteAccountingVo.voucherNo}</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/account/creditnote" class="m-nav__link">
										<span class="m-nav__link-text">Credit Note</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="credit_note_update_form" action="/account/creditnote/save" method="post">	
						<input type="hidden" name="creditNoteAccountId" value="${creditNoteAccountingVo.creditNoteAccountId}"/>
						<input type="hidden" name="voucherNo" value="${creditNoteAccountingVo.voucherNo}"/>
						<input type="hidden" name="prefix" value="${creditNoteAccountingVo.prefix}"/>
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-12"></div>
							<div class="col-lg-6 col-md-6 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Credit Note</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="m-widget28 m--padding-left-10">
											<div class="m-widget28__container" >	
												<!-- Start:: Content -->
												<div class="m-widget28__tab tab-content">
													<div class="m-widget28__tab-container tab-pane active">
													    <div class="m-widget28__tab-items">
															<div class="m-widget28__tab-item">
																<span class="m--regular-font-size-">Voucher No:</span>
																<span>${creditNoteAccountingVo.prefix} ${creditNoteAccountingVo.voucherNo}</span>
															</div>
														</div>					      	 		      	
													</div>
												</div>
												<!-- end:: Content --> 	
											</div>				 	 
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Date:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="input-group date" >
													<input type="text" class="form-control m-input todaybtn-datepicker" id="transactionDate"  name="transactionDate" readonly placeholder="Transaction Date" data-date-format="dd/mm/yyyy"
														data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'
														value='<fmt:formatDate pattern="dd/MM/yyyy" value="${creditNoteAccountingVo.transactionDate}"/>'/>
													<div class="input-group-append">
														<span class="input-group-text"><i class="la la-calendar"></i></span>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select From Account:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">																					
												<select class="form-control m-select2 " id="fromAccount" name="fromAccountCustomVo.accountCustomId"  placeholder="From Account" data-allow-clear="true">
													<option value=""></option>
													<c:forEach items="${fromAccountCustomVos}" var="fromAccountCustomVo">
														<option value="${fromAccountCustomVo.accountCustomId}"
															<c:if  test="${fromAccountCustomVo.accountCustomId==creditNoteAccountingVo.fromAccountCustomVo.accountCustomId}">selected="selected"</c:if>>
															${fromAccountCustomVo.accountName}
														</option>
													</c:forEach>
												</select>															
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select To Account:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">																					
												<select class="form-control m-select2 " id="toAccount" name="toAccountCustomVo.accountCustomId"  placeholder="To Account" data-allow-clear="true">
													<option value=""></option>
													<c:forEach items="${fromAccountCustomVos}" var="toAccountCustomVo">
														<option value="${toAccountCustomVo.accountCustomId}"
															<c:if  test="${toAccountCustomVo.accountCustomId==creditNoteAccountingVo.toAccountCustomVo.accountCustomId}">selected="selected"</c:if>>
															${toAccountCustomVo.accountName}
														</option>
													</c:forEach>
												</select>															
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Amount:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="amount" id="amount" placeholder="Amount" value="${creditNoteAccountingVo.amount}">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<textarea  class="form-control m-input" name="description" id="description"  placeholder="Description">${creditNoteAccountingVo.description}</textarea>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m-form__actions--right">
											<button type="submit" class="btn btn-brand" id="updatecreditnote">
												Submit
											</button>
											
											<a href="/account/creditnote/${creditNoteAccountingVo.creditNoteAccountId}" class="btn btn-secondary">
												Cancel
											</a>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/accounting/credit-note/credit-note-update-script.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	
	</script>	
</body>
<!-- end::Body -->
</html>