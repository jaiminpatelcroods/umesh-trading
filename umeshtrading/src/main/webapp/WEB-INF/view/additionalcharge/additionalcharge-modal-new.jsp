
<div class="modal fade" id="additionalcharge_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New Additional Charge</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<form id="additionalcharge_new_form">
				<div class="form-group m-form__group">
					<label for="recipient-name" class="form-control-label">
						Additional Charge
					</label>
					<input type="text" name="additionalCharge" class="form-control"  id="additionalCharge">
				</div>
				<div class="form-group m-form__group">
					<label for="message-text" class="form-control-label">
						Amount
					</label>
					<input type="text" class="form-control m-input" name="defaultAmount" id="defaultAmount" />
				</div>
				
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Tax:</label>
					<div class="col-lg-12 col-md-12 col-sm-12" id="tax_div">
						<select class="form-control m-select2" id="taxVo" name="taxVo.taxId" placeholder="Select Tax">
							<option value="">Select Tax</option>
							<c:forEach var="tax" items="${tax}" varStatus="index">	
								<option value="${tax.taxId}">${tax.taxName}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">HSN / SAC:</label>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<input type="text" class="form-control" name="hsnCode" id="hsnCode"/>
					</div>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary"  id="saveadditionalcharge">Save</button>
			</div>
		</div>
	</div>
</div>