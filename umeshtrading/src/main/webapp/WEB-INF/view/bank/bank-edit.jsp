<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${bankVo.bankName} | Bank</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Edit Bank</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/bank" class="m-nav__link">
										<span class="m-nav__link-text">Bank</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/bank/${bankVo.bankId}" class="m-nav__link">
										<span class="m-nav__link-text">${bankVo.bankName}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="bank_new_form" action="/bank/save" method="post">	
						<input type="hidden" name="bankId" id="bankId"  value="${bankVo.bankId}"/>
						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon">
											<i class="flaticon-cogwheel-2"></i>
										</span>
										<h3 class="m-portlet__head-text m--font-brand">Bank</h3>
									</div>
								</div>
							</div>
							<div class="m-portlet__body" >
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12">
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Bank Name:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="bankName" id="bankName" placeholder="Bank Name" value="${bankVo.bankName}" />
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Branch Name:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="bankBranch" id="bankBranch" placeholder="Branch Name" value="${bankVo.bankBranch}">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Account Holder Name:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="accountHolderName" id="accountHolderName"  placeholder="Account Holder Name" value="${bankVo.accountHolderName}">
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12">
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Account No.:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="bankAcNo" id="bankAcNo" placeholder="Account No." value="${bankVo.bankAcNo}">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">IFSC Code:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="ifscCode" id="ifscCode"  placeholder="Amount" value="${bankVo.ifscCode}">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Swift Code:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="swiftCode" id="swiftCode"  placeholder="Amount" value="${bankVo.swiftCode}">
											</div>
										</div>
									</div>
								</div>								
							</div>
						</div>	
						<!--end::Portlet-->
						
						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon">
											<i class="flaticon-cogwheel-2"></i>
										</span>
										<h3 class="m-portlet__head-text m--font-brand">Address Details</h3>
									</div>
								</div>
							</div>
							<div class="m-portlet__body" >
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12">
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Address Line 1:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="addressLine1" id="addressLine1"  placeholder="Address Line 1" value="${bankVo.addressLine1}">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Address Line 2:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="text"  class="form-control m-input" name="addressLine2" id="addressLine2"  placeholder="Address Line 2" value="${bankVo.addressLine2}">
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12">
										<div class="form-group m-form__group row">
											<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
												<label class="col-form-label">Select Country:</label>
												<select class="form-control m-select2 " id="countriesCode" name="countriesCode" data-default="${bankVo.countriesCode}" onchange="getAllStateAjax('countriesCode','stateCode')" placeholder="Select Country" data-allow-clear="true">
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub ">
												<label class="col-form-label">Select State:</label>																	
												<select class="form-control m-select2" id="stateCode" name="stateCode" data-default="${bankVo.stateCode}" onchange="getAllCityAjax('stateCode','cityCode')" data-allow-clear="false" placeholder="Select State">
													
												</select>																	
											</div>
										</div>
										
										<div class="form-group m-form__group row">
											<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
												<label class="col-form-label">Select City:</label>																	
												<select class="form-control m-select2" id="cityCode" name="cityCode" data-default="${bankVo.cityCode}" placeholder="Select City" data-allow-clear="true">
												</select>
												
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
												<label class="col-form-label">ZIP/Postal code:</label>
												<input type="text" class="form-control m-input" name="pinCode" id="pinCode" placeholder="ZIP/Postal code" value="${bankVo.pinCode}">
											</div>
											
										</div>
									</div>
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions m-form__actions--solid m-form__actions--right">
									<button type="submit" class="btn btn-brand" id="savebank">
										Submit
									</button>
									
									<a href="/bank/${bankVo.bankId}" class="btn btn-secondary">
										Cancel
									</a>
								</div>
							</div>
						</div>	
						<!--end::Portlet-->
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<%@include file="../global/location-ajax.jsp" %>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/bank/bank-new-script.js" type="text/javascript"></script>
	
</body>
<!-- end::Body -->
</html>