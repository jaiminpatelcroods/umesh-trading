<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant"%>

<html>
<head>
	
	<%@include file="../../header/head.jsp" %>
	
	<title>New Bank Transaction</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New Bank Transaction</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/bank/transaction" class="m-nav__link">
										<span class="m-nav__link-text">Bank Transaction</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-12"></div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
								<div class="m-portlet__head">
									<div class="m-portlet__head-tools">
										<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
											<li class="nav-item m-tabs__item">
												<a class="nav-link m-tabs__link active" data-toggle="tab" href="#withdraw_tab" role="tab">Withdraw Cash</a>
											</li>
											<li class="nav-item m-tabs__item">
												<a class="nav-link m-tabs__link" data-toggle="tab" href="#deposit_tab" role="tab">Deposit Cash</a>
											</li>
											<li class="nav-item m-tabs__item">
												<a class="nav-link m-tabs__link" data-toggle="tab" href="#transfer_tab" role="tab">Transfer</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="tab-content">
									<div class="tab-pane active" id="withdraw_tab" role="tabpanel">
										<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="withdraw_form" action="/bank/transaction/save" method="post">
											<input type="hidden" name="transactionType" value="${Constant.BANK_WITHDRAW}"/>
											<div class="m-portlet__body p-4">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" id="withdrawTransactionDate"  name="transactionDate" readonly placeholder="Transaction Date" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-calendar"></i></span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select From Account:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">																					
														<select class="form-control m-select2 " id="fromBank" name="fromBankVo.bankId"  placeholder="From Account" data-allow-clear="true">
															<option value=""></option>
															<c:forEach items="${bankVos}" var="bankVo">
																<option value="${bankVo.bankId}">${bankVo.bankName}</option>
															</c:forEach>
														</select>															
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Amount:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text"  class="form-control m-input" name="debitAmount" placeholder="Amount" value="0.0">
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea  class="form-control m-input" name="description" id="description"  placeholder="Description" value=""></textarea>
													</div>
												</div>
											</div>
											<div class="m-portlet__foot m-portlet__foot--fit">
												<div class="m-form__actions m-form__actions--solid m-form__actions--right">
													<button type="submit" class="btn btn-brand" id="savewithdraw">
														Submit
													</button>
													
													<a href="/bank/transaction" class="btn btn-secondary">
														Cancel
													</a>
												</div>
											</div>
										</form>
									</div>
									<div class="tab-pane" id="deposit_tab" role="tabpanel">
										<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="deposit_form" action="/bank/transaction/save" method="post">
											<input type="hidden" name="transactionType" value="${Constant.BANK_DEPOSIT}"/>
											<div class="m-portlet__body p-4" >
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" id="depositTransactionDate"  name="transactionDate" readonly placeholder="Transaction Date" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-calendar"></i></span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select To Account:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">																					
														<select class="form-control m-select2 " id="toBank" name="toBankVo.bankId"  placeholder="To Account" data-allow-clear="true">
															<option value=""></option>
															<c:forEach items="${bankVos}" var="bankVo">
																<option value="${bankVo.bankId}">${bankVo.bankName}</option>
															</c:forEach>
														</select>															
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Amount:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text"  class="form-control m-input" name="creditAmount" placeholder="Amount" value="0.0">
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea  class="form-control m-input" name="description" id="description"  placeholder="Description" value=""></textarea>
													</div>
												</div>
											</div>
											<div class="m-portlet__foot m-portlet__foot--fit">
												<div class="m-form__actions m-form__actions--solid m-form__actions--right">
													<button type="submit" class="btn btn-brand" id="savedeposit">
														Submit
													</button>
													
													<a href="/bank/transaction" class="btn btn-secondary">
														Cancel
													</a>
												</div>
											</div>
										</form>
									</div>
									<div class="tab-pane" id="transfer_tab" role="tabpanel">
										<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="transfer_form" action="/bank/transaction/save" method="post">
											<input type="hidden" name="transactionType" value="${Constant.BANK_TRANSFER}"/>
											<div class="m-portlet__body p-4" >
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" id="transferTransactionDate"  name="transactionDate" readonly placeholder="Transaction Date" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-calendar"></i></span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select From Account:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">																					
														<select class="form-control m-select2 " id="fromBank1" name="fromBankVo.bankId"  placeholder="From Account" data-allow-clear="true">
															<option value=""></option>
															<c:forEach items="${bankVos}" var="bankVo">
																<option value="${bankVo.bankId}">${bankVo.bankName}</option>
															</c:forEach>
														</select>															
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select To Account:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">																					
														<select class="form-control m-select2 " id="toBank1" name="toBankVo.bankId"  placeholder="To Account" data-allow-clear="true">
															<option value=""></option>
															<c:forEach items="${bankVos}" var="bankVo">
																<option value="${bankVo.bankId}">${bankVo.bankName}</option>
															</c:forEach>
														</select>															
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Amount:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text"  class="form-control m-input" name="creditAmount" placeholder="Amount" value="0.0">
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea  class="form-control m-input" name="description" id="description"  placeholder="Description" value=""></textarea>
													</div>
												</div>
											
											</div>
											<div class="m-portlet__foot m-portlet__foot--fit">
												<div class="m-form__actions m-form__actions--solid m-form__actions--right">
													<button type="submit" class="btn btn-brand" id="savetransfer">
														Submit
													</button>
													
													<a href="/bank/transaction" class="btn btn-secondary">
														Cancel
													</a>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../../footer/footer.jsp" %>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/bank/transaction/bank-transaction-new-script.js" type="text/javascript"></script>
	
</body>
<!-- end::Body -->
</html>