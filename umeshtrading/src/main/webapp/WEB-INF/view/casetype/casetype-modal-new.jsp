<div class="modal fade" id="casetype_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					New Case Type
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="casetype_new_form">
					<div class="form-group m-form__group">
						<label for="recipient-name" class="form-control-label">
							Case Name:
						</label>
						<input type="text" name="caseName" class="form-control"  id="caseName">
					</div>
				 	<div class="form-group m-form__group">
						<label for="message-text" class="form-control-label">
							Description:
						</label>
						<textarea class="form-control m-input" name="caseDescription" id="caseDescription"></textarea>
					</div> 
				</form>
			</div>
			<div class="modal-footer">
				<button type="button"  class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="savecasetype"  class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>	
						