<div class="modal fade" id="casetype_update_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Edit Case Type
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="casetype_update_form">
					<div class="form-group">
						<label class="form-control-label">Case name:</label>
						<input type="text" name="caseName"class="form-control" id="updateCaseName">
						<input type="hidden" class="form-control" id="caseTypeId">
					</div>
					<div class="form-group">
						<label class="form-control-label">Category Description:</label>
						<textarea class="form-control" name="caseDescription" id="updateCaseDescription"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<button type="button"id="updatecasetype" class="btn btn-primary">
					Save
				</button>
			</div>
		</div>
	</div>
</div>