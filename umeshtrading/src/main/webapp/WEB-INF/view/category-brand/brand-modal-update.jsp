<div class="modal fade" id="brand_update_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Edit Brand
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="brand_update_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">
							Brand name:
						</label>
						<input type="text" name="brupname" class="form-control" id="upbrandname">
						<input type="hidden" class="form-control" id="upbrandid">
					</div>
					<div class="form-group">
						<label for="message-text"class="form-control-label">
							Brand Description:
						</label>
						<textarea class="form-control"   name="brupdesc"  id="upbranddesc"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<button type="button"id="updatebrand" class="btn btn-primary">
					Save
				</button>
			</div>
		</div>
	</div>
</div>