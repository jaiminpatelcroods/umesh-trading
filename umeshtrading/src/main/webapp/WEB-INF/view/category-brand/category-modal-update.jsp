<div class="modal fade" id="category_update_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Edit Category
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="category_update_form">
					<div class="form-group">
						<label class="form-control-label">Category name:</label>
						<input type="text" name="catupname"class="form-control" id="upcatname">
						<input type="hidden" class="form-control" id="upcatid">
					</div>
					<div class="form-group">
						<label class="form-control-label">Category Description:</label>
						<textarea class="form-control" name="catupdesc" id="upcatdesc"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<button type="button"id="updatecategory" class="btn btn-primary">
					Save
				</button>
			</div>
		</div>
	</div>
</div>