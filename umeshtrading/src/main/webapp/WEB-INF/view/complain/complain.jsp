<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Complain</title>
	
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Complain</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="#" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="<%=request.getContextPath() %>/complain/new" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>Complain</span></span>
											</a>
										</div>			
									</div>
								</div>
								<div class="m-portlet__body" >
									
									<div  class="m_datatable"  >
										<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
											<thead>
						  						<tr>
				  									<th>#</th>
				  									<th>Complain No</th>
				  									<th>Complain Date</th>
				  									<th>Customer</th>			  									
				  									<th>Case Channel</th>
				  									<th>Case Type</th>
				  									<!-- <th>Purchase Tax(%)</th> -->
				  									<th>Actions</th>
							  					</tr>
											</thead>			
										</table>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
							
						</div>
				
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		var DatatablesDataSourceHtml = {
		    init: function() {
		        $("#m_table_1").DataTable({
		            responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax: {
						url: "<%=request.getContextPath()%>/complain/datatable",
						type: "POST",
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					  columns: [{
			                data: "complainId"
			            }, {
			                data: "complainId"
			            }, {
			                data: "complainDate"
			            }, {
			                data: "contactVo.companyName"
			            }, {
			                data: "caseChannel"
			            },{
			                data: "caseTypeVo.caseName"
			            },{
			                data: "complainId",
			            }],
					
					columnDefs: [{
						  targets: 6,
			                title: "Actions",
			                orderable: !1,
			               render: function(a, e, t, n) {
			            	   return '\n  <span class="dropdown">\n<a href="'+e+'" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" href="/complain/'+t.complainId+'/edit"><i class="la la-edit"></i> Edit Details</a>\n</div>\n</span>\n '
			                }
			            
			            },{
							targets: 0,
							orderable: !1,
							render: function(a, e, t, n) {
								
								return (n.row+1);
							}
						},{
			               targets:1,
			               orderable: !1,
			               render: function(a, e, t, n) {
			                  
			            	   return '\n  <a href="/complain/'+t.complainId+'" class="m-link m--font-bolder" aria-expanded="true">\n '+t.prefix+t.complainNo+'</a>\n '
			               }
					}],
		            
		            //lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
		            fixedHeader: {
		                header: false,
		                footer: false
		            },
		        })
		    }
		};
		
		jQuery(document).ready(function() {
			DatatablesDataSourceHtml.init();
		});
	</script>
	
</body>
<!-- end::Body -->
</html>