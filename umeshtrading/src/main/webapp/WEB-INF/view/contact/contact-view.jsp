<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${contactVo.companyName} | ${displayContactType}</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${contactVo.companyName}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/contact/${type}" class="m-nav__link">
										<span class="m-nav__link-text">${displayContactType}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<c:if test="${type == Constant.CONTACT_CUSTOMER}">
												<span class="m--font-boldest m--font-primary" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-html="true" data-original-title="<b>Customer Type</b>">
													${contactVo.contactType}
												</span>
											</c:if>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
												<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl  m-dropdown__toggle">
													<!-- <i class="la la-ellipsis-v"></i> -->
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>
						                        <div class="m-dropdown__wrapper">
						                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						                            <div class="m-dropdown__inner">
					                                    <div class="m-dropdown__body">              
					                                        <div class="m-dropdown__content">
					                                            <ul class="m-nav">
					                                                <%-- <li class="m-nav__item">
					                                                    <a href="<%=request.getContextPath() %>/contact/${type}/pdf/${contactVo.contactId}" target="_blank" class="m-nav__link">
					                                                        <i class="m-nav__link-icon fa fa-file-pdf"></i>
					                                                        <span class="m-nav__link-text">PDF</span>
					                                                    </a>
					                                                </li>
					                                                <li class="m-nav__separator m-nav__separator--fit">
					                                                </li> --%>
					                                                <li class="m-nav__item">
					                                                    <a href="/contact/${type}/${contactVo.contactId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																			<span><i class="flaticon-edit"></i><span>Edit</span></span>
																		</a>
					                                                    <button id="contact_delete" data-url="/contact/${type}/${contactVo.contactId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																			<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																		</button>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </div>
						                            </div>
						                        </div>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body m--padding-bottom-0 m--padding-right-10 m--padding-left-10">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<h5 class="display-5 text-center">${contactVo.companyName}</h5>
											<p class="text-center mb-0">
												<span class="m--font-info">
													<i class="fa fa-mobile-alt align-middle"></i> <c:if test="${empty contactVo.companyMobileno}">Mobile number is not provided</c:if>${contactVo.companyMobileno}
												</span>
											</p>
											<p class="text-center mb-0" style=" word-break: break-all">
												<span class="m--font-bolder ">
													<i class="la la-phone align-middle"></i>&nbsp; <c:if test="${empty contactVo.companyTelephone}">Telephone no is not provided</c:if>${contactVo.companyTelephone}
												</span>
											</p>
											<p class="text-center " style=" word-break: break-all">
												<span class="m--font-bolder ">
													<i class="flaticon-mail-1 align-middle"></i>&nbsp; <c:if test="${empty contactVo.companyEmail}">Email is not provided</c:if>${contactVo.companyEmail}
												</span>
											</p>
											<h3 class="text-center mb-0">
												<small class="text-muted"><c:if test="${empty contactVo.ownerName}">Owner Name is not provided</c:if>${contactVo.ownerName}</small>
											</h3>
											<p class="text-center">
												<span class="m--font-brand">
													<i class="fa fa-mobile-alt align-middle"></i> <c:if test="${empty contactVo.ownerMobileno}">Mobile number is not provided</c:if>${contactVo.ownerMobileno}
												</span>
											</p>
											<!-- <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div> -->
											
											<div class="m-widget4">
												<%-- <div class="m-widget4__item">
													<!-- <div class="m-widget4__img m-widget4__img--logo">
														<img src="../../assets/app/media/img/client-logos/logo5.png" alt="">
													</div> -->
													<div class="m-widget4__info">
														<span class="m-widget4__title">
															Due Amount
														</span>
														<br>
														<span class="m-widget4__sub">
															Make Metronic Great Again
														</span>
													</div>
													<span class="m-widget4__ext">
														<span class="m-widget4__number m--font-danger">
															${totalAndpaidAmount.get(0).get('totalamount')-totalAndpaidAmount.get(0).get('paidAmount')}
														</span>
													</span>
												</div> --%>
												<%-- <div class="m-widget4__item">
													<!-- <div class="m-widget4__img m-widget4__img--logo">
														<img src="../../assets/app/media/img/client-logos/logo5.png" alt="">
													</div> -->
													<div class="m-widget4__info">
														<span class="m-widget4__title">
															Paid Amount
														</span>
														<br>
														<span class="m-widget4__sub">
															Total Paid Amount By Customer
														</span>
													</div>
													<span class="m-widget4__ext">
														<span class="m-widget4__number m--font-success">
															${totalAndpaidAmount.get(0).get('paidAmount')}
															<c:if test="${ empty totalAndpaidAmount.get(0).get('paidAmount')}">0</c:if>
														</span>
													</span>
												</div> --%>
												<div class="m-widget4__item">
													<!-- <div class="m-widget4__img m-widget4__img--logo">
														<img src="../../assets/app/media/img/client-logos/logo5.png" alt="">
													</div> -->
													<%-- <div class="m-widget4__info">
														<span class="m-widget4__title">
															Total Amount
														</span>
														<br>
														<span class="m-widget4__sub">
														</span>
													</div>
													<span class="m-widget4__ext">
														<span class="m-widget4__number m--font-info">
															${totalAmount}
															<c:if test="${empty totalAmount}">0</c:if>
														</span>
													</span> --%>
												</div>
											</div>
											
										</div>
									</div>
									<div class="row m--margin-top-10">
										<div class="col-lg-12 col-md-12 col-sm-12" style="padding-right: 0.3rem !important;padding-left: 0.3rem !important">
											<!--begin::Section-->                                            
							                <div class="m-accordion m-accordion--bordered" id="m_accordion_2" role="tablist">   
							
							                    <!--begin::Item-->              
							                    <div class="m-accordion__item">
							                        <div class="m-accordion__item-head"  role="tab" id="gst_accordion_item_head" data-toggle="collapse" href="#gst_accordion_item_body" aria-expanded="false">
							                            <!-- <span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span> -->
							                            <span class="m-accordion__item-title m--font-bolder">GST Details</span>  
							                            <span class="m-accordion__item-mode"></span>
							                        </div>
							
							                        <div class="m-accordion__item-body m-accordion__item-body-background collapse show" id="gst_accordion_item_body" role="tabpanel" aria-labelledby="gst_accordion_item_head" data-parent="#m_accordion_2"> 
							                            <div class="m-accordion__item-content">
							                                <div class="m-widget28">
																<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">GST Type:</span>
																					<span><c:if test="${empty contactVo.gstType}">N/A</c:if>${contactVo.gstType}</span>
																				</div>
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">GSTIN</span>
																					<span><c:if test="${empty contactVo.gstin}">N/A</c:if>${contactVo.gstin}</span>
																				</div>
																				
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
							                            </div>
							                        </div>
							                    </div>
							                    <!--end::Item--> 
							                    
							                    <!--begin::Item--> 
							                    <div class="m-accordion__item">
							                        <div class="m-accordion__item-head collapsed" role="tab" id="other_accordion_item_head" data-toggle="collapse" href="#other_accordion_item_body" aria-expanded="    false">
							                        	<span class="m-accordion__item-title m--font-bolder">Other Details</span>
							                              
							                            <span class="m-accordion__item-mode"></span>     
							                        </div>
							
							                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="other_accordion_item_body" role="tabpanel" aria-labelledby="other_accordion_item_head" data-parent="#m_accordion_2"> 
							                            <div class="m-accordion__item-content">
							                            	<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Pan No.:</span>
																					<span><c:if test="${empty contactVo.panNo}">N/A</c:if>${contactVo.panNo}</span>
																				</div>
																				<c:if test="${type != Constant.CONTACT_TRANSPORT}">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Date Of Birth:</span>
																						<span><c:if test="${empty contactVo.dateOfBirth}">N/A</c:if>${contactVo.dateOfBirth}</span>
																					</div>
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Anniversary Date:</span>
																						<span><c:if test="${empty contactVo.anniversaryDate}">N/A</c:if>${contactVo.anniversaryDate}</span>
																					</div>
																				</c:if>
																				<%-- <c:if test="${type == Constant.CONTACT_CUSTOMER}">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Total No. of Shops:</span>
																						<span><c:if test="${empty contactVo.noOfShops}">N/A</c:if>${contactVo.noOfShops}</span>
																					</div>
																				</c:if> --%>
																				<c:if test="${type == Constant.CONTACT_TRANSPORT}">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Working Time:</span>
																						<span><c:if test="${empty contactVo.workingTime}">N/A</c:if>${contactVo.workingTime}</span>
																					</div>
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Available Location:</span>
																						<span><c:if test="${empty contactVo.serviceAvailable}">N/A</c:if>${contactVo.serviceAvailable}</span>
																					</div>
																				</c:if>
																				<c:if test="${type == Constant.CONTACT_AGENT}">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Agent Commission:</span>
																						<span>${contactVo.agentCommission}</span>
																					</div>
																				</c:if>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
							                            </div>
							                        </div>                       
							                    </div>
							                    <!--end::Item-->                     
							
							                </div>
							                <!--end::Section-->
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
								<div class="m-portlet__head">
									<div class="m-portlet__head-tools">
										<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
											<li class="nav-item m-tabs__item">
												<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
													Address Details
												</a>
											</li>
											<c:if test="${contactVo.type == Constant.CONTACT_CUSTOMER}">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_invoice" role="tab">
														Sales
													</a>
												</li>
												<!-- <li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_receipt" role="tab">
														Receipt
													</a>
												</li> -->
											</c:if>
											<c:if test="${contactVo.type == Constant.CONTACT_SUPPLIER}">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_material_inward" role="tab">
														Material Inward
													</a>
												</li>
												<!-- <li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_payment" role="tab">
														Payment
													</a>
												</li> -->
											</c:if>
										</ul>
									</div>
									
									<%-- <div class="m-portlet__head-tools">
										<span class="dropdown m--margin-top-5">
											<a href="/contact" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
												title="New" data-toggle="dropdown" aria-expanded="true" >
												<i class="fa fa-plus m--regular-font-size-lg4"></i></a>
												<div class="dropdown-menu dropdown-menu-right">
												<c:if test="${contactVo.type == Constant.CONTACT_CUSTOMER}">
													<form action="<%=request.getContextPath()%>/sales/${Constant.SALES_INVOICE}/new">
													<form action="#">
														<input type="hidden" name="contactId" value="${contactVo.contactId}"/>
														<button class="dropdown-item" type="submit" style="color: #575962;">Material Outward</button>
													</form>
													<form action="<%=request.getContextPath()%>/receipt/new">
														<input type="hidden" name="contactId" value="${contactVo.contactId}"/>
														<button class="dropdown-item" type="submit" style="color: #575962;">Receive Payment</button>
													</form>
												</c:if>
												
												<c:if test="${contactVo.type == Constant.CONTACT_SUPPLIER}">
													<form action="<%=request.getContextPath()%>/purchase/${Constant.PURCHASE_BILL}/new">
													<form action="#">
														<input type="hidden" name="contactId" value="${contactVo.contactId}"/>
														<button class="dropdown-item m--link" type="submit" style="color: #575962;">Material Inward</button>
													</form>
													<form action="<%=request.getContextPath()%>/payment/new">
														<input type="hidden" name="contactId" value="${contactVo.contactId}"/>
														<button class="dropdown-item" type="submit" style="color: #575962;">Make Payment</button>
													</form>
												</c:if>
											</div>
										</span>
									</div> --%>
									
									
								</div>
								<div class="m-portlet__body">									
									<div class="tab-content">
										<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
											<c:forEach items="${contactVo.contactAddressVos}" var="contactAddress" varStatus="status">
												<!--begin::Section-->
												<div class="m-section">
													<h3 class="m-section__heading">
														<c:if test="${contactAddress.isDefault==1}">Default Address</c:if>
														<c:if test="${contactAddress.isDefault==0}">
															Address Details
															<%-- <label class="m-checkbox m-checkbox--solid m-checkbox--brand float-right">
																<input type="checkbox" onchange="setAsDefaultAddress(${contactAddress.contactAddressId})"  id="setAsDefault" name="setAsDefault" value="" > Set As Default
																<span></span>
															</label> --%>
														</c:if>
													</h3>
													<div class="m-section__content">
														<div class="row">
															<div class="col-lg-6 col-md-6 col-sm-12">
																<table class="table m-table">
																  	<tbody>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Company Name:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${contactAddress.companyName}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Address Line 1:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${contactAddress.addressLine1}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Address Line 2:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${contactAddress.addressLine2}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">ZIP/Postal code:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${contactAddress.pinCode}</td>
																    	</tr>
																  	</tbody>
																</table>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
																<table class="table m-table">
																  	<tbody>
																    	<tr class="row">
																	      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Country:</th>
																		    <td class="col-lg-8 col-md-8 col-sm-12">${contactAddress.countriesName}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">State:</th>
																      		<td class="col-lg-8 col-md-8 col-sm-12">${contactAddress.stateName}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">City:</th>
																      		<td class="col-lg-8 col-md-8 col-sm-12">${contactAddress.cityName}</td>
																    	</tr>
																  	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</c:forEach>
											<!--end::Section-->
										</div>
										
										<c:if test="${contactVo.type == Constant.CONTACT_CUSTOMER}">
											<div class="tab-pane" id="m_tabs_invoice" role="tabpanel">
												<table class="table table-striped-table-bordered table-hover table-checkable" id="invoice_table" style="overflow-x: scroll;" >
													<thead>
														<tr>
															<th>
																#
															</th>
															<th>
																Invoice No 
															</th>
															<th>
																Date
															</th>
															<th>
																Total Amount
															</th>
															<th>
																Action
															</th>
														</tr>
													</thead>
												
													<tbody>
													</tbody>
												</table>
											</div>
										</c:if>
										
										<c:if test="${contactVo.type == Constant.CONTACT_SUPPLIER}">
											<div class="tab-pane" id="m_tabs_material_inward" role="tabpanel">
												<table class="table table-striped-table-bordered table-hover table-checkable" id="material_in_table" style="overflow-x: scroll;" >
													<thead>
														<tr>
															<th>
																#
															</th>
															<th>
																Material In No
															</th>
															<th>
																Date
															</th>
															<th>
																Total Amount
															</th>
															<th>
																Action
															</th>
														</tr>
													</thead>
												
													<tbody>
													</tbody>
												</table>
											</div>
										</c:if>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-accordion m-accordion--bordered" id="m_accordion_1" role="tablist">   
					                    <!--begin::Item-->              
					                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
					                        <div class="m-accordion__item-head collapsed"  role="tab" id="contact_accordion_item_head" data-toggle="collapse" href="#contact_accordion_item_body" aria-expanded="false">
					                            <span class="m-accordion__item-title m--font-bolder">Contact Details</span>  
					                            <span class="m-accordion__item-mode"></span>
					                        </div>
					
					                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="contact_accordion_item_body" class="" role="tabpanel" aria-labelledby="contact_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
					                            <div class="m-accordion__item-content">
					                            	<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Concern Person:</span>
																					<span><c:if test="${empty contactVo.concernPersonName}">N/A</c:if>${contactVo.concernPersonName}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
														<div class="col-lg-4 col-md-4 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Concern Person Mobile No.:</span>
																					<span><c:if test="${empty contactVo.concernPersonMobileno}">N/A</c:if>${contactVo.concernPersonMobileno}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
													</div>
													<c:forEach items="${contactVo.contactOtherVos}" var="contactOtherVo" varStatus="status">
														<div class="row">
															<div class="col-lg-4 col-md-4 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Contact Name:</span>
																						<span><c:if test="${empty contactOtherVo.name}">N/A</c:if>${contactOtherVo.name}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Contact Mobile No.:</span>
																						<span><c:if test="${empty contactOtherVo.mobileno}">N/A</c:if>${contactOtherVo.mobileno}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Contact Email:</span>
																						<span><c:if test="${empty contactOtherVo.email}">N/A</c:if>${contactOtherVo.email}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
														</div>
													</c:forEach>
					                            </div>
					                        </div>
					                    </div>
					                    <!--end::Item-->
					                    
					                    <c:if test="${type == Constant.CONTACT_CUSTOMER || type == Constant.CONTACT_SUPPLIER}">   
						                    <%-- <!--begin::Item-->              
						                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
						                        <div class="m-accordion__item-head collapsed"  role="tab" id="bank__payment__accordion_item_head" data-toggle="collapse" href="#bank__payment_accordion_item_body" aria-expanded="false">
						                            <span class="m-accordion__item-title m--font-bolder">Bank & Payment Details</span>  
						                            <span class="m-accordion__item-mode"></span>
						                        </div>
						
						                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="bank__payment_accordion_item_body" role="tabpanel" aria-labelledby="bank__payment_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
						                            <div class="m-accordion__item-content">
						                            	<div class="row">
															<div class="col-lg-6 col-md-6 col-sm-12">
																<div class="m-section">
																	<h3 class="m-section__heading">
																		Bank Details
																	</h3>
																	<div class="m-section__content p-2">
																		<table class="table m-table">
																		  	<tbody>
																		    	<tr class="row">
																		      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Bank Name:</th>
																			      	<td class="col-lg-8 col-md-8 col-sm-12">${contactVo.bankName}</td>
																		    	</tr>
																		    	<tr class="row">
																		      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Branch Name:</th>
																			      	<td class="col-lg-8 col-md-8 col-sm-12">${contactVo.bankBranch}</td>
																		    	</tr>
																		    	<tr class="row">
																		      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Account No.:</th>
																			      	<td class="col-lg-8 col-md-8 col-sm-12">${contactVo.bankAccountNo}</td>
																		    	</tr>
																		    	<tr class="row">
																		      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">IFSC Code:</th>
																			      	<td class="col-lg-8 col-md-8 col-sm-12">${contactVo.bankIfsc}</td>
																		    	</tr>
																		  	</tbody>
																		</table>
																	</div>
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12">
																<div class="m-section">
																	<h3 class="m-section__heading">
																		Payment Details
																	</h3>
																	<div class="m-section__content p-2">
																		<table class="table m-table">
																		  	<tbody>
																		    	<tr class="row">
																		      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Payment Type:</th>
																			      	<td class="col-lg-8 col-md-8 col-sm-12">${contactVo.paymentType}</td>
																		    	</tr>
																		    	<tr class="row">
																		      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">No. of Credit Days:</th>
																			      	<td class="col-lg-8 col-md-8 col-sm-12">${contactVo.creditDays}</td>
																		    	</tr>
																		    	<tr class="row">
																		      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Max. Credit Limit:</th>
																			      	<td class="col-lg-8 col-md-8 col-sm-12">${contactVo.maximumCreditLimit}</td>
																		    	</tr>
																		  	</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
						                            </div>
						                        </div>
						                    </div>
						                    <!--end::Item--> --%>
						                    <%-- <!--begin::Item-->              
						                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
						                        <div class="m-accordion__item-head collapsed"  role="tab" id="reference_accordion_item_head" data-toggle="collapse" href="#reference_accordion_item_body" aria-expanded="false">
						                            <span class="m-accordion__item-title m--font-bolder">Reference Details</span>  
						                            <span class="m-accordion__item-mode"></span>
						                        </div>
						
						                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="reference_accordion_item_body" class="" role="tabpanel" aria-labelledby="reference_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
						                            <div class="m-accordion__item-content">
						                            	<div class="row">
															<div class="col-lg-6 col-md-6 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Agent:</span>
																						<span>
																							<c:if test="${contactVo.agentId ==0}">N/A</c:if>
																							<c:if test="${contactVo.agentId != 0}">
																								<c:forEach items="${contactAgentList}" var="contactAgentList">
																				      				<c:if test="${contactAgentList.contactId eq contactVo.agentId}">${contactAgentList.companyName}</c:if>
																					      		</c:forEach>
																							</c:if>
																						</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Agent Commission:</span>
																						<span>${contactVo.agentCommission}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
														</div>
						                            </div>
						                        </div>
						                    </div>
						                    <!--end::Item--> --%>
						                    <%-- <c:if test="${type == Constant.CONTACT_CUSTOMER}">
							                    <!--begin::Item-->
							                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
							                        <div class="m-accordion__item-head collapsed"  role="tab" id="interest_accordion_item_head" data-toggle="collapse" href="#interest_accordion_item_body" aria-expanded="false">
							                            <span class="m-accordion__item-title m--font-bolder">Interest Details</span>  
							                            <span class="m-accordion__item-mode"></span>
							                        </div>
							
							                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="interest_accordion_item_body" class="" role="tabpanel" aria-labelledby="interest_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
							                            <div class="m-accordion__item-content">
							                                <div class="row">
																<div class="col-lg-6 col-md-6 col-sm-12">
																	<div class="m-widget28">
																		<div class="m-widget28__container" >	
																			<!-- Start:: Content -->
																			<div class="m-widget28__tab tab-content">
																				<div class="m-widget28__tab-container tab-pane active">
																				    <div class="m-widget28__tab-items">
																						<div class="m-widget28__tab-item">
																							<span class="m--regular-font-size-">Monthly Requirement:</span>
																							<span>${contactVo.interestedProductRequirements}</span>
																						</div>
																					</div>					      	 		      	
																				</div>
																			</div>
																			<!-- end:: Content --> 	
																		</div>				 	 
																	</div>
																</div>
																<div class="col-lg-6 col-md-6 col-sm-12">
																	<div class="m-widget28">
																		<div class="m-widget28__container" >	
																			<!-- Start:: Content -->
																			<div class="m-widget28__tab tab-content">
																				<div class="m-widget28__tab-container tab-pane active">
																				    <div class="m-widget28__tab-items">
																						<div class="m-widget28__tab-item">
																							<span class="m--regular-font-size-">Monthly Requirement Value:</span>
																							<span><c:if test="${empty contactVo.interestedProductValue}">N/A</c:if>${contactVo.interestedProductValue}</span>
																						</div>
																					</div>					      	 		      	
																				</div>
																			</div>
																			<!-- end:: Content --> 	
																		</div>				 	 
																	</div>
																</div>
															</div>
															<c:if test="${not empty contactVo.interestedProductIds}">
																<div class="m-divider mb-4 mt-4"><span></span></div>
																<div class="row">
																	<div class="col-lg-8 col-md-8 col-sm-12">
																		<c:set var="interestedProductIdList" scope="page" value="" />
																		<table class="table table-sm m-table table-striped m--margin-left-15" id="interested_product_table">
																		  	<thead>
																		  		<tr class="row">
																			      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
																		    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
																		    			<span>Intrested Products</span>
																		    			<span class="float-right">
																		    				<!-- <a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																								data-target="#terms_and_condition" id="terms_condition_button" title="Edit"> <i class="fa fa-edit"></i></a> -->
																						</span>
																		    		</th>
																		    	</tr>
																		  	</thead>
																		  	<tbody>
																				<c:set var="interestedProductIds" scope="page" value=""/>
																				<c:forEach items="${fn:split(contactVo.interestedProductIds, ',')}" var="interestedProductId" varStatus="status">
																					<tr class="row" data-terms-item="">
																				      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index="">
																				      		${status.index+1}
																				      	</td>
																				      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name="">
																				      		<c:forEach items="${productVariantList}" var="productVariant">
																				      			<c:if test="${productVariant.productVariantId eq interestedProductId}">
																				      				${productVariant.productVo.name}&nbsp;${productVariant.variantName}
																				      			</c:if>
																				      		</c:forEach>
																			      		</td>
																			    	</tr>
																				</c:forEach>
																		  	</tbody>
																		</table>
																	</div>
																</div>
															</c:if>
							                            </div>
							                        </div>
							                    </div>
							                    <!--end::Item-->
							                </c:if> --%>
							             </c:if>
					                 </div>
								</div>
							</div>
							<!--end::Portlet-->
						</div>
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".delete-btn").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
			
			
			$(".m-tabs__link").click(function(){
				
				if($(this).attr('href') == "#m_tabs_invoice") {
					setInvoiceDatatable();
				} 
				if($(this).attr('href') == "#m_tabs_material_inward") {
					setMaterialInDatatable();
				}
				
			});
			
		});
		
		function setAsDefaultAddress(id){
			
			$.post('/contact/${type}/${contactVo.contactId}/address/changedefault/'+id,		   
  				function(data, status){
				location.reload();
			}); 
		}
		
		function setInvoiceDatatable() {
			$("#invoice_table").dataTable().fnDestroy();
			DatatablesDataSourceAjaxServer.init();
		}
		
		function setMaterialInDatatable() {
			$("#material_in_table").dataTable().fnDestroy();
			MaterialInDatatablesDataSourceAjaxServer.init();
		}
		

		var DatatablesDataSourceAjaxServer =  {
			init: function() {
				var a;
				
				a = $("#invoice_table").DataTable({
					
					responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					
					ajax: {
						url: "<%=request.getContextPath()%>/material/sales/datatable",
						type: "POST",
						"data": function ( d ) {
							return $.extend( {}, d, {
								"contactId": ${contactVo.contactId},
								"fromDate": '<%=session.getAttribute("firstDateFinancialYear")%>',
								"toDate": '<%=session.getAttribute("lastDateFinancialYear")%>'
							});
						}
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					columns: [{
							data: "materialId"
						},{
							data: "materialNo"
						},{
							data: "materialDate"
						},{
							data: "total"
						},{
							data: "materialId"
					}],
					columnDefs: [{
						targets: 0,
						title: "#",
						orderable: !1,
						render: function(a, e, t, n) {
							return (n.row+1);
						}
					},{
						targets: 1,
						orderable: 1,
						render: function(a, e, t, n) {
							return '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'" class="m-link m--font-bolder">'+t.prefix+t.materialNo+'</a>';
						}
					},{
						targets: 2,
						render: function(a, e, t, n) {
							return moment(a).format('DD/MM/YYYY');
						}
					},{
						targets: 4,
						orderable: !1,
						render: function(a, e, t, n) {
							var action = "";
							action += '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
							action += '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill material_delete" title="Delete"> <i class="fa fa-trash"></i></a>'
							//action += '<span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="fa fa-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" target="_blank" href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/pdf"><i class="fa fa-file-pdf"></i> PDF</a>\n</div>\n</span>\n '
							action += '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/pos/pdf" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="POS"> <i class="fa fa-print"></i></a>'
							action += '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/a5/pdf" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" title="A5"> <i class="fa fa-file-pdf"></i></a>'
							return action;
						}
						}],
						
						 fixedHeader: {
				                header: false,
				                footer: false
				            },
					})
				}
		};
		
		
		var MaterialInDatatablesDataSourceAjaxServer =  {
				init: function() {
					var a;
					
					a = $("#material_in_table").DataTable({
						
						responsive: !0,
			            pageLength: 10,
			            searchDelay: 500,
						processing: !0,
						serverSide: true,
						
						ajax: {
							url: "<%=request.getContextPath()%>/material/materialin/datatable",
							type: "POST",
							"data": function ( d ) {
								return $.extend( {}, d, {
									"contactId": ${contactVo.contactId},
									"fromDate": '<%=session.getAttribute("firstDateFinancialYear")%>',
									"toDate": '<%=session.getAttribute("lastDateFinancialYear")%>'
								});
							}
						},
						lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
						columns: [{
								data: "materialId"
							},{
								data: "materialNo"
							},{
								data: "materialDate"
							},{
								data: "total"
							},{
								data: "materialId"
						}],
						columnDefs: [{
							targets: 0,
							title: "#",
							orderable: !1,
							render: function(a, e, t, n) {
								return (n.row+1);
							}
						},{
							targets: 1,
							orderable: 1,
							render: function(a, e, t, n) {
								return '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'" class="m-link m--font-bolder">'+t.prefix+t.materialNo+'</a>';
							}
						},{
							targets: 2,
							render: function(a, e, t, n) {
								return moment(a).format('DD/MM/YYYY');
							}
						},{
							targets: 4,
							orderable: !1,
							render: function(a, e, t, n) {
								var action = "";
								action += '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
								action += '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill material_delete" title="Delete"> <i class="fa fa-trash"></i></a>'
								//action += '<span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="fa fa-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" target="_blank" href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'/pdf"><i class="fa fa-file-pdf"></i> PDF</a>\n</div>\n</span>\n '
								return action;
							}
							}],
							
							 fixedHeader: {
					                header: false,
					                footer: false
					            },
						})
					}
			};
		
	</script>
	
</body>
<!-- end::Body -->
</html>