<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
     <%@page import="com.croods.umeshtrading.constant.Constant"%>
<!DOCTYPE html>
<html>
<head>
	<%@include file="../header/head.jsp" %>
	<title>Dashboard</title>
	<style type="text/css">
		.select2-container{display: block;}
	</style>
</head>

<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<div class="m-portlet ">
						<div class="m-portlet__body  m-portlet__body--no-padding">
							<div class="row m-row--no-padding m-row--col-separator-xl">
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::Total Profit-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Customers</h4> <br> 
											<span class="m-widget24__desc"> Total Active Customers</span> 
											<span class="m-widget24__stats m--font-brand" id="totalCustomers">${Total_Customer}</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-brand" role="progressbar"
													style="width: 100%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<!-- <span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 78% </span> -->
										</div>
									</div>
									<!--end::Total Profit-->
								</div>
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::New Feedbacks-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Suppliers</h4> <br> 
											<span class="m-widget24__desc"> Total Active Suppliers </span>
											<span class="m-widget24__stats m--font-info" id="totalSuppliers">${Total_Suppliers}</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-info" role="progressbar"
													style="width: 100%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<!-- <span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 84% </span> -->
										</div>
									</div>
									<!--end::New Feedbacks-->
								</div>
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::New Orders-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Product</h4>
											<br> 
											<span class="m-widget24__desc">Total Active Product</span> 
											<span class="m-widget24__stats m--font-danger" id="totalProduct">${Total_Product}</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-danger" role="progressbar"
													style="width: 100%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<!-- <span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 69% </span> -->
										</div>
									</div>
									<!--end::New Orders-->
								</div>
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::New Users-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Total Stock</h4>
											<br> <span class="m-widget24__desc">In Amount </span> 
												<span class="m-widget24__stats m--font-success" id="inamount">${Total_Stock}</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-success" role="progressbar"
													style="width: 100%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<!-- <span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 90% </span> -->
										</div>
									</div>
									<!--end::New Users-->
								</div>
							</div>
						</div>
					</div>
					<!--end:: Widgets/Stats-->
					<!-- Start CHart 1 -->
					<div class="m-portlet">
								<div class="m-portlet__body  m-portlet__body--no-padding">
									<div class="row m-row--no-padding m-row--col-separator-xl">
										<div class="col-xl-6">
											<!--begin:: Widgets/Daily Sales-->
											<div class="m-widget14">
												<div class="m-widget14__header m--margin-bottom-30">
													<h3 class="m-widget14__title">
														Last 15 Day Sales and Purchase              
													</h3>
													<span class="m-widget14__desc">
													Day Wise Details
													</span>
												</div>
												<div class="m-widget14__chart" style="height:120px;">
													<canvas  id="daywise_sales"></canvas>
												</div>
											</div>
							<!--end:: Widgets/Daily Sales-->	
									</div>
										
										<div class="col-xl-6 ">
											<!--begin:: Widgets/Revenue Change-->
											<div class="m-widget14">
												<div class="m-widget14__header">
													<h3 class="m-widget14__title">
														Customer Category          
													</h3>
													<span class="m-widget14__desc">
														
													</span>
												</div>
												<div class="row  align-items-left">
													<div class="col">
														<div id="contact_category" class="m-widget14__chart1" style="height: 180px">
														</div>
													</div>	
													<div class="col">
														<div class="m-widget14__legends">
															<div class="m-widget14__legend">
																<span class="m-widget14__legend-bullet m--bg-accent"></span>
																<span class="m-widget14__legend-text">${Total_Retailer}</span>
																<span class="m-widget14__legend-text">Retailer</span>
															</div>
															<div class="m-widget14__legend">
																<span class="m-widget14__legend-bullet m--bg-danger"></span>
																<span class="m-widget14__legend-text">${Total_Wholesaler}</span>
																	<span class="m-widget14__legend-text">Wholesaler</span>
															</div>
															<div class="m-widget14__legend">
																<span class="m-widget14__legend-bullet m--bg-brand"></span>
																<span class="m-widget14__legend-text">${Total_Other}</span>
																	<span class="m-widget14__legend-text">Other</span>
															</div>
														</div>
													</div>
												</div>
											</div>
							<!--end:: Widgets/Revenue Change-->			
							</div>
							
									</div>
								</div>
							</div>
							<!--End::Section-->
							
							
							
							<!-- Jaimin 
							<div class="row  m-row--no-padding m-row--col-separator-xl">
								    begin:: Widgets/Best Sellers
								<div class="col-md-6 col-lg-12 col-xl-6 m--padding-left-0">
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													Best Selling Product
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m-tabs_lastMonth" role="tab">
													Last Month
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m-tabs_lastYear" role="tab">
													last Year
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m-tabs_allTime" role="tab">
													All time
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										begin::Content
										<div class="tab-content">
											<div class="tab-pane active" id="m-tabs_lastMonth" aria-expanded="true">
												begin::m-widget5
												<div class="m-widget5" id="m-tabs_lastMonth_itm">
													
												</div>
												end::m-widget5
											</div>
											<div class="tab-pane" id="m-tabs_lastYear" aria-expanded="false">
												begin::m-widget5
												<div class="m-widget5" id="m-tabs_lastYear_itm">
												
													
													
												</div>
												end::m-widget5
											</div>
											<div class="tab-pane" id="m-tabs_allTime" aria-expanded="false">
												begin::m-widget5
												<div class="m-widget5" id="m-tabs_allTime_itm">
													
													
												
												</div>
												end::m-widget5
											</div>
										</div>
										end::Content
									</div>
								</div>
								</div>
								<div class="col-md-6 col-lg-12 col-xl-6 m--padding-left-0">
									
							begin:: Widgets/Support Requests
							<div class="m-portlet  m-portlet--full-height">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">  
												 Sales Vs Purchase Chart
											</h3>
										</div>
									</div>

								</div>
								<div class="m-portlet__body">

									<div id="m_morris_1"></div>

								</div>
							</div>
							end:: Widgets/Support Requests
						
								</div>
								end:: Widgets/Best Sellers  
								</div> -->
								
					<!-- End Chart1 -->
				</div>
				
			</div>
			
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
	
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	 <script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	  <script src="<%=request.getContextPath()%>/assets/app/js/dashboard.js" type="text/javascript"></script>  
	

	  	
	<script>
	var monthlySalesPurchaseChartData;
	function  ganrateData(data){
		var s="";
		//var dats=data;
		if(data.length==0){
			s=s+'<div class="m-widget5__item"><h4 class="m-widget5__title">No Data Available</h4></div>';
		}else{
		$.each( data, function( key, value ) {
			  //alert( value.name +":"+value.src + ":"+value.repeatitem );
			//alert(dats[index].name);
			var imgsrc='';
			if(value.src==null){
				imgsrc='/No-image-found.jpg'
			}
			else{
				imgsrc='${Constant.REALPATH}/'+value.src;
			}
		  s=s+'<div class="m-widget5__item"><div class="m-widget5__content"><div class="m-widget5__pic"><img class="m-widget7__img" src='+imgsrc+' alt=""></div><div class="m-widget5__section"><h4 class="m-widget5__title"><a href="/product/'+value.product_id+'" class="m-link m--font-bolder" target="_blank">'+value.name+'</a></h4><span class="m-widget5__desc">'+value.description+'</span><div class="m-widget5__info"><span class="m-widget5__author">Category:</span><span class="m-widget5__info-author m--font-info"> '+value.category_name+'</span></div><div class="m-widget5__info"><span class="m-widget5__info-label">Brand:</span><span class="m-widget5__info-date m--font-info"> '+value.brand_name+'</span></div></div></div><div class="m-widget5__content"><div class="m-widget5__stats1"><span class="m-widget5__number">'+value.repeatitem+'</span><br><span class="m-widget5__sales">sales</span></div></div></div>'; 
		});
		}
		return s;
		//alert(s);
	}
	function chart() {
        // BAR CHART
		new Morris.Line({
            element: 'm_morris_1',
            data: monthlySalesPurchaseChartData,
            xkey: 'y',
            
            ykeys: ['a','b'],
            labels: ['Sales','Purchase'],
            hideHover : 'auto',
          	 xLabelAngle : 45,
			 padding: 15,
			 axes:'y',
			 resize: true,
			 gridTextWeight: 'bold'
				 
        });
    }
	
	$( document ).ready(function() {
		
	// JAIMIN	$.post("/salesVsPurchase",).done( function( data ) {
	//		monthlySalesPurchaseChartData=data;
	//		chart();
	//	});
		
		
		
		$(".m-tabs__link").click(function(){
			
			if($(this).attr('href') == "#m-tabs_lastMonth") {
				
				$.post("/bestsellingproduct",{type:'lastMonth'}).done(function( data ) {
				
					$("#m-tabs_lastMonth_itm").empty();
					$("#m-tabs_lastMonth_itm").append(ganrateData(data));
					  
					});
			} else if($(this).attr('href') == "#m-tabs_allTime") {
				$.post("/bestsellingproduct",{type:'allTime'}).done( function( data ) {
					
					$("#m-tabs_allTime_itm").empty();
					$("#m-tabs_allTime_itm").append(ganrateData(data));
					
				});
			} else if($(this).attr('href') == "#m-tabs_lastYear") {
				$.post( "/bestsellingproduct",{type:'lastYear'}).done(function( data ) {
					
					$("#m-tabs_lastYear_itm").empty();
					$("#m-tabs_lastYear_itm").append(ganrateData(data));
					
				});		
			}
		});
		
		$(".active").click();
		
		var e = $("#daywise_sales");
        if (0 != e.length) {
            var t = { labels: ['${sales_date}'], datasets: [{ backgroundColor: mApp.getColor("success"), data: [${sales_data}] }] };
            new Chart(e, { type: "bar", data: t, options: { title: { display: !1 }, tooltips: { intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10 }, legend: { display: !1 }, responsive: !0, maintainAspectRatio: !1, barRadius: 4, scales: { xAxes: [{ display: !1, gridLines: !1, stacked: !0 }], yAxes: [{ display: !1, stacked: !0, gridLines: !1 }] }, layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } } } })
        }
        
         0 != $("#contact_category").length && Morris.Donut({ element: "contact_category", data: [{ label: "Retailer", value: ${Total_Retailer} }, { label: "Wholesaler", value: ${Total_Wholesaler} }, { label: "Other", value: ${Total_Other} }], colors: [mApp.getColor("accent"), mApp.getColor("danger"), mApp.getColor("brand")] })
        
		
	});
	
	

	</script>
	
</body>
<!-- end::Body -->
</html>