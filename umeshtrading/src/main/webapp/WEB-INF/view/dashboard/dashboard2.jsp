<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<%@include file="../header/head.jsp"%>

<title>Dashboard</title>
<style type="text/css">
.select2-container {
	display: block;
}
</style>

<link
	href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css"
	rel="stylesheet" type="text/css" />

</head>

<!-- begin::Body -->
<body
	class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp"%>

		<!-- begin::Body -->
		<div
			class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp"%>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Dashboard
							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home"><a href="/"
									class="m-nav__link m-nav__link--icon"> <i
										class="m-nav__link-icon la la-home"></i>
								</a></li>

							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->

				<div class="m-content">
					<!--begin:: Widgets/Stats-->
					<div class="m-portlet ">
						<div class="m-portlet__body  m-portlet__body--no-padding">
							<div class="row m-row--no-padding m-row--col-separator-xl">
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::Total Profit-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Customers</h4> <br> 
											<span class="m-widget24__desc"> Total Active Customers</span> 
											<span class="m-widget24__stats m--font-brand" id="totalCustomers"> 0 </span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-brand" role="progressbar"
													style="width: 78%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 78% </span>
										</div>
									</div>
									<!--end::Total Profit-->
								</div>
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::New Feedbacks-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Suppliers</h4> <br> 
											<span class="m-widget24__desc"> Total Active Suppliers </span>
											<span class="m-widget24__stats m--font-info" id="totalSuppliers">0</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-info" role="progressbar"
													style="width: 84%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 84% </span>
										</div>
									</div>
									<!--end::New Feedbacks-->
								</div>
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::New Orders-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Product</h4>
											<br> 
											<span class="m-widget24__desc">Total Active Product</span> 
											<span class="m-widget24__stats m--font-danger" id="totalProduct">0</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-danger" role="progressbar"
													style="width: 69%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 69% </span>
										</div>
									</div>
									<!--end::New Orders-->
								</div>
								<div class="col-md-12 col-lg-6 col-xl-3">
									<!--begin::New Users-->
									<div class="m-widget24">
										<div class="m-widget24__item">
											<h4 class="m-widget24__title">Employee</h4>
											<br> <span class="m-widget24__desc">Total Active Employee </span> 
												<span class="m-widget24__stats m--font-success" id="totalEmployee">0</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-success" role="progressbar"
													style="width: 90%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<span class="m-widget24__change"> Change </span> <span
												class="m-widget24__number"> 90% </span>
										</div>
									</div>
									<!--end::New Users-->
								</div>
							</div>
						</div>
					</div>
					<!--end:: Widgets/Stats-->

					<!--Begin::Section-->
					<div class="m-portlet">
						<div class="m-portlet__body m-portlet__body--no-padding">
							<div class="row m-row--no-padding m-row--col-separator-xl">
								<div class="col-md-12 col-lg-12 col-xl-4">
									<!--begin:: Widgets/Stats2-1 -->
									<div class="m-widget1">
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Order</h3>
													<span class="m-widget1__desc">Total JobWork Order From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-brand" id="totalJobworkOrder">0</span>
												</div>
											</div>
										</div>
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Scrap</h3>
													<span class="m-widget1__desc">Total Producation Scrap From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-danger" id="totalScrap">0</span>
												</div>
											</div>
										</div>  
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Producation</h3>
													<span class="m-widget1__desc">Total Producation From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-success" id="totalProducation">0</span>
												</div>
											</div>
										</div>
									</div>
									<!--end:: Widgets/Stats2-1 -->
								</div>
								<div class="col-md-12 col-lg-12 col-xl-4">
									<!--begin:: Widgets/Stats2-2 -->
									<div class="m-widget1">
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Total Sales</h3>
													<span class="m-widget1__desc">Total Sales From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-accent" id="totalSales">0</span>
												</div>
											</div>
										</div>
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Received Amount</h3>
													<span class="m-widget1__desc">Total Receive Amount From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-info" id="totalReceiveAmount">0</span>
												</div>
											</div>
										</div>
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Panding Amount</h3>
													<span class="m-widget1__desc">Total Panding Amount From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-warning" id="totalSalesPandingAmount">0</span>
												</div>
											</div>
										</div>
									</div>
									<!--begin:: Widgets/Stats2-2 -->
								</div>
								<div class="col-md-12 col-lg-12 col-xl-4">
									<!--begin:: Widgets/Stats2-3 -->
									<div class="m-widget1">
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Total Purchase</h3>
													<span class="m-widget1__desc">Total Purchase From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-success" id="totalPurchase">0</span>
												</div>
											</div>
										</div>
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Paid Amount</h3>
													<span class="m-widget1__desc">Total Paid Amount From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-danger" id="totalPaidAmount">0</span>
												</div>
											</div>
										</div>
										<div class="m-widget1__item">
											<div class="row m-row--no-padding align-items-center">
												<div class="col">
													<h3 class="m-widget1__title">Panding Amount</h3>
													<span class="m-widget1__desc">Total Panding Amount From ${financialYear}</span>
												</div>
												<div class="col m--align-right">
													<span class="m-widget1__number m--font-primary" id="totalPurchasePandingAmount">0</span>
												</div>
											</div>
										</div>
									</div>
									<!--begin:: Widgets/Stats2-3 -->
								</div>
							</div>
						</div>
					</div>
					<!--End::Section-->
 
					<!--Begin::Section-->
					<div class="row">
						<div class="col-xl-6">
							<!--begin:: Widgets/Product Sales-->
							<div
								class="m-portlet m-portlet--bordered-semi m-portlet--space m-portlet--full-height ">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Product Purchase 
												<span class="m-portlet__head-desc">Total Purchase By Products</span>
											</h3>
										</div>
									</div>
									 
								</div>
								
								<div class="m-portlet__body">
								<div class="m-widget25">
									<span class="m-widget25__price m--font-brand" id="totalMonthPurchase">$0</span> <span
										class="m-widget25__desc">Total Purchase This Month</span>
									<div class="m-widget25--progress">
										<div class="m-widget25__progress">
											<span class="m-widget25__progress-number" id="totalDayPurchase">$0</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-danger" role="progressbar"
													style="width: 63%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<span class="m-widget25__progress-sub">Day Purchase</span>
										</div>
										<div class="m-widget25__progress">
											<span class="m-widget25__progress-number" id="totalWeekPurchase">$0</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-accent" role="progressbar"
													style="width: 39%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<span class="m-widget25__progress-sub">Week Purchase</span>
										</div>
										<div class="m-widget25__progress">
											<span class="m-widget25__progress-number" id="totalYearPurchase">$0</span>
											<div class="m--space-10"></div>
											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-warning" role="progressbar"
													style="width: 54%;" aria-valuenow="50" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
											<span class="m-widget25__progress-sub" >Year Purchase</span>
										</div>
									</div>
								</div>
							</div>
								
							</div>
							<!--end:: Widgets/Product Sales-->

 
					</div>
					<div class="col-xl-6">
						<!--begin:: Widgets/Product Sales-->
						<div
							class="m-portlet m-portlet--bordered-semi m-portlet--space m-portlet--full-height ">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Product Sales <span class="m-portlet__head-desc">Total
												Sales By Products</span>
										</h3>
									</div>
								</div>
								 
							</div>
							<div class="m-portlet__body">
									<div class="m-widget25">
										<span class="m-widget25__price m--font-brand" id="totalMonthSales">$0</span>
										<span class="m-widget25__desc">Total Sales This Month</span>
										<div class="m-widget25--progress">
											<div class="m-widget25__progress">
												<span class="m-widget25__progress-number" id="totalDaySales">$0</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-danger" role="progressbar"
														style="width: 63%;" aria-valuenow="50" aria-valuemin="0"
														aria-valuemax="100"></div>
												</div>
												<span class="m-widget25__progress-sub"> Day Sales
												</span>
											</div>
											<div class="m-widget25__progress">
												<span class="m-widget25__progress-number" id="totalWeekSales">$0</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-accent" role="progressbar"
														style="width: 39%;" aria-valuenow="50" aria-valuemin="0"
														aria-valuemax="100"></div>
												</div>
												<span class="m-widget25__progress-sub">Week Sales</span>
											</div>
											<div class="m-widget25__progress">
												<span class="m-widget25__progress-number" id="totalYearSales">$0</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-warning" role="progressbar"
														style="width: 54%;" aria-valuenow="50" aria-valuemin="0"
														aria-valuemax="100"></div>
												</div>
												<span class="m-widget25__progress-sub">Year Sales</span>
											</div>
										</div>
									</div>
								</div>
						</div>
						<!--end:: Widgets/Product Sales-->

					</div>
				</div>
				<!--End::Section-->



				<!--Begin::Section-->
				<div class="row">
					<div class="col-xl-4">
						<!--begin:: Widgets/Blog-->
						<div
							class="m-portlet m-portlet--head-overlay m-portlet--full-height  m-portlet--rounded-force">
							<div class="m-portlet__head m-portlet__head--fit-">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text m--font-light">Producation</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										 
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">
								<div class="m-widget27 m-portlet-fit--sides">
									<div class="m-widget27__pic">
										<img src="/assets/app/media/img/bg/bg-4.jpg" alt="">
										<h3 class="m-widget27__title m--font-light">
											<span id="totalDayProducation" >$0</span>
										</h3>
										<div class="m-widget27__btn">
											<button type="button"
												class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--bolder">Inclusive
												All Unit</button>
										</div>
									</div>
									<div class="m-widget27__container">
										<!-- begin::Nav pills -->
										<ul class="m-widget27__nav-items nav nav-pills nav-fill"
											role="tablist">
											<li class="m-widget27__nav-item nav-item"><a
												class="nav-link active show" data-toggle="pill"
												href="#m_personal_income_quater_1" id="producation">Producation</a></li>
											<li class="m-widget27__nav-item nav-item"><a
												class="nav-link" data-toggle="pill"
												href="#m_personal_income_quater_2" id="scrap">Scrap</a></li>
											 
										</ul>
										<!-- end::Nav pills -->

										<!-- begin::Tab Content -->
										<div class="m-widget27__tab tab-content m-widget27--no-padding">
											<div id="m_personal_income_quater_1"
												class="tab-pane active show">
												<div class="row  align-items-center">
													<div class="col">
														 <div id="m_chart_personal_income_quater_1" class="m-widget27__chart" style="height: 160px">
															<div class="m-widget27__stat" id="circalTotal1">0</div>
														</div>
													</div>
													<div class="col">
														<div class="m-widget27__legends">
															<div class="m-widget27__legend m--hide" id="sunit1">
																<span class="m-widget27__legend-bullet m--bg-accent"></span>
																<span class="m-widget27__legend-text" id="scunit1">0</span>
															</div>
															<div class="m-widget27__legend m--hide" id="sunit2">
																<span class="m-widget27__legend-bullet m--bg-warning"></span>
																<span class="m-widget27__legend-text" id="scunit2">0</span>
															</div>
															<div class="m-widget27__legend m--hide" id="sunit3">
																<span class="m-widget27__legend-bullet m--bg-brand"></span>
																<span class="m-widget27__legend-text" id="scunit3">0</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="m_personal_income_quater_2"
												class="tab-pane show">
												<div class="row  align-items-center">
													<div class="col">
														 <div id="m_chart_personal_income_quater_2" class="m-widget27__chart" style="height: 160px">
															<div class="m-widget27__stat" id="circalTotal">0</div>
														</div>
													</div>
													<div class="col">
														<div class="m-widget27__legends">
															<div class="m-widget27__legend m--hide" id="unit1">
																<span class="m-widget27__legend-bullet m--bg-accent"></span>
																<span class="m-widget27__legend-text" id="punit1">0</span>
															</div>
															<div class="m-widget27__legend m--hide" id="unit2">
																<span class="m-widget27__legend-bullet m--bg-warning" ></span>
																<span class="m-widget27__legend-text" id="punit2">0</span>
															</div>
															<div class="m-widget27__legend m--hide" id="unit3">
																<span class="m-widget27__legend-bullet m--bg-brand"></span>
																<span class="m-widget27__legend-text" id="punit3">0</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- end::Tab Content -->
									</div>
								</div>
							</div>
						</div>
						<!--end:: Widgets/Blog-->


					</div>
					 
					<div class="col-xl-8">
						<!--begin:: Packages-->
						<div
							class="m-portlet m--bg-info m-portlet--bordered-semi m-portlet--full-height ">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text m--font-light">HRMS</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										 
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">
								<!--begin::Widget 29-->
								<div class="m-widget29">
									<div class="m-widget_content">
										<h3 class="m-widget_content-title">Employee</h3>
										<div class="m-widget_content-items">
											<div class="m-widget_content-item">
												<span>Salary Based</span> 
												<span class="m--font-accent" id="totalSalaryEmployee">0</span>
											</div>
											<div class="m-widget_content-item">
												<span>Production Based</span> 
												<span class="m--font-brand" id="totalProductionEmployee">0</span>
											</div>
											<div class="m-widget_content-item">
												<span>Hours Based</span> 
												<span id="totalHourEmployee">0</span>
											</div>
										</div>
									</div>
									<div class="m-widget_content">
										<h3 class="m-widget_content-title">Attendance</h3>
										<div class="m-widget_content-items">
											<div class="m-widget_content-item">
												<span>Present</span> <span class="m--font-accent">$680</span>
											</div>
											<div class="m-widget_content-item">
												<span>Absent</span> <span class="m--font-brand">+15%</span>
											</div>
											<div class="m-widget_content-item">
												<span>Total</span> <span>29</span>
											</div>
										</div>
									</div>
									<div class="m-widget_content">
										<h3 class="m-widget_content-title">Last Monthly Salary</h3>
										<div class="m-widget_content-items">
											<div class="m-widget_content-item">
												<span>Paid</span> <span class="m--font-accent">22.50</span>
											</div>
											<div class="m-widget_content-item">
												<span>Pending</span> <span class="m--font-brand">+15%</span>
											</div>
											<div class="m-widget_content-item">
												<span>Total</span> <span>701</span>
											</div>
										</div>
									</div>
									
								</div>
								<!--end::Widget 29-->
							</div>
						</div>
						<!--end:: Packages-->


					</div>
				</div>
				<!--End::Section-->


			</div>

		</div>

	</div>
	<!-- end:: Body -->

	<!-- Include Footer -->
	<%@include file="../footer/footer.jsp"%>

	</div>
	<!-- end:: Page -->

	<!-- begin::Scroll Top -->
	<div id="m_scroll_top" class="m-scroll-top">
		<i class="la la-arrow-up"></i>
	</div>
	<!-- end::Scroll Top -->

	<!--begin::Base Scripts -->
	<script
		src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js"
		type="text/javascript"></script>
	<script
		src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js"
		type="text/javascript"></script>
	<!--end::Base Scripts -->
	 <script src="<%=request.getContextPath()%>/assets/app/js/dashboard.js" type="text/javascript"></script>

	<script type="text/javascript">
	jQuery(document).ready(function() { 
		getContactInfo("customers"); 
		getContactInfo("suppliers");
		getProductInfo(); 
		getProducationInfo();
		getEmployeeInfo();
		getJobworkOrderInfo();
		getSalesTotal();
		getPurchaseTotal();
		getPurchaseInfo();
		getSalesInfo();
		getProducationData();
	});
	
	function getContactInfo(change) {  
		var type=change;
		$.post("<%=request.getContextPath()%>/contact/"+type+"/totalContect/json", { 
		}, function (data, status) {   
			if(type =='customers'){ 
				$("#totalCustomers").html(data);
			}else if(type =='suppliers'){ 
				$("#totalSuppliers").html(data);	
			}
		});
	}
	
	function getProductInfo() {   
		$.post("<%=request.getContextPath()%>/product/totalProduct/json", { 
		}, function (data, status) {    
				$("#totalProduct").html(data);	 
		});
	}
	
	function getEmployeeInfo() {   
		$.post("<%=request.getContextPath()%>/employee/totalEmployee/json", { 
		}, function (data, status) {  
			var tt=0;
			$.each(data,function(key,val){ 
				if(val[0] == 'Hour'){
					$("#totalHourEmployee").html(val[1]);	 
				}else if(val[0] == 'Production'){
					$("#totalProductionEmployee").html(val[1]);	 
				}else if(val[0] == 'Salary'){
					$("#totalSalaryEmployee").html(val[1]);	 
				} 
				tt += val[1];
			});
				
				$("#totalEmployee").html(tt);	 
		});
	}
	
	function getJobworkOrderInfo() {   
		$.post("<%=request.getContextPath()%>/jobwork/jobworkorder/totalJobworkOrder/json", { 
		}, function (data, status) {   
				$("#totalJobworkOrder").html(data);	 
		});
	}
	
	function getProducationInfo() {   
		$.post("<%=request.getContextPath()%>/production/production/totalProduction/json", { 
		}, function (data, status) { 
			
			$.each(data,function(key,val){ 
				if(val[0] == 'production_qty'){
					$("#totalProducation").html(val[1]);	 
				}else if(val[0] == 'production_scrap'){
					$("#totalScrap").html(val[1]);	 
				}  
			});  
		});
	}
	
	function getSalesTotal(){
		$.post("<%=request.getContextPath()%>/sales/invoice/totalSales/json", { 
		}, function (data, status) { 
			
			var salesTotal=0;
			var salesPending=0;
			var salesReturnTotal=0;
			var salesReturnPending=0;
			
			$.each(data,function(key,val){ 
				if(val[0] == 'invoice' || val[0] == 'billofsupply' || val[0] == 'pos'){
					salesTotal +=val[1]
					salesPending +=val[2]
				}else if(val[0] == 'creditnote'){
					salesReturnTotal +=val[1]
					salesReturnPending +=val[2]
				}   
			});  
			
			/* $("#totalSales").html(salesTotal-salesReturnTotal);	 
			$("#totalSalesPandingAmount").html(salesPending-salesReturnPending);
			$("#totalReceiveAmount").html((salesTotal-salesReturnTotal)-(salesPending-salesReturnPending));  */
			$("#totalSales").html(salesTotal);	 
			$("#totalSalesPandingAmount").html(salesPending);
			$("#totalReceiveAmount").html((salesTotal)-(salesPending)); 
			
			$("#totalYearSales").html(salesTotal);	 
		});
	}
	
	function getPurchaseTotal(){
		$.post("<%=request.getContextPath()%>/purchase/bill/totalPurchase/json", { 
		}, function (data, status) { 
			
			var PurchaseTotal=0;
			var PurchasePending=0;
			var PurchaseReturnTotal=0;
			var PurchaseReturnPending=0; 
			$.each(data,function(key,val){ 
				if(val[0] == 'bill'){
					PurchaseTotal +=val[1]
					PurchasePending +=val[2]
				}else if(val[0] == 'debitnote'){
					PurchaseReturnTotal +=val[1]
					PurchaseReturnPending +=val[2]
				}   
			});  
			
			/* $("#totalPurchase").html(PurchaseTotal-PurchaseReturnTotal);	 
			$("#totalPurchasePandingAmount").html(PurchasePending-PurchaseReturnPending);
			$("#totalPaidAmount").html((PurchaseTotal-PurchaseReturnTotal)-(PurchasePending-PurchaseReturnPending));  */
			
			$("#totalPurchase").html(PurchaseTotal);	 
			$("#totalPurchasePandingAmount").html(PurchasePending);
			$("#totalPaidAmount").html((PurchaseTotal)-(PurchasePending)); 
			
			$("#totalYearPurchase").html(PurchaseTotal);
			
		});
	}
	
	function getPurchaseInfo(){
		$.post("<%=request.getContextPath()%>/purchase/bill/totalPurchaseInfo/json", { 
		}, function (data, status) { 
			 
			$("#totalDayPurchase").html(data[0]);	 
			$("#totalWeekPurchase").html(data[1]);
			$("#totalMonthPurchase").html(data[2]); 
			
		});
	}
	
	function getSalesInfo(){
		$.post("<%=request.getContextPath()%>/sales/invoice/totalSalesInfo/json", { 
		}, function (data, status) { 
			 
			$("#totalDaySales").html(data[0]);	 
			$("#totalWeekSales").html(data[1]);
			$("#totalMonthSales").html(data[2]); 
			
		});
	}
	
	
	function getProducationData() {   
		$.post("<%=request.getContextPath()%>/production/production/productionData/json", { 
		}, function (data, status) { 
			var total=0.0;
			var sctotal=0.0;
			$.each(data,function(key,val){ 
				alert(val)
				$("#punit"+(key+1)).html(val[0]);	
				$("#scunit"+(key+1)).html(val[0]);	
				$("#unit"+(key+1)).removeClass("m--hide");	
				$("#sunit"+(key+1)).removeClass("m--hide");	
				total += val[1];
				sctotal += val[2];
			});  
			$("#totalDayProducation	").html(total);	
			$("#circalTotal").html(sctotal);	
			$("#circalTotal1").html(total);	
		});
	}
	</script>


</body>

<!-- end::Body -->
</html>


