<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${deliveryChallanVo.prefix}${deliveryChallanVo.deliveryChallanNo} | Delivery Challan</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New Delivery Challan</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/deliverychallan" class="m-nav__link">
										<span class="m-nav__link-text">Delivery Challan</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/deliverychallan/${deliveryChallanVo.deliveryChallanId}" class="m-nav__link">
										<span class="m-nav__link-text">${deliveryChallanVo.prefix}${deliveryChallanVo.deliveryChallanNo}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="deliverychallan_form" action="/deliverychallan/save" method="post">	
						<input type="hidden" name="deliveryChallanId" value="${deliveryChallanVo.deliveryChallanId}">
						<input type="hidden" name="deleteDeliveryChallanItemIds" id="deleteDeliveryChallanIds" value="">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
									<!-- <div class="card-container">
											  <div class="card" id="card">
											    <div class="side"><button class="btn btn-success card-btn" type="button" id="btn_per" style="border-top-right-radius: 0;border-bottom-right-radius: 0">Go!</button></div>
											    <div class="side back"><button class="btn btn-danger" type="button">GO!</button></div>
											  </div>
											</div> -->
										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Supplier:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2" id="contactVo" name="contactVo.contactId" onchange="getContactInfo(0)" placeholder="Select Supplier">
															<option value="">Select Supplier</option>
															<c:forEach items="${contactVos}" var="contactVo">
																<option value="${contactVo.contactId}" 
																<c:if test="${deliveryChallanVo.contactVo.contactId == contactVo.contactId}">selected="selected"</c:if>>
																	${contactVo.companyName}
																</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span id="lblContactGSTIN">-</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">-</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
													<h3 class="m-section__heading">Address Details</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted" data-address-message="">Address is not provided</small></h5>
													<div class="m-section__content m--hide" id="purchase_billing_address">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">																
																<h5 class=""><small class="text-muted" data-address-name=""></small></h5>
																<p class="mb-0">
																	<span data-address-line-1=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode=""></span>
																	<span data-address-city=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country=""></span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""></span></span>
																</p>
															</div>
														</div>
													</div>
													
												<%-- </c:if> --%>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Delivery Challan Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" name="deliveryChallanDate" readonly id="deliveryChallanDate" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>' 
																value='<fmt:formatDate pattern="dd/MM/yyyy" value="${deliveryChallanVo.deliveryChallanDate}"/>'/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Reference No:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="referenceNo" placeholder="Reference No." value="${deliveryChallanVo.referenceNo}">
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Delivery Challan No :</label>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" readonly="readonly" name="prefix" placeholder="Prefix" value="${deliveryChallanVo.prefix}">
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" readonly="readonly" name="deliveryChallanNo" placeholder="Delivery Challan No." value="${deliveryChallanVo.deliveryChallanNo}">
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Received By:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="receivedBy" name="receivedBy.employeeId" placeholder="Select Received By">
																<option value="">Select Received By</option>
																<c:forEach items="${employeeVos}" var="employeeVo">
																	<option value="${employeeVo.employeeId}"
																	<c:if test="${deliveryChallanVo.receivedBy.employeeId== employeeVo.employeeId}">selected="selected"</c:if>>
																	${employeeVo.employeeName}
																</option>
																	
																</c:forEach>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Note:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea class="form-control m-input" name="note" placeholder="Note">${deliveryChallanVo.note}</textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
														Product Details
													</a>
												</li>
											</ul>
										</div>
										
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    
															    <thead>
															      <tr>
															        <!-- <th>#</th> -->
															        <th>#</th>
															        <th>Product</th>
															        <th>Design No.</th>
															        <th>Bale No.</th>
															        <th>Qty</th>
															        <th></th>
															        
															      </tr>
															    </thead>
															    <tbody data-table-list="">
															    
															    	<tr data-table-item="template" class="m--hide">
															    		<td class="" style="width: 50px;">
															    			<span data-item-index></span>
															    		</td>
																        <td class="p-0" style="width: 370px;">
																        	<div class="form-group m-form__group p-0">
																			<select class="form-control m-input form-control-sm m-input1" name="deliveryChallanItemVos[{index}].productVariantVo.productVariantId" id="productVariantId{index}" onchange="getProductVariantInfo({index})" placeholder="Select Product">
																				<option value="">Select Product</option>
																				<c:forEach items="${productVariantVos}" var="productVariantVo">
																					<option value="${productVariantVo.productVariantId}">${productVariantVo.productVo.categoryVo.categoryName} ${productVariantVo.productVo.name} ${productVariantVo.variantName}</option>
																				</c:forEach>
																			</select>
																			</div>
																        </td>
																        <td class="p-0" style="width: 270px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control form-control m-input1" id="designNo{index}" name="deliveryChallanItemVos[{index}].designNo" placeholder="Design No." value="">
																        	</div>
																        </td>
																        <td class="p-0" style="width: 270px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control form-control m-input1" id="baleNo{index}" name="deliveryChallanItemVos[{index}].baleNo" placeholder="Bale No." value="">
																        	</div>
																        </td>
																        <td class="p-0" style="width: 270px;">
																        	<div class="form-group m-form__group p-0">
																        		<div class="m-input-icon m-input-icon--right">
																					<input type="text" class="form-control form-control m-input1 text-right" id="qty{index}" name="deliveryChallanItemVos[{index}].qty" placeholder="Qty" value="">
																					<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																				</div>
																        	</div>
																        </td>
																        <td class="p-0" style="width: 40px;">
																        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
																        </td>
															    	</tr>
															    		<c:set var="index" scope="page" value="0"/>
															    	<c:forEach items="${deliveryChallanVo.deliveryChallanItemVos}" var="deliveryChallanItemVo" varStatus="status">
																    	<input type="hidden" name="deliveryChallanItemVos[${index}].deliveryChallanItemId" id="deliveryChallanItemId${index}" value="${deliveryChallanItemVo.deliveryChallanItemId}">
																    	<tr data-table-item="${index}" class="">
																    		<td class="" style="width: 50px;">
																    			<span data-item-index>${status.index+1}</span>
																    		</td>
																	        <td class="p-0" style="width: 370px;">
																	        	<div class="form-group m-form__group p-0">
																					<select class="form-control m-input form-control-sm m-input1 m-select2" name="deliveryChallanItemVos[${index}].productVariantVo.productVariantId" id="productVariantId${index}" onchange="getProductVariantInfo({index})" placeholder="Select Product">
																						<option value="">Select Product</option>
																						<c:forEach items="${productVariantVos}" var="productVariantVo">
																							<option value="${productVariantVo.productVariantId}" 
																							<c:if test="${deliveryChallanItemVo.productVariantVo.productVariantId==productVariantVo.productVariantId}">selected="selected"</c:if>
																							>${productVariantVo.productVo.categoryVo.categoryName} ${productVariantVo.productVo.name} ${productVariantVo.variantName}</option>
																						</c:forEach>
																					</select>																					
																				</div>
																	        </td>
																	        
																	        <td class="p-0" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveDesignno==0}"><input type="text" class="form-control form-control m-input1" id="designNo${index}" name="deliveryChallanItemVos[${index}].designNo" disabled="disabled" placeholder="Design No." value=""></c:if>
																	        		<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveDesignno!=0}"><input type="text" class="form-control form-control m-input1" id="designNo${index}" name="deliveryChallanItemVos[${index}].designNo" placeholder="Design No." value="${deliveryChallanItemVo.designNo}"></c:if>
																	        	</div>
																	        </td>
																	        <td class="p-0" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveBaleno==0}"><input type="text" class="form-control form-control m-input1" id="baleNo{index}" name="deliveryChallanItemVos[{index}].baleNo" disabled="disabled" placeholder="Bale No." value="0"></c:if>
																	        		<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveBaleno!=0}"><input type="text" class="form-control form-control m-input1" id="baleNo${index}" name="deliveryChallanItemVos[${index}].baleNo" placeholder="Bale No." value="${deliveryChallanItemVo.baleNo}"></c:if>
																	        	</div>
																	        </td>
																	       
																	        <td class="p-0" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0">
																		        		<div class="m-input-icon m-input-icon--right">
																							<input type="text" class="form-control form-control m-input1 text-right" id="qty${index}" name="deliveryChallanItemVos[${index}].qty" placeholder="Qty" value="${deliveryChallanItemVo.qty}">
																							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom>${deliveryChallanItemVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</i></span></span>
																						</div>
																		        </div>
																        	</td>
																	       <td class="p-0" style="width: 40px;">
																	        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
																	        </td>
																    	</tr>
																    	 <c:set var="index" scope="page" value="${index+1}"/>
																    </c:forEach>
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th>
															        	<div class="m-demo-icon mb-0">
																			<div class="m-demo-icon__preview">
																				<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																			</div>
																			<div class="m-demo-icon__class">
																			<a href="JavaScript:void(0)" data-toggel="modal" class="m-link m--font-boldest" onclick="addTableItem()"> Add More</a>
																			</div>
																		</div>
															        </th>
															        <th></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id=""></span></th>
															        <th></th>
															      </tr>
															    </tfoot>
														  	</table>
														</div>
														
													</div>
													
												</div>
											</div>
										</div>
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="savedeliverychallan">
									Submit
								</button>
								
								<a href="<%=request.getContextPath()%>/deliverychallan" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/deliverychallan/deliverychallan-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		
		$(document).ready(function(){
			getContactInfo(0);
			
			setTableIndex('${index}');
			
			<%-- try {
	  			$('#deliveryChallanDate').datepicker('setDate',${diliverchalanvo.deliveryChallanDate});
	  			
	  		} catch(e) {
	  			$('#deliveryChallanDate').datepicker('setDate','<%=session.getAttribute("firstDateFinancialYear")%>');
	  		} --%>
	  		
		});
		
		
	</script>
	
</body>
<!-- end::Body -->
</html>