	<div class="modal fade" id="designation_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					New Designation
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="designation_new_form">
					<div class="form-group">
						<label class="form-control-label">
							Designation Name:
						</label>
						<input type="text" name="designationName" class="form-control" id="designationName">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button"id="savedesignation" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>