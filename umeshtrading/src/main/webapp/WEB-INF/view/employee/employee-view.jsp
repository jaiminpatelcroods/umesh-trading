<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${employeeVo.employeeName} | Employee</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		.rotate {
		    -moz-transition: all .5s linear;
		    -webkit-transition: all .5s linear;
		    transition: all .5s linear;
		}
		.rotate.down {
		    -moz-transform:rotate(45deg);
		    -webkit-transform:rotate(45deg);
		    transform:rotate(45deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${employeeVo.employeeName}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/employee" class="m-nav__link">
										<span class="m-nav__link-text">Employee</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon">
												<i class="flaticon-cogwheel-2"></i>
											</span>
											<h3 class="m-portlet__head-text m--font-brand">Employee Details</h3>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
												<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl  m-dropdown__toggle">
													<!-- <i class="la la-ellipsis-v"></i> -->
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>
						                        <div class="m-dropdown__wrapper">
						                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						                            <div class="m-dropdown__inner">
					                                    <div class="m-dropdown__body">              
					                                        <div class="m-dropdown__content">
					                                            <ul class="m-nav">
					                                                <li class="m-nav__separator m-nav__separator--fit">
					                                                </li>
					                                                <li class="m-nav__item">
					                                                    <a href="/employee/${employeeVo.employeeId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																			<span><i class="flaticon-edit"></i><span>Edit</span></span>
																		</a>
					                                                    <button id="contact_delete" data-url="/employee/${employeeVo.employeeId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																			<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																		</button>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </div>
						                            </div>
						                        </div>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body" >
									<div class="m-section">
										<h3 class="m-section__heading">Basic Details</h3>
										<div class="m-section__content">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
													<table class="table m-table">
													  	<tbody>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Employee Name:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.employeeName}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Mobile No.:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.employeeMobileno}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Email:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.employeeEmail}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Alternative Mobile No.:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.alternativeMobileno}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Alternative Email:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.alternativeEmail}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">CTC:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.ctc}</td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
													<table class="table m-table">
													  	<tbody>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Pan No.:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.panNo}</td>
													    	</tr>
													    	<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Department:</th>
															    <td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.departmentVo.departmentName}</td>
													    	</tr>
													    	<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Designation:</th>
															    <td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.designationVo.designationName}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Date of Birth:</th>
													      		<td class="col-lg-8 col-md-8 col-sm-12">
													      			<c:if test="${not empty employeeVo.dateOfBirth}">
													      				<fmt:formatDate pattern="dd/MM/yyyy" value="${employeeVo.dateOfBirth}"/>
													      			</c:if>
													      		</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Anniversary Date:</th>
													      		<td class="col-lg-8 col-md-8 col-sm-12">
													      			<c:if test="${not empty employeeVo.anniversaryDate}">
													      				<fmt:formatDate pattern="dd/MM/yyyy" value="${employeeVo.anniversaryDate}"/>
													      			</c:if>
													      		</td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<div class="m-section">
										<h3 class="m-section__heading">Address Details</h3>
										<div class="m-section__content">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
													<table class="table m-table">
													  	<tbody>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Address Line 1:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.addressLine1}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Address Line 2:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.addressLine2}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Country:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.countriesName}</td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
													<table class="table m-table">
													  	<tbody>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">State:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.stateName}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">City:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.cityName}</td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">ZIP/Postal code:</th>
														      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.pinCode}</td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<c:if test="${employeeVo.employeeContactVos.size() != 0}">
										<div class="m-section">
											<h3 class="m-section__heading">Other Contacts Details</h3>
											<div class="m-section__content">
												<div class="row">
													<c:forEach items="${employeeVo.employeeContactVos}" var="employeeContactVo" varStatus="status">
														<div class="col-lg-4 col-md-4 col-sm-12 m--padding-left-30">
															<table class="table m-table">
															  	<tbody>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Name:</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeContactVo.name}</td>
															    	</tr>
															    	<tr class="row">
																      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Relation:</th>
																	    <td class="col-lg-8 col-md-8 col-sm-12">${employeeContactVo.relation}</td>
															    	</tr>
															    	<tr class="row">
																      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Mobile No.:</th>
																	    <td class="col-lg-8 col-md-8 col-sm-12">${employeeContactVo.mobileno}</td>
															    	</tr>
															  	</tbody>
															</table>
														</div>
													</c:forEach>
												</div>
											</div>
										</div>
									</c:if>
									<c:if test="${not empty employeeVo.userFrontVo}">
										<div class="m-section">
											<h3 class="m-section__heading">Authentication Details</h3>
											<div class="m-section__content">
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<table class="table m-table">
														  	<tbody>
														    	<tr class="row">
														      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">User Name:</th>
															      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.userFrontVo.userName}</td>
														    	</tr>
														    	<tr class="row">
														      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">User Role:</th>
															      	<td class="col-lg-8 col-md-8 col-sm-12">${employeeVo.userFrontVo.roles.get(0).userRoleName}</td>
														    	</tr>
														  	</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</c:if>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".delete-btn").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
		});
	</script>
</body>
<!-- end::Body -->
</html>