<div class="modal fade" id="floor_update_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit floor</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<form id="floor_update_form" class="m-form m-form--state m-form--fit">
				<input type="hidden" id="floorId">
				<div class="modal-body">
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Place:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<select class="form-control m-select2" id="updatePlaceVo" name="placeVo.placeId" placeholder="Select Place" style="width: 100%" required="required" data-fv-notempty-message="Place is required"> 
								<option value="">Select Place</option>
								<c:forEach items="${placeList}" var="PlaceList">
									<option value="${PlaceList.placeId}">
										${PlaceList.placeCode} - ${PlaceList.placeName}	
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Floor Name:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="floorName" id="updateFloorName" placeholder="Floor Name">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Floor Code:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="floorCode" id="updateFloorCode" placeholder="Floor Code">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<textarea class="form-control m-input" name="description" id="updateDescription" placeholder="Description"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
						Close
					</button>
					<button type="button" onclick="" id="updatefloor" class="btn btn-primary">
						Save
					</button>
				</div>
			</form>
		</div>
	</div>
</div>