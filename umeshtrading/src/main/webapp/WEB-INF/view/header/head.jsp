<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

<!--begin::Web font -->
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
	WebFont.load({
		google : {
			"families" : [ "Poppins:300,400,500,600,700","Roboto:300,400,500,600,700" ]
		},
		active : function() {
			sessionStorage.fonts = true;
		}
	});
</script>
<!--end::Web font -->

<!--begin::Base Styles -->
<link href="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/assets/demo/demo12/base/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Base Styles -->

<link rel="shortcut icon" href="<%=request.getContextPath()%>/assets/demo/demo12/media/img/logo/favicon.ico" />