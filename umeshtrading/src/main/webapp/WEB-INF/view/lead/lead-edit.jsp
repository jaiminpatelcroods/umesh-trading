<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${leadVo.companyName} | Lead</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.css">
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Edit Lead</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/lead" class="m-nav__link">
										<span class="m-nav__link-text">Lead</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/lead/${leadVo.leadId}" class="m-nav__link">
										<span class="m-nav__link-text">${leadVo.companyName}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="lead_form" action="/lead/save" method="post">	
						<input type="hidden" name="leadId" value="${leadVo.leadId}"/>
						<input type="hidden" name="deleteAddress" id="deleted-address" value=""/>
						<input type="hidden" name="deletedLeadContact" id="deleted-lead-contact" value=""/>
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Basic Details</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="companyName" value="${leadVo.companyName}" placeholder="Company Name"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Mobile No.:</label>
														<input type="text" class="form-control m-input" name="companyMobileno" value="${leadVo.companyMobileno}" placeholder="Mobile No."/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Telephone No.:</label>
														<input type="text" class="form-control m-input" name=companyTelephone value="${leadVo.companyTelephone}" placeholder="Telephone No."/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Email:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="companyEmail" value="${leadVo.companyEmail}" placeholder="Email" />
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Owner Name:</label>
														<input type="text" class="form-control m-input" name="ownerName" value="${leadVo.ownerName}" placeholder="Name"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Owner Mobile No.:</label>
														<input type="text" class="form-control m-input" name="ownerMobileno" value="${leadVo.ownerMobileno}" placeholder="Mobile No."/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Concern Person:</label>
														<input type="text" class="form-control m-input" name="concernPersonName" value="${leadVo.concernPersonName}" placeholder="Name" />
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Concern Person Mobile No.:</label>
														<input type="text" class="form-control m-input" name="concernPersonMobileno" value="${leadVo.concernPersonMobileno}" placeholder="Mobile No." />
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">GST Type:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2" id="gstType" name="gstType" placeholder="Select GST Type">
															<option value="UnRegistered">UnRegistered</option>
															<option value="Registered">Registered</option>
															<option value="Composition Scheme">Composition Scheme</option>
															<option value="Input Service Distributor">Input Service Distributor</option>
															<option value="E-Commerce Operator">E-Commerce Operator</option>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">GSTIN:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" id="gstin"  name="gstin" value="${leadVo.gstin}" placeholder="GSTIN"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Pan No.:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="panNo" value="${leadVo.panNo}" placeholder="Pan No."/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Date of Birth:</label>
														<div class="input-group date" >
															<input type="text" class="form-control m-input clearbtn-datepicker" name="dateOfBirth" id="dateOfBirth" readonly placeholder="Date Of Birth"
																<c:if test="${not empty leadVo.dateOfBirth}">value='<fmt:formatDate pattern="dd/MM/yyyy" value="${leadVo.dateOfBirth}"/>'</c:if>
															/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Anniversary Date:</label>
														<div class="input-group date" >
															<input type="text" class="form-control m-input clearbtn-datepicker" readonly name="anniversaryDate" id="anniversaryDate" placeholder="Anniversary Date"
																<c:if test="${not empty leadVo.anniversaryDate}">value='<fmt:formatDate pattern="dd/MM/yyyy" value="${leadVo.anniversaryDate}"/>'</c:if>
															/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Total No. of Shops:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="noOfShops" value="${leadVo.noOfShops}" placeholder="Total No. of Shops"/>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m--padding-10">
											<div id="lead_contact_repeater"> 
					                            <div class="form-group  m-form__group row">
					                                <div data-repeater-list="" class="col-lg-12">
					                                	<c:if test="${leadVo.leadContactVos.size() == 0}">
					                                		<div data-repeater-item class="row m--margin-bottom-10">                                      
						                                        <div class="col-lg-4">
						                                            <div class="input-group">
						                                                <div class="input-group-prepend">
						                                                    <span class="input-group-text">
						                                                        <i class="la la-user"></i>
						                                                    </span>
						                                                </div>
						                                                <input type="text" class="form-control form-control-danger" name="leadContactVos[{index}].name" placeholder="Name">
						                                            </div>
						                                        </div>
						                                        <div class="col-lg-3">
						                                            <div class="input-group">
						                                                <div class="input-group-prepend">
						                                                    <span class="input-group-text">
						                                                        <i class="la la-phone"></i>
						                                                    </span>
						                                                </div>
						                                                <input type="text" class="form-control form-control-danger" name="leadContactVos[{index}].mobileno" placeholder="Mobile No.">
						                                            </div>
						                                        </div>
						                                        <div class="col-lg-4">
						                                            <div class="input-group">
						                                                <div class="input-group-prepend">
						                                                    <span class="input-group-text">
						                                                        <i class="la la-envelope"></i>
						                                                    </span>
						                                                </div>
						                                                <input type="text" class="form-control form-control-danger" name="leadContactVos[{index}].email" placeholder="Email">
						                                            </div>
						                                        </div>        
						                                        <div class="col-lg-1">
						                                            <a href="JavaScript:void(0)" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
						                                                <i class="la la-remove"></i>
						                                            </a>
						                                        </div>                     
						                                    </div>
					                                	</c:if>
					                                    <c:forEach items="${leadVo.leadContactVos}" var="leadContactVo" varStatus="status">
					                                    	<div data-repeater-item class="row m--margin-bottom-10">                                      
						                                        <input type="hidden" name="leadContactVos[{index}].leadContactId" id="leadContactId{index}" value="${leadContactVo.leadContactId}"/>
						                                        <div class="col-lg-4">
						                                            <div class="input-group">
						                                                <div class="input-group-prepend">
						                                                    <span class="input-group-text">
						                                                        <i class="la la-user"></i>
						                                                    </span>
						                                                </div>
						                                                <input type="text" class="form-control form-control-danger" name="leadContactVos[{index}].name" value="${leadContactVo.name}" placeholder="Name">
						                                            </div>
						                                        </div>
						                                        <div class="col-lg-3">
						                                            <div class="input-group">
						                                                <div class="input-group-prepend">
						                                                    <span class="input-group-text">
						                                                        <i class="la la-phone"></i>
						                                                    </span>
						                                                </div>
						                                                <input type="text" class="form-control form-control-danger" name="leadContactVos[{index}].mobileno" value="${leadContactVo.mobileno}" placeholder="Mobile No.">
						                                            </div>
						                                        </div>
						                                        <div class="col-lg-4">
						                                            <div class="input-group">
						                                                <div class="input-group-prepend">
						                                                    <span class="input-group-text">
						                                                        <i class="la la-envelope"></i>
						                                                    </span>
						                                                </div>
						                                                <input type="text" class="form-control form-control-danger" name="leadContactVos[{index}].email" value="${leadContactVo.email}" placeholder="Email">
						                                            </div>
						                                        </div>        
						                                        <div class="col-lg-1">
						                                            <a href="JavaScript:void(0)" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
						                                                <i class="la la-remove"></i>
						                                            </a>
						                                        </div>                     
						                                    </div>
						                                </c:forEach>                                                        
					                                </div>                 
					                            </div>
					                            
						                        <div class="m-demo-icon mb-0">
													<div class="m-demo-icon__preview">
														<span class=""><i class="flaticon-plus m--font-primary"></i></span>
													</div>
													<div class="m-demo-icon__class">
														<a href="#" data-toggle="modal" data-repeater-create="" class="m-link m--font-boldest">Add More Contact</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
								<div class="" id="address_repeater">
									<div data-repeater-list="">
									
										<c:forEach items="${leadVo.leadAddressVos}" var="leadAddressVo" varStatus="status">
											<!--begin::Portlet-->
											<div class="m-portlet" data-repeater-item>
												<div class="m-portlet__head">
													<div class="m-portlet__head-caption">
														<div class="m-portlet__head-title">
															<span class="m-portlet__head-icon">
																<i class="flaticon-cogwheel-2"></i>
															</span>
															<h3 class="m-portlet__head-text m--font-brand">
																Address Details
															</h3>
														</div>			
													</div>
													<div class="m-portlet__head-tools">
														<ul class="m-portlet__nav">
															<li class="m-portlet__nav-item">
																<a href="#" data-repeater-delete="" data-toggle="modal" class="m-portlet__nav-link btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air">
																	<i class="la la-close"></i>
																</a>
															</li>
														</ul>
													</div>
												</div>
												<div class="m-portlet__body">
													<input type="hidden" name="leadAddressVos[{index}].leadAddressId" id="leadAddressId{index}" value="${leadAddressVo.leadAddressId}"/>
													<input type="hidden" name="leadAddressVos[{index}].isDefault" value="${leadAddressVo.isDefault}" />
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="form-group m-form__group row ">
																<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label>
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<input type="text" class="form-control m-input" name="leadAddressVos[{index}].companyName" value="${leadAddressVo.companyName}" placeholder="Company Name" />
																</div>
															</div>
															<div class="form-group m-form__group row ">
																<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Address Line 1:</label>
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<input type="text" class="form-control m-input" name="leadAddressVos[{index}].addressLine1" value="${leadAddressVo.addressLine1}" placeholder="Address Line 1" />
																</div>
															</div>
															<div class="form-group m-form__group row ">
																<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Address Line 2:</label>
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<input type="text" class="form-control m-input" name="leadAddressVos[{index}].addressLine2" value="${leadAddressVo.addressLine2}" placeholder="Address Line 2" />
																</div>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="form-group m-form__group row ">
																<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Country:</label>
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<select class="form-control " id="countriesCode{index}" name="leadAddressVos[{index}].countriesCode" onchange="getAllStateAjax('countriesCode{index}','stateCode{index}')" data-default="${leadAddressVo.countriesCode}" placeholder="Select Country" data-allow-clear="true">
																	</select>
																</div>
															</div>
															<div class="form-group m-form__group row ">
																<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select State:</label>
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<select class="form-control" id="stateCode{index}" name="leadAddressVos[{index}].stateCode" onchange="getAllCityAjax('stateCode{index}','cityCode{index}')" data-default="${leadAddressVo.stateCode}" data-allow-clear="false" placeholder="Select State">
																	</select>
																</div>
															</div>
															
															<div class="form-group m-form__group row">
																<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
																	<label class="form-control-label">Select City:</label>
																	<select class="form-control" id="cityCode{index}" name="leadAddressVos[{index}].cityCode" data-default="${leadAddressVo.cityCode}" placeholder="Select City" data-allow-clear="true">
																	</select>
																</div>
																<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
																	<label class="form-control-label">ZIP/Postal code:</label>
																	<input type="text" class="form-control m-input" name="leadAddressVos[{index}].pinCode" value="${leadAddressVo.pinCode}" placeholder="ZIP/Postal code" />
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!--end::Portlet-->
										</c:forEach>
									</div>
									<div class="m-demo-icon">
										<div class="m-demo-icon__preview">
											<span class=""><i class="flaticon-plus m--font-primary"></i></span>
										</div>
										<div class="m-demo-icon__class">
										<a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest">Add More Address</a>
										</div>
									</div>
									
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-sm-12">Customer Type:</label>
													<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-10">
														<div class="m-radio-inline">
															<label class="m-radio m-radio--solid m-radio--brand">
																<input type="radio" name="leadType" value="${Constant.CONTACT_CATEGORY_RETAILER}"
																	<c:if test="${leadVo.leadType == Constant.CONTACT_CATEGORY_RETAILER}">checked="checked"</c:if>>Retailer
																<span></span>
															</label> <br>
															<label class="m-radio m-radio--solid m-radio--brand">
																<input type="radio" name="leadType" value="${Constant.CONTACT_CATEGORY_WHOLESALER}"
																	<c:if test="${leadVo.leadType == Constant.CONTACT_CATEGORY_WHOLESALER}">checked="checked"</c:if>> Wholesaler	
																<span></span>
															</label> <br>
															<label class="m-radio m-radio--solid m-radio--brand">
																<input type="radio" name="leadType" value="${Constant.CONTACT_CATEGORY_OTHER}"
																	<c:if test="${leadVo.leadType == Constant.CONTACT_CATEGORY_OTHER}">checked="checked"</c:if>> Other
																<span></span>
															</label>
														</div>
													</div>
												</div>
												
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
												
												<div class="m-form__heading">
													<h6 class="m-form__heading-title">Payment Details</h6>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Payment Type:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-bootstrap-select m_selectpicker" data-toggle="collapse" id="paymentType" name="paymentType">
															<option value="Advance">Advance</option>
															<option selected="selected" value="COD">COD</option>
															<option value="Credit">Credit</option>
														</select>
													</div>
												</div>
												<div class="collapse" id="payment_type_credit">
													<div class="form-group m-form__group row">
														<!-- <label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label> -->
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" name="creditDays" value="${leadVo.creditDays}" placeholder="No. of Credit Days"/>
														</div>
													</div>
													<div class="form-group m-form__group row pt-0">
														<!-- <label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label> -->
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" id="maximumCreditLimit" name="maximumCreditLimit" value="${leadVo.maximumCreditLimit}" placeholder="Max. Credit Limit"/>
														</div>
													</div>
												</div>
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
												
												<div class="m-form__heading">
													<h6 class="m-form__heading-title">Lead Source</h6>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Lead Source:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2" id="leadSource" name="leadSource" placeholder="Select Lead Source" data-allow-clear="true">
															<option value="">Select Lead Source</option>
															<option value="Phone">Phone</option>
															<option value="Reference">Reference</option>
															<option value="Agent">Agent</option>
														</select>
													</div>
												</div>
												<div class="collapse" id="reference_details">
													<div class="form-group m-form__group row">
														<!-- <label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label> -->
														<div class="col-lg-12 col-md-12 col-sm-12">
															<div class="input-group">
				                                                <div class="input-group-prepend">
				                                                    <span class="input-group-text"><i class="la la-user"></i></span>
				                                                </div>
				                                                <input type="text" class="form-control m-input" name="referenceName" value="${leadVo.referenceName}" placeholder="Reference Name"/>
				                                            </div>
														</div>
													</div>
													<div class="form-group m-form__group row pt-0">
														<!-- <label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label> -->
														<div class="col-lg-12 col-md-12 col-sm-12">
															<div class="input-group">
				                                                <div class="input-group-prepend">
				                                                    <span class="input-group-text"><i class="la la-phone"></i></span>
				                                                </div>
				                                                <input type="text" class="form-control m-input" id="referenceMobileno"  name="referenceMobileno" value="${leadVo.referenceMobileno}" placeholder="Reference Mobile no."/>
				                                            </div>
														</div>
													</div>
												</div>
												<div class="collapse" id="agent_details">
													<div class="form-group m-form__group row">
														<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Agent:</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<select class="form-control m-select2" id="agentId" name="agentId" placeholder="Select Agent" data-allow-clear="true">
																<option value="">Select Agent</option>
															</select>
														</div>
													</div>
													<div class="form-group m-form__group row collapse pt-0" id="agent_commission">
														<!-- <label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label> -->
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" id="agentCommission"  name="agentCommission" placeholder="Agent Commission"/>
															<span class="m-form__help">agent commission is in percentage only</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<!-- <div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Bank Details
												</h3>
											</div>			
										</div> -->
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#other__tab" role="tab">Other Details</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#bank_tab" role="tab">Bank Details</a>
												</li>
											</ul>
										</div>
									</div>
									
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="other__tab" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="m-form__heading">
															<h6 class="m-form__heading-title">Interest Details <span class="m-form__help">monthly requirement</span></h6>
														</div>
														<div class="form-group m-form__group row">
															<div class="col-lg-12 col-md-12 col-sm-12	">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--brand">
																		<input type="radio" name="interestedProductRequirements" checked="checked" value="Quantity Based"
																			<c:if test='${leadVo.interestedProductRequirements == "Quantity Based"}'>checked="checked"</c:if>>Quantity Based
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--brand">
																		<input type="radio" name="interestedProductRequirements" value="Amount Based"
																			<c:if test='${leadVo.interestedProductRequirements == "Amount Based"}'>checked="checked"</c:if>>Amount Based
																		<span></span>
																	</label>
																</div>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<!-- <label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label> -->
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" id="interestedProductValue" name="interestedProductValue" value="${leadVo.interestedProductValue}" placeholder="Quantity"/>
															</div>
														</div>
														<div class="form-group m-form__group row">
															<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Product:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<select class="form-control m-select2" name="interestedProductIds" id="interestedProductIds" multiple data-placeholder="Select Product">
																</select>
															</div>
														</div>
														
														<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
														<div class="m-form__heading">
															<h6 class="m-form__heading-title">Previous Supplier</h6>
														</div>
														
														<div class="form-group m-form__group row">
															<!-- <label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label> -->
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" name="previousSupplier" value="${leadVo.previousSupplier}" placeholder="Previous Supplier Name"/>
															</div>
														</div>
														
													</div>
												</div>
											</div>
											<div class="tab-pane" id="bank_tab" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="form-group m-form__group row m--padding-left-0">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Bank Name:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" name="bankName" value="${leadVo.bankName}" placeholder="Bank Name"/>
															</div>
														</div>
														
														<div class="form-group m-form__group row m--padding-left-0">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Branch Name:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" name="bankBranch" value="${leadVo.bankBranch}" placeholder="Branch Name"/>
															</div>
														</div>
														
														<div class="form-group m-form__group row m--padding-left-0">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Account No.:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" name="bankAccountNo" value="${leadVo.bankAccountNo}" placeholder="Account No."/>
															</div>
														</div>
														
														<div class="form-group m-form__group row m--padding-left-0">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">IFSC Code:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" name="bankIfsc" value="${leadVo.bankIfsc}" placeholder="IFSC Code"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="savelead">
									Submit
								</button>
								
								<a href="/lead" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/lead/lead-script.js" type="text/javascript"></script>
	
	<%@include file="../global/location-ajax.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#gstType").val("${leadVo.gstType}").trigger("change");
			$("#paymentType").val("${leadVo.paymentType}").trigger("change");
			
			$("#leadSource").val("${leadVo.leadSource}").trigger("change");
			
			<c:if test="${leadVo.agentId != 0}">
			$("#agentId").val("${leadVo.agentId}").trigger("change");
			</c:if>
		});
	</script>
	
</body>
<!-- end::Body -->
</html>