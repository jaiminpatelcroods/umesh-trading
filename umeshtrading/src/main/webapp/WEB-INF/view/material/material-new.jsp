<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ page import= "com.croods.umeshtrading.constant.Constant" %>
     
<!DOCTYPE html>
<html>
<head>
	<%@include file="../header/head.jsp" %>
	<title>New ${displayType}</title>
	<style type="text/css">
		.select2-container {
			display: block;
		}
		table .select2-container {
			width: 100% !important;
		}
		
		/* For Firefox */
		input[type='number'] {
		    -moz-appearance:textfield;
		}
		/* Webkit browsers like Safari and Chrome */
		input[type=number]::-webkit-inner-spin-button,
		input[type=number]::-webkit-outer-spin-button {
		    -webkit-appearance: none;
		    margin: 0;
		}
	</style>
</head>

<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New ${displayType}</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath() %>/material/${type}" class="m-nav__link">
										<span class="m-nav__link-text">${displayType}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="material_form" action="<%=request.getContextPath() %>/material/${type}/create" method="post">
						<input type="hidden" name="taxType" id="taxType" value="0"/>
						<input type="hidden" name="total" id="total" value="0"/>
						<input type="hidden" id="state_code" value="${sessionScope.stateCode}"/>
						<input type="hidden" id="roundoff" name="roundoff" value="0"/>
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<!-- <div class="m-portlet" style="min-height: 375.5px;"> -->
								<div class="m-portlet">
									<!-- <div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													General Details
												</h3>
											</div>			
										</div>
									</div> -->
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">
														<c:if test="${type==Constant.MATERIAL_IN}">Select Supplier:</c:if>
														<c:if test="${type==Constant.MATERIAL_OUT}">Select Customer:</c:if>
													</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="contactVo" name="contactVo.contactId" onchange="getContactInfo(0)" placeholder="Select Company">
																<option value=""></option>
																<c:forEach items="${ContactList}" var="ContactList">
																	<option value="${ContactList.contactId}">
																		${ContactList.companyName}
																	</option>
																</c:forEach>
															</select>
															<!-- <div class="input-group-append">
																<button class="btn btn-outline-metal" type="button" onclick="setContactModal('customers')" ><i class="fa fa-plus"></i></button>
															</div> -->
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span id="lblContactGSTIN">-</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">-</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 m--hide" >
												<div class="m-section mt-3 m--margin-bottom-15">
													<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
													<h3 class="m-section__heading">Billing Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted" data-address-message="">Billing Address is not provided</small></h5>
													<div class="m-section__content m--hide" id="purchase_billing_address">
														<input type="hidden" name="billingAddressId" id="billingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12" style='word-wrap: break-word;'>																
																<h5 class=""><small class="text-muted" data-address-name=""></small></h5>
																<h5 class=""><small class="text-muted" data-address-company-name=""></small></h5>
																<p class="mb-0">
																	<span data-address-line-1=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode=""></span>
																	<span data-address-city=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country=""></span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""></span></span>
																</p>
																<button type="button" class="btn btn-link" data-toggle="m-popover" data-trigger="click" 
																	title="Billing Address <a href='#' data-popover-close='' class='m--font-bolder m-link m-link--state  m-link--danger float-right'>Cancel</a>" data-html="true" data-content="" id="billing_address_btn">Change Address</button>
																
															</div>
														</div>
													</div>
												<%-- </c:if> --%>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 m--hide">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Shipping Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted" data-address-message="">Shipping Address is not provided</small></h5>
													<div class="m-section__content m--hide" id="purchase_shipping_address">
														<input type="hidden" name="shippingAddressId" id="shippingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12" style='word-wrap: break-word;'>
																<h5 class=""><small class="text-muted" data-address-name=""></small></h5>
																<h5 class=""><small class="text-muted" data-address-company-name=""></small></h5>
																<p class="mb-0">
																	<span data-address-line-1=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode=""></span>
																	<span data-address-city=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country=""></span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""></span></span>
																</p>
																<button type="button" class="btn btn-link" data-toggle="m-popover" data-trigger="click" 
																	title="Shipping Address <a href='#' data-popover-close='' class='m--font-bolder m-link m-link--state  m-link--danger float-right'>Cancel</a>" data-html="true" data-content="" id="shipping_address_btn">Change Address</button>
																	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
									
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<!-- <div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Bank Details
												</h3>
											</div>			
										</div>
									</div> -->
									<div class="m-portlet__body">
									
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">${displayType} Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" name="materialDate" readonly id="materialDate"  data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">${displayType} No.:</label>
													<div class="col-lg-6 m-form__group-sub m--padding-right-5">
														<input type="text" class="form-control m-input" name="prefix" placeholder="Prefix" value="${materialPrefix}">
													</div>	
													<div class="col-lg-6 m-form__group-sub m--padding-left-5">
														<input type="text" class="form-control m-input" name="materialNo" placeholder="${displayType} No" value="${NewMaterialNo}">
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row">
													
													<label class="col-form-label col-lg-2 col-md-2 col-sm-2 m--padding-top-20">Remarks </label>
													
													<div class="col-lg-10 col-md-10 col-sm-10 m--padding-top-15">
														<textarea class="form-control m-input" rows="1" cols="100" name="note"></textarea>
													</div>	
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<!-- <div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Address Details
												</h3>
											</div>			
										</div> -->
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
														Product Details
													</a>
												</li>
												<!-- Jaimin <li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab">
														Terms & Condition / Note
													</a>
												</li> -->
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<!-- <div class="col-lg-4 col-md-4 col-sm-12">
														<div class="form-group m-form__group">
											        		<input type="text" class="form-control m-input" id="searchBarcode" placeholder="Barcode" >
											        	</div>
													</div> -->
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    
															    <thead>
															     <tr>
															        <!-- <th>#</th> -->
															        <th style="width: 30px;">#</th>
															        <th style="width: 300px;">Product</th>
															        <th style="width: 180px;">Place</th>
															        <th style="width: 180px;">Floor</th>
															        <th style="width: 180px;">Rack</th>
															        <th style="width: 180px;  text-align: right;">Qty</th>
															        <th style="width: 180px;  text-align: right;">Price</th>
															        <th style="width: 180px;  text-align: right;">Discount</th>
															        <th style="width: 180px;  text-align: right;">Product Tax</th>
															        <th style="width: 180px;  text-align: right;">Total</th>
															        <th style="width: 40px;"></th>
															        
															      </tr>
															    </thead>
															    <tbody data-material-list="">
															    	<tr data-material-item="template" class="m--hide">
															    		<td class="" style="width: 50px;" data-item-index=""></td>
																        <td class="" style="width: 200px;">
																        	<div class="form-group m-form__group p-0">
																				<select class="form-control" id="productId{index}" onchange="getProductInfo({index})" name="materialItemVos[{index}].productVo.productId" placeholder="Product">
																					<option value="">Select Product</option>
																					<c:forEach items="${ProductList}" var="ProductList">
																						<option value="${ProductList.productId}">${ProductList.name}</option>
																					</c:forEach>
																				</select>
																			</div>
																			<div class="form-group m-form__group p-0">
																				<textarea class="form-control"  id="productDescription{index}" name="materialItemVos[{index}].productDescription" placeholder="Description"></textarea>
																			</div>
																        </td>
																        
																        
																        <c:if test="${type != Constant.MATERIAL_OUT}">
																        <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<select class="form-control" id="placeId{index}" onchange="getFloorList({index})" name="materialItemVos[{index}].placeVo.placeId" >
																					<option value="">Select Place</option>
																					<c:forEach items="${PlaceList}" var="PlaceList">
																						<option value="${PlaceList.placeId}">${PlaceList.placeCode}</option>
																					</c:forEach>
																				</select>
																        	</div>
																        </td>
																        <td class="" style="width: 200px;">
																        	<div class="form-group m-form__group p-0">
																				<select class="form-control" id="floorId{index}" onchange="getRackList({index})" name="materialItemVos[{index}].floorVo.floorId" >
																				</select>
																			</div>
																		</td>
																        <td class="" style="width: 200px;">
																        	<div class="form-group m-form__group p-0">
																				<select class="form-control" id="rackId{index}" name="materialItemVos[{index}].rackVo.rackId">
																				</select>
																			</div>
																		</td>
																        </c:if>
																        
																        
																        
																        <c:if test="${type == Constant.MATERIAL_OUT}">
																        <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<select class="form-control" id="placeId{index}" onchange="getFloorListForSales({index}),getQtyPriceForSales({index})" name="materialItemVos[{index}].placeVo.placeId" >
																					<option value="">Select Place</option>
																				</select>
																        	</div>
																        </td>
																        <td class="" style="width: 200px;">
																        	<div class="form-group m-form__group p-0">
																				<select class="form-control" id="floorId{index}" onchange="getRackListForSales({index})" name="materialItemVos[{index}].floorVo.floorId" >
																				</select>
																			</div>
																		</td>
																        <td class="" style="width: 200px;">
																        	<div class="form-group m-form__group p-0">
																				<select class="form-control" id="rackId{index}" name="materialItemVos[{index}].rackVo.rackId">
																				</select>
																			</div>
																		</td>
																        </c:if>
																        
																        
																        <td class="" style="width: 100px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control m-input text-right" id="qty{index}" name="materialItemVos[{index}].qty" onchange="setProductAmount({index})" placeholder="Qty" value="0" data-toggle="m-popover" data-trigger="focus" data-placement="top" data-html="true" title="Available Qty" data-content="" autocomplete="off">
																        	</div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control m-input text-right" id="price{index}" name="materialItemVos[{index}].price" onchange="setProductAmount({index})" placeholder="Price" data-toggle="m-popover" data-trigger="focus" data-placement="top" data-html="true" title="Price" data-content="0.0" autocomplete="off" step="Any">
																        	</div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control m-input text-right" id="discount{index}" name="materialItemVos[{index}].discount" onchange="setProductAmount({index})" placeholder="Discount" value="0" >
																        	</div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<!-- <input type="text" class="form-control m-input text-right" readonly="readonly" id="taxId{index}" name="materialItemVos[{index}].taxVo.taxId" onchange="setProductAmount({index})" placeholder="Tax" value="0"> -->
																        		<select class="form-control" id="taxId{index}" onchange="setTaxInfo({index})" name="materialItemVos[{index}].taxVo.taxId" placeholder="Product Tax">
																					<option value="">Select Tax</option>
																					<c:forEach items="${taxList}" var="taxList">
																						<option value="${taxList.taxId}" name="${taxList.taxRate}">${taxList.taxName}(${taxList.taxRate}%)</option>
																					</c:forEach>
																				</select>
																        	</div>
																        </td> 
																         <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control m-input text-right" readonly="readonly" id="total{index}" name="materialItemVos[{index}].total" onchange="setProductTotal({index})" placeholder="Total" value="0">
																        	</div>
																        </td>
																        <td class="" style="width: 40px;">
																        	<a href="#" data-item-remove="" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-times"></i></a>
																        	<input type="hidden" id="oldProductRackQty{index}"  name="materialItemVos[{index}].oldProductRackQty" value=""/>
																        	<input type="hidden" id="taxAmount{index}"  name="materialItemVos[{index}].taxAmount" value=""/>
																        	<input type="hidden" id="taxRate{index}" name="materialItemVos[{index}].taxRate"  value=""/> 
																        	<input type="hidden"  id="discountType{index}" name="materialItemVos[{index}].discountType" value="Percentage"/>
																        </td>
															    	</tr> 
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th>
															        	<div class="m-demo-icon mb-0">
																			<div class="m-demo-icon__preview">
																				<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																			</div>
																			<div class="m-demo-icon__class">
																			<a href="#" data-toggel="modal" class="m-link m--font-boldest" id="add_product"> Add More Product</a>
																			</div>
																		</div>
															        </th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id="product_total_qty">0</span></th>
															        <th><span class="m--font-boldest float-right" id="product_total_price">0</span></th>
															        <th><span class="m--font-boldest float-right" id="product_total_discount">0</span></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id="product_sub_total">0</span></th>
															        <th></th>
															      </tr>
															    </tfoot>
															 </table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet" style="margin-top: -2.2rem">
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m--padding-bottom-15">
											<div class="row m--padding-top-20 m--padding-left-20 m--padding-bottom-0 m--padding-right-20">
												<div class="col-lg-4 col-md-4 col-sm-12 ">
													<!-- <div class="m-demo-icon">
														<div class="m-demo-icon__preview " >
															<span class=""><i class="flaticon-plus m--font-primary"></i></span>
														</div>
														<div class="m-demo-icon__class">
															<a href="#additional_charge_div" id="additional_charge_button" data-toggle="collapse" class="m-link m--font-boldest">Add Additional Charges</a>
														</div>
													</div> -->
													<table class="table table-sm m-table table-striped mb-0 m--margin-left-20 collapse" id="tax_summary_table">
													  	<thead>
													  		<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax</th>
													    		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Rate</th>
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Amount</th>
														    </tr>
													  	</thead>
													  	<tbody>
													    	<tr class="row m--hide" data-tax-item="template">
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<table class="table m-table text-right mb-0">
													  	<tbody>
													    	<tr class="row ">
														      	<th scope="row" class="col-lg-8 col-md-8 col-sm-12 m--font-info"><a href="#tax_summary_table" data-toggle="collapse" class="m-link m-link--info m-link--state">Tax Amount</a></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="tax_amount">0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h6>Total Amount</h6></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="total_amount">0</h6></td>
													    	</tr>
													    	<tr class="row"> 
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12 ">
													      			<a href="javascript:void(0);" data-toggle="m-popover" data-trigger="click" title="Roundoff" data-html="true" id="roundoff_edit" data-content="" class="m-link m-link--info m-link--state" >Roundoff</a>
													      		</th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="round_off">0.0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h4>Net Amount</h4></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h3 id="net_amount">0</h3></td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
											</div>
											<!-- <a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest m--margin-left-30">Show Tax Details</a> -->
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="save_material">
									Submit
								</button>
								<a href="<%=request.getContextPath() %>/material/${type}" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
						
						
						
						<!--begin::Modal-->
						<div class="modal fade" id="printType" tabindex="-1" role="dialog"
							aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">
											Sales Print Type
										</h5>
										<button type="button" data-dismiss="modal" aria-label="Close"
											class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only  delete-row-btn">
											<i class="fa fa-close" aria-hidden="true"></i>
										</button>
									</div>
									<div class="modal-body">
										<div class="form-group m-form__group row">
											<label for="example-text-input"
												class="col-lg-4 col-md-4 col-sm-12 col-form-label"></label>
	
											<div class="col-4">
												<label class="m-radio m-radio--bold m-radio--state-brand">
													<input type="radio" name="radios" value="a4" id="a4"
													checked="checked" /> A4 INVOICE
													<span></span>
												</label>
											</div>
	
											<div class="col-4">
												<label class="m-radio m-radio--bold m-radio--state-brand">
													<input type="radio" name="radios" value="pos" id="pos" />
													POS <span></span>
												</label>
											</div>
										</div>
									</div>
									
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">
											Close
										</button>
									</div>
	
								</div>
							</div>
						</div>
						<!--end::Modal-->
						
						
					</form>
				</div>
			</div>
		</div>
		<!-- end:: Body -->
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<div class="m-section m--hide" id="roundoff_section">
			<div class="m-section__content">
				<div class="form-group m-form__group row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<input type="text" class="form-control m-input text-right" name="roundoff" id="" placeholder="Roundoff" value="">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-6 col-md-6 col-sm-6">
					<a type="button" class="btn btn-secondary" data-popover-close=''>Cancel</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
					<button type="button" class="btn btn-brand float-right" id="" onclick="setRoundoff()">Set</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="m-section m--hide" id="address_template">
			<div class="m-section__content" data-address-list="">
				<div class="row" data-address-item="">
					<div class="col-lg-12 col-md-12 col-sm-12 mt-3 mb-3"><div class="m-divider"><span></span></div></div>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<h4 class=""><small class="text-muted" data-address-name=""></small> <button type="button" data-change-address="" class="btn btn-outline-brand btn-sm float-right ml-5">Select</button></h4>
						<h5 class=""><small class="text-muted " data-address-company-name="">Alphabet Inc.</small></h5>
						<p class="mb-0">
							<span class="" data-address-line-1="">Shapath IV</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-line-2="">Prahalad Nagar,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-pincode="">380001</span>
							<span class="" data-address-city="">Ahmedavad,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-state="">Gujarat,</span>
							<span class="" data-address-country="">India</span>
						</p>
						<p class="mb-0">
							<span class="m--font-info"><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno="">9714866160</span></span>
						</p>
					</div>
				</div>
			</div>
		</div>
	 
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/formvalidation/formValidation.min.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/assets/vendors/custom/formvalidation/framework/bootstrap.min.js"></script><!-- Form Validation -->
	
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	 
	<%@include file="../global/location-ajax.jsp" %>
	<%-- <%@include file="../global/contact/contact_model_new.jsp" %> --%>
	
	
	
	<script>
	
		var index = 0, additionalChargeId=0, productId=0;
		var _additionalChargeData,_termsAndConditionData,_productData,_floorData; 
	
		var priceValidator = {
				validators : {
					notEmpty : {
						message : 'The price is required'
					},
					stringLength : {
						max : 20,
						message : 'The price must be less than 20 characters long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The price is invalid'
					}
				}
			},
			discountValidator = {
				validators : {
					notEmpty : {
						message : 'Disc Amount is required'
					},
					stringLength : {
						max : 20,
						message : 'The Discount must be less than 20 digit long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'invalid'
					}
				}
			},
			additionalChargeValidator={
				validators: {
					notEmpty: {
						message: 'Select Additional Charge'
		            },
		          
				}	
			},
			productValidator={
					validators: {
						notEmpty: {
							message: 'Select Product'
			            },
			          
					}	
				},
			taxValidator={
					validators: {
						notEmpty: {
							message: 'Select Tax '
			            }, 
					}	
				},
			floorValidator={
				validators: {
					notEmpty: {
						message: 'Select Floor '
		            }, 
				}	
			},
			placeValidator={
				validators: {
					notEmpty: {
						message: 'Select Place '
		            }, 
				}	
			},
			rackValidator={
				validators: {
					notEmpty: {
						message: 'Select Rack '
		            }, 
				}	
			},
			/* batchNoValidator={
					validators: {
						notEmpty: {
							message: 'The BatchNo is required'
			            },
			          
					}	
				}, */
			materialValidator={
				validators: {
					notEmpty: {
						message: ' No is required'
		            },
		          
				}	
			}
		 	additionalChargeAmountValidator = {
				validators : {
					notEmpty : {
						message : 'The Amount is required'
					},
					stringLength : {
						max : 20,
						message : 'The Amount must be less than 20 characters long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid'
					}
				}
			},
			qtyValidator = {
				validators : {
					notEmpty : {
						message : 'The Qty is required'
					},
					stringLength : {
						max : 20,
						message : 'The Qty must be less than 20 characters long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,4})?))$/,
						message : 'The Qty is invalid'
					}
				}
			};
		 
		
		$(document).ready(function() {
			
			//$("#ContactVo").select2();
			$("#billing_address_btn").attr("data-content",$("#address").html());
			$roundoffSection = $("#roundoff_section").clone();
			$roundoffSection.find("[name='roundoff']").attr("id","edit_roundoff");
			$("#roundoff_edit").attr("data-content",$roundoffSection.html());
			
			$("#roundoff_edit").click(function (e){
				//$(this).stopPropagation();
				$("#edit_roundoff").val($("#round_off").text());
				$("#edit_roundoff").focus();
			});
			
			try {
	  			$('#materialDate').datepicker('setDate','today');
	  		}catch(e){	
	  			$('#materialDate').datepicker('setDate','<%=session.getAttribute("firstDateFinancialYear")%>');
	  		}
	  		
	  		<%-- Jaimin try {
	  			$('#dueDate').datepicker('setDate','today');
	  		}catch(e){	
	  			$('#dueDate').datepicker('setDate','<%=session.getAttribute("firstDateFinancialYear")%>');
	  		} --%>
	  		
			$(document).on("click", ".popover a[data-popover-close]" , function(){
				$(this).parents(".popover").popover('hide');
				return false;
			});
			 
			$("body").on("click",'button[data-change-address]',function() {
				var id,$selectedAddress=$(this).closest('[data-address-item]');
				
				if($(this).attr("data-change-address") == 'billing') {
					id='#purchase_billing_address';
					$("#billingAddressId").val($selectedAddress.attr("data-address-item"));
				} else {
					id='#purchase_shipping_address';
					$("#lblPlaceofSupply").text($selectedAddress.find("[data-address-state]").text());
					$("#shippingAddressId").val($selectedAddress.attr("data-address-item"));
				}
				
				$(id).find("[data-address-name]").html($selectedAddress.find("[data-address-name]").text()).end()
					.find("[data-address-company-name]").html($selectedAddress.find("[data-address-company-name]").text()).end()
					.find("[data-address-line-1]").html($selectedAddress.find("[data-address-line-1]").text()).end()
					.find("[data-address-line-2]").html($selectedAddress.find("[data-address-line-2]").text()).end()
					.find("[data-address-pincode]").html($selectedAddress.find("[data-address-pincode]").text()).end()
					.find("[data-address-city]").html($selectedAddress.find("[data-address-city]").text()).end()
					.find("[data-address-state]").html($selectedAddress.find("[data-address-state]").text()).end()
					.find("[data-address-country]").html($selectedAddress.find("[data-address-country]").text()).end()
					.find("[data-address-city]").attr("data-address-city",$selectedAddress.find("[data-address-city]").attr("data-address-city")).end()
					.find("[data-address-state]").attr("data-address-state",$selectedAddress.find("[data-address-state]").attr("data-address-state")).end()
					.find("[data-address-country]").attr("data-address-country",$selectedAddress.find("[data-address-country]").attr("data-address-country")).end()
					.find("[data-address-phoneNo]").html($selectedAddress.find("[data-address-phoneNo]").text()).end()
					.removeClass("m--hide").end()
					.find("[data-address-message]").addClass("m--hide").end();
				  
				  $selectedAddress.closest(".popover").find("a[data-popover-close]").click();
				  
				  changeTaxType();
			});
			
			$(document).on("click",'#add_product',function(e) {
				e.preventDefault();
				addProduct();
			});
			
			$("#product_table").on("click",'a[data-item-remove]',function(e) {
				//var i=$(this).closest("[data-material-item]").attr("data-material-item");
				e.preventDefault();
				var i=$(this).closest("[data-material-item]").attr("data-material-item");
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].productVo.productId");
				/* Jaimin $('#material_form').formValidation('removeField',"materialItemVos["+i+"].batchNo"); */
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].placeVo.placeId");
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].floorVo.floorId");
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].rackVo.rackId");
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].qty");
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].price");
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].discount");
				$('#material_form').formValidation('removeField',"materialItemVos["+i+"].taxVo.taxId"); 
				
				$(this).closest("[data-material-item]").remove();
				   
				setProductSubTotal();
				setProductSrNo();
			});
			 
			 
			/* Jaimin $(document).on("click",'#additional_charge_button',function(e) {
				getAdditionalCharge();
			});
			
			$(document).on("click",'#add_additional_charge',function(e) {
				e.preventDefault();
				addAdditionalCharge();
			});
			
			$(document).on("click",'#additional_charge_cancel',function(e) {
				
				var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
				
				$additionalChargeItem.each(function (){
					var i=$(this).attr("data-charge-item");
					$('#material_form').formValidation('removeField',"materialAdditionalChargeVos["+i+"].additionalChargeVo.additionalChargeId");
					$('#material_form').formValidation('removeField',"materialAdditionalChargeVos["+i+"].amount");
					
					if($(this).find("input[id*='materialAdditionalChargeId']" ).val()) {
						$("#deleteAdditionalChargeIds").val($("#deleteAdditionalChargeIds").val() + $(this).find("input[id*='materialAdditionalChargeId']" ).val() + ",");
					}
				});
				
				$("#additional_charge_table").find("[data-charge-item]").not(".m--hide").remove();
				$("#additional_charge_button").closest(".m-demo-icon").removeClass("m--hide");
			});
			$("#additional_charge_table").on("click",'a[data-item-remove]',function(e) {
				var i=$(this).closest("[data-charge-item]").attr("data-charge-item");
				e.preventDefault();
				$('#material_form').formValidation('removeField',"materialAdditionalChargeVos["+i+"].additionalChargeVo.additionalChargeId");
				$('#material_form').formValidation('removeField',"materialAdditionalChargeVos["+i+"].amount");
				$(this).closest("[data-charge-item]").remove();
				
				setAdditionalChargeSubTotal();
				setAdditionalChargeSrNo();
				setTaxSummary();
			}); */
			
			$("#save_material").click(function (){
				
				if($("#product_table").find("[data-material-item]").not(".m--hide").length == 0) {
					toastr.options = {
					  "closeButton": true,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-top-center",
					  "preventDuplicates": true,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};
					toastr.error("","Add atleast one Product.");
					
					return false;
				}
				
				if ($('#material_form').data('formValidation').isValid() == null) {
					$('#material_form').data('formValidation').validate();
				}
					
				if($('#material_form').data('formValidation').isValid() == true)
				{
					$("#product_table").find("[data-material-item='template']").remove();
					// Jaimin $("#product_table").find("[data-material-item='template']").remove();
					// Jaimin $("#additional_charge_table").find("[data-charge-item='template']").remove();
					$("#total").val($("#net_amount").text());
					$("#roundoff").val($("#round_off").text());
					
					/* Jaimin if($("#paymentTermVo").val() == "") {
						$("#paymentTermVo").remove();
					} */
					
					<c:if test="${type==Constant.MATERIAL_OUT}">
						//$('#printType').modal('show');
					</c:if>
				}
				
				
				
				
			});
			
			
			/* Jaimin $("#termsAndConditionIds").val("${termsAndConditionIds}");
			
			$(document).on("click",'#terms_condition_button',function(e) {
				getTermsAndCondition();
			}); */
			 
			
			/* Jaimin if("${contactId}" != "") {
				$("#contactVo").val("${contactId}").trigger("change");
			} */
			
			//-------------------- Material Validation -------------------------------
			$('#material_form').formValidation({
				framework: 'bootstrap',
				excluded: ":disabled",
				/*live:'disabled', */
				button: {
					selector: "#save_material",
					disabled: "disabled"
				},
				icon : null,
				fields : {
					"contactVo.contactId" : {
						validators : {
							notEmpty : {
								message : 'Select Company'
							},
						}
					},
					materialDate : {
						validators : {
							notEmpty : {
								message : 'The Material Date is required'
							}
						}
					},
					/* reverseCharge : {
						validators : {
							notEmpty : {
								message : 'select Reverse Charge'
							}
						}
					}, */
					prefix : {
						validators : {
							notEmpty : {
								message : 'The Prefix is required'
							},
							stringLength : {
								max : 20,
								message : 'The Prefix must be less than 20 characters long'
							},
							regexp : {
								regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
								message : 'Prefix is not valid'
							}
						}
					},
					materialNo : {
						verbose : false,
						validators : {
							notEmpty : {
								message : ' No is required. '
							},
							stringLength : {
								max : 8,
								message : ' Max 8 digit no. '
							},
							regexp : {
								regexp : /^[0-9]+$/,
								message : ' Only consist of number. '
							}
						}
					},
				}
			});
			//-------------------- End Material Validation ---------------------------
		});//End Document Ready
		
		 
		
		function getContactInfo(change) {
			var id=$("#contactVo").val();
			var contactType;
			if(${type == Constant.MATERIAL_IN}) 
			{ 
				contactType = '${Constant.CONTACT_SUPPLIER}';
			}else{
				contactType = '${Constant.CONTACT_CUSTOMER}';
			}
			
			$.post("<%=request.getContextPath()%>/contact/"+contactType+"/"+id+"/json", {
					
			}, function (data, status) {
				
				
				if(status == 'success') {
					
					$("#lblContactGSTIN").text(data.gstin=='' ? "-" : data.gstin);
					
					address=data.contactAddressVos;
					
					var $template = $('#address_template'),
			        $clone = $template.clone()
			        		.removeClass('m--hide')
					        .removeAttr('id')
					        .attr('id', "billing_address");
					        //.insertBefore($template);
					
					$addressList=$clone.find("[data-address-list]").clone();
					$clone.find("[data-address-list]").html("");
					$.each(data.contactAddressVos, function( key, value ) {
						
						$addressItem=$addressList.clone();
						
						$addressItem.find("[data-address-item]").attr("data-address-item",value.contactAddressId).end()
									.find("[data-address-name]").html(value.name).end()
									.find("[data-address-company-name]").html(value.companyName).end()
									.find("[data-address-line-1]").html(value.addressLine1).end()
									.find("[data-address-line-2]").html(value.addressLine2).end()
									.find("[data-address-pincode]").html(value.pinCode).end()
									.find("[data-address-city]").html(value.cityName).end()
									.find("[data-address-state]").html(value.stateName).end()
									.find("[data-address-country]").html(value.countriesName).end()
									.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
									.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
									.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
									.find("[data-address-phoneNo]").html(value.phoneNo == "" ? "Mobile no. is not provided": value.phoneNo).end();
						
						if(key == 0) {
							$addressItem.find(".m-divider").parent().remove();
							
							if(change == 0) {
								$("#purchase_billing_address,#purchase_shipping_address").find("[data-address-name]").html(value.name).end()
									.find("[data-address-company-name]").html(value.companyName).end()
									.find("[data-address-line-1]").html(value.addressLine1).end()
									.find("[data-address-line-2]").html(value.addressLine2).end()
									.find("[data-address-pincode]").html(value.pinCode).end()
									.find("[data-address-city]").html(value.cityName).end()
									.find("[data-address-state]").html(value.stateName).end()
									.find("[data-address-country]").html(value.countriesName).end()
									.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
									.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
									.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
									.find("[data-address-phoneNo]").html(value.phoneNo == "" ? "Mobile no. is not provided": value.phoneNo).end()
									.removeClass("m--hide").end()
									.find("[data-address-message]").addClass("m--hide").end();
								
								$("#lblPlaceofSupply").text(value.stateName);
								$("#shippingAddressId").val(value.contactAddressId);
								$("#billingAddressId").val(value.contactAddressId);
							}
						}
						//console.log($addressItem.html());
						$clone.find("[data-address-list]").append($addressItem.html());	
						//console.log("key::"+key)
						//console.log($clone.find("[data-address-list]").html());
						
					});
					
					$clone.find("[data-change-address]").attr("data-change-address","billing");
					$("#billing_address_btn").attr("data-content",$clone.html());
					
					$clone.find("[data-change-address]").attr("data-change-address","shipping");
					$("#shipping_address_btn").attr("data-content",$clone.html());
					
					if(change == 0) {
						changeTaxType();
					}
					//alert($(".m-section__content").find("[data-address-list]:hidden").length);
					
					/* $(".m-section__content").find("[data-change-address]").click( function(){
						alert($(this).attr("data-change-address"));
					}); */
					$('#material_form').formValidation('removeField',"materialVo.materialId"); 
					<c:if test="${type==Constant.MATERIAL_IN || type==Constant.MATERIAL_OUT}">
					//loadMaterial();
					$('#material_form').formValidation('addField',"materialVo.materialId",materialValidator); 
					</c:if>
				} else {
					console.log("status: "+status);
				}
			});
		}
		
		
		<%-- Jaimin function loadMaterial() {
			
			var id = $("#contactVo").val();
			
			if(id != "") {
				
				$.get("<%=request.getContextPath()%>/material/${Constant.JOBWORK_ORDER}/contact/" + id + "/json", {
					
				}, function (data, status) {
					if(status == 'success') {
						$("#materialId").empty();
						$("#materialId").append($('<option></option>').val("").html("Select Material"));
						$.each(data, function (key, value) {
							$("#materialId").append($('<option></option>').val(value.materialId).html(value.prefix + value.materialNo).attr("name",value.type));
						});
						
					}
				});
			}
		} --%>
		
		
		
		//---------------------- Product Methods --------------------------------
		 
		function setTaxInfo(i) {  
			$("#taxRate"+i).val($("#taxId"+i).children("option:selected").attr("name")); 
			setProductAmount(i);
		} 
		
		function addProduct() {
			 
			//if(_productData != undefined) {
				
				
				$productItemTemplate=$("#product_table").find("[data-material-item='template']").clone();
				console.log($productItemTemplate);
				$productItemTemplate.removeClass("m--hide").attr("data-material-item",productId);
				$productItemTemplate.find("input[type='text'],input[type='number'],input[type='hidden'],select,textarea").each(function (){
					
					n=$(this).attr("id");
					n ? $(this).attr("id",n.replace(/{index}/g,productId)) : "";
					
					n=$(this).attr("name");
					n ? $(this).attr("name",n.replace(/{index}/g,productId)) : "";
					 
					$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,productId)) : "";

				});
				
				console.log($productItemTemplate);

				//$('#material_form').formValidation('removeField',"materialItemVos[{index}].price");
				
				$("#product_table").find("[data-material-list]").append($productItemTemplate);

				/* $.each(_productData, function (key, value) {
					alert(value.productId)
					$("#productId"+productId).append($('<option></option>').val(value.productId).html(value.name));
				});*/
				
				$("#productId"+productId).addClass("m-select2");
				$("#productId"+productId).select2({placeholder:"Select Product",allowClear:0});
				
				/* Jaimin $("#batchNo"+productId).addClass("m-select2");
				$("#batchNo"+productId).select2({placeholder:"Select BatchNo",allowClear:0}); */
				
				$("#placeId"+productId).addClass("m-select2");
				$("#placeId"+productId).select2({placeholder:"Select Place",allowClear:0});
				
				$("#taxId"+productId).addClass("m-select2");
				$("#taxId"+productId).select2({placeholder:"Select Tax",allowClear:0});
				
				$("#qty"+productId).popover();
				$("#price"+productId).popover();

				
					$("#price"+productId).prop('type','number');
					$("#price"+productId).prop('required',true);
					$("#price"+productId).prop('min',0);

					$("#discount"+productId).prop('type','number');
					$("#discount"+productId).prop('required',true);
					$("#discount"+productId).prop('min',0);
					
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].productVo.productId",productValidator);
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].placeVo.placeId",placeValidator);
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].floorVo.floorId",floorValidator);
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].rackVo.rackId",rackValidator);
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].qty",qtyValidator);
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].price");
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].discount");
				$('#material_form').formValidation('addField',"materialItemVos["+productId+"].taxVo.taxId",taxValidator); 
				//$('#material_form').formValidation('addField',"materialItemVos["+productId+"].batchNo",batchNoValidator);
				productId++;
				setProductSrNo();
			//}
		}
		
		function setProductSrNo() {
			var $productItem=$("#product_table").find("[data-material-item]").not(".m--hide");
			var i = 0;
			$productItem.each(function (){
				$(this).find("[data-item-index]").html(++i);
			});
		}
		 
		function getProductInfo(i) {
			
			var id = $("#productId"+i).val();
			 
			if(id != "") {
				
				$.post("<%=request.getContextPath()%>/product/"+id+"/json", {
					
				}, function (data, status) {
					if(status == 'success') {
						//setProductOnModal(data);
						_productData = data; 
						//$("#add_product").click();
					
					$('#productDescription'+i).html(data.description);
					getLastPurchasePriceForSales(i);
					
					//getQtyPriceForSales(i,data);
					getTotalAvailableQty(i,data);
					
					if(${type == Constant.MATERIAL_IN})
					{
						
						$('#price'+i).val(data.purchasePrice);
						
						$('#taxId'+i).val(data.purchaseTaxVo.taxId);
						$('#taxId'+i).trigger('change.select2');
						$("#taxRate"+i).val(data.purchaseTaxVo.taxRate);
						$("#discount"+i).val(data.discount);         // work only for purchase (material in)
						$("#discountType"+id).val(data.discountType); //percentage only
						var taxAmount=parseFloat(data.purchasePrice)*parseFloat(data.purchaseTaxVo.taxRate)/100;
					} 
					else if(${type == Constant.MATERIAL_OUT})
					{
						getPlaceListForSales(i,data.productId);

						if(data.applicableType==0)
						{
							$('#price'+i).val(data.purchasePrice);//$('#price'+i).val(data.salePrice);
							$("#discount"+i).prop("max",data.discount);  //need to change type to Number.
							$('#discount'+i).attr("data-fv-lessthan-message","Discount must be less than or equal to Purchase Discount "+data.discount+"%");
 						}else{
							$('#price'+i).val(data.salePrice);
							$("#discount"+i).prop("max",100);	
						}
						
						$('#price'+i).attr("min",data.marketPrice);
						$('#price'+i).attr("data-fv-greaterthan-message","Sale price must be greater than or equal to Market Price");
						//$('#price'+i).attr("data-fv-greaterthan-inclusive",true);
						
						$('#material_form').formValidation('addField',"materialItemVos["+i+"].price");
						$('#material_form').formValidation('addField',"materialItemVos["+i+"].discount");
						
						$('#taxId'+i).val(data.salesTaxVo.taxId);
						$('#taxId'+i).trigger('change.select2');
						$("#taxRate"+i).val(data.salesTaxVo.taxRate);

						
						$("#discountType"+id).val(data.discountType); //wrong as i-->id
						var taxAmount=parseFloat(data.salesPrice)*parseFloat(data.salesTaxVo.taxRate)/100;
					}
					
					
					$productItem=$("#product_table").find("[data-material-item='"+i+"']");
					 
					/* if(data.haveBatchNo == 1 ) {
						$("#batchNo"+i).prop('disabled',false);
						$('#material_form').formValidation('addField',"materialItemVos["+i+"].batchNo",batchNoValidator);
						if(${type == Constant.MATERIAL_OUT}) { 
							getProductBatchList(i);
						}
					}else{
						$("#batchNo"+i).val("");
						$('#material_form').formValidation('removeField',"materialItemVos["+i+"].batchNo");
						$("#batchNo"+i).prop('disabled',true);
						$("#batchNo"+i).select2();
					} */ 
						 
					setProductAmount(i);
					}
				});
			}

			
		}
		
		//For MATERIAL OUT ONLY MAKE PLACE LIST AS PRODUCT AVAILABLE
		function getPlaceListForSales(tr,proId) {

			var i = tr;
			var id = proId;//$("#productId"+i).val();
			
			if(id != "") 
			{
				$.get("<%=request.getContextPath()%>/product/place/"+id, {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#placeId"+i).empty();
						$("#placeId"+i).append($('<option></option>').val("").html("Select Place"));
						$.each(data, function (key, value) {  
							$("#placeId"+i).append($('<option></option>').val(value.placeid).html(value.placecode)); 
						});
						$("#placeId"+i).select2();
					}
				});
			}
		}
		
		function getFloorListForSales(i){
			var i = i;
			var placeid = $("#placeId"+i).val();
			var productid = $("#productId"+i).val();
			
			if(placeid != "") 
			{
				$.get("<%=request.getContextPath()%>/product/"+placeid+"/floor/"+productid, {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#floorId"+i).empty();
						$("#floorId"+i).append($('<option></option>').val("").html("Select Floor"));
						$.each(data, function (key, value) {  
							$("#floorId"+i).append($('<option></option>').val(value.floorid).html(value.floorcode)); 
						});
						$("#floorId"+i).select2();
					}
				});
			}
		}


		function getRackListForSales(i){
			var i = i;
			var floorid = $("#floorId"+i).val();
			var productid = $("#productId"+i).val();
			
			if(floorid != "") 
			{
				$.get("<%=request.getContextPath()%>/product/"+floorid+"/rack/"+productid, {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#rackId"+i).empty();
						$("#rackId"+i).append($('<option></option>').val("").html("Select Rack"));
						$.each(data, function (key, value) {  
							$("#rackId"+i).append($('<option></option>').val(value.rackid).html(value.rackcode)); 
						});
						$("#rackId"+i).select2();
					}
				});
			}
		}
		
		function getQtyPriceForSales(tr) {

			var i = tr;
			var id = $("#productId"+tr).val();
			var placeid = $("#placeId"+tr).val();
			//alert("product id value "+ $("#productId"+tr).val());
			//alert("place id value "+ $("#placeId"+tr).val());
			//var uomcode = data.unitOfMeasurementVo.measurementCode;
			if(id != "") 
			{
				$.get("<%=request.getContextPath()%>/product/"+id+"/qtyandprice/"+placeid, {
				}, function (data, status) {
					if(status == 'success') 
					{
						var json = JSON.parse(data);
						if(typeof json.placeqty === 'undefined')
							$('#qty'+i).attr('data-content', "0.0" +" ");
						else
							$('#qty'+i).attr('data-content', "Place : "+ json.placeqty +" ");
					}
				});
			}
		}
		
		
		function getTotalAvailableQty(tr,data) {

			var i = tr;
			var id = $("#productId"+tr).val();
			var uomcode = data.unitOfMeasurementVo.measurementCode;
			if(id != "") 
			{
				$.get("<%=request.getContextPath()%>/product/"+id+"/qtyandprice", {
				}, function (data, status) {
					if(status == 'success') 
					{
						var json = JSON.parse(data);
						if(typeof json.totalqty === 'undefined')
							$('#qty'+i).attr('data-content', "0.0" +" ");
						else
							$('#qty'+i).attr('data-content', "Total : "+ json.totalqty +" ");
					}
				});
			}
		}


		function getLastPurchasePriceForSales(tr) {

			
			var i  = tr;
			var id = $("#productId"+i).val();
			//var marketprice = data.marketPrice;
			if(id != "") 
			{
				$.get("<%=request.getContextPath()%>/product/"+id+"/lastpurchaseprice", {
				}, function (data, status) {
					if(status == 'success') 
					{
						var json = JSON.parse(data);
						if(typeof json.lastPurchasePrice === 'undefined')
							$('#price'+i).attr('data-content', "0.0" +" ");
						else
							$('#price'+i).attr('data-content', "Last Purchase Price : "+ json.lastPurchasePrice +" <hr>  Market Price : "+ json.marketPrice );
					}
				});
			}
		}
		
		//FOR Material Inward Use
		function getFloorList(i) {
			
			var id = $("#placeId"+i).val();
			if(id != "") 
			{
				$.get("<%=request.getContextPath()%>/floor/place/"+id, {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#floorId"+i).empty();
						$("#floorId"+i).append($('<option></option>').val("").html("Select Floor"));
						$.each(data, function (key, value) {  
							$("#floorId"+i).append($('<option></option>').val(value.floorId).html(value.floorCode)); 
						});
						$("#floorId"+i).select2();
					}
				});
			}
		}
		
		//FOR Material Inward Use
		function getRackList(i) {
			var id = $("#floorId"+i).val();
			if(id != "") 
			{
				$.get("<%=request.getContextPath()%>/rack/floor/"+id, {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#rackId"+i).empty();
						$("#rackId"+i).append($('<option></option>').val("").html("Select Rack"));
						$.each(data, function (key, value) {  
							$("#rackId"+i).append($('<option></option>').val(value.rackId).html(value.rackCode)); 
						});
						$("#rackId"+i).select2();
					}
				});
			}
		}
		
		<%-- Jaimin function getProductBatchList(i) { 
			var id = $("#productId"+i).val(); 
			if(id != "") { 
				$.post("<%=request.getContextPath()%>/product/"+id+"/batchList/json/${type}", { 
				}, function (data, status) {
					if(status == 'success') {
						$("#batchNo"+i).empty();
						$("#batchNo"+i).append($('<option></option>').val("").html("Select BatchNo"));
						$.each(data, function (key, value) {  
							$("#batchNo"+i).append($('<option></option>').val(value).html(value)); 
						});
						if($("#useBatchNo"+i).val()!=null || $("#useBatchNo"+i).val()!=""){
							$("#batchNo"+i).val($("#useBatchNo"+i).val());
						} 
						$("#batchNo"+i).select2();
						
					}
				});
			}
		} --%>
		
		
		function setProductAmount(id) {
			
			var qty, totalQty=0;
	  		var rate;
	  		var discount;
			var taxRate=parseFloat("0.0");
			var taxableAmount,taxAmount = 0.0, totalTaxAmount = 0.0;
			var total=0.0,totalAmount= 0.0;
			var discountType; 
			
			if($("#qty"+id).val()=="") {
				qty=parseFloat("0.0");
			} else {
				qty=parseFloat($("#qty"+id).val());
			}
			
			if($("#taxRate"+id).val()=="") {
				taxRate=parseFloat("0.0");
			} else {
				taxRate=parseFloat($("#taxRate"+id).val());
			}
			
			if($("#price"+id).val()=="") {
				rate=parseFloat("0.0");
			} else {
				rate = parseFloat($("#price"+id).val());
			}
			
			if($("#discount"+id).val()=="") {
				discount=parseFloat("0.0");
			} else {
				discount = parseFloat($("#discount"+id).val());
			}
			
			discountType = $("#discountType"+id).val();
			
			amount=qty*rate;
			
			if(discountType == "Amount") {
				taxableValue=amount-discount;
			} else {
				taxableValue=amount-((amount*discount)/100);
			}
			
			taxAmount=taxableValue*taxRate/100;
			total=taxableValue+taxAmount;
			
			totalAmount += total;
			totalTaxAmount += taxAmount;
			totalQty += qty;
			
			$("#taxAmount"+id).val(taxAmount.toFixed(2));
			
			 
			$productItem=$("#product_table").find("[data-material-item='"+id+"']");
			 
			/* $productItem.find("[data-item-qty]").html(totalQty);
			$productItem.find("[data-item-tax-amount]").html(totalTaxAmount.toFixed(2));
			$productItem.find("[data-item-amount]").html(totalAmount.toFixed(2)); */
			$("#total"+id).val(total.toFixed(2));
		 
			setProductSubTotal();
			
			setTaxSummary(); 
		}
	 
		function setProductSubTotal() {
			var $productItem=$("#product_table").find("[data-material-item]").not(".m--hide"), subTotal=0.0
			, discountTotal=0.0 , qtyTotal=0.0 , priceTotal=0.0;
			
			$productItem.each(function (){
				var materialItemId=$(this).attr("data-material-item");
				
				subTotal += parseFloat($('#total'+materialItemId).val());
				discountTotal += parseFloat($('#discount'+materialItemId).val());
				qtyTotal +=  parseFloat($('#qty'+materialItemId).val());
				priceTotal +=  parseFloat($('#price'+materialItemId).val());
			});
			
			$("#product_sub_total").html(subTotal.toFixed(2));
			$("#product_total_qty").html(qtyTotal.toFixed(2));
			$("#product_total_price").html(priceTotal.toFixed(2));
			$("#product_total_discount").html(discountTotal.toFixed(2));
			
			setAllTotal();
		}
		
		
		//-----------------------------------All Total Methods----------------------- 
		function setAllTotal() {
			var taxAmount = 0.0, totalAmount = 0.0, roundoff=0.0, netAmount = 0.0;
			
			totalAmount = parseFloat($("#product_sub_total").text()); //Jaimin + parseFloat($("#additional_charge_sub_total").text());
			
			var $materialItem=$("#product_table").find("[data-material-item]").not(".m--hide"), subTotal=0.0;
			
			$materialItem.each(function (){
				var materialItemId=$(this).attr("data-material-item");
				taxAmount += parseFloat($('#taxAmount'+materialItemId).val());
				
				//taxAmount += parseFloat($(this).find("[data-item-tax-amount]").html());
			});
			
			/* Jaimin $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide")
			$additionalChargeItem.each(function (){
			taxAmount += parseFloat($(this).find("[data-item-tax-amount]").html());
			}); */
			$("#tax_amount").text(taxAmount.toFixed(2));
			$("#total_amount").text(totalAmount.toFixed(2));
			$("#round_off").text(parseFloat(Math.round(totalAmount.toFixed(2))-totalAmount.toFixed(2)).toFixed(2));
	  		//$("#roundoff").val(parseFloat(Math.round(totalAmount.toFixed(2))-totalAmount.toFixed(2)).toFixed(2));
	  		$("#net_amount").text(Math.round(totalAmount.toFixed(2)));
	  		
		}
		
		//-------------------------Tax Summary Methods---------------------
		function setTaxSummary() {
			
			var split=1;
			
			$("#tax_summary_table").children('tbody').find("[data-tax-item]").not(".m--hide").remove();
			
			var $materialItem = $("#product_table").find("[data-material-item]"),
				$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
			
			$materialItem.each(function() {
				
				var materialItemId=$(this).attr("data-material-item");
				
				if(materialItemId != "template") {
					setTax($("#taxId"+materialItemId).val(), $("#taxAmount"+materialItemId).val(), $("#taxRate"+materialItemId).val());
				}
			});
			
		}
		
		function setTax(taxId, taxAmount, taxRate) {
			var isExist = 0,
				$taxItem = $("#tax_summary_table").children('tbody'), amount=0;
			
			$taxItem.find("[data-tax-item]").not(".m--hide").each(function () {
				
				if($(this).attr("data-tax-item") == taxId) {
					isExist=1;
					if($("#taxType").val() == 1) {
						amount =parseFloat(taxAmount);
						
					} else {
						amount =parseFloat(taxAmount)/2;
					}
					$(this).find('td').eq(2).text(amount + parseFloat($(this).find('td').eq(2).text()));
				}
			});
			
			if(isExist == 0) {
				$taxItemTemplate=$taxItem.find("[data-tax-item='template']").clone();
				$taxItemTemplate.removeClass("m--hide").attr("data-tax-item",taxId);
				
				if($("#taxType").val() == 1) {
					amount = parseFloat(taxAmount);
					taxRate = parseFloat(taxRate);
					
				} else {
					amount =parseFloat(taxAmount)/2;
					taxRate = parseFloat(taxRate)/2;
				}
				
				$taxItemTemplate.find('td').eq(1).text(taxRate + " %");
				$taxItemTemplate.find('td').eq(2).text(amount);
				
				if($("#taxType").val() == 1) {
					
					$taxItemTemplate.find('td').eq(0).text("IGST");
					$taxItem.append($taxItemTemplate);
					
				} else {
					
					$taxItemTemplate.find('td').eq(0).text("CGST");
					$taxItem.append($taxItemTemplate.clone());
					
					$taxItemTemplate.find('td').eq(0).text("SGST");
					$taxItem.append($taxItemTemplate);
				}
			}
		}
		
		 
		function changeTaxType()
		{
			
			/* if($("#sez").prop("checked") == true){
				$("#taxType").val(1);
			} else { */
				if($("#purchase_shipping_address").find("[data-address-state]").attr("data-address-state") == $("#state_code").val()){
					$("#taxType").val(0);
				}
				else{
					$("#taxType").val(1);
				}
			/* } */
			setTaxSummary();
		}
		
		function setRoundoff()
	  	{
	  		
			if($("#edit_roundoff").val()==="")
	  			$("#edit_roundoff").val(0);
	  		
	  		$("#round_off").text($("#edit_roundoff").val());
	  		$("#net_amount").text((parseFloat($("#total_amount").text())+parseFloat($("#edit_roundoff").val())).toFixed(2));
	  		
	  		$("#roundoff_edit").click();
	  	}
		
		
		
		function check(e, c) {

	        var allowedKeyCodesArr = [9, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 37, 39, 109, 189, 46, 110, 190];  // allowed keys
	        if ($.inArray(e.keyCode, allowedKeyCodesArr) === -1) {  // if event key is not in array and its not Ctrl+V (paste) return false;
	            e.preventDefault();
	        } else if ($.trim($(c).val()).indexOf('.') > -1 && $.inArray(e.keyCode, [110, 190]) !== -1) {  // if float decimal exists and key is not backspace return fasle;
	            e.preventDefault();
	        } else {
	            return true;
	        }
	    }
		
	</script>
</body>
<!-- end::Body -->
</html>