<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <%@page import="com.croods.umeshtrading.constant.Constant"%>
     
<!DOCTYPE html>
<html>
<head>
	<%@include file="../header/head.jsp" %>
	<title>${materialVo.prefix}${materialVo.materialNo} | ${displayType}</title>
	<style type="text/css">
		.select2-container {
			display: block;
		}
		table .select2-container {
			
			width: 100% !important;
		}
	</style>
</head>
<%-- <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/vendors/custom/formvalidation/framework/bootstrap.min.css"> --%>
     <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.css">
	 <link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${materialVo.prefix}${materialVo.materialNo}</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath() %>/material/${type}" class="m-nav__link">
										<span class="m-nav__link-text">${displayType}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					
					
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left">
						
						<input type="hidden" name="taxType" id="taxType" value="${materialVo.taxType}"/>
						<input type="hidden" id="roundoff" name="roundoff" value="0"/>
						<input type="hidden" id="termsAndConditionIds" name="termsAndConditionIds" value=""/>
						
						
						
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								
								<!--begin::Portlet-->
								<div class="m-portlet">
									<!-- <div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													General Details
												</h3>
											</div>			
										</div>
									</div> -->
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<c:if test="${type==Constant.MATERIAL_IN}"><span class="m--regular-font-size-">Supplier:</span></c:if>
																		<c:if test="${type==Constant.MATERIAL_OUT}"><span class="m--regular-font-size-">Customer:</span></c:if>
																		<span>
																			<a href="<%=request.getContextPath() %>/contact/${materialVo.contactVo.type}/${materialVo.contactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																				${materialVo.contactVo.companyName}
																			</a>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span id="lblContactGSTIN"><c:if test="${empty materialVo.contactVo.gstin}">N/A</c:if>${materialVo.contactVo.gstin}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">${materialVo.shippingStateName}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<%-- <div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Billing Address</h3>
													<div class="m-section__content" id="purchase_billing_address">
														<input type="hidden" name="billingAddressId" id="billingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12" style='word-wrap: break-word;'>																
																<h5 class=""><small class="text-muted" data-address-name="">${materialVo.billingName}</small></h5>
																Jaimin <h5 class=""><small class="text-muted" data-address-company-name="">${materialVo.billingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${materialVo.billingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${materialVo.billingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${materialVo.billingPinCode}</span>
																	<span data-address-city="">${materialVo.billingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${materialVo.billingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${materialVo.billingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""><c:if test="${empty materialVo.contactVo.companyTelephone}">Mobile no. is not provided</c:if>${materialVo.contactVo.companyTelephone}</span></span>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Shipping Address</h3>
													<div class="m-section__content" id="purchase_shipping_address">
														<input type="hidden" name="shippingAddressId" id="shippingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12" style='word-wrap: break-word;'>
																<h5 class=""><small class="text-muted" data-address-name="">${materialVo.shippingName}</small></h5>
																Jaimin <h5 class=""><small class="text-muted" data-address-company-name="">${materialVo.shippingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${materialVo.shippingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${materialVo.shippingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${materialVo.shippingPinCode}</span>
																	<span data-address-city="">${materialVo.shippingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${materialVo.shippingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${materialVo.shippingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""><c:if test="${empty materialVo.contactVo.companyTelephone}">Mobile no. is not provided</c:if>${materialVo.contactVo.companyTelephone}</span></span>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div> --%>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
									
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<!-- <span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Bank Details
												</h3> -->
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<c:if test="${type==Constant.MATERIAL_OUT}">				
												<li class="m-portlet__nav-item">
													<a href="#" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only" onclick="printJobworkReport()"
														data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
												</li>
												<%-- <li class="m-portlet__nav-item">
													<a href="<%=request.getContextPath() %>/material/${materialVo.type}/${materialVo.materialId}/pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" target="_blank"	
														data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="PDF"><i class="fa fa-file-pdf"></i></a>	
												</li> --%>
												
												
												
												
												<li class="m-portlet__nav-item">
													<!--begin: Dropdown-->
					                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-danger m-btn m-btn--icon m-btn--icon-only">
					                                        <i class="fa fa-file-pdf"></i>
					                                    </a>
					                                    <div class="m-dropdown__wrapper">
				                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
				                                            <div class="m-dropdown__inner">
				                                                <div class="m-dropdown__body">              
				                                                    <div class="m-dropdown__content">
				                                                        <ul class="m-nav">
				                                                           <li class="m-nav__item">
							                                                    <a href="<%=request.getContextPath() %>/material/${materialVo.type}/${materialVo.materialId}/a4/pdf" target="_blank" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																					<span><i class="fa fa-file-pdf"></i><span>A4</span></span>
																				</a>
																				<span></span>
							                                                    <a href="<%=request.getContextPath() %>/material/${materialVo.type}/${materialVo.materialId}/a5/pdf" target="_blank" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																					<span><i class="fa fa-file-pdf"></i><span>A5</span></span>
																				</a>
																				<span></span>
							                                                    <button type="button" onclick="printJobworkReport()" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right">
																					<span><i class="fa fa-file-pdf"></i><span>POS</span></span>
																				</button>
																				
							                                                </li>
							                                            </ul>
				                                                    </div>
				                                                </div>
				                                            </div>
					                                    </div>
					                                </div>
					                                <!--end: Dropdown-->
					                            </li>
												
												
												</c:if>
												<li class="m-portlet__nav-item">
													<!--begin: Dropdown-->
					                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--icon m-btn--icon-only">
					                                        <i class="la la-ellipsis-h"></i>
					                                    </a>
					                                    <div class="m-dropdown__wrapper">
				                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
				                                            <div class="m-dropdown__inner">
				                                                <div class="m-dropdown__body">              
				                                                    <div class="m-dropdown__content">
				                                                        <ul class="m-nav">
				                                                            <!-- <li class="m-nav__section m-nav__section--first">
				                                                                <span class="m-nav__section-text">Quick Actions</span>
				                                                            </li> -->
				                                                             <%-- <li class="m-nav__item">
				                                                                <a href="<%=request.getContextPath()%>/materialin/new" class="m-nav__link">
				                                                                    <i class="m-nav__link-icon fa fa-credit-card"></i>
				                                                                    <span class="m-nav__link-text">Receive Payment</span>
				                                                                </a>
				                                                            </li> --%>
							                                                <!-- <li class="m-nav__separator m-nav__separator--fit">
							                                                </li> -->
							                                                <li class="m-nav__item">
							                                                    <a href="<%=request.getContextPath() %>/material/${type}/${materialVo.materialId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																					<span><i class="flaticon-edit"></i><span>Edit</span></span>
																				</a>
							                                                    <button id="material_delete" type="button" data-url="<%=request.getContextPath() %>/material/${type}/${materialVo.materialId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right">
																					<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																				</button>
							                                                </li>
				                                                        </ul>
				                                                    </div>
				                                                </div>
				                                            </div>
					                                    </div>
					                                </div>
					                                <!--end: Dropdown-->
					                            </li>
												
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
									
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">${displayType} Date:</span>
																		<span>
																			<fmt:formatDate pattern="dd/MM/yyyy" value="${materialVo.materialDate}" />
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">${displayType} No:</span>
																		<span>${materialVo.prefix}${materialVo.materialNo}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Remark :</span>
																		<span>
																			${materialVo.note}
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											
											
											 
										</div>
									</div>
									<%-- <div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid p-0">
											<div class="row">
												 
												<c:if test='${type != Constant.JOBWORK_MATERIAL_IN}'>
													<div class="col-lg-4 col-md-4 col-sm-12">
														<div class="m-widget4">
															<div class="m-widget4__item">
																<div class="m-widget4__info">
																	<span class="m-widget4__title">
																		Due Amount
																	</span><br/>
																	<span class="m-widget4__ext">
																		<span class="m-widget4__number m--font-danger">
																			${materialVo.total - materialVo.paidAmount}
																		</span>
																	</span>
																</div>
																
															</div>
														</div>
													</div>
												
													<div class="col-lg-4 col-md-4 col-sm-12">
														<div class="m-widget4">
															<div class="m-widget4__item">
																<div class="m-widget4__info">
																	<span class="m-widget4__title">
																		Paid Amount
																	</span>
																	<br>
																	<span class="m-widget4__ext">
																		<span class="m-widget4__number m--font-success">
																			${materialVo.paidAmount}
																		</span>
																	</span>
																</div>
																
															</div>
														</div>
													</div>
												</c:if>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="m-widget4">
														<div class="m-widget4__item">
															<div class="m-widget4__info">
																<span class="m-widget4__title">
																	Total Amount
																</span>
																<br>
																<span class="m-widget4__ext">
																	<span class="m-widget4__number m--font-info">
																		${materialVo.total}
																	</span>
																</span>
															</div>
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div> --%>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<!-- <div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Address Details
												</h3>
											</div>			
										</div> -->
										
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
														Product Details
													</a>
												</li>
												<%-- Jaimin <li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab">
														Terms & Condition / Note
													</a>
												</li>
												<c:if test="${type==Constant.JOBWORK_ORDER}">
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_materialIn" role="tab">
															Material In
														</a>
													</li>
												
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_materialOut" role="tab">
															Material Out
														</a>
													</li>
												</c:if> --%>
											</ul>
										</div>
										
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    
															    <thead>
															      <tr>
															        <!-- <th>#</th> -->
															        <th style="width: 30px;">#</th>
															        <th style="width: 300px;">Product</th>
															        <th style="width: 180px; text-align: right;">Place</th>
															        <th style="width: 180px; text-align: right;">Floor</th>
															        <th style="width: 180px; text-align: right;">Rack</th>
															        <th style="width: 180px;  text-align: right;">Qty</th>
															        <th style="width: 180px;  text-align: right;">Price</th>
															        <th style="width: 180px;  text-align: right;">Discount</th>
															        <th style="width: 180px;  text-align: right;">Product Tax</th>
															        <th style="width: 180px;  text-align: right;">Total</th>
															        <th style="width: 40px;"></th>
															      </tr>
															    </thead>
															    <tbody data-material-list="">
															    	<c:set var="index" scope="page" value="0"/>
															    	<c:forEach items="${materialVo.materialItemVos}" var="materialItemVos">
																    	<tr data-material-item="${index}" class="">
																    		<td class="" style="width: 30px;">
																    			<span data-item-index></span>
																    		</td>
																	        <td class="" style="width: 300px;">
																	        	<span class="m--font-info m--font-bolder" data-item-name="">
																	        		${materialItemVos.productVo.name}
																	        	</span>
																				<br/>
																				<span class="m--font-bold" data-item-description="">
																					${materialItemVos.productDescription}
																				</span>
																	        </td>
																	         <td class="" style="width: 180px;"> 
																	        	<div class="p-0 text-right m--font-bolder"  data-item-designno="">
																	        		${materialItemVos.placeVo.placeCode}
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;"> 
																	        	<div class="p-0 text-right m--font-bolder"  data-item-designno="">
																	        		${materialItemVos.floorVo.floorCode}
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;"> 
																	        	<div class="p-0 text-right m--font-bolder"  data-item-designno="">
																	        		${materialItemVos.rackVo.rackCode}
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-qty="">
																	        		${materialItemVos.qty}
																	        	</div>
																	        	 
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-price="">
																	        		${materialItemVos.price}
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-discount="">
																	        		${materialItemVos.discount}
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 m--font-bolder" >
																	        		
																	        		<span class="m--font-info" data-item-tax-name="">${materialItemVos.taxVo.taxName} (${materialItemVos.taxRate} %)</span>
																	        		<span class="float-right">Rs. <span data-item-tax-amount="">${materialItemVos.taxAmount}</span></span>
																	        	</div>
																	        	
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-amount="">0</div>
																	        </td>
																	        <td class="" style="width: 40px;">
																	        	<input type="hidden" id="productId${index}" name="materialItemVos[${index}].productVo.productId" value="${materialItemVos.productVo.productId}"/>
															        			<input type="hidden" id="placeId${index}" name="materialItemVos[${index}].placeVo.placeId" value="${materialItemVos.placeVo.placeId}"/>
															        			<input type="hidden" id="floorId${index}" name="materialItemVos[${index}].floorVo.floorId" value="${materialItemVos.floorVo.floorId}"/>
															        			<input type="hidden" id="rackId${index}" name="materialItemVos[${index}].rackVo.rackId" value="${materialItemVos.rackVo.rackId}"/> 
																	        	<input type="hidden" id="qty${index}" name="materialItemVos[${index}].qty" value="${materialItemVos.qty}"/>
																	        	<input type="hidden" id="price${index}" name="materialItemVos[${index}].price" value="${materialItemVos.price}"/>
																	        	<input type="hidden" id="productDescription${index}" name="materialItemVos[${index}].productDescription" value="${materialItemVos.productDescription}"/>
																	        	<input type="hidden" id="taxAmount${index}" name="materialItemVos[${index}].taxAmount" value="${materialItemVos.taxAmount}"/>
																	        	<input type="hidden" id="taxRate${index}"  name="materialItemVos[${index}].taxRate" value="${materialItemVos.taxRate}"/>
																	        	<input type="hidden" id="taxId${index}" name="materialItemVos[${index}].taxVo.taxId" value="${materialItemVos.taxVo.taxId}"/>
																	        	<input type="hidden" id="discount${index}" name="materialItemVos[${index}].discount" value="${materialItemVos.discount}"/>
																	        	<input type="hidden" id="discountType${index}" name="materialItemVos[${index}].discountType" value="${materialItemVos.discountType}"/> 
																	        	<%-- Jaimin <input type="hidden" id="haveBatchNo${index}" name=""  value="${materialItemVos.productVo.haveBatchNo}" /> --%>
																	        	<input type="hidden" id="total${index}" value="0">
																	        </td>
																    	</tr>
																    	<c:set var="index" scope="page" value="${index+1}"/>
																    </c:forEach>
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id="product_sub_total">0</span></th>
															        <th></th>
															      </tr>
															    </tfoot>
														  	</table>
														</div>
														
													</div>
													
												</div>
											</div>
											<%-- Jaimin <div class="tab-pane" id="m_tabs_7_2" role="tabpanel">
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														
														<table class="table table-sm m-table table-striped" id="terms_and_condition_table">
														  	<thead>
														  		<tr class="row">
															      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
														    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
														    			<span>Terms & Condition</span>
														    		</th>
														    	</tr>
														  	</thead>
														  	<tbody>
																<c:forEach items="${TermsAndCondition}" var="TermsAndCondition" varStatus="status">
																	<tr class="row" data-terms-item="">
																      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index="">
																      		${status.index+1}
																      	</td>
																      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name="">${TermsAndCondition.termsCondition}</td>
															    	</tr>
																</c:forEach>
														  	</tbody>
														</table>
													</div>
													<div class="col-lg-5 col-md-5 col-sm-12 m--margin-left-50">
														<table class="table table-sm m-table  m-table--head-no-border">
														  	<thead>
														  		<tr class="row">
														    		<th scope="row" class="col-lg-12 col-md-12 col-sm-12">Note:</th>
														    	</tr>
														  	</thead>
														  	<tbody>
														    	<tr class="row">
															      	<td class="col-lg-12 col-md-12 col-sm-12">
															      		<span><c:if test="${empty materialVo.note}">No Note.</c:if>${materialVo.note}</span>
															      	</td>
														    	</tr>
														  	</tbody>
														</table>
													</div>
												</div>
											</div> --%>
											<div class="tab-pane" id="m_tabs_materialIn" role="tabpanel">
												<table class="table table-striped-table-bordered table-hover table-checkable" id="materialin_table" style="overflow-x: scroll;" >
													<thead>
														<tr>
															<th>
																#
															</th>
															<th>
																MaterialIn No
															</th>
															<th>
																Date
															</th>
															 
															<th>
																Action
															</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
											<div class="tab-pane " id="m_tabs_materialOut" role="tabpanel">
												<table class="table table-striped-table-bordered table-hover table-checkable" id="materialout_table" style="overflow-x: scroll;" >
														<thead>
															<tr>
																<th>
																	#
																</th>
																<th>
																	Jobwork No
																</th>
																<th>
																	Date
																</th>
																 
																<th>
																	Action
																</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
												</table>
											</div>
										</div>
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							
						</div>
						
						<%-- Jaimin <div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12" id="additional_charge_div">
										
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Additional Charge
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="additional_charge_table">
													    
													    <thead>
													      <tr>
													        <!-- <th>#</th> -->
													        <th style="width: 50px;">#</th>
													        <th style="width: 280px;">Additional Charge</th>
													        <th style="width: 180px;">Amount</th>
													        <th style="width: 180px;">Tax</th>
													        <th style="width: 180px;">Total</th>
													        <th style="width: 40px;"></th>
													      </tr>
													    </thead>
													    <tbody data-charge-list="">
													    	<c:if test='${materialVo.materialAdditionalChargeVos.size() == 0}'>
														    	<tr>
														    		<td colspan="6" class="text-center m--font-bolder">No Additional Charges</td>
														    	</tr>
														    </c:if>
														    
														    <c:set var="additionalChargeIndex" scope="page" value="0"/>
													    	
													    	<c:forEach items="${materialVo.materialAdditionalChargeVos}" var="materialAdditionalChargeVos">
														    	<tr data-charge-item="${additionalChargeIndex}" class="">
														    		<td class="" style="width: 50px;" data-item-index=""></td>
															        <td class="" style="width: 280px;">
															        	<div class="form-group m-form__group p-0">
																			${materialAdditionalChargeVos.additionalChargeVo.additionalCharge}
																		</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="form-group m-form__group p-0">
															        		${materialAdditionalChargeVos.amount}
															        		<input type="hidden" id="additionalChargeAmount${additionalChargeIndex}" value="${materialAdditionalChargeVos.amount}" />
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class=" m--font-bolder">
															        		<span class="m--font-info" data-item-tax-name="">${materialAdditionalChargeVos.taxVo.taxName} (${materialAdditionalChargeVos.taxRate} %)</span>
															        		<span class="float-right">Rs. <span data-item-tax-amount="">${materialAdditionalChargeVos.taxAmount}</span></span>
															        	</div>
															        	
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="text-right m--font-bolder" data-item-amount="">
															        		
															        	</div>
															        </td>
															        <td class="" style="width: 40px;">
															        	<input type="hidden" name="purchaseAdditionalChargeVos[${additionalChargeIndex}].taxAmount" id="additionalChargeTaxAmount${additionalChargeIndex}" value="${materialAdditionalChargeVos.taxAmount}"/>
															        	<input type="hidden" name="purchaseAdditionalChargeVos[${additionalChargeIndex}].taxRate" id="additionalChargeTaxRate${additionalChargeIndex}" value="${materialAdditionalChargeVos.taxRate}"/>
															        	<input type="hidden" name="purchaseAdditionalChargeVos[${additionalChargeIndex}].taxVo.taxId" id="additionalChargeTaxId${additionalChargeIndex}" value="${materialAdditionalChargeVos.taxVo.taxId}"/>
															        </td>
														    	</tr>
														    	<c:set var="additionalChargeIndex" scope="page" value="${additionalChargeIndex+1}"/>
														    </c:forEach>
														</tbody>
														<tfoot>
															<tr>
																<th></th>
																<th>
																</th>
																<th></th>
																<th></th>
																<th><span class="m--font-boldest float-right" id="additional_charge_sub_total">0.0</span></th>
																<th></th>
													      	</tr>
													   </tfoot>
												  	</table>
												</div>
												
											</div>
											
										</div>
										
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							
						</div> --%>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet" style="margin-top: -2.2rem">
									
									<div class="m-portlet__foot m-portlet__foot--fit">
										
										<div class="m-form__actions m-form__actions--solid m--padding-bottom-15">
											<div class="row m--padding-top-20 m--padding-left-20 m--padding-bottom-0 m--padding-right-20">
										
												<div class="col-lg-4 col-md-4 col-sm-12 ">
													
													<table class="table table-sm m-table table-striped mb-0 m--margin-left-20 collapse" id="tax_summary_table">
													  	<thead>
													  		<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax</th>
													    		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Rate</th>
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Amount</th>
														      	
													    	</tr>
													  	</thead>
													  	<tbody>
													    	
													    	<tr class="row m--hide" data-tax-item="template">
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
													    	</tr>
													  	</tbody>
													</table>
													
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<table class="table m-table text-right mb-0">
													  	<tbody>
													    	<tr class="row ">
														      	<th scope="row" class="col-lg-8 col-md-8 col-sm-12 m--font-info"><a href="#tax_summary_table" data-toggle="collapse" class="m-link m-link--info m-link--state">Tax Amount</a></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="tax_amount">0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h6>Total Amount</h6></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="total_amount">0</h6></td>
													    	</tr>
													    	<tr class="row"> 
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12 ">
													      			<h6>Roundoff</h6>
													      		</th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="round_off">0.0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h4>Net Amount</h4></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h3 id="net_amount">0</h3></td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												
											</div>
											<!-- <a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest m--margin-left-30">Show Tax Details</a> -->
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								
							</div>
							
						</div>
						
					</form>
				</div>
				
			</div>
			
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<div id="targetDiv"></div>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/formvalidation/formValidation.min.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/assets/vendors/custom/formvalidation/framework/bootstrap.min.js"></script><!-- Form Validation -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<%-- <script src="<%=request.getContextPath()%>/script/jquery.spring-friendly.js" type="text/javascript"></script> --%>
	<script>
	
		var index = 0, additionalChargeId=0;
		var _additionalChargeData,_termsAndConditionData;
	//$("#address_btn").popover({ content:$("#address").html(), placement:'bottom', });

	//$("#address_btn").popover("toggle");
	/* $('body')
	  .on('mousedown', '.popover', function(e) {
	    console.log("clicked inside popover")
	    e.preventDefault()
	  }); */
	  
	  
	  
	  $(document).ready(function() {
						
			var $purchaseItem=$("#product_table").find("[data-material-item]").not(".m--hide");
			
			$purchaseItem.each(function (){
				setAmount($(this).attr("data-material-item"));
			});
			
	  		setSrNo();
	  		
			var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide"), subTotal=0.0;
			
			$additionalChargeItem.each(function (){
				setAdditionalChargeAmount($(this).attr("data-charge-item"));
			});
			
			setAdditionalChargeSrNo();
			
			$("#round_off").text(${materialVo.roundoff});
	  		$("#net_amount").text((parseFloat($("#total_amount").text())+parseFloat(${materialVo.roundoff})).toFixed(2));
	  		
			$("#material_delete").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
			
			$(".m-tabs__link").click(function(){
				
				if($(this).attr('href') == "#m_tabs_materialOut") {
					
					setMaterialOutDatatable();
				} else if($(this).attr('href') == "#m_tabs_materialIn") {
					setMaterialInDatatable();
				}
				
				
			});
		});//End Document Ready
		
		function getContactInfo(change) {
			var id=$("#contactVo").val();
			
			$.post("<%=request.getContextPath()%>/contact/${Constant.CONTACT_CUSTOMER}/"+id+"/json", {
				
			}, function (data, status) {
				
				
				if(status == 'success') {
					
					$("#lblContactGSTIN").text(data[0].gstin=='' ? "-" : data[0].gstin);
					
					address=data[0].contactAddressVos;
					
					var $template = $('#address_template'),
			        $clone = $template.clone()
			        		.removeClass('m--hide')
					        .removeAttr('id')
					        .attr('id', "billing_address");
					        //.insertBefore($template);
					
					$addressList=$clone.find("[data-address-list]").clone();
					$clone.find("[data-address-list]").html("");
					$.each(data[0].contactAddressVos, function( key, value ) {
						
						$addressItem=$addressList.clone();
						
						$addressItem.find("[data-address-item]").attr("data-address-item",value.contactAddressId).end()
									.find("[data-address-name]").html(value.name).end()
									.find("[data-address-company-name]").html(value.companyName).end()
									.find("[data-address-line-1]").html(value.addressLine1).end()
									.find("[data-address-line-2]").html(value.addressLine2).end()
									.find("[data-address-pincode]").html(value.pinCode).end()
									.find("[data-address-city]").html(value.cityName).end()
									.find("[data-address-state]").html(value.stateName).end()
									.find("[data-address-country]").html(value.countriesName).end()
									.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
									.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
									.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
									.find("[data-address-phoneNo]").html(value.phoneNo == "" ? "Mobile no. is not provided": value.phoneNo).end();
						
						if(key == 0) {
							$addressItem.find(".m-divider").parent().remove();
							
							if(change == 0) {
								$("#purchase_billing_address,#purchase_shipping_address").find("[data-address-name]").html(value.name).end()
									.find("[data-address-company-name]").html(value.companyName).end()
									.find("[data-address-line-1]").html(value.addressLine1).end()
									.find("[data-address-line-2]").html(value.addressLine2).end()
									.find("[data-address-pincode]").html(value.pinCode).end()
									.find("[data-address-city]").html(value.cityName).end()
									.find("[data-address-state]").html(value.stateName).end()
									.find("[data-address-country]").html(value.countriesName).end()
									.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
									.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
									.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
									.find("[data-address-phoneNo]").html(value.phoneNo == "" ? "Mobile no. is not provided": value.phoneNo).end()
									.removeClass("m--hide").end()
									.find("[data-address-message]").addClass("m--hide").end();
								
								$("#lblPlaceofSupply").text(value.stateName);
								$("#shippingAddressId").val(value.contactAddressId);
								$("#billingAddressId").val(value.contactAddressId);
							}
						}
						//console.log($addressItem.html());
						$clone.find("[data-address-list]").append($addressItem.html());	
						//console.log("key::"+key)
						//console.log($clone.find("[data-address-list]").html());
						
					});
					
					$clone.find("[data-change-address]").attr("data-change-address","billing");
					$("#billing_address_btn").attr("data-content",$clone.html());
					
					$clone.find("[data-change-address]").attr("data-change-address","shipping");
					$("#shipping_address_btn").attr("data-content",$clone.html());
					
					if(change == 0) {
						changeTaxType();
					}
					//alert($(".m-section__content").find("[data-address-list]:hidden").length);
					
					/* $(".m-section__content").find("[data-change-address]").click( function(){
						alert($(this).attr("data-change-address"));
					}); */
					
				} else {
					console.log("status: "+status);
				}
			});
		}
		
		function getProductInfo() {
			
			var id = $("#productIdModal").val();
			
			if(id != "") {
				
				
				
				$.post("<%=request.getContextPath()%>/product/variant/"+id+"/json", {
					
				}, function (data, status) {
					if(status == 'success') {
						setProductOnModal(data);
					}
				});
			}
		}
		
		function setProductOnModal(data) {
			
			$("#modalTax").val(data.materialItemIndex ? data.productVo.taxVo.taxName  : data.productVo.taxVo.taxName +" ("+data.productVo.taxVo.taxRate+" %)");
			$("#modalTaxId").val(data.productVo.taxVo.taxId);
			$("#modalTaxRate").val(data.productVo.taxVo.taxRate);
			$("#haveDesignno").val("0");
			$("#modalDiscount").val("0");
			$("#modalDiscountType").val("amount");
					
			var haveDesignno = $("#modalHaveDesignno");
			
			$("#materialItemIndex").val(data.materialItemIndex ? data.materialItemIndex : "-1");
			haveDesignno.val(data.productVo.haveDesignno ? data.productVo.haveDesignno : "0");
			
			$("#modalPurchasePrice").val(data.purchasePrice);
			$("#modalProductVariantId").val(data.productVarientId);
			$("#modalDesignNo").val(data.designNo ? data.designNo : "");
			$("#modalDiscount").val(data.discount ? data.discount : "0");
			$("#modalDiscountType").val(data.discountType ? data.discountType : "amount");
			$("#modalQty").val(data.qty ? data.qty : "1");
		
			if(haveDesignno.val() == 1) {
				$("#designno_div").removeClass("m--hide");
				$("#qty_div").removeClass("col-lg-12 col-md-12");
				$("#qty_div").addClass("col-lg-6 col-md-6");
				$("#modalDesignNo").focus();
				
			} else {
				$("#designno_div").addClass("m--hide");
				$("#qty_div").removeClass("col-lg-6 col-md-6");
				$("#qty_div").addClass("col-lg-12 col-md-12");
				$("#modalQty").focus();
				
			}
			
			/* if(haveVariant.val() == 1) {
				$("#designno_div").removeClass("col-lg-6 col-md-6");
				$("#designno_div").addClass("col-lg-12 col-md-12");
				$("#qty_div").addClass("m--hide");
				$("#variant_div").removeClass("m--hide");
			} else {
				$("#variant_div").addClass("m--hide");
				$("#designno_div").removeClass("col-lg-12 col-md-12");
				$("#designno_div").addClass("col-lg-6 col-md-6");
				$("#qty_div").removeClass("m--hide");
				
			} */
			
		}
		
		function setProduct() {
			//modalDescription
			
			var taxName = $("#modalTax").val(), taxId = $("#modalTaxId").val(), taxRate = parseFloat($("#modalTaxRate").val()),
				haveDesignno = $("#haveDesignno").val(), designno = $("#modalDesignNo").val(), discount = parseFloat($("#modalDiscount").val()),
				discountType = $("#modalDiscountType").val(),
				materialItemIndex = $("#materialItemIndex").val(),
				price = parseFloat($("#modalPurchasePrice").val()), haveVariant = $("#modalHaveVariant").val(),
				productVariantId=$("#modalProductVariantId").val(),
				taxAmount = 0.0, qty = 0, productId=$("#productIdModal").val();
			
			
			var	$materialItemTemplate;
			var mainIndex=index;
			var selector = materialItemIndex==-1 ? "{index}" : materialItemIndex;
			
			if(materialItemIndex == -1) {
				$materialItemTemplate=$("#product_table").find("[data-material-item='template']").clone();
			} else {
				$materialItemTemplate=$("#product_table").find("[data-material-item='"+materialItemIndex+"']");
				mainIndex = materialItemIndex;
			}
			
			
			qty = $("#modalQty").val();
			
			$materialItemTemplate.attr("data-material-item",mainIndex)
				.removeClass("m--hide");
			
			$materialItemTemplate.find("[data-item-qty]").html(qty).end()
				.find("[data-item-name]").html($('#productIdModal option[value="'+$('#productIdModal').val()+'"]').html()).end()
				.find("[name='materialItemVos["+selector+"].productVarientsVo.productVarientId']").val(productVariantId).end()
				.find("[name='materialItemVos["+selector+"].qty']").val(qty).end()
				.find("[name='materialItemVos["+selector+"].price']").val(price).end()
				.find("[name='materialItemVos["+selector+"].taxAmount']").val(taxAmount).end()
				.find("[name='materialItemVos["+selector+"].taxRate']").val(taxRate).end()
				.find("[name='materialItemVos["+selector+"].taxVo.taxId']").val(taxId).end()
				.find("[name='materialItemVos["+selector+"].discount']").val(discount).end()
				.find("[name='materialItemVos["+selector+"].discountType']").val(discountType).end()
				.find("[name='materialItemVos["+selector+"].designNo']").val(designno).end()
				.find("[name='materialItemVos["+selector+"].productDescription']").val($("#modalDescription").val()).end()
				.find("[data-item-description]").html($("#modalDescription").val()).end()
				.find("[data-item-designno]").html(designno).end()
				.find("[data-item-price]").html(price).end()
				.find("[data-item-discount]").html(discount).end()
				.find("[data-item-tax-name]").html(taxName).end()
				.find("[data-item-tax-amount]").html(price).end()
				.find("[data-item-amount]").html(price).end()
				.find("[id='haveDesignno"+selector+"']").val(haveDesignno).end()
				.find("[id='haveDesignno"+selector+"']").attr("id",$materialItemTemplate.find("[id='haveDesignno"+selector+"']").attr("id").replace(/{index}/g,mainIndex)).end();
			
				
			$materialItemTemplate.find("input[type='hidden']").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,index)) : "";
				
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,index)) : "";
			});
			
			if(materialItemIndex == -1) {
				$("#product_table").find("[data-material-list]").append($materialItemTemplate);
				index++;
			}
			
			setAmount(mainIndex)
			setSrNo();
		}
		
		function setAmount(id) {
			
			var qty, totalQty=0;
	  		var rate;
	  		var discount;
			var taxRate=parseFloat("0.0");
			var taxableAmount,taxAmount = 0.0, totalTaxAmount = 0.0;
			var total=0.0,totalAmount= 0.0;
			var discountType;
			
			$materialItem=$("#product_table").find("[data-material-item='"+id+"']");
			
			$materialItem.each(function (){	
				
				if($(this).find("input[id*='qty']" ).val()=="") {
					qty=parseFloat("0.0");
				} else {
					qty = parseFloat($(this).find("input[id*='qty']" ).val());
				}
				
				if($(this).find("input[id*='taxRate']" ).val()=="") {
					taxRate=parseFloat("0.0");
				} else {
					taxRate = parseFloat($(this).find("input[id*='taxRate']" ).val());
				}
				
				if($(this).find("input[id*='price']" ).val()=="") {
					rate=parseFloat("0.0");
				} else {
					rate = parseFloat($(this).find("input[id*='price']" ).val());
				}
								
				if($(this).find("input[id*='discount']" ).val()=="") {
					discount=parseFloat("0.0");
				} else {
					discount = parseFloat($(this).find("input[id*='discount']" ).val());
				}
				
				discountType = $(this).find("input[id*='discountType']" ).val();
				amount=qty*rate;
				
				if(discountType == "amount") {
					taxableValue=amount-discount;
				} else {
					taxableValue=amount-((amount*discount)/100);
				}
				
				taxAmount=taxableValue*taxRate/100;
				total=taxableValue+taxAmount;
				
				totalAmount += total;
				totalTaxAmount += taxAmount;
				totalQty += qty;
				
				$(this).find("input[id*='taxAmount']").val(taxAmount.toFixed(2));
			});
			
			$materialItem.find("[data-item-qty]").html(totalQty);
			$materialItem.find("[data-item-tax-amount]").html(totalTaxAmount.toFixed(2));
			$materialItem.find("[data-item-amount]").html(totalAmount.toFixed(2));
			
			setTaxSummary();
			
			setSubTotal()
		}
		
		function setSubTotal() {
			var $materialItem=$("#product_table").find("[data-material-item]").not(".m--hide"), subTotal=0.0;
			
			$materialItem.each(function (){
				subTotal += parseFloat($(this).find("[data-item-amount]").html());
			});
			
			$("#product_sub_total").html(subTotal.toFixed(2));
			
			setAllTotal();
		}
		
		function setAllTotal() {
			var taxAmount = 0.0, totalAmount = 0.0, roundoff=0.0, netAmount = 0.0;
			
			totalAmount = parseFloat($("#product_sub_total").text()) //Jaimin + parseFloat($("#additional_charge_sub_total").text());
			
			var $materialItem=$("#product_table").find("[data-material-item]").not(".m--hide"), subTotal=0.0;
			
			$materialItem.each(function (){
				taxAmount += parseFloat($(this).find("[data-item-tax-amount]").html());
			});
			
			/* Jaimin $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide")
			$additionalChargeItem.each(function (){
				taxAmount += parseFloat($(this).find("[data-item-tax-amount]").html());
			}); */
			$("#tax_amount").text(taxAmount.toFixed(2));
			$("#total_amount").text(totalAmount.toFixed(2));
			$("#round_off").text(parseFloat(Math.round(totalAmount.toFixed(2))-totalAmount.toFixed(2)).toFixed(2));
	  		//$("#roundoff").val(parseFloat(Math.round(totalAmount.toFixed(2))-totalAmount.toFixed(2)).toFixed(2));
	  		$("#net_amount").text(Math.round(totalAmount.toFixed(2)));
	  		
		}
		function setSrNo() {
			var $materialItem=$("#product_table").find("[data-material-item]").not(".m--hide"), subTotal=0.0;
			var i = 0;
			$materialItem.each(function (){
				$(this).find("[data-item-index]").html(++i);
			});
		}
		
		function getAdditionalCharge() {
			//mApp.blockPage()
			
			if(_additionalChargeData == undefined) {
				$.post("<%=request.getContextPath()%>/additionalcharge/json", {
									
				}, function (data, status) {
					if(status == 'success') {
						$("#additional_charge_button").closest(".m-demo-icon").addClass("m--hide");
						_additionalChargeData = data;
						$("#add_additional_charge").click();
					}
				});
			} else {
				$("#add_additional_charge").click();
			}
		}
		
		function addAdditionalCharge() {
			if(_additionalChargeData != undefined) {
				
				$additionalChargeItemTemplate=$("#additional_charge_table").find("[data-charge-item='template']").clone();
				$additionalChargeItemTemplate.removeClass("m--hide").attr("data-charge-item",additionalChargeId);
				
				$additionalChargeItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
					n=$(this).attr("id");
					n ? $(this).attr("id",n.replace(/{index}/g,additionalChargeId)) : "";
					n=$(this).attr("name");
					n ? $(this).attr("name",n.replace(/{index}/g,additionalChargeId)) : "";
					
					$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,additionalChargeId)) : "";
				});
				
				console.log($additionalChargeItemTemplate);
				
				$("#additional_charge_table").find("[data-charge-list]").append($additionalChargeItemTemplate);

				$.each(_additionalChargeData, function (key, value) {
					$("#additionalCharge"+additionalChargeId).append($('<option></option>').val(value.additionalChargeId).html(value.additionalCharge));
				});
				
				$("#additionalCharge"+additionalChargeId).addClass("m-select2");
				$("#additionalCharge"+additionalChargeId).select2({placeholder:"Select Additional",allowClear:0});
				
				additionalChargeId++;
				
				setAdditionalChargeSrNo();
			}
		}
		
		function setAdditionalChargeSrNo() {
			var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
			var i = 0;
			$additionalChargeItem.each(function (){
				$(this).find("[data-item-index]").html(++i);
			});
		}
		
		function getAdditionalChargeInfo(i) {
			
			var id = $("#additionalCharge"+i).val();
			
			if(id != "") {
				var data = jQuery.map(_additionalChargeData, function(obj) {
					if (obj.additionalChargeId == id) return obj;
				});
				
				var taxAmount=parseFloat(data[0].defaultAmount) * parseFloat(data[0].tax.taxRate) / 100;
				$("#additionalChargeAmount"+i).val(data[0].defaultAmount);
				$("#additionalChargeTaxId"+i).val(data[0].tax.taxId);
				$("#additionalChargeTaxRate"+i).val(data[0].tax.taxRate);
				$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item='"+i+"']");
				$additionalChargeItem.find("[data-item-tax-name]").text(data[0].tax.taxName + " ("+data[0].tax.taxRate+" %)");
				
				setAdditionalChargeAmount(i);
			}
		}
		
		function setAdditionalChargeAmount(id) {
			
			var taxRate=0.0;
			var taxableAmount,taxAmount = 0.0;
			
			if($("#additionalChargeTaxRate"+id).val()=="") {
				taxRate=parseFloat("0.0");
			} else {
				taxRate=parseFloat($("#additionalChargeTaxRate"+id).val());
			}
			
			if($("#additionalChargeAmount"+id).val()=="") {
				taxableAmount=parseFloat("0.0");
			} else {
				taxableAmount = parseFloat($("#additionalChargeAmount"+id).val());
			}
			
			taxAmount=parseFloat(taxableAmount) * taxRate / 100;
			
			$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item='"+id+"']");
			
			$additionalChargeItem.find("[data-item-tax-amount]").text(taxAmount.toFixed(2));
			$additionalChargeItem.find("[data-item-amount]").text(parseFloat(taxableAmount+taxAmount).toFixed(2));
			$("#additionalChargeTaxAmount"+id).val(taxAmount.toFixed(2));
			
			setAdditionalChargeSubTotal();
			
			setTaxSummary();
		}
		
		function setAdditionalChargeSubTotal() {
			var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide"), subTotal=0.0;
			
			$additionalChargeItem.each(function (){
				
				if(!isNaN(parseFloat($(this).find("[data-item-amount]").html()))) {
					subTotal += parseFloat($(this).find("[data-item-amount]").html());
				}
			});
			
			$("#additional_charge_sub_total").html(subTotal.toFixed(2));
			
			setAllTotal();
		}
		
		function setTaxSummary() {
			
			var split=1;
			
			$("#tax_summary_table").children('tbody').find("[data-tax-item]").not(".m--hide").remove();
			
			var $materialItem = $("#product_table").find("[data-material-item]"),
				$additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
			
			$materialItem.each(function() {
				
				var materialItemId=$(this).attr("data-material-item");
				
				if(materialItemId != "template") {
					
					setTax($("#taxId"+materialItemId).val(), $("#taxAmount"+materialItemId).val(), $("#taxRate"+materialItemId).val());
				}
			});
			
			$additionalChargeItem.each(function() {
				
				var additionalChargeId=$(this).attr("data-charge-item");
				
				if(additionalChargeId != "template") {
					
					setTax($("#additionalChargeTaxId"+additionalChargeId).val(), $("#additionalChargeTaxAmount"+additionalChargeId).val(), $("#additionalChargeTaxRate"+additionalChargeId).val());
				}
			});
			
		}
		
		function setTax(taxId, taxAmount, taxRate) {
			var isExist = 0,
				$taxItem = $("#tax_summary_table").children('tbody'), amount=0;
			
			$taxItem.find("[data-tax-item]").not(".m--hide").each(function () {
				
				if($(this).attr("data-tax-item") == taxId) {
					isExist=1;
					if($("#taxType").val() == 1) {
						amount =parseFloat(taxAmount);
						
					} else {
						amount =parseFloat(taxAmount)/2;
					}
					$(this).find('td').eq(2).text(amount + parseFloat($(this).find('td').eq(2).text()));
				}
			});
			
			if(isExist == 0) {
				$taxItemTemplate=$taxItem.find("[data-tax-item='template']").clone();
				$taxItemTemplate.removeClass("m--hide").attr("data-tax-item",taxId);
				
				if($("#taxType").val() == 1) {
					amount = parseFloat(taxAmount);
					taxRate = parseFloat(taxRate);
					
				} else {
					amount =parseFloat(taxAmount)/2;
					taxRate = parseFloat(taxRate)/2;
				}
				
				$taxItemTemplate.find('td').eq(1).text(taxRate + " %");
				$taxItemTemplate.find('td').eq(2).text(amount);
				
				if($("#taxType").val() == 1) {
					
					$taxItemTemplate.find('td').eq(0).text("IGST");
					$taxItem.append($taxItemTemplate);
					
				} else {
					
					$taxItemTemplate.find('td').eq(0).text("CGST");
					$taxItem.append($taxItemTemplate.clone());
					
					$taxItemTemplate.find('td').eq(0).text("SGST");
					$taxItem.append($taxItemTemplate);
				}
			}
		}
		
		function changeDueDate(){
			
			if($('#paymentTermVo').val()==null || $('#paymentTermVo').val()=='')
			{
				var date2 = $('#materialDate').datepicker('getDate', '+1d'); 
				date2.setDate(date2.getDate()); 
				$('#dueDate').datepicker('setDate', date2);
			}
			else
			{
				day=$('#paymentTermVo option[value="'+$('#paymentTermVo').val()+'"]').attr("name");
				var date2 = $('#materialDate').datepicker('getDate', '+1d'); 
				var d=parseInt(day);
				date2.setDate(date2.getDate()+d); 
				$('#dueDate').datepicker('setDate', date2);	
			}
		}
		
		function changeTaxType()
		{
			/* if($("#sez").prop("checked") == true){
				$("#taxType").val(1);
			} else { */
				if($("#purchase_shipping_address").find("[data-address-state]").attr("data-address-state") == $("#state_code").val()){
					$("#taxType").val(0);
				}
				else{
					$("#taxType").val(1);
				}
				
			/* } */
			setTaxSummary();
		}
		
		function setRoundoff()
	  	{
	  		
			if($("#edit_roundoff").val()==="")
	  			$("#edit_roundoff").val(0);
	  		
	  		$("#round_off").text($("#edit_roundoff").val());
	  		$("#net_amount").text((parseFloat($("#total_amount").text())+parseFloat($("#edit_roundoff").val())).toFixed(2));
	  		
	  		$("#roundoff_edit").click();
	  	}
		
		function getTermsAndCondition() {
			if(_termsAndConditionData == undefined) {
				$.get("<%=request.getContextPath()%>/termsandcondition/json", {
									
				}, function (data, status) {
					if(status == 'success') {
						
						_termsAndConditionData = data;
						
						setTermsAndConditionOnModal();
					}
				});
			} else {
				setTermsAndConditionOnModal();
			}
		}
		
		function setTermsAndConditionOnModal() {
			if(_termsAndConditionData != undefined) {
				
				$("#term_condition_edit_table").find("[data-terms-item]").not(".m--hide").remove();
				
				var $termConditionTemplate;
				
				$.each(_termsAndConditionData, function (key, value) {
					
					$termConditionTemplate = $("#term_condition_edit_table").find("[data-terms-item='template']").clone();
					
					$termConditionTemplate.attr("data-terms-item",value.termandconditionId).removeClass("m--hide");
					
					$termConditionTemplate.find("input[type='checkbox']").attr("name",value.termandconditionId);
					
					$termConditionTemplate.find("[data-terms-name]").html(value.termsCondition);
					
					var termsArray = $("#termsAndConditionIds").val().split(',');
					
					$.each(termsArray, function (i, val) {
						
						if(val==value.termandconditionId) {
							$termConditionTemplate.find("input[type='checkbox']").prop("checked",true);
							return;
						}
					});
					
					$("#term_condition_edit_table").children('tbody').append($termConditionTemplate);
				});
			}
		}
		function setTermsAndCondition() {
			var termsAndConditionIds="";
			$("#terms_and_condition_table").find("[data-terms-item]").not(".m--hide").remove();
			var $termConditionTemplate, i=0;
			$("#term_condition_edit_table").find("[data-terms-item]").not(".m--hide").each(function() {
				if($(this).find("input[type='checkbox']").prop("checked")) {
					
					termsAndConditionIds += $(this).find("input[type='checkbox']").attr("name") +",";
					
					$termConditionTemplate = $("#terms_and_condition_table").find("[data-terms-item='template']").clone();
					
					$termConditionTemplate.removeClass("m--hide").attr("data-terms-item","");
					$termConditionTemplate.find("[data-terms-index]").html(++i);
					$termConditionTemplate.find("[data-terms-name]").html($(this).find("[data-terms-name]").html());
					
					$("#terms_and_condition_table").children('tbody').append($termConditionTemplate);
				}
				
			});
			
			$("#termsAndConditionIds").val(termsAndConditionIds);
			
			$("#terms_and_condition").modal("hide");
		}
		
		function printJobworkReport(){
			
			var iframe = $('<iframe>');
            iframe.attr('id', 'iFramePrintRetailInvoice');
            iframe.attr('class', 'm--hide');
            iframe.attr('src', '<%=request.getContextPath()%>/material/${materialVo.type}/${materialVo.materialId}/pos/pdf');
			
            $('#targetDiv').html(iframe);
            if($("#targetDiv").html() != '') {
				document.getElementById("iFramePrintRetailInvoice").focus();
				document.getElementById("iFramePrintRetailInvoice").contentWindow.print();
			}
			
		}
		
		function setMaterialInDatatable() {
			$("#materialin_table").dataTable().fnDestroy();			
			var DatatablesDataSourceAjaxServer =  function(){
				/* $.fn.dataTable.Api.register("column().title()", function() {
					return $(this.header()).text().trim()
				}); */
				
				return {
					
				init: function() {
					var a;
					
					a = $("#materialin_table").DataTable({
						/*  responsive: !0,
						searchDelay: 500,
						processing: !0,
						serverSide: !0,
						ajax: "/product/list",
						*/
						responsive: !0,
						pageLength: 10,
						language: {
							lengthMenu: "Display _MENU_"
						},
						searchDelay: 500,
						processing: !0,
						serverSide: !0,
						ajax: {
							url: "<%=request.getContextPath()%>/material/materialin/materialOrder/${materialVo.materialId}/datatable",
							type: "POST",
							data: {
								columnsDef: ["#", "Jobwork No","Date" ]
							}
						},
						columns: [
							{
								data: "materialId"
							}, {
								data: "materialNo"
							}, {
								data: "materialDate"
							}, {
								data: "materialNo"
							}],
							columnDefs: [{
								targets: 1,
								title: "MaterialIn No",
								orderable: !1,
								render: function(a, e, t, n) {
									//console.log(a);
									//console.log(e);
									console.log();
									//console.log(n);
									return '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'" class="m-link m--font-bolder">'+t.prefix+t.materialNo+'</a>';
								}
							},{
								targets: 0,
								title: "#",
								orderable: !1,
								render: function(a, e, t, n) {
									
									return (n.row+1);
								}
							},{
								targets: 3,
								title: "Action",
								orderable: !1,
								render: function(a, e, t, n) {
									return '\n  <span class="dropdown">\n<a href="'+e+'" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" href="/material/'+t.materialId+'/edit"><i class="la la-edit"></i> Edit Details</a>\n</div>\n</span>\n '
								}
							},{
								targets: 2,
								render: function(a, e, t, n) {
									return moment(a).format('DD/MM/YYYY');
								}
							}]
						})
					}
				}
			}();
			
			DatatablesDataSourceAjaxServer.init();
		}
		function setMaterialOutDatatable() {
			$("#materialout_table").dataTable().fnDestroy();		
			//alert("/material/${displayType}/materialout/${materialVo.materialId}/datatable")
			var DatatablesDataSourceAjaxServer =  function(){
				/* $.fn.dataTable.Api.register("column().title()", function() {
					return $(this.header()).text().trim()
				}); */
				
				return {
					
				init: function() {
					var a;
					
					a = $("#materialout_table").DataTable({
						/*  responsive: !0,
						searchDelay: 500,
						processing: !0,
						serverSide: !0,
						ajax: "/product/list",
						*/
						responsive: !0,
						pageLength: 10,
						language: {
							lengthMenu: "Display _MENU_"
						},
						searchDelay: 500,
						processing: !0,
						serverSide: !0,
						ajax: {
							url: "<%=request.getContextPath()%>/material/materialout/materialOrder/${materialVo.materialId}/datatable",
							type: "POST",
							data: {
								columnsDef: ["#", "Jobwork No","Date" ]
							}
						},
						columns: [
							{
								data: "materialId"
							}, {
								data: "materialNo"
							}, {
								data: "materialDate"
							},
							{
								data: "materialId"
							}],
							columnDefs: [{
								targets: 1,
								title: "Jobwork No",
								orderable: !1,
								render: function(a, e, t, n) {
									//console.log(a);
									//console.log(e);
									console.log();
									//console.log(n);
									return '<a href="${pageContext.request.contextPath}/material/'+t.type+'/'+t.materialId+'" class="m-link m--font-bolder">'+t.prefix+t.materialNo+'</a>';
								}
							},{
								targets: 0,
								title: "#",
								orderable: !1,
								render: function(a, e, t, n) { 
									
									return (n.row+1);
								}
							},{
								targets: 3,
								title: "Action",
								orderable: !1,
								render: function(a, e, t, n) {
									return '\n  <span class="dropdown">\n<a href="'+e+'" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" href="/material/${displayType}/'+t.materialId+'/edit"><i class="la la-edit"></i> Edit Details</a>\n</div>\n</span>\n '
								}
							},{
								targets: 2,
								render: function(a, e, t, n) {
									return moment(a).format('DD/MM/YYYY');
								}
							}]
						})
					}
				}
			}();
			
			DatatablesDataSourceAjaxServer.init();
		}

	</script>
</body>
<!-- end::Body -->
</html>