<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${paymentVo.prefix}${paymentVo.paymentNo} | Payment</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${paymentVo.prefix}${paymentVo.paymentNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/payment" class="m-nav__link">
										<span class="m-nav__link-text">Payment</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="payment_form" action="/payment/save" method="post">	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Payment Details
												</h3>
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
													<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl  m-dropdown__toggle">
														<!-- <i class="la la-ellipsis-v"></i> -->
														<i class="la la-ellipsis-h m--font-brand"></i>
													</a>
							                        <div class="m-dropdown__wrapper">
							                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
							                            <div class="m-dropdown__inner">
						                                    <div class="m-dropdown__body">              
						                                        <div class="m-dropdown__content">
						                                            <ul class="m-nav">
						                                                <li class="m-nav__item">
						                                                    <a href="/payment/pdf/${paymentVo.paymentId}" target="_blank" class="m-nav__link">
						                                                        <i class="m-nav__link-icon fa fa-file-pdf"></i>
						                                                        <span class="m-nav__link-text">PDF</span>
						                                                    </a>
						                                                </li>
						                                                <li class="m-nav__separator m-nav__separator--fit">
						                                                </li>
						                                                <li class="m-nav__item">
						                                                    <a href="/payment/${paymentVo.paymentId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																				<span><i class="flaticon-edit"></i><span>Edit</span></span>
																			</a>
						                                                    <a  href="/payment/${paymentVo.paymentId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right">
																				<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																			</a>
						                                                </li>
						                                            </ul>
						                                        </div>
						                                    </div>
							                            </div>
							                        </div>
												</li>
											</ul>
										</div>	
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-12">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Suppplier:</span>
																		<span>
																			<a href="<%=request.getContextPath() %>/contact/${paymentVo.contactVo.type}/${paymentVo.contactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																				${paymentVo.contactVo.companyName}
																			</a>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Payment Date:</span>
																		<span>
																			<fmt:formatDate pattern="dd/MM/yyyy" value="${paymentVo.paymentDate}" />
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Payment Method:</span>
																		<span>
																			${paymentVo.paymentMode}
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<c:if test="${paymentVo.paymentMode==Constant.BANK}">
												<div class="col-lg-3 col-md-3 col-sm-12">
													<div class="m-widget28">
														<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
														<div class="m-widget28__container" >	
															<!-- Start:: Content -->
															<div class="m-widget28__tab tab-content">
																<div class="m-widget28__tab-container tab-pane active">
																    <div class="m-widget28__tab-items">
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Bank Name:</span>
																			<span>
																				${paymentVo.bankVo.bankName}
																			</span>
																		</div>
																	</div>					      	 		      	
																</div>
															</div>
															<!-- end:: Content --> 	
														</div>				 	 
													</div>
												</div>
											</c:if>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-15">
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
												<div class="form-group m-form__group pl-0 pr-0">
													<div class="row">
														<div class="col-lg-4">
															<label class="m-option">
																<span class="m-option__control">
																	<span class="">
																		<c:choose>
																		    <c:when test="${paymentVo.type=='OnAccount'}">
																		       <span class="m-type m-type--bg m--bg-success" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		    </c:when>    
																		    <c:otherwise>
																		      <span class="m-type m-type--bg m--bg-metal" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>				       
																		    </c:otherwise>
																		</c:choose>
																	</span>
																</span>
																<span class="m-option__label">
																	<span class="m-option__head">												 
																		<span class="m-option__title m-option__focus">On Account</span>											 
																	</span>
																	<span class="m-option__body">
																		Estimated 14-20 Day Shipping (&nbsp;Duties end taxes may be due upon delivery&nbsp;)
																	</span>
																</span>		
															</label> 
														</div>
														<div class="col-lg-4">
															<label class="m-option">
																<span class="m-option__control">
																	<span class="">
																		<c:choose>
																		    <c:when test="${paymentVo.type=='AdvancePayment'}">
																		       <span class="m-type m-type--bg m--bg-success" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		    </c:when>    
																		    <c:otherwise>
																		      <span class="m-type m-type--bg m--bg-metal" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>				       
																		    </c:otherwise>
																		</c:choose>
																	</span>
																</span>
																<span class="m-option__label">
																	<span class="m-option__head">												 
																		<span class="m-option__title m-option__focus">Advance Payment</span>
																	</span>
																	<span class="m-option__body">
																		Estimated 2-5 Day Shipping (&nbsp;Duties end taxes may be due upon delivery&nbsp;)
																	</span>
																</span>		
															</label> 
														</div>
														<div class="col-lg-4">
															<label class="m-option">
																<span class="m-option__control">
																	<span class="">
																		<c:choose>
																		    <c:when test="${paymentVo.type=='AgainstBill'}">
																		       <span class="m-type m-type--bg m--bg-success" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		       
																		    </c:when>    
																		    <c:otherwise>
																		      <span class="m-type m-type--bg m--bg-metal" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		       
																		    </c:otherwise>
																		</c:choose>
																	</span>
																</span>
																<span class="m-option__label">
																	<span class="m-option__head">												 
																		<span class="m-option__title m-option__focus">Against Bill</span>
																	</span>
																	<span class="m-option__body">
																		Estimated 2-5 Day Shipping (&nbsp;Duties end taxes may be due upon delivery&nbsp;)
																	</span>
																</span>		
															</label> 
														</div>
													</div>
													<div class="m-form__help"><!--must use this helper element to display error message for the options--></div>
												</div>
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Amount:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group input-group-lg m-input-group m-input-group--air">
															<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="fa 	fa-rupee-sign"></i></span></div>
															<input type="text" class="form-control m-input m-input--air m--font-boldest m--regular-font-size-lg5" name="totalPayment" readonly="readonly" value="${paymentVo.totalPayment}" placeholder="Amount" aria-describedby="basic-addon1">
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<table class="table table-sm m-table  m-table--head-no-border">
													  	<thead>
													  		<tr class="row">
													    		<th scope="row" class="col-lg-12 col-md-12 col-sm-12">Description:</th>
													    	</tr>
													  	</thead>
													  	<tbody>
													    	<tr class="row">
														      	<td class="col-lg-12 col-md-12 col-sm-12">
														      		<span><c:if test="${empty paymentVo.description}">Description</c:if>${paymentVo.description}</span>
														      	</td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<c:if test="${paymentVo.paymentBillVos.size() > 0}">
								<div class="col-lg-12 col-md-12 col-sm-12" id="bill_details">
									<!--begin::Portlet-->
									<div class="m-portlet">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">
													<span class="m-portlet__head-icon">
														<i class="flaticon-cogwheel-2"></i>
													</span>
													<h3 class="m-portlet__head-text m--font-brand">
														Bill Details
													</h3>
												</div>			
											</div>
										</div>
										<div class="m-portlet__body" >
											<div class="row">
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="bill_table">
													    <thead>
													      <tr>
													        <th>#</th>
													        <th>Bill No.</th>
													        <th class="text-right">Original Amount</th>
													        <th class="text-right">Paid Amount</th>
													        <th class="text-right">Pending Amount</th>
													        <th class="text-right">Kasar Amount</th>
													        <th class="text-right">Payment</th>
													        <th></th>
													      </tr>
													    </thead>
													    <tbody data-bill-list="">
													    	<c:set var="total" scope="page" value="0"/>
													    	<c:forEach items="${paymentVo.paymentBillVos}" var="paymentBillVo" varStatus="status">
														    	<tr data-bill-item="">
														    		<td class="" style="width: 50px;">
														    			<span data-item-index>${status.index+1}</span>
														    		</td>
															        <td style="width: 280px;">
															        	<div class="form-group m-form__group p-0">
																			<a href="${pageContext.request.contextPath}/purchase/${paymentBillVo.purchaseVo.type}/${paymentBillVo.purchaseVo.purchaseId}" target="_blank"
																				class="m-link m--font-bolder">
																				${paymentBillVo.purchaseVo.billNo}
																			</a> 
																		</div>
															        </td>
															        <td style="width: 180px;">
															        	<div class="p-0 text-right text-right" data-item-original="">${paymentBillVo.purchaseVo.total}</div>
															        </td>
															        <td style="width: 180px;">
															        	<div class="p-0 text-right text-right" data-item-paid="">${paymentBillVo.purchaseVo.paidAmount}</div>
															        </td>
															        <td style="width: 180px;">
															        	<div class="p-0 text-right text-right" data-item-pending="">${paymentBillVo.purchaseVo.total-paymentBillVo.purchaseVo.paidAmount}</div>
															        </td>
															        <td style="width: 180px;">
															        	<div class="form-group m-form__group p-0 text-right">
															        		${paymentBillVo.kasar}
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="form-group m-form__group p-0 text-right">
															        		${paymentBillVo.payment}
															        		<c:set var="total" scope="page" value="${total + paymentBillVo.payment}" />
															        	</div>
															        </td>
															        <td class="p-0" style="width: 40px;">
															        </td>
														    	</tr>
													    	</c:forEach>
														</tbody>
														<tfoot>
													      <tr>
													        <th></th>
													        <th></th>
													        <th></th>
													        <th></th>
													        <th></th>
													        <th></th>
													        <th><span class="m--font-boldest float-right" id="payment_sub_total">${total}</span></th>
													        <th></th>
													      </tr>
													    </tfoot>
												  	</table>
												</div>
											</div>
										</div>
									</div>	
									<!--end::Portlet-->
								</div>
							</c:if>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>