<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Payment</title>
	
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Payment</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="#" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="<%=request.getContextPath()%>/payment/new" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>Payment</span></span>
											</a>
										</div>			
									</div>
									
									<!-- <div class="m-portlet__head-tools">
										<a href="#" id="export_print" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
										<a href="#" id="export_excel" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="Excel"> <i class="fa fa-file-excel"></i>
										</a>
										<a href="#" id="export_pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="PDF"> <i class="fa fa-file-pdf"></i>
										</a>
									</div> -->
								</div>
								<div class="m-portlet__body" >
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div  class="m_datatable"  >
												<table class="table table-striped- table-bordered table-hover table-checkable" id="payment_table">
													<thead>
								  						<tr>
						  									<th>#</th>
						  									<th>Payment No.</th>
						  									<th>Payment Date</th>
						  									<th>Supplier Name</th>			  									
						  									<th>Payment Mode</th>
						  									<th>Total Amount</th>
						  									<th>Action</th>
									  					</tr>
													</thead>			
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
							
						</div>
				
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		var DatatablesDataSourceHtml = {
		    init: function() {
		        $("#payment_table").DataTable({
		            responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax: {
						url: "<%=request.getContextPath()%>/payment/datatable",
						type: "POST",
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					
					columns: [{
			                data: "paymentId"
			            }, {
			                data: "paymentNo"
			            }, {
			                data: "paymentDate"
			            }, {
			                data: "contactVo.companyName"
			            },{
			                data: "paymentMode"
			            },{
			                data: "totalPayment"
			            },{
			                data: "paymentId"
			            }],
					
					columnDefs: [{
							targets: 0,
							orderable: !1,
							render: function(a, e, t, n) {
								return (n.row+n.settings._iDisplayStart+1);
							}
						},{
							targets:1,
			                orderable: !1,
			               	render: function(a, e, t, n) {
			                  
			            	   return '\n  <a href="/payment/'+t.paymentId+'" class="m-link m--font-bolder" aria-expanded="true">\n '+t.prefix+t.paymentNo+'</a>\n '
			                }
						},{
							targets: 2,
							render: function(a, e, t, n) {
								return moment(a).format('DD/MM/YYYY');
							}
						},{
							targets:3,
			                orderable: !1,
			               	render: function(a, e, t, n) {
			                  
			            	   return '\n  <a href="/contact/'+t.contactVo.type+'/'+t.contactVo.contactId+'" target="_blank" class="m-link m--font-bolder" aria-expanded="true">\n '+a+'</a>\n '
			                }
						},{
							targets: 6,
							orderable: !1,
							render: function(a, e, t, n) {
								var action = "";
								action += '<a href="${pageContext.request.contextPath}/payment/'+t.paymentId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
								action += '<button onclick="deletePayment("'+t.paymentId+'")" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></button>'
								action += '<span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="fa fa-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" target="_blank" href="${pageContext.request.contextPath}/payment/'+t.paymentId+'/pdf"><i class="fa fa-file-pdf"></i> PDF</a>\n</div>\n</span>\n '
								return action;
							}
			            }],
		            fixedHeader: {
		                header: false,
		                footer: false
		            }
		            
		        })
		    }
		};
		
		jQuery(document).ready(function() {
			
			DatatablesDataSourceHtml.init();
			
			$(".dt-buttons").addClass("m--hide");
		});
	</script>
	
</body>
<!-- end::Body -->
</html>