<div class="modal fade" id="paymentterm_update_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Edit Payment Term
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="paymentterm_update_form" >
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">
							Payment Term Name
						</label>
						<input type="text" name="paymentTermName"class="form-control" id="updatePaymentTermName">
						<input type="hidden" class="form-control" id="paymentTermId">
						<input type="hidden" class="form-control" id="dataindex">
					</div>
					<div class="form-group">
						<label for="message-text" class="form-control-label">
							Payment Term Day:
						</label>
						<input type="text" class="form-control" name="paymentTermDay" id="updatePaymentTermDay"/>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<button type="button"  id="updatepaymentterm" class="btn btn-primary">
					Save
				</button>
			</div>
		</div>
	</div>
</div>