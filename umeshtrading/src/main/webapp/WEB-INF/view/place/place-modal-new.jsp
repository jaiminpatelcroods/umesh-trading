<div class="modal fade" id="place_new_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New Place</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<form id="place_new_form" class="m-form m-form--state m-form--fit">
				<div class="modal-body">
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Place Name:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="placeName" id="placeName" placeholder="Place Name" value="">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Place Code:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="placeCode" id="placeCode" placeholder="Place Code" value="">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<textarea class="form-control m-input" name="description" id="description" placeholder="Description" cols="100" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
						Close
					</button>
					<button type="button" onclick="" id="saveplace" class="btn btn-primary">
						Save
					</button>
				</div>
			</form>
		</div>
	</div>
</div>