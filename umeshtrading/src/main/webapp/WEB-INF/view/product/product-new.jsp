<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>New Product</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.css">
	
	<style type="text/css">
		.select2-container{display: block;}
		/* .select2-container {
			width: 100% !important;
			
		} */
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1 {
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New Product</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/product" class="m-nav__link">
										<span class="m-nav__link-text">Product</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="product_form" action="/product/save" method="post">	
						<input type="hidden" name="haveVariation" id="haveVariation" value="0"/>
						<input type="hidden" name="discountType" id="discountType" value="Percentage"/>
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">General Details</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Product Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="name" id="name" placeholder="Product Name"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Print Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="displayName" id="displayName" placeholder="Print Name"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea class="form-control m-input" name="description" placeholder="Enter Description"></textarea>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Applicable Type:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="m-radio-inline">
															<label class="m-radio">
																<input type="radio" id="applicableType0" name="applicableType" checked="checked" value="0"> Purchase Discount
																<span></span>
															</label>
															<label class="m-radio">
																<input type="radio" id="applicableType1" name="applicableType" value="1"> Sales Margin
																<span></span>
															</label>
														</div>
														<span class="form-text text-muted"></span>
													</div>
												</div>
												
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Category:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="categoryId" name="categoryVo.categoryId" placeholder="Select Category">
																<option value="">Select Category</option>
																<c:forEach items="${categoryVos}" var="categoryVo">																			
																	<option value="${categoryVo.categoryId}"
																	<c:if test="${categoryVo.isDefault == 1}">selected="selected"</c:if>>
																	    ${categoryVo.categoryName}
																	</option>
																</c:forEach>	
															</select>
															<div class="input-group-append">
																<button class="btn btn-outline-metal" type="button"  data-toggle="modal" data-target="#category_new_modal" ><i class="fa fa-plus" ></i></button>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Brand:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="brandId" name="brandVo.brandId" placeholder="Select Brand">
																<option value="">Select Brand</option>
																<c:forEach items="${brandVos}" var="brandVo">																			
																	    <option value="${brandVo.brandId}"
																	    <c:if test="${brandVo.isDefault == 1}">selected="selected"</c:if>>
																	    	${brandVo.brandName}
																	    </option>
																</c:forEach>																
															</select>
															<div class="input-group-append">
																<button class="btn btn-outline-metal" type="button"  data-toggle="modal" data-target="#brand_new_modal" ><i class="fa fa-plus"></i></button>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Unit of Measurement:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="measurementId" name="unitOfMeasurementVo.measurementId" placeholder="Unit of Measurements">
																<option value="">Select Unit Of Measurement</option>
																<c:forEach items="${unitOfMeasurementVos}" var="unitOfMeasurementVo">																			
																    <option value="${unitOfMeasurementVo.measurementId}"
																    <c:if test="${unitOfMeasurementVo.isDefault == 1}">selected="selected"</c:if>>
																    	${unitOfMeasurementVo.measurementName}
																    </option>
																</c:forEach>
																
															</select>
															<div class="input-group-append">
																<button class="btn btn-outline-metal" type="button" data-toggle="modal" data-target="#uom_new_modal"><i class="fa fa-plus"></i></button>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">HSN Code:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="hsnCode" placeholder="HSN Code" value="">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-3 col-md-3 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-form__heading"></div>
												<div class="m-form__heading"></div>
												<br>
												<div class="m-form__heading">
													<h6 class="m-form__heading-title">Sales Tax Details</h6>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Sales Tax:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="salesTaxId" name="salesTaxVo.taxId" placeholder="Select Sales Tax">
																<option value="">Select Tax</option>	
																<c:forEach items="${taxVos}" var="taxVo">																			
																    <option value="${taxVo.taxId}"
																    <c:if test="${taxVo.isDefault == 1}">selected="selected"</c:if>>
																    	${taxVo.taxName}
																    </option>
																</c:forEach>
															</select>
															<div class="input-group-append">
																<button class="btn btn-outline-metal" type="button" data-toggle="modal" data-target="#tax_new_modal" ><i class="fa fa-plus"></i></button>
															</div>
														</div>
													</div>
												</div>
												
												<!-- Jaimin 2019-04-15 <div class="form-group m-form__group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="m-checkbox-inline">
															<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
															<input type="checkbox" name="salesTaxIncluded" value="0">Sales Tax Included 
															<span></span>
															</label>
															<i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="If different than the corresponding address"></i>
														</div>
													</div>
												</div> -->
												
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
												
												<div class="m-form__heading">
													<h6 class="m-form__heading-title">Purchase Tax Details</h6>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Purchase Tax:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="purchaseTaxId" name="purchaseTaxVo.taxId" placeholder="Select Purchase Tax">
																<option value="">Select Tax</option>	
																<c:forEach items="${taxVos}" var="taxVo">																			
																    <option value="${taxVo.taxId}"
																    <c:if test="${taxVo.isDefault == 1}">selected="selected"</c:if>>
																    	${taxVo.taxName}
																    </option>
																</c:forEach>
															</select>
															<div class="input-group-append">
																<button class="btn btn-outline-metal" type="button"  data-toggle="modal" data-target="#tax_new_modal" ><i class="fa fa-plus"></i></button>
															</div>
														</div>
													</div>
												</div>
												
												<!-- Jaimin 2019-04-15 <div class="form-group m-form__group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="m-checkbox-inline">
															<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
															<input type="checkbox" name="purchaseTaxIncluded" value="0"> Purchase Tax Included 
															<span></span>
															</label>
															<i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="If different than the corresponding address"></i>
														</div>
													</div>
												</div> -->
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-12" id="single_variant">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row ">
													<div class="col-lg-4 col-md-4 col-sm-12 m-form__group-sub">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Purchase Price:</label>
														<input type="number" required="required" class="form-control m-input" id="purchasePrice" name="purchasePrice" placeholder="Purchase Price" value="" step="Any" min="0.0" >
													</div>
													<div class="col-lg-4 col-md-4 col-sm-12 m-form__group-sub ">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Purchase Discount:</label>
														<div class="input-group">
															<div class="input-group-prepend" style="display: flex; flex-direction: column">
																<!-- Jaimin <button class="btn btn-secondary" type="button" id="btn_percentage" onclick="" style="background-color: #ebedf2;"><i class="fa fa-percentage m--font-info" style="padding: 0.6rem;"></i></button>
																 <button class="btn btn-secondary btn-icon" type="button" id="btn_amount" style="background-color: #ebedf2;"><i class="fa fa-rupee-sign m--font-brand" style="padding: 0.6rem; padding-right: 0.8rem"></i></button> -->
																<button class="btn btn-secondary" type="button" style="background-color: #ebedf2;"><i class="fa fa-percentage m--font-info" style="padding: 0.6rem;"></i></button> 
															</div>
															<input type="number" required="required" class="form-control" id="modalDiscount" name="discount" placeholder="Percentage" step="Any" min="0.0" max="100" value="0.0">
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-12 m-form__group-sub">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Purchase Price Rate:</label>
														<input type="number" class="form-control m-input" id="purchasePriceRate" name="purchasePriceRate" placeholder="Purchase Price Rate" value="" step="Any" min="0.0" readonly="readonly">
													</div>
													
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row ">
													<div class="col-lg-4 col-md-4 col-sm-12 m-form__group-sub ">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Sales Margin:</label>
														<div class="input-group">
															<div class="input-group-prepend" style="display: flex; flex-direction: column">
																<!-- Jaimin <button class="btn btn-secondary" type="button" id="btn_percentage" onclick="" style="background-color: #ebedf2;"><i class="fa fa-percentage m--font-info" style="padding: 0.6rem;"></i></button>
																 <button class="btn btn-secondary btn-icon" type="button" id="btn_amount" style="background-color: #ebedf2;"><i class="fa fa-rupee-sign m--font-brand" style="padding: 0.6rem; padding-right: 0.8rem"></i></button> -->
																	<button class="btn btn-secondary" type="button" style="background-color: #ebedf2;"><i class="fa fa-percentage m--font-info" style="padding: 0.6rem;"></i></button> 
															</div>
														<input type="number" required="required" class="form-control m-input" id="sellingMargin" name="sellingMargin" placeholder="Sales Margin" value="0.0" step="Any" min="0.0" readonly="readonly">	
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-12 m-form__group-sub ">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Sales Price:</label>
														<input type="number" class="form-control m-input" id="salePrice" name="salePrice" placeholder="Sales Price" value="" step="Any" min="0.0" >
													</div>
													<div class="col-lg-4 col-md-4 col-sm-12 m-form__group-sub">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Market Price:</label>
														<input type="number" required="required" class="form-control m-input" id="marketPrice" name="marketPrice" placeholder="Market Price" value="" step="Any" min="0.0">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							<div class="col-lg-3 col-md-3 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row ">
													<div class="col-lg-12 col-md-12 col-sm-12 m-form__group-sub">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Minimum Stock Limit :</label>
														<input type="number" required="required" class="form-control m-input" id="minStock" name="minStock" placeholder="Min. Stock Qty" value="" step="Any" min="0.0">
													</div>
													<div class="col-lg-12 col-md-12 col-sm-12 m-form__group-sub ">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Maximum Stock Limit :</label>
														<input type="number" required="required" class="form-control m-input" id="maxStock" name="maxStock" placeholder="Max. Stock Qty" value="" step="Any" min="0.0">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="save_product">
									Submit
								</button>
								
								<a href="/product" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<%@include file="../category-brand/category-modal-new.jsp" %>
		<%@include file="../category-brand/brand-modal-new.jsp" %>
		<%@include file="../unitofmeasurement/unitofmeasurement-modal-new.jsp" %>
		<%@include file="../tax/tax-modal-new.jsp" %>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<%-- <script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script> --%>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	<%-- <script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script> --%>
	
	<%-- <script src="<%=request.getContextPath()%>/script/category-brand/category-brand-new-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/unitofmeasurement/unitofmeasurement-new-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/tax/tax-new-script.js" type="text/javascript"></script> --%>
	<%-- <script src="<%=request.getContextPath()%>/script/product/product-new-script.js" type="text/javascript"></script> --%>
	<script type="text/javascript">
	
	var priceValidator = {
			validators : {
				notEmpty : {
					message : 'The price is required'
				},
				stringLength : {
					max : 20,
					message : 'The price must be less than 20 characters long'
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,2})?))$/,
					message : 'The price is invalid'
				}
			}
		};
	
	$(document).ready(function(){
		
		$("#name").change(function() {
			$('#displayName').val($("#name").val());
			$('#product_form').formValidation('revalidateField', 'displayName');
		});
		
		/*
		* whene click on Tax Included checkbox value is change 0 or 1
		*/
		$('input[name="salesTaxIncluded"],input[name="purchaseTaxIncluded"]').click(function(){
	        if($(this).prop("checked") == true){
	            $(this).val(1);
	        }
	        else if($(this).prop("checked") == false){
	        	  $(this).val(0);
	        }
	    });
		
		/**
		 * 
		 * 
		 */
		
		if($("#discountType").val() == 'Amount') {
			$('#btn_percentage').slideUp()
		} else {
			$('#btn_amount').slideUp()
		}
		
		
		$('#btn_percentage').click(function (){
			$('#btn_percentage').slideUp();
			$('#btn_amount').slideDown();
			$("#modalDiscount").attr("placeholder", 'Amount');
			$("#discountType").attr("value", 'Amount');
		});
		
		$('#btn_amount').click(function (){
			$('#btn_percentage').slideDown();
			$('#btn_amount').slideUp()
			$("#modalDiscount").attr("placeholder", 'Percentage');
			$("#discountType").attr("value", 'Percentage');
  		});


		$(function() {
		    $('input:radio[name="applicableType"]').change(function() {
		        if ($(this).val() == '0') {
		        	$('#modalDiscount').attr('readonly',false);
		        	$('#sellingMargin').attr('readonly', true);
					$('#sellingMargin').val(0);
					$('#sellingMargin').change();
					$('#salePrice').attr('readonly',false);
		        } else {
			        $('#modalDiscount').attr('readonly',true);
			        $('#modalDiscount').val(0);
			        $('#modalDiscount').change();
			        
		        	$('#sellingMargin').attr('readonly', false);
		        	$('#salePrice').attr('readonly',true);
		        }
		    });
		});


		
		/* 2019-04-13 Jaimin START Purchase Price Rate Calculation  */
		
		$('input[name="purchasePrice"],input[name="discount"],input[name="sellingMargin"]').bind('change', function () {
			var purchasePrice=0.0;
			var discount=0.0;    
			var discountAmount = 0.0;
			var purchasePriceRate = 0.0;
			var sellingMargin = 0.0;
			var salePrice = 0.0;

			//Purchase
			discountAmount = parseFloat( ( parseFloat($('#purchasePrice').val()) * parseFloat($('#modalDiscount').val()) ) /100 ); 
			purchasePriceRate  =  (parseFloat($('#purchasePrice').val()) - discountAmount);
			$("#purchasePriceRate").val(purchasePriceRate.toFixed(2));

			//Sale
			salePrice = ( purchasePriceRate + ( purchasePriceRate * (parseFloat($('#sellingMargin').val())/100) ) ); 
			$("#salePrice").val(salePrice.toFixed(2));
		});
		
		/* 2019-04-13 Jaimin END Purchase Price Rate Calculation */	
		
		$('#product_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#save_product",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				name : {
					validators : {
						notEmpty : {
							message : 'The product name is required'
						},
						stringLength : {
							max : 75,
							message : 'The product name must be less than 75 characters long'
						}
					}
				},
				displayName : {
					validators : {
						notEmpty : {
							message : 'The print name is required'
						},
						stringLength : {
							max : 75,
							message : 'The print name must be less than 75 characters long'
						}
					}
				},
				'unitOfMeasurementVo.measurementId' : {
					validators : {
						notEmpty : {
							message : 'select Unit of Measurement'
						}
					}
				},
				'salesTaxVo.taxId' : {
					validators : {
						notEmpty : {
							message : 'select sales tax'
						}
					}
				},
				'purchaseTaxVo.taxId' : {
					validators : {
						notEmpty : {
							message : 'select purchase tax'
						}
					}
				},
				hsnCode : {
					validators : {
						stringLength : {
							max : 8,
							message : 'The hsn code must be 8 characters long'
						},
						regexp : {
							regexp : /^[0-9]+$/,
							message : 'The hsn can only consist of number'
						}
					}
				},
				'categoryVo.categoryId' : {
					validators : {
						notEmpty : {
							message : 'select Category'
						}
					}
				},
				'brandVo.brandId' : {
					validators : {
						notEmpty : {
							message : 'select Brand'
						}
					}
				}
			}
		});
	});

	
	</script>
	
	<!-- START : CATEGORY-BRAND SCRIPT FOR NEW  -->
	<script type="text/javascript">
	
		$(document).ready(function(){
		
		//----------Category------------
		$("#category_new_form").formValidation({
			framework : 'bootstrap',
			live:'disabled',
			excluded : ":disabled",
			button:{
				selector : "#savecategory",
				disabled : "disabled",
			},
			icon : null,
			fields : {
				category:{
					validators: {
						notEmpty: {
							message: 'The Name is Required'
						}
					}
				},
				description:{
					validators: {
					}
				}
			}
		}).on('success.form.fv', function(e) {
			 e.preventDefault();//stop the from action methods
			 
			 $.post("/categorybrand/category/save", {
				 name: $('#category').val(),
				 description:$('#description').val()
			 }, function( data,status ) {
				toastr["success"]("Category Inserted....");
				$('#category_new_modal').modal('toggle');
				
				$.get("<%=request.getContextPath()%>/categorybrand/category/", {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#categoryId").empty();
						$("#categoryId").append($('<option></option>').val("").html("Select Category"));
						$.each(data, function (key, value) {  
							$("#categoryId").append($('<option></option>').val(value.categoryId).html(value.categoryName)); 
						});
						$("#categoryId").select2();
					}
				});
				
				
				
			 });
			 
		});
		
		$('#category_new_modal').on('show.bs.modal', function() {
			$('#category_new_form').formValidation('resetForm', true);
		});
		
		$("#savecategory").click(function() {
			$('#category_new_form').data('formValidation').validate();
		});
		
		//----------End Category------------
		
		//----------Brand------------
		$("#brand_new_form").formValidation({
			framework : 'bootstrap',
			live:'disabled',
			excluded : ":disabled",
			button:{
				selector : "#savebrand",
				disabled : "disabled",
			},
			icon : null,
			fields : {
				brandname:{
					validators: {
						notEmpty: {
							message: 'The Name is Required'
						}
					}
				},
				branddesc:{
					validators: {
					}
				}
			}
		}).on('success.form.fv', function(e) {
			 e.preventDefault();//stop the from action methods
			 
			 $.post("/categorybrand/brand/save", {
				 name: $('#brandname').val(),
				 description:$('#branddescription').val()
			 }, function( data,status ) {
				toastr["success"]("Brand Inserted....");
				$('#brand_new_modal').modal('toggle');
				
				$.get("<%=request.getContextPath()%>/categorybrand/brand/", {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#brandId").empty();
						$("#brandId").append($('<option></option>').val("").html("Select Brand"));
						$.each(data, function (key, value) {  
							$("#brandId").append($('<option></option>').val(value.brandId).html(value.brandName)); 
						});
						$("#brandId").select2();
					}
				});
				
			 });
			 
		});
		
		$('#brand_new_modal').on('show.bs.modal', function() {
			$('#brand_new_form').formValidation('resetForm', true);
		});
		
		$("#savebrand").click(function() {
			$('#brand_new_form').data('formValidation').validate();
		});
		
		//----------End Brand------------
	});
	</script>
	<!-- END : CATEGORY-BRAND SCRIPT FOR NEW  -->	
	
	
	<!-- START : UOM NEW  -->
	<script type="text/javascript">
	$(document).ready(function(){
		$("#uom_new_form").formValidation({
			framework : 'bootstrap',
			live:'disabled',
			excluded : ":disabled",
			button:{
				selector : "#savetax",
				disabled : "disabled",
			},
			icon : null,
			fields : {
				Uom:{
					validators: {
						notEmpty: {
							message: 'The Name is Required'
						}
					}
				},
				UomCode:{
					validators: {
						notEmpty: {
							message: 'The Code is Required'
						}
					}
				},
				noOfDecimalPlaces:{
					validators: {
						notEmpty: {
							message: 'The No. of Decimal is reqired'
						},
						regexp: {
							regexp:  /^[0-4,/d{1}]$/,
							message: 'The No. of Decimal Places can only consist numberical value O tO 4'
						}
					}
				}
			}
		}).on('success.form.fv', function(e) {
			 e.preventDefault();//stop the from action methods
			 
			 $.post("/unitofmeasurement/save", {
				 name: $('#Uom').val(),
				 code:$('#Code').val(),
				 decimal:$('#Nodp').val()
			 }, function( data,status ) {
				toastr["success"]("UOM Inserted....");
				$('#uom_new_modal').modal('toggle');
				
				$.get("<%=request.getContextPath()%>/unitofmeasurement/uom/", {
				}, function (data, status) {
					if(status == 'success') 
					{
						$("#measurementId").empty();
						$("#measurementId").append($('<option></option>').val("").html("Select Uom"));
						$.each(data, function (key, value) {  
							$("#measurementId").append($('<option></option>').val(value.measurementId).html(value.measurementName)); 
						});
						$("#measurementId").select2();
					}
				});
				
				
			 });
			 
		});
		
		$('#uom_new_modal').on('show.bs.modal', function() {
			$('#uom_new_form').formValidation('resetForm', true);
		});
		
		$("#saveuom").click(function() {
			$('#uom_new_form').data('formValidation').validate();
		});
		
	});
		
	</script>
	<!-- END : UOM NEW  -->
	
	
	
	<!-- START : TAX NEW -->
	 <script type="text/javascript">
	 $(document).ready(function(){
			$("#tax_new_form").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				button:{
					selector : "#savetax",
					disabled : "disabled",
				},
				icon : null,
				fields : {
					tax:{
						validators: {
							notEmpty: {
								message: 'The Name is Required'
							}
						}
					},
					taxrate:{
						validators: {
							notEmpty: {
								message: 'The Rate is Required'
							},
							regexp: {
								regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
								message: 'The Tax Rate can contain only numeric value and not more than two decimal value'
							}
						}
					}
				}
			}).on('success.form.fv', function(e) {
				 e.preventDefault();//stop the from action methods
				 
				 $.post("/tax/create", {
					 name: $('#tax').val(),
					 rate:$('#rate').val()
				 }, function( data,status ) {
					toastr["success"]("Tax Inserted....");
					$('#tax_new_modal').modal('toggle');
					
					$.get("<%=request.getContextPath()%>/tax/taxlist/", {
					}, function (data, status) {
						if(status == 'success') 
						{
							
							//For Sales Tax
							$("#salesTaxId").empty();
							$("#salesTaxId").append($('<option></option>').val("").html("Select Tax"));
							$.each(data, function (key, value) {  
								$("#salesTaxId").append($('<option></option>').val(value.taxId).html(value.taxName)); 
							});
							$("#salesTaxId").select2();
							
							//For Purchase Tax
							$("#purchaseTaxId").empty();
							$("#purchaseTaxId").append($('<option></option>').val("").html("Select Tax"));
							$.each(data, function (key, value) {  
								$("#purchaseTaxId").append($('<option></option>').val(value.taxId).html(value.taxName)); 
							});
							$("#purchaseTaxId").select2();
							
						}
					});
					
				 });
				
			});
			
			$('#tax_new_modal').on('show.bs.modal', function() {
				$('#tax_new_form').formValidation('resetForm', true);
			});
			
			$("#savetaxnew").click(function() {
				$('#tax_new_form').data('formValidation').validate();
			});
			
		});
	 
	 </script>
	<!-- END : TAX NEW  -->
</body>
<!-- end::Body -->
</html>