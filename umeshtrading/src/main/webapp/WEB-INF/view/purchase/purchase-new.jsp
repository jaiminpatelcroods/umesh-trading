<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>New ${displayType} | Purchase</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
		
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New ${displayType}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/purchase/${type}" class="m-nav__link">
										<span class="m-nav__link-text">${displayType}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="purchase_form" action="/purchase/${type}/save" method="post">	
						<input type="hidden" name="total" id="total" value="0"/>
						<input type="hidden" name="taxType" id="taxType" value="0"/>
						<input type="hidden" name="termsAndConditionIds" id="termsAndConditionIds" value="0"/>
						<input type="hidden" id="state_code" value="${sessionScope.stateCode}"/>
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
									<!-- <div class="card-container">
											  <div class="card" id="card">
											    <div class="side"><button class="btn btn-success card-btn" type="button" id="btn_per" style="border-top-right-radius: 0;border-bottom-right-radius: 0">Go!</button></div>
											    <div class="side back"><button class="btn btn-danger" type="button">GO!</button></div>
											  </div>
											</div> -->
									
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Supplier:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2" id="contactVo" name="contactVo.contactId" onchange="getContactInfo(0)" placeholder="Select Supplier">
															<option value="">Select Supplier</option>
															<c:forEach items="${contactVos}" var="contactVo">
																<option value="${contactVo.contactId}">
																	${contactVo.companyName}
																</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span id="lblContactGSTIN">-</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">-</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
													<h3 class="m-section__heading">Billing Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted" data-address-message="">Billing Address is not provided</small></h5>
													<div class="m-section__content m--hide" id="purchase_billing_address">
														<input type="hidden" name="billingAddressId" id="billingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">																
																<h5 class=""><small class="text-muted" data-address-name=""></small></h5>
																<p class="mb-0">
																	<span data-address-line-1=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode=""></span>
																	<span data-address-city=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country=""></span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""></span></span>
																</p>
																<button type="button" class="btn btn-link" data-toggle="m-popover" data-trigger="click" 
																	title="Billing Address <a href='#' data-popover-close='' class='m--font-bolder m-link m-link--state  m-link--danger float-right'>Cancel</a>" data-html="true" data-content="" id="billing_address_btn">Change Address</button>
																
															</div>
														</div>
													</div>
													
												<%-- </c:if> --%>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Shipping Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted" data-address-message="">Shipping Address is not provided</small></h5>
													<div class="m-section__content m--hide" id="purchase_shipping_address">
														<input type="hidden" name="shippingAddressId" id="shippingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">
																<h5 class=""><small class="text-muted" data-address-name=""></small></h5>
																<p class="mb-0">
																	<span data-address-line-1=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2=""></span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode=""></span>
																	<span data-address-city=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state=""></span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country=""></span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""></span></span>
																</p>
																<button type="button" class="btn btn-link" data-toggle="m-popover" data-trigger="click" 
																	title="Shipping Address <a href='#' data-popover-close='' class='m--font-bolder m-link m-link--state  m-link--danger float-right'>Cancel</a>" data-html="true" data-content="" id="shipping_address_btn">Change Address</button>
																	
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
										
												<div class="form-group m-form__group row">
													<!-- <label class="col-form-label col-lg-12 col-sm-12">&nbsp;</label> -->
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="m-checkbox-inline">
															<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
															<input type="checkbox" name="sez" value="0"> Export / SEZ
															<span></span>
															</label><i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="If different than the corresponding address"></i>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Bill Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" name="purchaseDate" readonly id="purchaseDate" onchange="changeDueDate()" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Reference Bill No:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="billNo" placeholder="Bill No" value="">
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">${displayType} No :</label>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" readonly="readonly" name="prefix" placeholder="Prefix" value="${prefix}">
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" readonly="readonly" name="purchaseNo" placeholder="Purchase No" value="${purchaseNo}">
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Payment Term:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2" id="paymentTermVo" name="paymentTermsVo.paymentTermId" placeholder="Select Payment Term" onchange="changeDueDate()">
																<option value="">Select Payment Term</option>
																<c:forEach items="${paymentTermVos}" var="paymentTermVo">
																	<option value="${paymentTermVo.paymentTermId}" name="${paymentTermVo.paymentTermDay}">
																		${paymentTermVo.paymentTermName}
																	</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Due Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input clearbtn-datepicker" data-date-format="dd/mm/yyyy" name="dueDate" id="purchaseDueDate"/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Delivery Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input today_clear-btn-datepicker" data-date-format="dd/mm/yyyy" name="deliveryDate" id="deliveryDate"/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
														Product Details
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab">
														Terms & Condition / Note
													</a>
												</li>
												
											</ul>
										</div>
										
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    
															    <thead>
															      <tr>
															        <!-- <th>#</th> -->
															        <th>#</th>
															        <th>Product</th>
															        <th>Qty</th>
															        <th>Price</th>
															        <th>Discount</th>
															        <th>Tax</th>
															        <th>Total</th>
															        <th></th>
															        
															      </tr>
															    </thead>
															    <tbody data-purchase-list="">
															    	<tr data-purchase-item="template" class="m--hide">
															    		<td class="" style="width: 50px;">
															    			<span data-item-index></span>
															    		</td>
																        <td class="" style="width: 280px;">
																        	<a href="#purchase-item-variant{index}" data-purchase-item-toggle="" data-item-name aria-expanded="true" data-toggle="collapse" class="m-link m--font-bolder collapsed	"></a>
																			<br/>
																			<span class="m--font-bold" ><abbr title="Description" data-item-description=""></abbr></span>
																			<br/>
																			<c:if test="${type==Constant.PURCHASE_BILL}">
																				<span class="m-badge m-badge--success m-badge--wide m-badge--rounded mt-1 m--hide" data-item-designno>Design No</span>
																				<span class="m-badge m-badge--info m-badge--wide m-badge--rounded mt-1 m--hide" data-item-baleno>Bale no</span>
																			</c:if>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="p-0 text-right m--font-bolder">
																        		<span class="m--regular-font-size-sm1" data-item-qty="">psc</span>
																        		<span class="m--regular-font-size-sm1" data-item-uom="">psc</span>
																        	</div>
																        	<!-- <a href="#" class="m-link m--font-bolder">Add Design no</a> -->
																        	<!-- <span class="m--font-boldest float-right" data-item-designno=""></span> -->
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="p-0 text-right m--font-bolder" data-item-price=""></div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="p-0 text-right m--font-bolder" data-item-discount=""></div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="p-0 m--font-bolder" >
																        		
																        		<span class="m--font-info" data-item-tax-name=""></span>
																        		<span class="float-right">Rs. <span data-item-tax-amount="">0</span></span>
																        	</div>
																        	
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="p-0 text-right m--font-bolder" data-item-amount="">0</div>
																        </td>
																        <td class="" style="width: 40px;">
																        	<a href="#" data-item-remove="" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-times"></i></a>
																        	<input type="hidden" name="" id="haveVariant{index}" value=""/>
																        	<input type="hidden" name="" id="haveDesignno{index}" value=""/>
																        	<input type="hidden" name="" id="taxIncluded{index}" value="0"/>
																        	<input type="hidden" name="" id="noOfDecimalPlaces{index}" value="0"/>
																        	<input type="hidden" name="" id="haveBaleno{index}" value="0"/>
																        </td>
															    	</tr>
															    	<tr class="collapse" id="purchase-item-variant{index}" data-variant-row="template">
																      	<td colspan="8" class="bg-light" >
																	      	<table class="table m-table m--margin-left-50 border-0 " data-variant-table="" style="width: 590px">
																	    
																			    <tbody>
																			      <tr data-variant-item="">
																			      	<td class="" style="width:380px; style="background-color: #f9fafb">
																			        	<span class="m--font-info m--font-bolder" data-variant-name=""></span>
																			        </td>
																			        <td class="" style="width: 210px" >
																			        	<div class="p-0 text-right m--font-bolder" data-variant-qty="">0</div>
																			        	<input type="hidden" name="purchaseItemVos[{index}].product.productId" id="productId{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].productVariantVo.productVariantId" id="productVariantId{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].qty" id="qty{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].price" id="price{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].productDescription" id="productDescription{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].taxAmount" id="taxAmount{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].taxRate" id="taxRate{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].taxVo.taxId" id="taxId{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].discount" id="discount{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].discountType" id="discountType{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].designNo" id="designNo{index}" value=""/>
																			        	<input type="hidden" name="purchaseItemVos[{index}].baleNo" id="baleNo{index}" value=""/>
																			        </td>
																			      </tr>
																			    </tbody>
																			 </table>
																		</td>
															      	</tr>
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th>
															        	<select class="form-control m-select2" id="productId" name="" placeholder="Select Product">
																			<option value="">Select Product</option>
																			<c:forEach items="${productVos}" var="productVo">
																				<option value="${productVo.productId}">${productVo.categoryVo.categoryName} ${productVo.name}</option>
																			</c:forEach>
																		</select>
															        </th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id="product_sub_total">0</span></th>
															        <th></th>
															      </tr>
															    </tfoot>
														  	</table>
														</div>
														
													</div>
													
												</div>
											</div>
											<div class="tab-pane" id="m_tabs_7_2" role="tabpanel">
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Transport:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<div class="input-group" >
																	<select class="form-control m-select2" id="transportId" name="contactTransportVo.contactId" placeholder="Select Transport">
																		<option value="">Select Transport</option>
																		<c:forEach items="${contactTransportVos}" var="contactVo">
																			<option value="${contactVo.contactId}">
																				${contactVo.companyName}
																			</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Agent:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<select class="form-control m-select2" id="agentId" name="contactAgentVo.contactId" placeholder="Select Agent">
																	<option value="">Select Agent</option>
																	<c:forEach items="${contactAgentVos}" var="contactVo">
																		<option value="${contactVo.contactId}">
																			${contactVo.companyName}
																		</option>
																	</c:forEach>
																</select>
															</div>
														</div>
													</div>
												</div>
												<!-- <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div> -->
												<div class="m-divider mb-5 mt-5"><span></span></div>
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<c:set var="termsAndConditionIds" scope="page" value="" />
														<table class="table table-sm m-table table-striped" id="terms_and_condition_table">
														  	<thead>
														  		<tr class="row">
															      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
														    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
														    			<span>Terms & Condition</span>
														    			<span class="float-right">
														    				<a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																				data-target="#terms_and_condition" id="terms_condition_button" title="Edit"> <i class="fa fa-edit"></i></a>
																		</span>
														    		</th>
														    	</tr>
														  	</thead>
														  	<tbody>
																<c:set var="termsAndConditionIds" scope="page" value=""/>
																<c:forEach items="${termsAndConditionVos}" var="termsAndConditionVo" varStatus="status">
																	<c:set var="termsAndConditionIds" scope="page" value="${termsAndConditionIds}${termsAndConditionVo.termsandConditionId},"/>
																	<tr class="row" data-terms-item="">
																      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index="">
																      		${status.index+1}
																      	</td>
																      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name="">${termsAndConditionVo.termsCondition}</td>
															    	</tr>
																</c:forEach>
																
																<tr class="row m--hide" data-terms-item="template">
															      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index=""></td>
															      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name=""></td>
														    	</tr>
														  	</tbody>
														</table>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Note:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<textarea class="form-control m-input" name="note" placeholder="Enter a note"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 collapse" id="additional_charge_div">
										
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Additional Charge
												</h3>
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<button type="button" class="btn btn-link" href="#additional_charge_div" data-toggle="collapse" id="additional_charge_cancel">Cancel</button>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="additional_charge_table">
													    
													    <thead>
													      <tr>
													        <!-- <th>#</th> -->
													        <th style="width: 50px;">#</th>
													        <th style="width: 280px;">Additional Charge</th>
													        <th style="width: 180px;">Amount</th>
													        <th style="width: 180px;">Tax</th>
													        <th style="width: 180px;">Total</th>
													        <th style="width: 40px;"></th>
													      </tr>
													    </thead>
													    <tbody data-charge-list="">
													    	<tr data-charge-item="template" class="m--hide">
													    		<td class="" style="width: 50px;" data-item-index=""></td>
														        <td class="" style="width: 280px;">
														        	<div class="form-group m-form__group p-0">
																		<select class="form-control" id="additionalCharge{index}" onchange="getAdditionalChargeInfo({index})" name="purchaseAdditionalChargeVos[{index}].additionalChargeVo.additionalChargeId" placeholder="Additional Charge">
																			<option value="">Select Additional Charge</option>
																		</select>
																	</div>
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class="form-group m-form__group p-0">
														        		<input type="text" class="form-control m-input text-right" id="additionalChargeAmount{index}" name="purchaseAdditionalChargeVos[{index}].amount" onchange="setAdditionalChargeAmount({index})" placeholder="Amount" value="0">
														        	</div>
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class=" m--font-bolder">
														        		<span class="m--font-info" data-item-tax-name=""></span>
														        		<span class="float-right">Rs. <span data-item-tax-amount="">0</span></span>
														        	</div>
														        	
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class="text-right m--font-bolder" data-item-amount="">
														        		
														        	</div>
														        </td>
														        <td class="" style="width: 40px;">
														        	<a href="#" data-item-remove="" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-times"></i></a>
														        	<input type="hidden" name="purchaseAdditionalChargeVos[{index}].taxAmount" id="additionalChargeTaxAmount{index}" value=""/>
														        	<input type="hidden" name="purchaseAdditionalChargeVos[{index}].taxRate" id="additionalChargeTaxRate{index}" value=""/>
														        	<input type="hidden" name="purchaseAdditionalChargeVos[{index}].taxVo.taxId" id="additionalChargeTaxId{index}" value=""/>
														        </td>
													    	</tr>
														</tbody>
														<tfoot>
															<tr>
																<th></th>
																<th>
																	<div class="m-demo-icon mb-0">
																		<div class="m-demo-icon__preview">
																			<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																		</div>
																		<div class="m-demo-icon__class">
																		<a href="#" data-toggel="modal" class="m-link m--font-boldest" id="add_additional_charge"> Add More Additional Charge</a>
																		</div>
																	</div>
																</th>
																<th></th>
																<th></th>
																<th><span class="m--font-boldest float-right" id="additional_charge_sub_total">0.0</span></th>
																<th></th>
													      	</tr>
													   </tfoot>
												  	</table>
												</div>
												
											</div>
											
										</div>
										
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet" style="margin-top: -2.2rem">
									
									<div class="m-portlet__foot m-portlet__foot--fit">
										
										<div class="m-form__actions m-form__actions--solid m--padding-bottom-15">
											<div class="row m--padding-top-20 m--padding-left-20 m--padding-bottom-0 m--padding-right-20">
										
												<div class="col-lg-4 col-md-4 col-sm-12 ">
													<div class="m-demo-icon">
														<div class="m-demo-icon__preview " >
															<span class=""><i class="flaticon-plus m--font-primary"></i></span>
														</div>
														<div class="m-demo-icon__class">
															<a href="#additional_charge_div" id="additional_charge_button" data-toggle="collapse" class="m-link m--font-boldest">Add Additional Charges</a>
														</div>
													</div>
													
													<table class="table table-sm m-table table-striped mb-0 m--margin-left-20 collapse" id="tax_summary_table">
													  	<thead>
													  		<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax</th>
													    		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Rate</th>
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Amount</th>
														      	
													    	</tr>
													  	</thead>
													  	<tbody>
													    	
													    	<tr class="row m--hide" data-tax-item="template">
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
													    	</tr>
													  	</tbody>
													</table>
													
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<table class="table m-table text-right mb-0">
													  	<tbody>
													    	<tr class="row ">
														      	<th scope="row" class="col-lg-8 col-md-8 col-sm-12 m--font-info"><a href="#tax_summary_table" data-toggle="collapse" class="m-link m-link--info m-link--state">Tax Amount</a></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="tax_amount">0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h6>Total Amount</h6></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="total_amount">0</h6></td>
													    	</tr>
													    	<tr class="row"> 
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12 ">
													      			<a href="JavaScript:void(0);" data-toggle="m-popover" data-trigger="click" title="Roundoff" data-html="true" id="roundoff_edit" data-content="" class="m-link m-link--info m-link--state" >Roundoff</a>
													      		</th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="round_off">0.0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h4>Net Amount</h4></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h3 id="net_amount">0</h3></td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												
											</div>
											<!-- <a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest m--margin-left-30">Show Tax Details</a> -->
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="savepurchase">
									Submit
								</button>
								
								<a href="<%=request.getContextPath()%>/purchase/${type}" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<div class="m-section m--hide" id="roundoff_section">
			<div class="m-section__content">
				<div class="form-group m-form__group row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<input type="text" class="form-control m-input text-right" name="roundoff" id="" placeholder="Roundoff" value="">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-6 col-md-6 col-sm-6">
					<a type="button" class="btn btn-secondary" data-popover-close=''>Cancel</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
					<button type="button" class="btn btn-brand float-right" id="" onclick="setRoundoff()">Set</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="m-section m--hide" id="address_template">
			<div class="m-section__content" data-address-list="">
				<div class="row" data-address-item="">
					<div class="col-lg-12 col-md-12 col-sm-12 mt-3 mb-3"><div class="m-divider"><span></span></div></div>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<h4 class=""><small class="text-muted" data-address-name="">Nilesh Desai</small> <button type="button" data-change-address="" class="btn btn-outline-brand btn-sm float-right ml-5">Select</button></h4>
						<p class="mb-0">
							<span class="" data-address-line-1="">Shapath X</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-line-2="">Shamal Cross Road,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-pincode="">380001</span>
							<span class="" data-address-city="">Ahmedavad,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-state="">Gujarat,</span>
							<span class="" data-address-country="">India</span>
						</p>
						<p class="mb-0">
							<span class="m--font-info"><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno="">9714866160</span></span>
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<!--begin::Modal-->
		<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<input type="hidden" id="modalTax" value=""/>
						<input type="hidden" id="modalTaxId" value=""/>
						<input type="hidden" id="modalTaxRate" value=""/>
						<input type="hidden" id="modalDiscountType" value=""/>
						<input type="hidden" id="modalHaveDesignno" value="1"/>
						<input type="hidden" id="modalHaveBaleno" value="0"/>
						<input type="hidden" id="modalHaveVariant" value="1"/>
						<input type="hidden" id="modalProductVariantId" value=""/>
						<input type="hidden" id="modalUOM" value=""/>
						<input type="hidden" id="modalTaxIncluded" value="0"/>
						<input type="hidden" id="purchaseItemIndex" value="-1"/>
						<input type="hidden" id="modalNoOfDecimalPlaces" value="0"/>
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Product:</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<select class="form-control m-select2" id="productIdModal" name="" placeholder="Select Product" onchange="getProductInfo()">
									<option value="">Select Product</option>
									<c:forEach items="${productVos}" var="productVo">
										<option value="${productVo.productId}">${productVo.categoryVo.categoryName} ${productVo.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<c:if test="${type == Constant.PURCHASE_BILL }">
							<div class="form-group m-form__group row ">
								<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub " id="designno_div">
									<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Design No.:</label>
									<input type="text" class="form-control m-input" id="modalDesignNo" name="" placeholder="Design No." value="">
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub " id="baleno_div">
									<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Bale No.:</label>
									<input type="text" class="form-control m-input" id="modalBaleno" name="" placeholder="Bale No." value="">
								</div>
							</div>
						</c:if>
									
						<div class="form-group m-form__group row" id="qty_div">
							<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Qty:</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<input type="text" class="form-control m-input" id="modalQty" name="" data-decimal="3" placeholder="Qty" value="">
							</div>
						</div>
						<div class="form-group m-form__group row ">
							<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Price:</label>
								<input type="text" class="form-control m-input" id="modalPurchasePrice" name="" placeholder="Price" value="">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub ">
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Discount:</label>
								<!-- <input type="text" class="form-control m-input" id="modalDiscount" name="" placeholder="Discount" value=""> -->
								<div class="input-group">
									<div class="input-group-prepend" style="display: flex; flex-direction: column">
										<button class="btn btn-secondary" type="button" id="btn_percentage" onclick="" style="background-color: #ebedf2;"><i class="fa fa-percentage m--font-info" style="padding: 0.6rem;"></i></button>
										<button class="btn btn-secondary btn-icon" type="button" id="btn_amount" style="background-color: #ebedf2;"><i class="fa fa-rupee-sign m--font-brand" style="padding: 0.6rem; padding-right: 0.8rem"></i></button>
									</div>
									<input type="text" class="form-control" id="modalDiscount" name="" placeholder="Percentage">
								</div>
							</div>
						</div>
						
						<div class="row" id="variant_div">
							<!-- <span class="m--font-bolder" >Variants:</span> -->
							
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="table-responsive">
									<table class="table m-table" id="variant_table">
									    <tbody data-variant-list="">
									    	<tr data-variant-item="" class="m--hide">
										      	<td class="align-middle" style="width: 230px">
										        	<span class="m--font-bolder" data-variant-name=""></span>
										        </td>
										        <td class="" style="width: 220px">
										        	<input type="text" id="modalQty{index}" class="form-control m-input"  placeholder="Qty" value=""/>
										        	<input type="hidden" id="modalProductVariantId{index}" value=""/>
										        </td>
									      	</tr>
									    </tbody>
									 </table>
								 </div>
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<textarea class="form-control m-input" id="modalDescription" placeholder="Description"></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="setProduct()">Add</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
		
		<!--begin::Modal-->
		<div class="modal fade" id="terms_and_condition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<!-- <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div> -->
					<div class="modal-body">
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<table class="table m-table" id="term_condition_edit_table">
								  	<thead>
								  		<tr class="row">
									      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
								    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
								    			<span>Terms & Condition</span>
								    			<!-- <span class="float-right">
								    				<a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
														data-target="#terms_and_condition" title="Edit"> <i class="fa fa-edit"></i></a>
												</span> -->
								    		</th>
								    	</tr>
								  	</thead>
								  	<tbody>
								    	<tr class="row m--hide" data-terms-item="template">
									      	<td class="col-lg-1 col-md-1 col-sm-12">
									      		<div class="m-checkbox-inline">
													<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
													<input type="checkbox" name="{index}" value="0">
													<span></span>
													</label>
												</div>
									      	</td>
									      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name=""></td>
								    	</tr>
									
								  	</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="setTermsAndCondition()">Add</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/purchase/purchase-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		setPurchaseNoVerifyURL("<%=request.getContextPath()%>/purchase/${type}/purchaseno/verify");
		
		setPurchaseType('${type}');
		$(document).ready(function(){
			try {
	  			$('#purchaseDate').datepicker('setDate','today');
	  		} catch(e) {
	  			$('#purchaseDate').datepicker('setDate','<%=session.getAttribute("firstDateFinancialYear")%>');
	  		}
	  		$('#btn_amount').slideUp()
			
			
			setTermsAndConditionIds("${termsAndConditionIds}");
			
		});
	</script>
	
</body>
<!-- end::Body -->
</html>