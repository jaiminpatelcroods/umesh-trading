<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${purchaseVo.prefix}${purchaseVo.purchaseNo} | ${displayType} | Purchase</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
		
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${purchaseVo.prefix}${purchaseVo.purchaseNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/purchase/${type}" class="m-nav__link">
										<span class="m-nav__link-text">${displayType}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="purchase_form" action="/purchase/${type}/save" method="post">
						<input type="hidden" name="taxType" id="taxType" value="${purchaseVo.taxType}"/>
						<input type="hidden" id="state_code" value="${sessionScope.stateCode}"/>
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
									<!-- <div class="card-container">
											  <div class="card" id="card">
											    <div class="side"><button class="btn btn-success card-btn" type="button" id="btn_per" style="border-top-right-radius: 0;border-bottom-right-radius: 0">Go!</button></div>
											    <div class="side back"><button class="btn btn-danger" type="button">GO!</button></div>
											  </div>
											</div> -->
									
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Suppplier:</span>
																		<span>
																			<a href="<%=request.getContextPath() %>/contact/${purchaseVo.contactVo.type}/${purchaseVo.contactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																				${purchaseVo.contactVo.companyName}
																			</a>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span><c:if test="${empty purchaseVo.contactVo.gstin}">N/A</c:if>${purchaseVo.contactVo.gstin}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">${purchaseVo.shippingStateName}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
													<h3 class="m-section__heading">Billing Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Billing Address is not provided</small></h5>
													<div class="m-section__content " id="purchase_billing_address">
														<input type="hidden" name="billingAddressId" id="billingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">																
																<h5 class=""><small class="text-muted" data-address-name="">${purchaseVo.billingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${purchaseVo.billingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${purchaseVo.billingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${purchaseVo.billingPinCode}</span>
																	<span data-address-city="">${purchaseVo.billingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${purchaseVo.billingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${purchaseVo.billingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""><c:if test="${empty purchaseVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${purchaseVo.contactVo.companyMobileno}</span></span>
																</p>
															</div>
														</div>
													</div>
													
												<%-- </c:if> --%>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Shipping Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Shipping Address is not provided</small></h5>
													<div class="m-section__content" id="purchase_shipping_address">
														<input type="hidden" name="shippingAddressId" id="shippingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">
																<h5 class=""><small class="text-muted" data-address-name="">${purchaseVo.shippingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${purchaseVo.shippingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${purchaseVo.shippingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${purchaseVo.shippingPinCode}</span>
																	<span data-address-city="">${purchaseVo.shippingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${purchaseVo.shippingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${purchaseVo.shippingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""><c:if test="${empty purchaseVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${purchaseVo.contactVo.companyMobileno}</span></span>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%-- <div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
												<div class="form-group m-form__group row">
													<!-- <label class="col-form-label col-lg-12 col-sm-12">&nbsp;</label> -->
													<div class="col-lg-12 col-md-12 col-sm-12">
														<span class="m--font-bolder m--regular-font-size-">Export / SEZ : </span>
														<span>
															<c:if test="${purchaseVo.sez==0}">No</c:if>
															<c:if test="${purchaseVo.sez==1}">Yes</c:if>
														</span>
													</div>
												</div>
											</div> --%>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<a href="#" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
														data-skin="dark" data-toggle="m-tooltip" data-placement="top" onclick="printPurchaseReport()" title="Print"><i class="fa fa-print"></i></a>
												</li>
												<li class="m-portlet__nav-item">
													<a href="/purchase/${type}/${purchaseVo.purchaseId}/pdf" target="_blank" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
														data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="PDF"><i class="fa fa-file-pdf"></i></a>	
												</li>
												<li class="m-portlet__nav-item">
													<!--begin: Dropdown-->
					                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--icon m-btn--icon-only">
					                                        <i class="la la-ellipsis-h"></i>
					                                    </a>
					                                    <div class="m-dropdown__wrapper">
					                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
					                                            <div class="m-dropdown__inner">
					                                                <div class="m-dropdown__body">              
					                                                    <div class="m-dropdown__content">
					                                                        <ul class="m-nav">
					                                                            <li class="m-nav__section m-nav__section--first">
					                                                                <span class="m-nav__section-text">Quick Actions</span>
					                                                            </li>
					                                                            <c:if test="${type==Constant.PURCHASE_BILL}">
						                                                            <li class="m-nav__item">
						                                                                <a href="<%=request.getContextPath()%>/payment/new" class="m-nav__link">
						                                                                    <i class="m-nav__link-icon fa fa-credit-card"></i>
						                                                                    <span class="m-nav__link-text">Make Payment</span>
						                                                                </a>
						                                                            </li>
					                                                            </c:if>
					                                                            <c:if test="${type==Constant.PURCHASE_BILL}">
						                                                            <li class="m-nav__item">
						                                                                <form action="<%=request.getContextPath()%>/stocktransfer/new" method="post" id="stocktransfer_form">
						                                                                	<input type="hidden" name="purchaseId" value="${purchaseVo.purchaseId}"> 
						                                                                </form>
						                                                                
						                                                                <a id="stocktransfer_link" class="m-nav__link" href="#">
						                                                                    <i class="m-nav__link-icon fa fa-credit-card"></i>
						                                                                    <span class="m-nav__link-text">Generate Stock Transfer</span>
						                                                                </a>
						                                                            </li>
					                                                            </c:if>
								                                                <li class="m-nav__separator m-nav__separator--fit">
								                                                </li>
								                                                <li class="m-nav__item">
								                                                    <a href="<%=request.getContextPath() %>/purchase/${type}/${purchaseVo.purchaseId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																						<span><i class="flaticon-edit"></i><span>Edit</span></span>
																					</a>
								                                                    <button id="purchase_delete" type="button" data-url="<%=request.getContextPath() %>/purchase/${type}/${purchaseVo.purchaseId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right">
																						<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																					</button>
								                                                </li>
					                                                           
					                                                        </ul>
					                                                    </div>
					                                                </div>
					                                            </div>
					                                    </div>
					                                </div>
					                                <!--end: Dropdown-->
					                            </li>
												
											</ul>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Bill Date:</span>
																		<span>
																			<fmt:formatDate pattern="dd/MM/yyyy" value="${purchaseVo.purchaseDate}" />
																		</span>
																	</div>
																	<c:if test='${type != Constant.PURCHASE_BILL}'>
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Reference Bill No:</span>
																			<span>${purchaseVo.billNo}</span>
																		</div>
																	</c:if>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Purchase Order No:</span>
																		<span>${purchaseVo.prefix}${purchaseVo.purchaseNo}</span>
																	</div>
																	
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Delivery Date:</span>
																		<span>
																			<c:if test="${empty purchaseVo.deliveryDate}">N/A</c:if>
																			<c:if test="${not empty purchaseVo.deliveryDate}"><fmt:formatDate pattern="dd/MM/yyyy" value="${purchaseVo.deliveryDate}" /></c:if>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Export / SEZ :</span>
																		<span>
																			<c:if test="${purchaseVo.sez==0}">No</c:if>
																			<c:if test="${purchaseVo.sez==1}">Yes</c:if>
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Payment Term:</span>
																		<span>
																			<c:if test="${empty purchaseVo.paymentTermsVo}">N/A</c:if>
																			<c:if test="${not empty purchaseVo.paymentTermsVo}">${purchaseVo.paymentTermsVo.paymentTermName}</c:if>
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Due Date:</span>
																		<span>
																			<c:if test="${empty purchaseVo.dueDate}">N/A</c:if>
																			<c:if test="${not empty purchaseVo.dueDate}"><fmt:formatDate pattern="dd/MM/yyyy" value="${purchaseVo.dueDate}" /></c:if>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid p-0">
											<div class="row">
												<c:if test='${type == Constant.PURCHASE_BILL}'>
													<div class="col-lg-4 col-md-4 col-sm-12">
														<div class="m-widget4">
															<div class="m-widget4__item">
																<div class="m-widget4__info">
																	<span class="m-widget4__title">
																		Due Amount
																	</span><br/>
																	<span class="m-widget4__ext">
																		<span class="m-widget4__number m--font-danger">
																			${purchaseVo.total - salesVo.paidAmount}
																		</span>
																	</span>
																</div>
																
															</div>
														</div>
													</div>
												
													<div class="col-lg-4 col-md-4 col-sm-12">
														<div class="m-widget4">
															<div class="m-widget4__item">
																<div class="m-widget4__info">
																	<span class="m-widget4__title">
																		Paid Amount
																	</span>
																	<br>
																	<span class="m-widget4__ext">
																		<span class="m-widget4__number m--font-success">
																			${purchaseVo.paidAmount}
																		</span>
																	</span>
																</div>
																
															</div>
														</div>
													</div>
												</c:if>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="m-widget4">
														<div class="m-widget4__item">
															<div class="m-widget4__info">
																<span class="m-widget4__title">
																	Total Amount
																</span>
																<br>
																<span class="m-widget4__ext">
																	<span class="m-widget4__number m--font-info">
																		${purchaseVo.total}
																	</span>
																</span>
															</div>
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
														Product Details
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab">
														Terms & Condition / Note
													</a>
												</li>
												
											</ul>
										</div>
										
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    
															    <thead>
															      <tr>
															        <!-- <th>#</th> -->
															        <th>#</th>
															        <th>Product</th>
															        <th>Qty</th>
															        <th>Price</th>
															        <th>Discount</th>
															        <th>Tax</th>
															        <th>Total</th>
															        <th></th>
															        
															      </tr>
															    </thead>
															    <tbody data-purchase-list="">
															    	<c:set var="index" scope="page" value="0"/>
															    	<c:forEach items="${productIds}" var="productId">
															    		
															    		<c:set scope="page" var="purchaseItemVos" value="${purchaseVo.purchaseItemVos.stream().filter( item -> item.getProductVariantVo().getProductVo().getProductId() == productId).toList()}"/>
																    	
															    		<tr data-purchase-item="${index}">
																    		<td class="" style="width: 50px;">
																    			<span data-item-index></span>
																    		</td>
																	        <td class="" style="width: 280px;">
																	        	<c:if test="${purchaseItemVos.get(0).productVariantVo.productVo.haveVariation == 1}">
																					<a href="#purchase-item-variant${index}" data-purchase-item-toggle="" data-item-name aria-expanded="true" data-toggle="collapse" class="m-link m--font-bolder collapsed">
																						${purchaseItemVos.get(0).productVariantVo.productVo.categoryVo.categoryName}
																						${purchaseItemVos.get(0).productVariantVo.productVo.name}
																						<i class="fa fa-clone m--regular-font-size-sm2 "></i>
																					</a>
																				</c:if>
																				<c:if test="${purchaseItemVos.get(0).productVariantVo.productVo.haveVariation == 0}">
																					<a href="#" data-table-name="" aria-expanded="true" data-toggle="collapse" class="m-link m--font-bolder collapsed">
																						${purchaseItemVos.get(0).productVariantVo.productVo.categoryVo.categoryName}
																						${purchaseItemVos.get(0).productVariantVo.productVo.name}
																					</a>
																				</c:if>
																				<br/>
																				<span class="m--font-bold" data-item-description="">${purchaseItemVos.get(0).productDescription}</span>
																				<br/>
																				<c:if test="${type==Constant.PURCHASE_BILL}">
																					<c:if test="${purchaseItemVos.get(0).productVariantVo.productVo.haveDesignno == 1}">
																						<span class="m-badge m-badge--success m-badge--wide m-badge--rounded mt-1" data-item-designno>${purchaseItemVos.get(0).designNo}</span>
																					</c:if>
																					
																					<c:if test="${purchaseItemVos.get(0).productVariantVo.productVo.haveBaleno == 1}">
																						<span class="m-badge m-badge--info m-badge--wide m-badge--rounded mt-1" data-item-baleno>${purchaseItemVos.get(0).baleNo}</span>
																					</c:if>
																				</c:if>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder">
																	        		<span class="m--regular-font-size-sm1" data-item-qty="">${purchaseItemVos.get(0).qty}</span>
																	        		<span class="m--regular-font-size-sm1" data-item-uom="">${purchaseItemVos.get(0).productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</span>
																	        	</div>
																	        	<!-- <a href="#" class="m-link m--font-bolder">Add Design no</a> -->
																	        	<span class="m--font-boldest float-right" data-item-designno=""></span>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-price="">${purchaseItemVos.get(0).price}</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-discount="">${purchaseItemVos.get(0).discount}</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 m--font-bolder" >
																	        		
																	        		<span class="m--font-info" data-item-tax-name="">
																	        			${purchaseItemVos.get(0).taxVo.taxName} (${purchaseItemVos.get(0).taxVo.taxRate} %)
																	        		</span>
																	        		<span class="float-right">Rs. <span data-item-tax-amount="">${purchaseItemVos.get(0).taxAmount}</span></span>
																	        	</div>
																	        	
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-amount="">0</div>
																	        </td>
																	        <td class="" style="width: 40px;">
																	        	<input type="hidden" name="" id="haveVariant${index}" value="${purchaseItemVos.get(0).productVariantVo.productVo.haveVariation}"/>
																	        	<input type="hidden" name="" id="haveDesignno${index}" value="${purchaseItemVos.get(0).productVariantVo.productVo.haveDesignno}"/>
																	        	<input type="hidden" name="" id="taxIncluded${index}" value="${purchaseItemVos.get(0).productVariantVo.productVo.purchaseTaxIncluded}"/>
																	        	<input type="hidden" name="" id="noOfDecimalPlaces${index}" value="${purchaseItemVos.get(0).productVariantVo.productVo.unitOfMeasurementVo.noOfDecimalPlaces}"/>
																	        	<input type="hidden" name="" id="haveBaleno{index}" value="${purchaseItemVos.get(0).productVariantVo.productVo.haveBaleno}"/>
																	        </td>
																    	</tr>
																    	
																    	<tr class="collapse" id="purchase-item-variant${index}" data-variant-row="${index}">
																	      	<td colspan="8" class="bg-light" >
																		      	<table class="table m-table m--margin-left-50 border-0 " data-variant-table="" style="width: 590px">
																		    
																				    <tbody>
																				      <c:forEach items="${purchaseItemVos}" var="purchaseItemVo">
																				      <tr data-variant-item="${index}">
																				      	<td class="" style="width:380px; style="background-color: #f9fafb">
																				        	<span class="m--font-info m--font-bolder" data-variant-name="">${purchaseItemVo.productVariantVo.variantName}</span>
																				        </td>
																				        <td class="" style="width: 210px" >
																				        	<div class="p-0 text-right m--font-bolder" data-variant-qty="">${purchaseItemVo.qty}</div>
																				        	<input type="hidden" name="purchaseItemVos[${index}].product.productId" id="productId${index}" value="${purchaseItemVo.productVariantVo.productVo.productId}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].productVariantVo.productVariantId" id="productVariantId${index}" value="${purchaseItemVo.productVariantVo.productVariantId}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].qty" id="qty${index}" value="${purchaseItemVo.qty}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].price" id="price${index}" value="${purchaseItemVo.price}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].productDescription" id="productDescription${index}" value="${purchaseItemVo.productDescription}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].taxAmount" id="taxAmount${index}" value="${purchaseItemVo.taxAmount}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].taxRate" id="taxRate${index}" value="${purchaseItemVo.taxRate}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].taxVo.taxId" id="taxId${index}" value="${purchaseItemVo.taxVo.taxId}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].discount" id="discount${index}" value="${purchaseItemVo.discount}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].discountType" id="discountType${index}" value="${purchaseItemVo.discountType}"/>
																				        	<input type="hidden" name="purchaseItemVos[${index}].designNo" id="designNo${index}" value="${purchaseItemVo.designNo}"/>
																			        		<input type="hidden" name="purchaseItemVos[${index}].baleNo" id="baleNo${index}" value="${purchaseItemVo.baleNo}"/>
																				        </td>
																				      </tr>
																				      <c:set var="index" scope="page" value="${index+1}"/>
																				      </c:forEach>
																				    </tbody>
																				 </table>
																			</td>
																      	</tr>
															    	</c:forEach>
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id="product_sub_total">0</span></th>
															        <th></th>
															      </tr>
															    </tfoot>
														  	</table>
														</div>
														
													</div>
													
												</div>
											</div>
											<div class="tab-pane" id="m_tabs_7_2" role="tabpanel">
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="m-widget28">
															<div class="m-widget28__container" >	
																<!-- Start:: Content -->
																<div class="m-widget28__tab tab-content">
																	<div class="m-widget28__tab-container tab-pane active">
																	    <div class="m-widget28__tab-items">
																			<div class="m-widget28__tab-item">
																				<span class="m--regular-font-size-">Transport:</span>
																				<span>
																					<c:if test="${empty purchaseVo.contactTransportVo}">N/A</c:if>
																					<c:if test="${not empty purchaseVo.contactTransportVo}">${purchaseVo.contactTransportVo.companyName}</c:if>
																				</span>
																			</div>
																		</div>					      	 		      	
																	</div>
																</div>
																<!-- end:: Content --> 	
															</div>				 	 
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="m-widget28">
															<div class="m-widget28__container" >	
																<!-- Start:: Content -->
																<div class="m-widget28__tab tab-content">
																	<div class="m-widget28__tab-container tab-pane active">
																	    <div class="m-widget28__tab-items">
																			<div class="m-widget28__tab-item">
																				<span class="m--regular-font-size-">Agent:</span>
																				<span>
																					<c:if test="${empty purchaseVo.contactAgentVo}">N/A</c:if>
																					<c:if test="${not empty purchaseVo.contactAgentVo}">${purchaseVo.contactAgentVo.companyName}</c:if>
																				</span>
																			</div>
																		</div>					      	 		      	
																	</div>
																</div>
																<!-- end:: Content --> 	
															</div>				 	 
														</div>
													</div>
												</div>
												<!-- <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div> -->
												<div class="m-divider mb-4 mt-4"><span></span></div>
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<c:set var="termsAndConditionIds" scope="page" value="" />
														<table class="table table-sm m-table table-striped" id="terms_and_condition_table">
														  	<thead>
														  		<tr class="row">
															      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
														    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
														    			<span>Terms & Condition</span>
														    			<span class="float-right">
														    				<!-- <a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																				data-target="#terms_and_condition" id="terms_condition_button" title="Edit"> <i class="fa fa-edit"></i></a> -->
																		</span>
														    		</th>
														    	</tr>
														  	</thead>
														  	<tbody>
																<c:set var="termsAndConditionIds" scope="page" value=""/>
																<c:forEach items="${termsAndConditionVos}" var="termsAndConditionVo" varStatus="status">
																	<tr class="row" data-terms-item="">
																      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index="">
																      		${status.index+1}
																      	</td>
																      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name="">${termsAndConditionVo.termsCondition}</td>
															    	</tr>
																</c:forEach>
														  	</tbody>
														</table>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<table class="table table-sm m-table  m-table--head-no-border">
														  	<thead>
														  		<tr class="row">
														    		<th scope="row" class="col-lg-12 col-md-12 col-sm-12">Note:</th>
														    	</tr>
														  	</thead>
														  	<tbody>
														    	<tr class="row">
															      	<td class="col-lg-12 col-md-12 col-sm-12">
															      		<span><c:if test="${empty purchaseVo.note}">No Note.</c:if>${purchaseVo.note}</span>
															      	</td>
														    	</tr>
														  	</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12" id="additional_charge_div">
										
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Additional Charge
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="additional_charge_table">
													    
													    <thead>
													      <tr>
													        <!-- <th>#</th> -->
													        <th style="width: 50px;">#</th>
													        <th style="width: 280px;">Additional Charge</th>
													        <th style="width: 180px;">Amount</th>
													        <th style="width: 180px;">Tax</th>
													        <th style="width: 180px;">Total</th>
													        <th style="width: 40px;"></th>
													      </tr>
													    </thead>
													    <tbody data-charge-list="">
													    	<c:if test='${purchaseVo.purchaseAdditionalChargeVos.size() == 0}'>
														    	<tr>
														    		<td colspan="6" class="text-center m--font-bolder">No Additional Charges</td>
														    	</tr>
														    </c:if>
													    	<c:set var="additionalChargeIndex" scope="page" value="0"/>
													    	<c:forEach items="${purchaseVo.purchaseAdditionalChargeVos}" var="purchaseAdditionalChargeVos">
														    	<tr data-charge-item="${additionalChargeIndex}">
														    		<td class="" style="width: 50px;" data-item-index=""></td>
															        <td class="" style="width: 280px;">
															        	<div class="form-group m-form__group p-0">
																			${purchaseAdditionalChargeVos.additionalChargeVo.additionalCharge}
																		</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="p-0 text-right m--font-bolder">
															        		${purchaseAdditionalChargeVos.amount}
															        		<input type="hidden" id="additionalChargeAmount${additionalChargeIndex}" value="${purchaseAdditionalChargeVos.amount}" />
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class=" m--font-bolder">
															        		<span class="m--font-info" data-item-tax-name="">${purchaseAdditionalChargeVos.taxVo.taxName} (${purchaseAdditionalChargeVos.taxRate} %)</span>
															        		<span class="float-right">Rs. <span data-item-tax-amount="">${purchaseAdditionalChargeVos.taxAmount}</span></span>
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="text-right m--font-bolder" data-item-amount="">
															        	</div>
															        </td>
															        <td class="" style="width: 40px;">
															        	<input type="hidden" name="purchaseAdditionalChargeVos[${additionalChargeIndex}].taxAmount" id="additionalChargeTaxAmount${additionalChargeIndex}" value="${purchaseAdditionalChargeVos.taxAmount}"/>
															        	<input type="hidden" name="purchaseAdditionalChargeVos[${additionalChargeIndex}].taxRate" id="additionalChargeTaxRate${additionalChargeIndex}" value="${purchaseAdditionalChargeVos.taxRate}"/>
															        	<input type="hidden" name="purchaseAdditionalChargeVos[${additionalChargeIndex}].taxVo.taxId" id="additionalChargeTaxId${additionalChargeIndex}" value="${purchaseAdditionalChargeVos.taxVo.taxId}"/>
															        </td>
														    	</tr>
														    </c:forEach>
														</tbody>
														<tfoot>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th><span class="m--font-boldest float-right" id="additional_charge_sub_total">0.0</span></th>
																<th></th>
													      	</tr>
													   </tfoot>
												  	</table>
												</div>
												
											</div>
											
										</div>
										
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet" style="margin-top: -2.2rem">
									
									<div class="m-portlet__foot m-portlet__foot--fit">
										
										<div class="m-form__actions m-form__actions--solid m--padding-bottom-15">
											<div class="row m--padding-top-20 m--padding-left-20 m--padding-bottom-0 m--padding-right-20">
										
												<div class="col-lg-4 col-md-4 col-sm-12 ">
													
													<table class="table table-sm m-table table-striped mb-0 m--margin-left-20 collapse" id="tax_summary_table">
													  	<thead>
													  		<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax</th>
													    		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Rate</th>
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Amount</th>
														      	
													    	</tr>
													  	</thead>
													  	<tbody>
													    	
													    	<tr class="row m--hide" data-tax-item="template">
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
													    	</tr>
													  	</tbody>
													</table>
													
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<table class="table m-table text-right mb-0">
													  	<tbody>
													    	<tr class="row ">
														      	<th scope="row" class="col-lg-8 col-md-8 col-sm-12 m--font-info"><a href="#tax_summary_table" data-toggle="collapse" class="m-link m-link--info m-link--state">Tax Amount</a></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="tax_amount">0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h6>Total Amount</h6></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="total_amount">0</h6></td>
													    	</tr>
													    	<tr class="row"> 
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12 ">
													      			<h6>Roundoff</h6>
													      		</th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="round_off">0.0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h4>Net Amount</h4></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h3 id="net_amount">0</h3></td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												
											</div>
											<!-- <a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest m--margin-left-30">Show Tax Details</a> -->
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/purchase/purchase-script.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(document).ready(function () {
			
			$(document).on("click",'a[data-purchase-item-toggle]',function(e) {
				if($(this).closest('[data-purchase-item]').hasClass("m-table__row--brand")) {
					$(this).closest('[data-purchase-item]').removeClass("m-table__row--brand");
				} else {
					$(this).closest('[data-purchase-item]').addClass("m-table__row--brand");
				}
				e.stopPropagation();
			});
			
			var $purchaseItem=$("#product_table").find("[data-purchase-item]").not(".m--hide");
			
			$purchaseItem.each(function (){
				setAmount($(this).attr("data-purchase-item"));
			});
			
	  		setSrNo();
	  		
			var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide"), subTotal=0.0;
			
			$additionalChargeItem.each(function (){
				setAdditionalChargeAmount($(this).attr("data-charge-item"));
			});
			
			setAdditionalChargeSrNo();
			
			$("#round_off").text(${purchaseVo.roundoff});
	  		$("#net_amount").text((parseFloat($("#total_amount").text())+parseFloat(${purchaseVo.roundoff})).toFixed(2));
	  		
		});
	</script>
</body>

<!-- end::Body -->
</html>