<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${displayType} | Purchase</title>
	
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${displayType}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="#" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="<%=request.getContextPath()%>/purchase/${type}/new" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>${displayType}</span></span>
											</a>
										</div>			
									</div>
									
									<!-- <div class="m-portlet__head-tools">
										<a href="#" id="export_print" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
										<a href="#" id="export_excel" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="Excel"> <i class="fa fa-file-excel"></i>
										</a>
										<a href="#" id="export_pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="PDF"> <i class="fa fa-file-pdf"></i>
										</a>
									</div> -->
								</div>
								<div class="m-portlet__body" >
									<div class="row m--margin-bottom-20">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Customer:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<select class="form-control m-select2" id="contactVo" name="contactVo.contactId" placeholder="Select Customer">
													<option value="0">All Customer</option>
													<c:forEach items="${contactVos}" var="contactVo">
														<option value="${contactVo.contactId}">
															${contactVo.companyName}
														</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">From Date:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="input-group date" >
													<input type="text" class="form-control m-input todaybtn-datepicker" name="fromDate" readonly id="fromDate" data-date-format="dd/mm/yyyy" value="<%=session.getAttribute("firstDateFinancialYear")%>"
														data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
													<div class="input-group-append">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">To Date:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="input-group date" >
													<input type="text" class="form-control m-input todaybtn-datepicker" name="toDate" readonly id="toDate" data-date-format="dd/mm/yyyy" value="<%=session.getAttribute("lastDateFinancialYear")%>"
														data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
													<div class="input-group-append">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div  class="m_datatable"  >
												<table class="table table-striped- table-bordered table-hover table-checkable" id="purchase_table">
													<thead>
								  						<tr>
						  									<th>#</th>
						  									<th>${displayType} No.</th>
						  									<th>${displayType} Date</th>
						  									<th>Supplier Name</th>			  									
						  									<th>Total Amount</th>
						  									<c:if test="${type==Constant.PURCHASE_BILL}">
						  										<th>Paid Amount</th>
						  										<th>Due Amount</th>
						  									</c:if>
						  									<th>Action</th>
									  					</tr>
													</thead>			
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
							
						</div>
				
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		var DatatablesDataSourceHtml = {
		    init: function() {
		        $("#purchase_table").DataTable({
		            responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax: {
						url: "<%=request.getContextPath()%>/purchase/${type}/datatable",
						type: "POST",
						"data": function ( d ) {
							return $.extend( {}, d, {
								"contactId": $('#contactVo').val(),
								"fromDate": $('#fromDate').val(),
								"toDate": $('#toDate').val()
							});
						}
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					
					columns: [{
			                data: "purchaseId"
			            }, {
			                data: "purchaseNo"
			            }, {
			                data: "purchaseDate"
			            }, {
			                data: "contactVo.companyName"
			            },{
			                data: "total"
			            },
			            <c:if test="${type==Constant.PURCHASE_BILL}">
			            {
			                data: "paidAmount"
			            },{
			                data: "total"
			            },
			            </c:if>{
			                data: "purchaseId"
			            }],
					
					columnDefs: [{
							targets: 0,
							orderable: !1,
							render: function(a, e, t, n) {
								return (n.row+n.settings._iDisplayStart+1);
							}
						},{
							targets:1,
			                orderable: !1,
			               	render: function(a, e, t, n) {
			                  
			            	   return '\n  <a href="/purchase/${type}/'+t.purchaseId+'" class="m-link m--font-bolder" aria-expanded="true">\n '+t.prefix+t.purchaseNo+'</a>\n '
			                }
						},{
							targets: 2,
							render: function(a, e, t, n) {
								return moment(a).format('DD/MM/YYYY');
							}
						},
						<c:if test="${type==Constant.PURCHASE_BILL}">
						{
							targets: 6,
							render: function(a, e, t, n) {
								
								return (t.total*1)-(t.paidAmount*1);
							}
						},
						</c:if>
						{
							<c:if test="${type!=Constant.PURCHASE_BILL}">
							targets: 5,
							</c:if>
							<c:if test="${type==Constant.PURCHASE_BILL}">
							targets: 7,
							</c:if>
							orderable: !1,
							render: function(a, e, t, n) {
								var action = "";
								action += '<a href="${pageContext.request.contextPath}/purchase/${type}/'+t.purchaseId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
								action += '<button onclick="deletePurchase("'+t.purchaseId+'")" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></button>'
								action += '<span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="fa fa-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" target="_blank" href="${pageContext.request.contextPath}/purchaserequest/'+t.purchaseId+'/pdf"><i class="fa fa-file-pdf"></i> PDF</a>\n</div>\n</span>\n '
								return action;
							}
			            }],
		            fixedHeader: {
		                header: false,
		                footer: false
		            }
		            
		        }),$("#contactVo,#fromDate,#toDate").on("change", function(t) {
		        	$('#purchase_table').DataTable().draw()
	            })
		    }
		};
		
		jQuery(document).ready(function() {
			
			DatatablesDataSourceHtml.init();
			
			$(".dt-buttons").addClass("m--hide");
		});
	</script>
	
</body>
<!-- end::Body -->
</html>