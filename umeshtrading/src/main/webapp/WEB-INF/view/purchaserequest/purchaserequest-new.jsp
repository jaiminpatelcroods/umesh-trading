<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>New Purchase Request</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New Purchase Request</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/purchaserequest" class="m-nav__link">
										<span class="m-nav__link-text">Purchase Request</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="purchaserequest_new_form" action="/purchaserequest/save" method="post">	
						<input type="hidden" name="total" id="total" value="0"/>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Purchase Request</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Request No.:</label>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" readonly="readonly" name="prefix" placeholder="Prefix" value="${prefix}">
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" readonly="readonly" name="requestNo" placeholder="Request No" value="${requestNo}">
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Notes:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea class="form-control m-input" placeholder="Notes" name="note"></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">	
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Request Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" id="requestDate"  name="requestDate" readonly placeholder="Request Date" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-calendar"></i></span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Required Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" id="requireDate"  name="requireDate" readonly placeholder="Required Date" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-calendar"></i></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">																					
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="product_table">
														<thead>
															<tr>
																<th>#</th>
																<th>Product</th>
																<th>Qty</th>
																<th></th>
															</tr>
													    </thead>
													    <tbody data-table-list="">
													    	<tr data-table-item="template" class="m--hide">
													    		<td style="width: 50px;" class="m--font-bold">
													    			<span data-table-index>0</span>
													    		</td>
														        <td class="m--font-info m--font-bolder"  style="width: 280px;">
																	<a href="#purchase-item-variant{index}" data-purchase-item-toggle="" data-table-name="" aria-expanded="true" data-toggle="collapse" class="m-link m--font-bolder collapsed	"></a>
														        </td>
														        <td class="m--font-bold" style="width: 180px;">
														        	<span data-table-qty="">25</span>
														        	<span class="m--regular-font-size-sm1" data-table-uom="">psc</span>
														        </td>
														        <td style="width: 40px;">
														        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
														        	<input type="hidden" name="" id="haveVariant{index}" value=""/>
														        </td>
													    	</tr>
													    	<tr class="collapse" id="purchase-item-variant{index}" data-variant-row="template">
														      	<td colspan="8" class="bg-light" >
															      	<table class="table m-table m--margin-left-50 border-0 " data-variant-table="" style="width: 590px">
															    
																	    <tbody>
																	      <tr data-variant-item="">
																	      	<td class="" style="width:380px; style="background-color: #f9fafb">
																	        	<span class="m--font-info m--font-bolder" data-variant-name=""></span>
																	        </td>
																	        <td class="" style="width: 210px" >
																	        	<div class="p-0 text-right m--font-bolder" data-variant-qty="">0</div>
																	        	<input type="hidden" name="purchaseRequestItemVos[{index}].product.productId" id="productId{index}" value=""/>
																	        	<input type="hidden" name="purchaseRequestItemVos[{index}].productVariantVo.productVariantId" id="productVariantId{index}" value=""/>
																	        	<input type="hidden" name="purchaseRequestItemVos[{index}].qty" id="qty{index}" value=""/>
																	        </td>
																	      </tr>
																	    </tbody>
																	 </table>
																</td>
													      	</tr>
														</tbody>
														<tfoot>
													      <tr>
													        <th></th>
													        <th>
													        	<select class="form-control m-select2" id="productId" name="" placeholder="Select Product">
																	<option value="">Select Product</option>
																	<c:forEach items="${productVos}" var="productVo">
																		<option value="${productVo.productId}">${productVo.categoryVo.categoryName} ${productVo.name}</option>
																	</c:forEach>
																</select>
													        </th>
													        <th></th>
													        <th></th>
													      </tr>
													    </tfoot>
												  	</table>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m-form__actions--right">
											<button type="submit" class="btn btn-brand" id="savepurchaserequest">
												Submit
											</button>
											
											<a href="/purchaserequest" class="btn btn-secondary">
												Cancel
											</a>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!--begin::Modal-->
		<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<!-- <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div> -->
					<div class="modal-body">
						<input type="hidden" id="modalUOM" value=""/>
						<input type="hidden" id="modalHaveVariant" value="1"/>
						<input type="hidden" id="modalProductVariantId" value=""/>
						<input type="hidden" id="purchaseItemIndex" value="-1"/>
						
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Product:</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<select class="form-control m-select2" id="productIdModal" name="" placeholder="Select Product" onchange="getProductInfo()">
									<option value="">Select Product</option>
									<c:forEach items="${productVos}" var="productVo">
										<option value="${productVo.productId}" >${productVo.categoryVo.categoryName} ${productVo.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group m-form__group row ">
							<div class="col-lg-12 col-md-12 col-sm-12 m-form__group-sub " id="qty_div">
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Qty:</label>
								<input type="text" class="form-control m-input" id="modalQty" name="" placeholder="Qty" value="">
							</div>
						</div>
						<div class="row" id="variant_div">
							<!-- <span class="m--font-bolder" >Variants:</span> -->
							
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="table-responsive">
									<table class="table m-table" id="variant_table">
									    <tbody data-variant-list="">
									    	<tr data-variant-item="" class="m--hide">
										      	<td class="align-middle" style="width: 230px">
										        	<span class="m--font-bolder" data-variant-name=""></span>
										        </td>
										        <td class="" style="width: 220px">
										        	<input type="text" id="modalQty{index}" class="form-control m-input"  placeholder="Qty" value=""/>
										        	<input type="hidden" id="modalProductVariantId{index}" value=""/>
										        </td>
									      	</tr>
									    </tbody>
									 </table>
								 </div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="setProduct()">Add</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<!--[if IE]>
		Nileshhhhhshhhhhhh
		<![endif]-->
		
		 <!-- <![if IE]>
	    <p>The page content for all other browsers.</p>
	    <![endif]>	 -->
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/purchaserequest/purchaserequest-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			try {
	  			$('#requestDate').datepicker('setDate','today');
	  		}catch(e){	
	  			$('#requestDate').datepicker('setDate','<%=session.getAttribute("firstDateFinancialYear")%>');
	  		}
	  		 // Internet Explorer 6-11
	  	    var isIE = /*@cc_on!@*/false || !!document.documentMode;

	  	    // Edge 20+
	  	    var isEdge = !isIE && !!window.StyleMedia;
	  		if(isIE==true) {
	  			document.body.innerHTML = "You’re using Internet Explorer, which is not supported by VasyERP."
	  		}
	  		 var ua = window.navigator.userAgent;
		});
	</script>
	
</body>
<!-- end::Body -->
</html>