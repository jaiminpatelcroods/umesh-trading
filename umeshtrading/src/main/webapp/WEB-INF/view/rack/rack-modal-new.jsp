<div class="modal fade" id="rack_new_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New Rack</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<form id="rack_new_form" class="m-form m-form--state m-form--fit">
				<div class="modal-body">
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Place:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<select class="form-control m-select2" id="placeVo" name="placeVo.placeId" placeholder="Select Place" style="width: 100%" required="required" data-fv-notempty-message="Place is required" onchange="getAllFloorAjax()"> 
								<option value="">Select Place</option>
								<c:forEach items="${placeList}" var="PlaceList">
									<option value="${PlaceList.placeId}">
										${PlaceList.placeCode} - ${PlaceList.placeName}	
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Floor:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<select class="form-control m-select2" id="floorVo" name="floorVo.floorId" placeholder="Select Floor" style="width: 100%" required="required" data-fv-notempty-message="Floor is required"> 
								<option value="">Select Floor</option>
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Rack Name:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="rackName" id="rackName" placeholder="Rack Name" value="" required="required" data-fv-notempty-message="Rack Name is required">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Rack Code:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="rackCode" id="rackCode" placeholder="Rack Code" value="" >
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<textarea class="form-control m-input" name="description" id="description" placeholder="Description" cols="100" rows="3" required="required" data-fv-notempty-message="Desc. is required"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
						Close
					</button>
					<button type="button" onclick="" id="saverack" class="btn btn-primary">
						Save
					</button>
					
				</div>
			</form>
		</div>
	</div>
</div>