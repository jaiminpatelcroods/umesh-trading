<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
        <%@page import="com.croods.umeshtrading.constant.Constant"%>
<!DOCTYPE html>
<html>

<head>
	
	<%@include file="../../header/head.jsp" %>
	
	<title>Customer Product Ledger</title>
	
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>

<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../../header/navigation.jsp"%>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Customer Product Ledger
							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath() %>/report" class="m-nav__link">
										<span class="m-nav__link-text">Reports</span>
									</a>
								</li>
							</ul>

						</div>
					</div>
				</div>
				<!-- END: Subheader -->

				<div class="m-content">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="row m--margin-bottom-20">
										<div class="col-lg-3 col-md-3 col-sm-12">
											<label class="col-form-label">Select Customer:</label>
											<select class="form-control m-select2" id="contact_id">
										  		<c:forEach items="${ContactList}" var="ContactList">
													<option value="${ContactList.contactId}" >${ContactList.name} - ${ContactList.companyName}</option>
												</c:forEach>
										  	</select>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12">
											<label class="col-form-label">Select Product:</label>
											<select class="form-control m-select2" id="product_id">
										  		<c:forEach items="${ProductList}" var="ProductList">
													<option value="${ProductList.productId}">${ProductList.name}</option>
												</c:forEach>
										  	</select>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12">
											<label class="col-form-label">From Date:</label>
											<div class="input-group date" >
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar"></i>
													</span>
												</div>
												<input type="text" class="form-control m-input todaybtn-datepicker" name="fromDate" readonly id="fromDate" data-date-format="dd/mm/yyyy" value="<%=session.getAttribute("firstDateFinancialYear")%>"
													data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12">
											<label class="col-form-label">To Date:</label>
											<div class="input-group date" >
												<div class="input-group-append">
													<span class="input-group-text">
														To &nbsp;<i class="la la-calendar"></i>
													</span>
												</div>
												<input type="text" class="form-control m-input todaybtn-datepicker" name="toDate" readonly id="toDate" data-date-format="dd/mm/yyyy" value="<%=session.getAttribute("lastDateFinancialYear")%>"
													data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
											</div>
										</div>
										<div class="col-3" style="text-align: center;margin-top: 40px;">
										
										<button type="button" onclick="getReport('view')" class="btn btn-primary">
											<span>
												<i class="fa fa-search"></i>
											</span>
										</button>
										
										<button type="button" onclick="getReport('pdf')" class="btn btn-danger">
											<span>
												<i class="fa fa fa-file-pdf"></i>
											</span>
										</button>
										
									</div> 
				
									
									<form id="pdfForm"  action="<%=request.getContextPath() %>/report/customerproductledgerReport" method="post"  target="iframe_a">
										<input type="hidden" name="formType" id="formType" value="">
										<input type="hidden" name="fromDate" id="fromDate_pdf" value="">
										<input type="hidden" name="toDate" id="toDate_pdf" value="">
										<input type="hidden" name="productId" id="productId_pdf" value="">
										<input type="hidden" name="contactId" id="contactId_pdf" value="">
								 	</form> 
									</div>
									
									<!--begin: Datatable -->
								<div class="wrap">
									<iframe class="frame" style="border:1px solid transparent;" height="600px" width="100%" name="iframe_a" id="frame"></iframe>
								</div>
								<!--end: Datatable -->

								</div>
							</div>
							<!--end::Portlet-->

						</div>

					</div>
				</div>

			</div>

		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/jquery.spring-friendly.js" type="text/javascript"></script>
	<script type="text/javascript">
	
	
 	function getReport(formType)
	{
	 	blockUIContent("body");
	
	 	if(formType=='view'){
	 		$("#pdfForm").removeAttr("target")
	 		$("#pdfForm").attr("target","iframe_a")
	 	}else{
	 		$("#pdfForm").removeAttr("target")
	 		$("#pdfForm").attr("target","_blank")
	 	}
		$("#formType").val(formType)
		$("#fromDate_pdf").val($("#fromDate").val())
		$("#toDate_pdf").val($("#toDate").val())
		$("#productId_pdf").val($("#product_id").val())
		$("#contactId_pdf").val($("#contact_id").val())
		
		$("#pdfForm").submit();
		
		unBlockUIContent("body");
		

	}


	$("#frame").on("load", function () {
	  $(this).contents().find(".jrPage").css("width","100%");

	  $(this).contents().find("body table tbody tr > td:first").css("width","0%");
	  $(this).contents().find("body table tbody tr > td:last").css("width","0%");
	})

	function blockUIContent(selectr)
	{
		mApp.block(selectr, {
              overlayColor: '#000000',
              type: 'loader',
              state: 'primary',
              message: 'Processing...'
          });
	}
	
	function unBlockUIContent(selectr)
	{
		mApp.unblock(selectr);	
	}
</script>
	
	
</body>

<!-- end::Body -->
</html>


