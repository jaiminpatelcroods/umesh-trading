<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Reports</title>
	<style type="text/css">
		.select2-container{display: block;}
	</style>
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>

<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Reports </h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
					            <!-- <div class="m-portlet__head">
					                <div class="m-portlet__head-caption">
					                    <div class="m-portlet__head-title">
					                        <h3 class="m-portlet__head-text">
					                            Search Result List
					                        </h3>
					                    </div>
					                </div>
					            </div> -->
					            <div class="m-portlet__body">
					                <!--begin::Section-->
					                <div class="m-section m-section--last">
					                    <div class="m-section__content">
					                      	<div class="row">
												<div class="col-lg-3 col-md-3 col-sm-12">
							                        <!--begin::Preview-->
							                        <div class="m-demo">
							                            <div class="m-demo__preview">
							                                <div class="m-list-search">
							                                    <div class="m-list-search__results">
							                                        <span class="m-list-search__result-category m-list-search__result-category--first">
							                                            Product
							                                        </span>
							                                        	
							                                        <a href="<%=request.getContextPath()%>/report/productwiseledger" class="m-list-search__result-item">
							                                            <span class="m-list-search__result-item-icon"><i class="flaticon-graph m--font-success"></i></span>
							                                            <span class="m-list-search__result-item-text">Product Wise Stock Ledger</span>
							                                        </a>
							                                        
							                                        <%-- <a href="<%=request.getContextPath()%>/report/customerproductledger" class="m-list-search__result-item">
							                                            <span class="m-list-search__result-item-icon"><i class="flaticon-graph m--font-warning"></i></span>
							                                            <span class="m-list-search__result-item-text">Customer Product Ledger</span>
							                                        </a> --%>
							                                    </div>
							                                </div>
							                            </div>
							                        </div>
							                        <!--end::Preview-->
							                     </div>
							                     
							                    
							                </div>
					                    </div>
					                </div>
					                <!--end::Section-->
					            </div>
					        </div>
							<!--end::Portlet-->
						</div>
					</div>
				</div>
			
			</div>
		
		</div>
		<!-- end:: Body -->
		
	<!-- Include Footer -->
	<%@include file="../footer/footer.jsp" %>
			
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->

    <%-- <script src="<%=request.getContextPath()%>/assets/vendors/custom/formvalidation/framework/bootstrap.min.js"></SCRIPT> --%>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	
</body>

<!-- end::Body -->
</html>