<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Setting</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		/* .m-widget28__nav-item {
			width: 206.017px !important;
		} */
		.m--font-boldest {
			font-weight: 600 !important;
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<%-- <div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Setting</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div> --%>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
						
						<!-- Department -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--head-overlay m-portlet--full-height   m-portlet--rounded-force">
								<div class="m-portlet__head m-portlet__head--fit">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">				
											<h3 class="m-portlet__head-text m--font-light" id="main_menu_label">Setting</h3>
											<a href="JavaScript:void(0)" style="color: #fff;" id="back" class="m-portlet__nav-link btn m-btn m-btn--icon m-btn--icon-only m--hide"><i class="la la-angle-left" style="font-size: 2.3rem !important;"></i></a>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover"
												data-skin="light" data-toggle="m-tooltip" data-placement="left" title="" data-html="true" data-original-title="<b>Financial Year</b>">
												<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill m-btn btn-outline-light m-btn--hover-light">
													${sessionScope.financialYear}
												</a>					
												<div class="m-dropdown__wrapper">
													<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
													<div class="m-dropdown__inner">
														<div class="m-dropdown__body">
															<div class="m-dropdown__content">
																<ul class="m-nav">
																	<li class="m-nav__section m-nav__section--first">
																		<span class="m-nav__section-text">Change &nbsp;Fiscal year</span>
																	</li>
																	<c:forEach items="${financialYearVos}"  var="financialYearVo">
																		<li class="m-nav__item" style="padding-left:8px ">
																			<a href="<%=request.getContextPath()%>/financial/year/set/${financialYearVo.yearInterval}" class="m-nav__link">
																			<span class="m-nav__link-text ${financialYearVo.yearInterval == sessionScope.financialYear? 'm--font-brand m--font-boldest' : ''}">${financialYearVo.yearInterval}</span>
																			</a>
																		</li>
																	</c:forEach>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="m-widget28">
										<div class="m-widget28__pic m-portlet-fit--sides"></div>			  
										<div class="m-widget28__container" >	
											<!-- begin::Nav pills -->			   	
											<ul class="m-widget28__nav-items nav nav-pills nav-fill" role="tablist" id="main_menu">
									            <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link active" data-toggle="pill" href="#profile_tab"><span><i class="fa flaticon-pie-chart"></i></span><span>Profile</span></a> 
									            </li>
									            
									             <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link" data-toggle="pill" href="#master_entries"><span><i class="fa flaticon-clipboard"></i></span><span>Master Entries</span></a> 		
									            </li>
									            <!-- <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link" data-toggle="pill" href="#menu31" onclick="setData()"><span><i class="fa flaticon-clipboard"></i></span><span>Master Entries2</span></a> 		
									            </li> -->
									            <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link" href="<%=request.getContextPath()%>/prefix"><span><i class="fa flaticon-clipboard"></i></span><span>Prefix</span></a> 		
									            </li>
									            <%-- <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link" href="<%=request.getContextPath()%>/userrole"><span><i class="fa flaticon-clipboard"></i></span><span>User Role</span></a> 		
									            </li> --%>
									            <!-- <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link has-sub" data-toggle="pill" href="#sub_menu"><span><i class="fa flaticon-file-1"></i></span><span>Reports Setting</span></a>
									            </li> -->
									            <!-- <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link" data-toggle="pill" href="#menu31"><span><i class="fa flaticon-clipboard"></i></span><span>Default Setting</span></a> 		
									            </li> -->
									            <!-- <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link" data-toggle="pill" href="#menu31"><span><i class="fa flaticon-clipboard"></i></span><span>Main Notes</span></a> 		
									            </li> -->		                 
									        </ul>
									        <ul class="m-widget28__nav-items nav nav-pills nav-fill sub m--hide" role="tablist" id="sub_menu">
									            <li class="m-widget28__nav-item nav-item" style="">
									                <a class="nav-link active" data-toggle="pill" href="#menu11"><span><i class="fa flaticon-pie-chart"></i></span><span>Invoice</span></a> 
									            </li>
									            <li class="m-widget28__nav-item nav-item">
									                <a class="nav-link" data-toggle="pill" href="#menu21"><span><i class="fa flaticon-file-1"></i></span><span>Supplier Bill</span></a> 	
									            </li>
									            
									            <li class="m-widget28__nav-item nav-item" style=""></li>
									            <li class="m-widget28__nav-item nav-item" style=""></li>
									            <li class="m-widget28__nav-item nav-item" style=""></li>
									            <li class="m-widget28__nav-item nav-item" style=""></li>
									            <li class="m-widget28__nav-item nav-item" style=""></li>
									                       
									        </ul>
									        <!-- end::Nav pills --> 
							
									        <!-- begin::Tab Content -->
											<div class="m-widget28__tab tab-content">
												<div id="profile_tab" class="m-widget28__tab-container tab-pane active">
												    
												    <div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12">
															<h3 class="" style="color: #5075e4">Profile
															
															<a href="<%=request.getContextPath() %>/profile/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill float-right"
																title="Edit"> <i class="fa fa-edit"></i></a>
															</h3>
														</div>
													</div>
												    <div class="m-divider m--margin-top-10 m--margin-bottom-25"><span></span></div>
												    <div class="m-section">
														<h3 class="m-section__heading">Basic Details</h3>
														<div class="m-section__content">
															<div class="row">
																<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-24">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Company Name:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.name}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Email:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.email}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Contact Name:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.contactName}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Contact Mobile No.:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.contactNo}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Telephone No.:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.telephone}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
																<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																		      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">GST Registration Type :</th>
																			    <td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.gstRegistrationType}</td>
																	    	</tr>
																	    	<tr class="row">
																		      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">GSTIN:</th>
																			    <td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.gst}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Pan No.:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.panNo}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Website:</th>
																	      		<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.website}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
													<div class="m-section">
														<h3 class="m-section__heading">Address Details</h3>
														<div class="m-section__content">
															<div class="row">
																<div class="col-lg-6 col-md-6 col-sm-12">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Address:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.address}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Country:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.countriesName}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">State:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.stateName}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
																<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">City:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.cityName}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">ZIP/Postal code:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.pincode}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
													
													<%-- <div class="alert m-alert--default" role="alert">
													  	<div class="row">
															<div class="col-lg-6 col-md-6 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Financial Year :</span>
																						<span>${userFrontVo.defaultYearInterval}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Month Interval:</span>
																						<span>${userFrontVo.monthInterval}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
														</div>
													</div> --%>
													<div class="m-section">
														<h3 class="m-section__heading">Bank Details</h3>
														<div class="m-section__content">
															<div class="row">
																<div class="col-lg-6 col-md-6 col-sm-12">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Bank Name:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.bankName}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Bank Branch:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.bankBranch}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Account Holder Name:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.bankAcholderName}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Account No.:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.bankAcno}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
																<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">IBAN No.:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.ibanNo}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">IFSC Code:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.bankIFSC}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">SWIFT Code:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.bankSwiftCode}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
													<div class="m-section">
														<h3 class="m-section__heading">Financial Details</h3>
														<div class="m-section__content">
															<div class="row">
																<div class="col-lg-6 col-md-6 col-sm-12">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Financial Year:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.defaultYearInterval}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Month Interval:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${userFrontVo.monthInterval}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="master_entries" class="m-widget28__tab-container tab-pane fade">
												     <!--begin::Section-->
									                <div class="m-section m-section--last">
									                    <div class="m-section__content">
									                      	<div class="row">
																<%-- <div class="col-lg-3 col-md-3 col-sm-12">
											                        <!--begin::Preview-->
											                        <div class="m-demo">
											                            <div class="m-demo__preview">
											                                <div class="m-list-search">
											                                    <div class="m-list-search__results">
											                                        <span class="m-list-search__result-message m--hide">
											                                            No record found
											                                        </span>
											                                        <span class="m-list-search__result-category m-list-search__result-category--first">
											                                            CONTACT
											                                        </span>
											                                        <a href="<%=request.getContextPath()%>/designation" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-graph m--font-warning"></i></span>
											                                            <span class="m-list-search__result-item-text">Designation</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath()%>/department" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-graph m--font-success"></i></span>
											                                            <span class="m-list-search__result-item-text">Department</span>
											                                        </a>
											                                       
											                                        <a href="<%=request.getContextPath()%>" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-business m--font-primary"></i></span>
											                                            <span class="m-list-search__result-item-text">Debit Note List</span>
											                                        </a>
											                                    </div>
											                                </div>
											                            </div>
											                        </div>
											                        <!--end::Preview-->
											                     </div> --%>
											                     <div class="col-lg-3 col-md-3 col-sm-12">
											                        <!--begin::Preview-->
											                        <div class="m-demo">
											                            <div class="m-demo__preview">
											                                <div class="m-list-search">
											                                    <div class="m-list-search__results">
											                                        <span class="m-list-search__result-category m-list-search__result-category--first">
											                                            INVENTORY
											                                        </span>
											                                        <a href="<%=request.getContextPath()%>/categorybrand" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-layers m--font-warning"></i></span>
											                                            <span class="m-list-search__result-item-text">Category / Brand</span>
											                                        </a>
											                                       
											                                        <a href="<%=request.getContextPath()%>/unitofmeasurement" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-layers m--font-info"></i></span>
											                                            <span class="m-list-search__result-item-text">Unit of Measurement</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/tax" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-coins m--font-success"></i></span>
											                                            <span class="m-list-search__result-item-text">Tax </span>
											                                        </a>
											                                    </div>
											                                </div>
											                            </div>
											                        </div>
											                        <!--end::Preview-->
											                    </div>
											                    <%-- <div class="col-lg-3 col-md-3 col-sm-12">
											                        <!--begin::Preview-->
											                        <div class="m-demo">
											                            <div class="m-demo__preview">
											                                <div class="m-list-search">
											                                    <div class="m-list-search__results">
											                                        <span class="m-list-search__result-category m-list-search__result-category--first">
											                                        	PRODUCTION
											                                        </span>
											                                        <!-- <a href="#" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-coins m--font-warning"></i></span>
											                                            <span class="m-list-search__result-item-text">Category Wise Stock </span>
											                                        </a> -->
											                                        <a href="<%=request.getContextPath() %>/fabric" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-coins m--font-success"></i></span>
											                                            <span class="m-list-search__result-item-text">Fabric Type</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/style" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-lifebuoy m--font-info"></i></span>
											                                            <span class="m-list-search__result-item-text">Style</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/rack" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-lifebuoy m--font-danger"></i></span>
											                                            <span class="m-list-search__result-item-text">Rack</span>
											                                        </a>
											                                    </div>
											                                </div>
											                            </div>
											                        </div>
											                        <!--end::Preview-->
											                    </div> --%>
											                    <%-- <div class="col-lg-3 col-md-3 col-sm-12">
											                        <!--begin::Preview-->
											                        <div class="m-demo">
											                            <div class="m-demo__preview">
											                                <div class="m-list-search">
											                                    <div class="m-list-search__results">
											                                        <span class="m-list-search__result-category m-list-search__result-category--first">
											                                        	OTHERS
											                                        </span>
											                                        <!-- <a href="#" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-coins m--font-warning"></i></span>
											                                            <span class="m-list-search__result-item-text">Category Wise Stock </span>
											                                        </a> -->
											                                        <a href="<%=request.getContextPath() %>/tax" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-coins m--font-success"></i></span>
											                                            <span class="m-list-search__result-item-text">Tax </span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/paymentterm" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-lifebuoy m--font-info"></i></span>
											                                            <span class="m-list-search__result-item-text">Payment Term</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/additionalcharge" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-lifebuoy m--font-info"></i></span>
											                                            <span class="m-list-search__result-item-text">Additional Charges</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/termsandcondition" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-lifebuoy m--font-danger"></i></span>
											                                            <span class="m-list-search__result-item-text">Terms & Condition</span>
											                                        </a>
											                                    </div>
											                                </div>
											                            </div>
											                        </div>
											                        <!--end::Preview-->
											                    </div> --%>
											                    
											                    <div class="col-lg-3 col-md-3 col-sm-12">
											                        <!--begin::Preview-->
											                        <div class="m-demo">
											                            <div class="m-demo__preview">
											                                <div class="m-list-search">
											                                    <div class="m-list-search__results">
											                                        <span class="m-list-search__result-category m-list-search__result-category--first">
											                                        	LOCATION MASTER 
											                                        </span>
											                                        <!-- <a href="#" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-coins m--font-warning"></i></span>
											                                            <span class="m-list-search__result-item-text">Category Wise Stock </span>
											                                        </a> -->
											                                        <a href="<%=request.getContextPath() %>/place" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-coins m--font-success"></i></span>
											                                            <span class="m-list-search__result-item-text">Place Master</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/floor" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-lifebuoy m--font-info"></i></span>
											                                            <span class="m-list-search__result-item-text">Floor Master</span>
											                                        </a>
											                                        <a href="<%=request.getContextPath() %>/rack" class="m-list-search__result-item">
											                                            <span class="m-list-search__result-item-icon"><i class="flaticon-lifebuoy m--font-danger"></i></span>
											                                            <span class="m-list-search__result-item-text">Rack Master</span>
											                                        </a>
											                                    </div>
											                                </div>
											                            </div>
											                        </div>
											                        <!--end::Preview-->
											                    </div>
											                </div>
									                    </div>
									                </div>
									                <!--end::Section-->					      	
												</div>
												<div id="menu31" class="m-widget28__tab-container tab-pane fade"> 
												    <%-- <iframe src="<%=request.getContextPath()%>/tax"  class="iframe" frameborder="0" scrolling="no" onload="resizeIframe(this)" style=" width: 100%; height: 500px">
												    </iframe> --%>				      	
												</div>					     
											</div>
											<!-- end::Tab Content --> 	
										</div>				 	 
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<!-- End Department -->
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script type="text/javascript">
		$(document).ready(function () {
			$(".has-sub").click(function(){
				$($(this).attr("href")).removeClass("m--hide");
				$("#main_menu").addClass("m--hide");
				$("#main_menu_label").addClass("m--hide");
				$("#back").removeClass("m--hide");
			});
			
			$("#back").click(function(){
				$($(this).attr("href")).removeClass("m--hide");
				$("#main_menu").removeClass("m--hide");
				$("#main_menu_label").removeClass("m--hide");
				$("#back").addClass("m--hide");
				$(".sub").addClass("m--hide");
			});
			
			
		});
		
		function setData() {
			/* $.get("/tax", {
				 
			 }, function( data,status ) {
				$("#menu31").html(data);
			 }); */
			 
			 
		}
		 function resizeIframe(obj){
		     /* obj.style.height = 0;
		     alert($('iframe').contents().outerHeight());
		     obj.style.height = obj.contentDocument.body.scrollHeight + 'px'; */
			 //setTimeout(iResize, 5000);
		  }
		
	</script>
	<script type='text/javascript'>
	var iFrames = $('iframe');
    
	function iResize() {
		
		for (var i = 0, j = iFrames.length; i < j; i++) {
		  alert(iFrames[i].style.height);
			iFrames[i].style.height = iFrames[i].contentWindow.document.body.offsetHeight+500 + 'px';
			 
		}
	   }
    $(function(){
    
        
    	
        	/* if ($.browser.safari || $.browser.opera) { 
        		
        	   iFrames.load(function(){
        	       setTimeout(iResize, 0);
               });
            
        	   for (var i = 0, j = iFrames.length; i < j; i++) {
        			var iSource = iFrames[i].src;
        			iFrames[i].src = '';
        			iFrames[i].src = iSource;
               }
               
        	} else {
        	   iFrames.load(function() { 
        	       alert(this.contentWindow.document.body.offsetHeight)
        		   this.style.height = this.contentWindow.document.body.offsetHeight + 'px';
        	   });
        	} */
        
       });

</script>
	 	
</body>
<!-- end::Body -->
</html>