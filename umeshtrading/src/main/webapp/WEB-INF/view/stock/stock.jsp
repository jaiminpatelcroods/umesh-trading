<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.croods.umeshtrading.constant.Constant"%>
<!DOCTYPE html>
<html>
<head>
	<%@include file="../header/head.jsp" %>
	<title>${displayType}</title>
	<style type="text/css">
		.select2-container{display: block;}
	</style>
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp"%>
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${displayType}
							</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				<div class="m-content">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<%-- Jaimin <div class="m-portlet__head-title">
											<a href="<%=request.getContextPath()%>/stock/${type}/new" class="btn btn-primary m-btn m-btn--icon"> 
												<span>
													<i class="la la-plus"></i>
													<span>${displayType}</span>
												</span>
											</a>
										</div> --%>
									</div>
									<div class="m-portlet__head-tools">
										<a href="#" id="export_print" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
										<a href="#" id="export_excel" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="Excel"> <i class="fa fa-file-excel"></i>
										</a>
										<a href="#" id="export_pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="PDF"> <i class="fa fa-file-pdf"></i>
										</a>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="row m--margin-bottom-20">
										
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Product:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<select class="form-control m-select2" id="productVo" name="productVo.productId" >
													<option value="0">All Product</option>
													<c:forEach items="${productList}" var="productList">
														<option value="${productList.productId}">
															 ${productList.name}
														</option>
													</c:forEach>
												</select>
											</div>
										</div>
										
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Place:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<select class="form-control m-select2" id="placeVo" name="palceVo.placeId" >
													<option value="0">All Place</option>
													<c:forEach items="${placeList}" var="placeList">
														<option value="${placeList.placeId}">
															 ${placeList.placeCode} 
														</option>
													</c:forEach>
												</select>
											</div>
										</div>
										
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Floor:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<select class="form-control m-select2" id="floorVo" name="floorVo.floorId" >
													<option value="0">All Floor</option>
													<c:forEach items="${floorList}" var="floorList">
														<option value="${floorList.floorId}">
															 ${floorList.floorCode}
														</option>
													</c:forEach>
												</select>
											</div>
										</div>
										
									</div>
									<table class="table table-striped- table-bordered table-hover table-checkable"
										id="m_table_1">
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Product Name</th>
												<th>Place Code</th>
												<th>Floor Code</th>
												<th>Rack Code</th>
												<th>Quantity</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
							<!--end::Portlet-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end:: Body -->
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
	</div>
	<!-- end:: Page -->
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	<script type="text/javascript">
		//https://datatables.net/reference/button/excelHtml5
		//https://github.com/darrachequesne/spring-data-jpa-datatables
		
	
		var DatatablesDataSourceHtml = {
		    init: function() {
		        $("#m_table_1").DataTable({
		            responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax: {
						url: "<%=request.getContextPath()%>/stock/datatable",
						type: "POST",
						"data": function ( d ) {
							return $.extend( {}, d, {
								"productId": $('#productVo').val(),
								"placeId": $('#placeVo').val(),
								"floorId": $('#floorVo').val()
							});
						}
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					columns: [{
							data: "stockMasterId"
						},{
							data: "productVo.name"
						},{
							data: "placeVo.placeCode"
						},{
							data: "floorVo.floorCode"
						},{
							data: "rackVo.rackCode"
						},{
							data: "quantity"
					}],
					
					columnDefs: [{
						targets: 0,
						title: "#",
						orderable: !1,
						render: function(a, e, t, n) {
							return (n.row+1);
						}
					}/* ,{
						targets: 1,
						orderable: 1,
						render: function(a, e, t, n) {
							return '<a href="${pageContext.request.contextPath}/product/'+t.productVo.productId+'" class="m-link m--font-bolder">'+t.productVo.name+'</a>';
						}
					} */],
		            
		            //lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
		            fixedHeader: {
		                header: false,
		                footer: false
		            },
		            /* buttons: ["print", "copyHtml5", "excelHtml5", "csvHtml5", "pdfHtml5"], */
		            buttons: [
		            {
		            	extend: 'print',
		            	exportOptions: {
		                    columns: [0,1,2,3,4,5]
		                },
		                title :'Umesh Trading - ${displayType} List',
		                messageTop: function () {
	                    	return '<span class="sub-heading"><b>${displayType} List </b><br/> </span>';
	                    },
	                    autoPrint: true,
			            customize: function ( win ) {
					    	 $(win.document.body).find('h1').css('text-align', 'center');
					    	$( win.document.body).find("div").css('text-align', 'center');
					    	
					    	var css = '@page { size: auto;margin-top:5mm;margin-left:5mm;margin-right:5mm; }',
		                    head = win.document.head || win.document.getElementsByTagName('head')[0],
		                    style = win.document.createElement('style');
			                style.type = 'text/css';
			                style.media = 'print';
			 
			                if (style.styleSheet)
			                {
			                  style.styleSheet.cssText = css;
			                }
			                else
			                {
			                  style.appendChild(win.document.createTextNode(css));
			                }
			 
			                head.appendChild(style);
					    	
	              		 } 
		            },
		            {
		            	extend: 'pdfHtml5',
		            	//extension: '.pdf'
		            	exportOptions: {
		                    columns: [0,1,2,3,4,5]
		                },
		                pageSize: 'LEGAL',
		                title :'Umesh Trading - ${displayType} List',
		                messageTop: function () {
	                    	return '${displayType} List \n';
	                    },
	                    customize: function(doc) {
	                    	doc.styles.tableHeader = {
	                            	fillColor:"#88898c",
	                            	color: '#ffffff',
	                            	bold: 'true',
	                        }
	                    
	                    	doc.styles.title = {
	                          fontSize: '18',
	                          alignment: 'center',
	                          bold: 'true'
                        	}
	                        doc.pageMargins = [ 10, 20, 10, 10 ];
                            doc.content.forEach(function(item) {
		                        if (item.table) {
		                        	item.table.widths = [ 40, 125, 85, 85, 85, 125];
		                        	item.table.body[1][3].alignment = 'right';
		                        	
		                        	for(i = 0; i < item.table.body.length; i++) {
		                        		item.table.body[i][0].alignment = 'center' ;
		                        		item.table.body[i][1].alignment = 'left' ;
		                        		item.table.body[i][2].alignment = 'left' ;
		                        		item.table.body[i][3].alignment = 'left' ;
		                        		item.table.body[i][4].alignment = 'right' ;
		                        		item.table.body[i][5].alignment = 'right' ;
		                        		
		                        		//item.table.body[i][6].margin = [ 0, 0, 5, 0 ];
		                        	}
		                        	
		                        }
                            });
                           
	                        doc.styles.message = {
	  	                          fontSize: '12',
		                          alignment: 'center'
	                        	}
	                    }
		            },
		            {
		            	extend: 'excelHtml5',
		            	//extension: '.pdf'
		            	exportOptions: {
		                    columns: [0,1,2,3,4,5]
		                },
		                title :'Umesh Trading - ${displayType} List',
		                messageTop: function () {
	                    	return '${displayType} List \n ';
	                    },
	                     customize: function(doc) {
	                    	var sheet = doc.xl.worksheets['sheet1.xml'];
	                    }
		            }
		        ],
		        }),$("#productVo,#placeVo,#floorVo").on("change", function(t) {
		        	$('#m_table_1').DataTable().draw()
	            }), $("#export_print").on("click", function(e) {
	                e.preventDefault(), $("#m_table_1").DataTable().button(0).trigger()
	            }), $("#export_excel").on("click", function(e) {
	                e.preventDefault(), $("#m_table_1").DataTable().button(2).trigger()
	            }), $("#export_pdf").on("click", function(e) {
	                e.preventDefault(), $("#m_table_1").DataTable().button(1).trigger()
	            })
		    }
		};
		jQuery(document).ready(function() {
		    DatatablesDataSourceHtml.init();
		    $(".dt-buttons").addClass("m--hide");
		});
		
		
		
	</script>
	
	
</body>

<!-- end::Body -->
</html>


