				
	<div class="modal fade" id="tax_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					New Tax
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<form id="tax_new_form">
				<div class="modal-body">
				
					<div class="form-group m-form__group">
						<label for="recipient-name" class="form-control-label">
							Tax Name:
						</label>
						<input type="text" name="tax" class="form-control"  id="tax">
					</div>
				 	<div class="form-group m-form__group">
						<label for="message-text" class="form-control-label">
							Tax Rate:
						</label>
						<input type="text" class="form-control m-input" name="taxrate" id="rate" />
					</div> 
				
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<button type="button" id="savetaxnew"  class="btn btn-primary">
					Save
				</button>
			</div>
			</form>
		</div>
	</div>
</div>
						