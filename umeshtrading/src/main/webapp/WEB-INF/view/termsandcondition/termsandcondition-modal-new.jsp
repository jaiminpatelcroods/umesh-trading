<div class="modal fade" id="termsandcondition_new_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New Terms And Condition</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<div class="modal-body">
				<form id="termsandcondition_new_form">
					<div class="form-group m-form__group row ">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Terms And Condition :</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<textarea class="form-control m-input"  maxlength="150" rows="5" name="termsCondition" id="termsCondition"
								placeholder="Enter a Condition"></textarea>
						</div>
					</div>
					<div class="form-group m-form__group row ">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Modules:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<select class="form-control m-bootstrap-select m_selectpicker" multiple data-actions-box="true" name="modules" id="modules">
								<optgroup label="Purchase">
									<option value="${Constant.PURCHASE_ORDER}">Purchase Order</option>
									<option value="${Constant.PURCHASE_BILL}">Supplier Bill</option>
									<option value="${Constant.PURCHASE_DEBIT_NOTE}">Debit Note</option>
								</optgroup>
								<optgroup label="Sales">
									<option value="${Constant.SALES_ORDER}">Sales Order</option>
									<option value="${Constant.SALES_INVOICE}">Invoice</option>
									<option value="${Constant.SALES_CREDIT_NOTE}">Credit Note</option>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row ">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="m-checkbox-inline">
								<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
									<input type="checkbox" name="isDefault" id="isDefault" value="0">Default <span></span>
								</label><i data-toggle="m-tooltip" data-width="auto"
									class="m-form__heading-help-icon flaticon-info"
									title="Default Term Of Your Product"></i>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close</button>
				<button type="button" id="savetermsandcondition" class="btn btn-primary">
					Save</button>
			</div>
		</div>
	</div>
</div>
