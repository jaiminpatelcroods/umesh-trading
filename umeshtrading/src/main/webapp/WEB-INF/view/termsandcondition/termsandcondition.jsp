<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.umeshtrading.constant.Constant"%>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Terms And Condition</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Terms And Condition</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/setting" class="m-nav__link">
										<span class="m-nav__link-text">Settings</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="#" data-toggle="modal" data-target="#termsandcondition_new_modal" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>Terms And Condition</span></span>
											</a>
										</div>			
									</div>
								</div>
								<div class="m-portlet__body" >
									<div  class="m_datatable"  >
										<table class="table table-striped- table-bordered table-hover table-checkable" id="termsandcondition_table">
											<thead>
						  						<tr>
				  									<th>#</th>
				  									<th>Term And Condition</th>
				  									<th>Modules</th>
				  									<th>Is Default</th>
				  									<th>Actions</th>
							  					</tr>
											</thead>
											<tbody>
												<c:forEach var="termsandConditionVo" items="${termsandConditionVos}" varStatus="index">
													<tr>
														<td>${index.index+1}</td>
														<td>${termsandConditionVo.termsCondition}</td>
														<td>${termsandConditionVo.modules}</td>
														<td><c:if test="${termsandConditionVo.isDefault==1}">Yes</c:if><c:if test="${termsandConditionVo.isDefault==0}">No</c:if></td>
														<td>
															<a href="JavaScript:Void(0)" data-toggle="modal"  onclick="updateTermsandCondition(this,${termsandConditionVo.termsandConditionId})" data-target="#termsandcondition_update_modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																title="Edit"> <i class="fa fa-edit"></i></a>
															<a href="JavaScript:Void(0)" onclick="deleteTermsandCondition(this,${termsandConditionVo.termsandConditionId})" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
																title="Delete"> <i class="fa fa-trash"></i></a>
														</td>
													</tr>
												</c:forEach>
											</tbody>			
										</table>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
				
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
		<%@include file="termsandcondition-modal-new.jsp" %>
		<%@include file="termsandcondition-modal-update.jsp" %>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/termsandcondition/termsandcondition-new-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/termsandcondition/termsandcondition-update-script.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	var DatatablesDataSourceHtml = {
	    init: function() {
	        $("#termsandcondition_table").DataTable({
	            responsive: !0,
	            
	        })  
	    }
	};
	
	$(document).ready(function() {
		
		DatatablesDataSourceHtml.init();	
	});
	
	function deleteTermsandCondition(rows,id)
    {  		
	 	
		swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!"
        }).then(function(e) {
        	
        	if(e.value) {
        		$.post("/termsandcondition/delete", {
   		         id:id
   		        }, function( data,status ) {
   		        	location.reload();
   		        	t.on( 'order.dt search.dt', function () {
   					        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
   					            cell.innerHTML = i+1;
   					        } );
   					}).draw();
   		        	 
   					toastr["success"]("Record Deleted....");
   				
   				
   				});            		
        	}
        	
        });
		
    }
	</script>	
</body>
<!-- end::Body -->
</html>