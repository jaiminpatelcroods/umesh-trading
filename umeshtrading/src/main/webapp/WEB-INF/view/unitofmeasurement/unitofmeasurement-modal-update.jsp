<div class="modal fade" id="uom_update_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Unit Of Measurement</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<div class="modal-body">
				<form id="uom_update_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Name:</label>
						<input type="text" name="UomUpName" class="form-control" id="UpUomName">
						<input type="hidden" id="UpUomId">
						<input type="hidden" id="DataIndex">
					</div>
					<div class="form-group">
						<label for="message-text" class="form-control-label">Code:</label>
						<input type="text" class="form-control" name="UomupCode" id="UpUomCode"/>
					</div>
					<div class="form-group">
						<label for="message-text" class="form-control-label">No. of Decimal Places:</label>
						<input type="text" class="form-control" name="NoofDescimalPlaces" id="UpNodp"/>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="updateuom" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>